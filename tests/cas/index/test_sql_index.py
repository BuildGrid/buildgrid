# Copyright (C) 2019 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import random
import sqlite3
import tempfile
from datetime import datetime, timedelta
from typing import Iterable
from unittest import mock

import pytest
from sqlalchemy import select, update

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Digest
from buildgrid.server.cas.storage.index.sql import SQLIndex
from buildgrid.server.cas.storage.lru_memory_cache import LRUMemoryCache
from buildgrid.server.cas.storage.storage_abc import create_write_session
from buildgrid.server.cas.storage.with_cache import WithCacheStorage
from buildgrid.server.metrics_names import METRIC
from buildgrid.server.monitoring import get_monitoring_bus, set_monitoring_bus
from buildgrid.server.settings import HASH
from buildgrid.server.sql.models import IndexEntry
from buildgrid.server.sql.provider import SqlProvider
from buildgrid.server.utils.digests import create_digest
from tests.utils.fixtures import pg_schema_provider
from tests.utils.metrics import mock_create_timer_record

# Skip certain tests if sqlite3 driver is less than 3.25
sql_version_skip = bool(
    (int(sqlite3.sqlite_version_info[0]) == 3 and int(sqlite3.sqlite_version_info[1]) < 25)
    or int(sqlite3.sqlite_version[0]) < 3
)
sql_version_skip_message = "Skipped test due to sqlite3 version being less than 3.25."
check_sqlite3_version = pytest.mark.skipif(sql_version_skip, reason=sql_version_skip_message)

BLOBS = [b"abc", b"defg", b"hijk", b""]
DIGESTS = [create_digest(blob) for blob in BLOBS]
EXTRA_BLOBS = [b"lmno", b"pqr"]
EXTRA_DIGESTS = [create_digest(blob) for blob in EXTRA_BLOBS]


@pytest.fixture()
def blobs_digests():
    return list(zip(BLOBS, DIGESTS))


@pytest.fixture()
def extra_blobs_digests():
    return list(zip(EXTRA_BLOBS, EXTRA_DIGESTS))


def get_hash(x: Digest) -> str:
    return x.hash


def sorted_digests(items: Iterable[Digest]) -> list[Digest]:
    return sorted(items, key=get_hash)


def sorted_digest_lists(arrays: Iterable[list[Digest]]) -> list[Digest]:
    return sorted((d for a in arrays for d in a), key=get_hash)


@pytest.fixture(params=["sqlite", "postgresql", "postgresql-with-cache-fallback-writes-only", "postgresql-inlined"])
def any_index(request, postgres):
    set_monitoring_bus(mock.Mock())
    if request.param == "sqlite":
        storage = LRUMemoryCache(256)
        with tempfile.NamedTemporaryFile() as db:
            sql_provider = SqlProvider(connection_string=f"sqlite:///{db.name}", automigrate=True)
            index = SQLIndex(sql_provider=sql_provider, storage=storage)
            yield index

    elif request.param == "postgresql":
        storage = LRUMemoryCache(256)
        with pg_schema_provider(postgres) as sql_provider:
            index = SQLIndex(sql_provider=sql_provider, storage=storage)
            yield index

    elif request.param == "postgresql-with-cache-fallback-writes-only":
        cache_storage = LRUMemoryCache(256)
        fallback_storage = LRUMemoryCache(256)
        storage = WithCacheStorage(cache_storage, fallback_storage)
        with mock.patch(
            "buildgrid.server.cas.storage.with_cache.WithCacheStorage.commit_write",
            new=fallback_storage.commit_write,
        ):
            with pg_schema_provider(postgres) as sql_provider:
                index = SQLIndex(sql_provider=sql_provider, storage=storage)
                yield index

    elif request.param == "postgresql-inlined":
        storage = LRUMemoryCache(256)
        with pg_schema_provider(postgres) as sql_provider:
            index = SQLIndex(sql_provider=sql_provider, storage=storage, max_inline_blob_size=256)
            yield index


def _write(storage, digest, blob):
    with create_write_session(digest) as session:
        session.write(blob)
        storage.commit_write(digest, session)


def _write_to_index_only(index_storage: SQLIndex, digest, blob):
    with index_storage._sql.scoped_session() as session:
        index_storage._save_digests_to_index([(digest, blob)], session)


def test_has_blob(any_index, blobs_digests, extra_blobs_digests):
    """The index should accurately reflect its contents with has_blob."""
    for _, digest in blobs_digests:
        assert any_index.has_blob(digest) is False
    for _, digest in extra_blobs_digests:
        assert any_index.has_blob(digest) is False

    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    for _, digest in blobs_digests:
        assert any_index.has_blob(digest) is True
    for _, digest in extra_blobs_digests:
        assert any_index.has_blob(digest) is False


@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_get_blob(any_index, blobs_digests, extra_blobs_digests):
    """The index should properly return contents under get_blob."""
    for _, digest in blobs_digests:
        assert any_index.get_blob(digest) is None
    for _, digest in extra_blobs_digests:
        assert any_index.get_blob(digest) is None

    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    for blob, digest in blobs_digests:
        assert any_index.get_blob(digest).read() == blob
    for _, digest in extra_blobs_digests:
        assert any_index.get_blob(digest) is None

    mock_get_blob_time = mock_create_timer_record(METRIC.STORAGE.READ_DURATION, type=any_index.TYPE)
    call_list = [mock.call(mock_get_blob_time)]
    get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_sqlite3_version
def test_timestamp_not_updated_by_get(any_index, blobs_digests):
    """When a blob is accessed, the timestamp should not be updated."""
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    second_blob, second_digest = blobs_digests[1]

    old_order = []
    for digest in any_index.least_recent_digests():
        old_order.append(digest)

    assert second_digest == old_order[1]
    any_index.get_blob(second_digest)

    updated_order = []
    for digest in any_index.least_recent_digests():
        updated_order.append(digest)

    # Performing the get should not have updated the timestamps
    assert old_order == updated_order


@check_sqlite3_version
def test_timestamp_updated_by_write(any_index, blobs_digests):
    """Writes should also update the timestamp."""
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    second_blob, second_digest = blobs_digests[1]

    old_order = []
    for digest in any_index.least_recent_digests():
        old_order.append(digest)

    assert second_digest == old_order[1]
    _write(any_index, second_digest, second_blob)

    updated_order = []
    for digest in any_index.least_recent_digests():
        updated_order.append(digest)

    # Performing the get should have updated the timestamp
    assert second_digest == updated_order[-1]


def _digestified_range(max):
    """Generator for digests for bytestring representations of all numbers in
    the range [0, max)
    """
    for i in range(max):
        blob = bytes(i)
        yield remote_execution_pb2.Digest(hash=HASH(blob).hexdigest(), size_bytes=len(blob))


@check_sqlite3_version
def test_timestamp_updated_by_random_gets_and_writes(any_index):
    """Test throwing a random arrangement of gets and writes against the
    index to ensure that it updates the blobs in the correct order.

    With an index primed with a modest number of blobs, we call get_blob
    on a third of them chosen randomly, _write on another third, and leave
    the last third alone. The index should be left in the proper state
    at the end."""
    digest_map = {}
    for i, digest in enumerate(_digestified_range(100)):
        _write(any_index, digest, bytes(i))
        digest_map[digest.hash] = i

    old_order = []
    for i, digest in enumerate(any_index.least_recent_digests()):
        assert digest_map[digest.hash] == i
        old_order.append(digest)

    untouched_digests = []
    updated_digests = []

    gets = ["get"] * (len(old_order) // 3)
    writes = ["write"] * (len(old_order) // 3)
    do_nothings = ["do nothing"] * (len(old_order) - len(gets) - len(writes))
    actions = gets + writes + do_nothings
    random.shuffle(actions)

    for i, (action, digest) in enumerate(zip(old_order, actions)):
        if action == "get":
            any_index.get_blob(digest)
            updated_digests.append(digest)
        elif action == "write":
            _write(any_index, digest, bytes(i))
            updated_digests.append(digest)
        elif action == "do nothing":
            untouched_digests.append(digest)

    # The proper order should be every blob that wasn't updated (in relative
    # order), followed by every blob that was updated (in relative order)
    new_order = untouched_digests + updated_digests

    for actual_digest, expected_digest in zip(any_index.least_recent_digests(), new_order):
        assert actual_digest == expected_digest


def test_bulk_read_on_missing_blobs(any_index, blobs_digests, extra_blobs_digests):
    """Check that attempting to do a bulk read on blobs that are missing
    returns the blobs for blobs found and None for all others.

    'extra_blobs_digests' will be our missing blobs in this test."""
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)
    results = any_index.bulk_read_blobs([digest for (blob, digest) in blobs_digests + extra_blobs_digests])
    for blob, digest in blobs_digests:
        assert results.get(digest.hash, None) is not None
    for blob, digest in extra_blobs_digests:
        assert results.get(digest.hash, None) is None


def test_get_blob_backfills_inline_blobs(any_index, blobs_digests):
    # Skip testing any indexes that are already doing inlining
    if any_index._max_inline_blob_size > 0:
        return

    any_index._max_inline_blob_size = 0

    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    empty_digest_hash = HASH(b"").hexdigest()

    digest_list = [digest for (blob, digest) in blobs_digests]

    # Blobs should not be inlined
    for entry in any_index._bulk_select_digests(digest_list, fetch_blobs=True):
        if entry.digest_hash != empty_digest_hash:
            assert entry.inline_blob is None

    # Enable inlining after writing blobs
    any_index._max_inline_blob_size = 10000

    # Rerun get_blob to backfill
    for blob, digest in blobs_digests:
        any_index.get_blob(digest)

    # Blobs should be visible in the index entries
    for entry in any_index._bulk_select_digests(digest_list, fetch_blobs=True):
        assert entry.inline_blob is not None


def test_bulk_read_backfills_inline_blobs(any_index, blobs_digests):
    # Skip testing any indexes with preset inlining
    if any_index._max_inline_blob_size > 0:
        return

    any_index._max_inline_blob_size = 0

    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    digest_list = [digest for (blob, digest) in blobs_digests]
    any_index.bulk_read_blobs(digest_list)
    empty_digest_hash = HASH(b"").hexdigest()

    # Blobs should not be inlined
    for entry in any_index._bulk_select_digests(digest_list, fetch_blobs=True):
        if entry.digest_hash != empty_digest_hash:
            assert entry.inline_blob is None

    # Enable inlining after writing blobs
    any_index._max_inline_blob_size = 10000

    # Rerun bulk_read_blobs to backfill
    any_index.bulk_read_blobs([digest for (blob, digest) in blobs_digests])

    # Blobs should be visible in the index entries
    for entry in any_index._bulk_select_digests(digest_list, fetch_blobs=True):
        assert entry.inline_blob is not None


def test_get_blob_adjusted_inlining(any_index, blobs_digests):
    """Ensure that get_blob can fetch blobs even when the inlining size is adjusted
    to be smaller or larger. This won't happen in practice, but it simulates an
    environment with multiple BuildGrids configured with different inlining sizes
    (which can happen when deploying a configuration change to the inlining size)."""
    # Skip testing any indexes with preset inlining
    if any_index._max_inline_blob_size > 0:
        return

    any_index._max_inline_blob_size = 3

    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    # '' and 'abc' will have been inlined, but 'defg' and 'hijk' won't

    # Now adjust the size up and verify we can get each blob
    any_index._max_inline_blob_size = 10000
    for blob, digest in blobs_digests:
        assert any_index.get_blob(digest).read() == blob

    # Now adjust the size DOWN and verify we can get each blob
    any_index._max_inline_blob_size = 0
    for blob, digest in blobs_digests:
        assert any_index.get_blob(digest).read() == blob


def test_get_blob_repairs_index(any_index: SQLIndex, extra_blobs_digests):
    """If somehow a blob is in the index but is missing from the storage and is
    not inlined then it should be removed from the index in a get_blob call.
    This should never happen in practice until someone manually modifies the underlying storage"""
    # Skip testing any indexes with preset inlining
    if any_index._max_inline_blob_size > 0:
        return

    # Write some blobs to index only
    for blob, digest in extra_blobs_digests:
        _write_to_index_only(any_index, digest, blob)

    # Assert that the blob is not in the underlying storage
    for _, digest in extra_blobs_digests:
        assert any_index._storage.get_blob(digest) is None

    # Assert that the blobs exists in the index by calling missing blobs
    assert any_index.missing_blobs([digest for (_, digest) in extra_blobs_digests]) == []

    # get_blob should return none and remove the digest from the index
    for _, digest in extra_blobs_digests:
        assert any_index.get_blob(digest) is None

    # Assert that the blobs are removed and find missing returns all the blobs
    assert any_index.missing_blobs([digest for (_, digest) in extra_blobs_digests]) == any_index.missing_blobs(
        [digest for (_, digest) in extra_blobs_digests]
    )


def test_get_blob_repairs_index_with_inline(any_index: SQLIndex, extra_blobs_digests):
    """If some blobs are being inlined then the index should still be repaired if blobs are missing form storage"""
    # Skip testing any indexes with preset inlining
    if any_index._max_inline_blob_size > 0:
        return

    any_index._max_inline_blob_size = 3

    # To make things interesting let's enable fallback too
    any_index._fallback_on_get = True

    # Write some blobs to index only
    for blob, digest in extra_blobs_digests:
        _write_to_index_only(any_index, digest, blob)

    # Assert that the blob is not in the underlying storage
    for _, digest in extra_blobs_digests:
        assert any_index._storage.get_blob(digest) is None

    # Assert that the blobs exists in the index by calling missing blobs
    assert any_index.missing_blobs([digest for (_, digest) in extra_blobs_digests]) == []

    # get_blob should return none and remove the digest from the index if the blob is not inlined
    for blob, digest in extra_blobs_digests:
        if digest.size_bytes <= any_index._max_inline_blob_size:
            assert any_index.get_blob(digest).read() == blob
        else:
            assert any_index.get_blob(digest) is None

    # Assert that the blobs are removed and find missing returns all the blobs that are not inlined
    assert any_index.missing_blobs([digest for (_, digest) in extra_blobs_digests]) == any_index.missing_blobs(
        [digest for (_, digest) in extra_blobs_digests if digest.size_bytes > any_index._max_inline_blob_size]
    )


def test_bulk_read_blobs_adjusted_inlining(any_index, blobs_digests):
    """Ensure that bulk_read_blobs can fetch blobs even when the inlining size is adjusted
    to be smaller or larger. This won't happen in practice, but it simulates an
    environment with multiple BuildGrids configured with different inlining sizes
    (which can happen when deploying a configuration change to the inlining size)."""
    # Skip testing any indexes with preset inlining
    if any_index._max_inline_blob_size > 0:
        return

    any_index._max_inline_blob_size = 3

    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    # '' and 'abc' will have been inlined, but 'defg' and 'hijk' won't

    # Now adjust the size up and verify we can get each blob
    any_index._max_inline_blob_size = 10000
    results = any_index.bulk_read_blobs([digest for (blob, digest) in blobs_digests])
    for blob, digest in blobs_digests:
        assert results[digest.hash] == blob

    # Now adjust the size DOWN and verify we can get each blob
    any_index._max_inline_blob_size = 0
    results = any_index.bulk_read_blobs([digest for (blob, digest) in blobs_digests])
    for blob, digest in blobs_digests:
        assert results[digest.hash] == blob


def _first_batch_seen_before_second(index, first_batch, second_batch):
    """Returns true if the first batch of digests is located entirely before
    the second group in the index's LRU order.
    """
    first_batch_hashes = set([digest.hash for digest in first_batch])
    second_batch_hashes = set([digest.hash for digest in second_batch])

    # The sets must be disjoint
    assert first_batch_hashes.isdisjoint(second_batch_hashes)

    for digest in index.least_recent_digests():
        # Delete the element if it's in the first set
        first_batch_hashes.discard(digest.hash)
        # If we see anything in the second set, we should have seen everything
        # in the first set
        if digest.hash in second_batch_hashes:
            return not first_batch_hashes
    return False


@check_sqlite3_version
@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_timestamp_updated_by_bulk_update(any_index, blobs_digests, extra_blobs_digests):
    """Test that timestamps are updated by bulk writes."""
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)
    for blob, digest in extra_blobs_digests:
        _write(any_index, digest, blob)

    first_batch_digests = [digest for (blob, digest) in blobs_digests]
    second_batch_digests = [digest for (blob, digest) in extra_blobs_digests]

    # At the start, the blobs in the first batch have an earlier timestamp
    # than the blobs in the second
    assert _first_batch_seen_before_second(any_index, first_batch_digests, second_batch_digests)

    any_index.bulk_update_blobs([(digest, blob) for (blob, digest) in blobs_digests])

    # After rewriting the first batch, the first batch appears after the second
    assert _first_batch_seen_before_second(any_index, second_batch_digests, first_batch_digests)

    mock_save_digests_time = mock_create_timer_record(METRIC.STORAGE.SQL_INDEX.SAVE_DIGESTS_DURATION)
    call_list = [mock.call(mock_save_digests_time)]
    get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_sqlite3_version
@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_timestamp_not_updated_by_bulk_read(any_index, blobs_digests, extra_blobs_digests):
    """Test that timestamps are updated by bulk reads."""
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)
    for blob, digest in extra_blobs_digests:
        _write(any_index, digest, blob)

    first_batch_digests = [digest for (blob, digest) in blobs_digests]
    second_batch_digests = [digest for (blob, digest) in extra_blobs_digests]

    # At the start, the blobs in the first batch have an earlier timestamp
    # than the blobs in the second
    assert _first_batch_seen_before_second(any_index, first_batch_digests, second_batch_digests)

    any_index.bulk_read_blobs(first_batch_digests)

    # The order of digests doesn't change after a bulk read
    assert _first_batch_seen_before_second(any_index, first_batch_digests, second_batch_digests)


@check_sqlite3_version
@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_timestamp_updated_by_missing_blobs(any_index, blobs_digests):
    """FindMissingBlobs should also update the timestamp."""
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    second_blob, second_digest = blobs_digests[1]

    old_order = []
    for digest in any_index.least_recent_digests():
        old_order.append(digest)

    assert second_digest == old_order[1]
    any_index.missing_blobs([second_digest])

    updated_order = []
    for digest in any_index.least_recent_digests():
        updated_order.append(digest)

    # Performing the get should have updated the timestamp
    assert second_digest == updated_order[-1]

    mock_bulk_update_time = mock_create_timer_record(METRIC.STORAGE.SQL_INDEX.UPDATE_TIMESTAMP_DURATION)
    call_list = [mock.call(mock_bulk_update_time)]
    get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


@check_sqlite3_version
@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_timestamp_not_updated_by_missing_blobs(any_index, blobs_digests):
    """FindMissingBlobs should also not update the timestamp
    when the refresh threshold is configured."""
    REFRESH_THRESHOLD_IN_SECONDS = 1

    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    # Test 1: The written order is the same as the LRU query
    second_blob, second_digest = blobs_digests[1]
    old_order = []
    for digest in any_index.least_recent_digests():
        old_order.append(digest)
    assert second_digest == old_order[1]

    # Test 2: When a refresh threshold is configured, there's no timestamp update
    # if the find missing blob is performed too soon, such as immediately
    any_index.refresh_accesstime_older_than = REFRESH_THRESHOLD_IN_SECONDS
    any_index.missing_blobs([second_digest])
    updated_order = []
    for digest in any_index.least_recent_digests():
        updated_order.append(digest)
    assert second_digest != updated_order[-1]

    # Test 3: After the refresh threshold has elapsed, timestamp updates occur
    # Mock datetime.utcnow() so that it posts time from after the threshold has elapsed
    threshold_seconds_later = datetime.utcnow() + timedelta(seconds=REFRESH_THRESHOLD_IN_SECONDS)
    with (
        mock.patch(
            "buildgrid.server.cas.storage.index.sql_dialect_delegates.postgresqldelegate.datetime"
        ) as mock_date_pstgr,
        mock.patch(
            "buildgrid.server.cas.storage.index.sql_dialect_delegates.sqlitedelegate.datetime"
        ) as mock_date_sqlite,
        mock.patch("buildgrid.server.cas.storage.index.sql.datetime") as mock_date_generic,
    ):
        mock_date_pstgr.utcnow.return_value = threshold_seconds_later
        mock_date_sqlite.utcnow.return_value = threshold_seconds_later
        mock_date_generic.utcnow.return_value = threshold_seconds_later
        # Perform the underlying read
        any_index.missing_blobs([second_digest])
    updated_order = []
    for digest in any_index.least_recent_digests():
        updated_order.append(digest)
    assert second_digest == updated_order[-1]

    mock_bulk_update_time = mock_create_timer_record(METRIC.STORAGE.SQL_INDEX.UPDATE_TIMESTAMP_DURATION)
    call_list = [mock.call(mock_bulk_update_time)]
    get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_missing_blobs_marked_deleted(
    any_index: SQLIndex, blobs_digests: list[tuple[bytes, Digest]], extra_blobs_digests: list[tuple[bytes, Digest]]
):
    # Normal rows
    for blob, digest in blobs_digests:
        _write_to_index_only(any_index, digest, blob)
    # Rows marked as deleted
    with any_index._sql.session() as session:
        rows = [
            IndexEntry(
                digest_hash=digest.hash,
                digest_size_bytes=digest.size_bytes,
                deleted=True,
                accessed_timestamp=datetime.now(),
            )
            for _, digest in extra_blobs_digests
        ]
        session.bulk_save_objects(rows)
    all_digests = [digest for _, digest in blobs_digests] + [digest for _, digest in extra_blobs_digests]
    missing = any_index.missing_blobs(all_digests)

    assert set(d.hash for d in missing) == set(d.hash for _, d in extra_blobs_digests)


def test_large_missing_blobs(any_index):
    """Ensure that a large missing_blobs query can be handled by the index
    implementation. We'll just use an empty index for this test since
    we don't really care about its state.

    SQLite can only handle 999 bind variables, so we'll test a search for
    a much larger number."""
    large_input = [digest for digest in _digestified_range(50000)]
    expected_hashes = {digest.hash for digest in large_input}
    for digest in any_index.missing_blobs(large_input):
        assert digest.hash in expected_hashes
        expected_hashes.remove(digest.hash)
    assert not expected_hashes


def test_large_missing_blobs_with_some_present(any_index):
    """Same as above, but let's add some blobs to the index."""
    digests_blobs = [(digest, bytes(i)) for i, digest in enumerate(_digestified_range(1000))]
    any_index.bulk_update_blobs(digests_blobs)
    not_missing = set(digest.hash for digest, _ in digests_blobs)

    large_input = [digest for digest in _digestified_range(50000)]
    expected_hashes = {digest.hash for digest in large_input if digest.hash not in not_missing}
    for digest in any_index.missing_blobs(large_input):
        assert digest.hash in expected_hashes
        expected_hashes.remove(digest.hash)
    assert not expected_hashes


def test_delete_blob(any_index, blobs_digests):
    """Deleting a blob should make has_blob return False."""
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    second_blob, second_digest = blobs_digests[1]

    assert any_index.has_blob(second_digest) is True

    any_index.delete_blob(second_digest)

    assert any_index.has_blob(second_digest) is False


def test_get_blob_fallback(any_index, blobs_digests):
    """If fallback_on_get is enabled, blobs not present in the index should
    get pulled in from the backend storage if they're not in the index when
    get_blob is called."""

    for blob, digest in blobs_digests:
        # Write the blobs to the storage directly but not through the index
        _write(any_index._storage, digest, blob)

    second_blob, second_digest = blobs_digests[1]

    # First let's show what happens when fallback is disabled. The
    # digest isn't pulled into the index.
    any_index._fallback_on_get = False
    assert any_index.has_blob(second_digest) is False
    assert any_index.get_blob(second_digest) is None
    assert any_index.has_blob(second_digest) is False

    # Now let's enable fallback to demonstrate that the index will
    # contain the element after fetching
    any_index._fallback_on_get = True
    assert any_index.get_blob(second_digest).read() == second_blob
    assert any_index.has_blob(second_digest) is True

    # Now let's check that if you pass in a blob that doesn't exist in the
    # backend that it doesn't try and write into the index
    missing_blob = remote_execution_pb2.Digest(hash="deadbeef", size_bytes=666)
    assert any_index.has_blob(missing_blob) is False

    assert any_index.get_blob(missing_blob) is None
    assert any_index.bulk_read_blobs([missing_blob]) == {}
    result = any_index.bulk_read_blobs([missing_blob, second_digest])

    assert second_digest.hash in result


def test_duplicate_writes_ok(any_index, blobs_digests):
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)
        _write(any_index, digest, blob)
    for blob, digest in blobs_digests:
        assert any_index.has_blob(digest) is True
        assert any_index.get_blob(digest).read() == blob


def test_bulk_read_blobs_fallback(any_index, blobs_digests):
    """Similar to the above, BatchUpdateBlobs should retrieve blobs from
    storage when fallback is enabled."""
    for blob, digest in blobs_digests:
        # Write the blobs to the storage directly but not through the index
        _write(any_index._storage, digest, blob)

    # To make things slightly more interesting we'll add the first blob
    # to the index
    first_blob, first_digest = blobs_digests[0]
    _write(any_index, first_digest, first_blob)

    digests = [digest for (blob, digest) in blobs_digests]

    # We'll first show what happens when fallback is disabled
    any_index._fallback_on_get = False
    for digest in digests:
        assert any_index.has_blob(digest) is (digest == first_digest)
    any_index.bulk_read_blobs(digests)
    for digest in digests:
        assert any_index.has_blob(digest) is (digest == first_digest)

    # Now with fallback. All of the blobs should be present after a read.
    any_index._fallback_on_get = True
    any_index.bulk_read_blobs(digests)
    for digest in digests:
        assert any_index.has_blob(digest) is True


@pytest.mark.parametrize("enable_fallback", [False, True])
def test_bulk_read_blobs_index_repair(any_index, extra_blobs_digests, enable_fallback):
    """Index should be repaired if blobs are missing in storage even with fallback."""

    # Skip testing any indexes with preset inlining
    if any_index._max_inline_blob_size > 0:
        return
    # Write some blobs to index only to check repair
    for blob, digest in extra_blobs_digests:
        _write_to_index_only(any_index, digest, blob)

    any_index._fallback_on_get = enable_fallback
    digests = [digest for (blob, digest) in extra_blobs_digests]

    # Digests are there in the index
    assert any_index.missing_blobs(digests) == []

    # No blob should be read
    assert any_index.bulk_read_blobs(digests) == {}

    # Digests should not be there in the index
    assert any_index.missing_blobs(digests) == digests


@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_get_total_size(any_index, blobs_digests):
    """Test if summing total size is accurate"""
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    assert any_index.get_total_size() == 11

    mock_size_calc_time = mock_create_timer_record(METRIC.STORAGE.SQL_INDEX.SIZE_CALCULATION_DURATION)
    call_list = [mock.call(mock_size_calc_time)]
    get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


def test_delete_n_bytes(any_index, blobs_digests):
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)
    delete_size = 7

    # Check that the entries are correctly marked as deleted
    deleted_bytes = any_index.delete_n_bytes(delete_size)

    # Verify that rows were deleted from the index and the backing
    # storage and that the size of the missing blobs equals the
    # returned number of deleted bytes
    digests = [digest for _, digest in blobs_digests]
    missing_index_blobs = any_index.missing_blobs(digests)
    assert len(missing_index_blobs) > 0
    assert missing_index_blobs == any_index._storage.missing_blobs(missing_index_blobs)
    sum_of_missing_blobs = 0
    for missing_blob in missing_index_blobs:
        sum_of_missing_blobs += missing_blob.size_bytes
    assert sum_of_missing_blobs == deleted_bytes

    # Check that we got at least the requested number of bytes marked
    # for deletion
    assert deleted_bytes >= delete_size

    # Should never mark more than the requested amount + the largest item
    max_size = max(digest.size_bytes for _, digest in blobs_digests)
    assert deleted_bytes <= delete_size + max_size


def test_delete_n_bytes_dry_run(any_index, blobs_digests):
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    delete_size = 7
    # Check that "dry run" functionality doesn't delete anything
    # but still returns the right number of bytes deleted
    returned_bytes = any_index.delete_n_bytes(delete_size, True)
    with any_index._sql.session() as session:
        marked_entries = session.query(IndexEntry).filter_by(deleted=True).all()
        marked = set(entry.digest_hash for entry in marked_entries)
    assert not marked

    digests = [digest for _, digest in blobs_digests]
    missing_index_blobs = any_index.missing_blobs(digests)
    assert len(missing_index_blobs) == 0

    # Check that we got at least the requested number of bytes marked
    # for deletion
    assert returned_bytes >= delete_size

    # Should never mark more than the requested amount + the largest item
    max_size = max(digest.size_bytes for _, digest in blobs_digests)
    assert returned_bytes <= delete_size + max_size


def test_delete_n_bytes_protect_blobs(any_index, blobs_digests, extra_blobs_digests):
    two_days_ago = datetime.utcnow() - timedelta(days=2)
    almost_two_days_ago = datetime.utcnow() - timedelta(days=2) + timedelta(seconds=30)
    one_day_ago = datetime.utcnow() - timedelta(days=1)

    # Mock datetime.utcnow() so that it posts time from two days ago
    # Blobs in blobs_digests will appear as if they were last accessed
    # 2 days ago within the index
    with (
        mock.patch(
            "buildgrid.server.cas.storage.index.sql_dialect_delegates.postgresqldelegate.datetime"
        ) as mock_date_pstgr,
        mock.patch(
            "buildgrid.server.cas.storage.index.sql_dialect_delegates.sqlitedelegate.datetime"
        ) as mock_date_sqlite,
        mock.patch("buildgrid.server.cas.storage.index.sql.datetime") as mock_date_generic,
    ):
        mock_date_pstgr.utcnow.return_value = almost_two_days_ago
        mock_date_sqlite.utcnow.return_value = almost_two_days_ago
        mock_date_generic.utcnow.return_value = almost_two_days_ago
        for blob, digest in blobs_digests:
            _write(any_index, digest, blob)

    for blob, digest in extra_blobs_digests:
        _write(any_index, digest, blob)

    digests = [digest for _, digest in blobs_digests]
    extra_digests = [digest for _, digest in extra_blobs_digests]
    all_digests = digests + extra_digests
    delete_size = 20

    bytes_deleted = any_index.delete_n_bytes(delete_size, protect_blobs_after=two_days_ago)
    assert bytes_deleted == 0
    # Use has_blob instead to not update timestamps like missing_blobs does
    missing_blobs = []
    for digest in all_digests:
        if not any_index.has_blob(digest):
            missing_blobs.append(digest)

    assert missing_blobs == []

    bytes_deleted = any_index.delete_n_bytes(delete_size, protect_blobs_after=one_day_ago)
    assert bytes_deleted > 0
    missing_blobs = []
    for digest in all_digests:
        if not any_index.has_blob(digest):
            missing_blobs.append(digest)

    assert sorted_digests(digests) == sorted_digests(missing_blobs)

    bytes_deleted = any_index.delete_n_bytes(delete_size)
    assert bytes_deleted > 0
    missing_blobs = []
    for digest in all_digests:
        if not any_index.has_blob(digest):
            missing_blobs.append(digest)
    assert sorted_digests(all_digests) == sorted_digests(missing_blobs)


def test_delete_n_bytes_large_blobs(any_index, blobs_digests, extra_blobs_digests):
    two_days_ago = datetime.utcnow() - timedelta(days=2)
    almost_two_days_ago = datetime.utcnow() - timedelta(days=2) + timedelta(seconds=30)
    one_day_ago = datetime.utcnow() - timedelta(days=1)
    large_blobs_digests = [
        (b"0123456789" * 10, create_digest(b"123456789" * 10)),
        (b"0123456789" * 20, create_digest(b"0123456789" * 20)),
    ]

    # Mock datetime.utcnow() so that it posts time from two days ago
    # Blobs in blobs_digests will appear as if they were last accessed
    # 2 days ago within the index
    with (
        mock.patch(
            "buildgrid.server.cas.storage.index.sql_dialect_delegates.postgresqldelegate.datetime"
        ) as mock_date_pstgr,
        mock.patch(
            "buildgrid.server.cas.storage.index.sql_dialect_delegates.sqlitedelegate.datetime"
        ) as mock_date_sqlite,
        mock.patch("buildgrid.server.cas.storage.index.sql.datetime") as mock_date_generic,
    ):
        mock_date_pstgr.utcnow.return_value = almost_two_days_ago
        mock_date_sqlite.utcnow.return_value = almost_two_days_ago
        mock_date_generic.utcnow.return_value = almost_two_days_ago
        for blob, digest in large_blobs_digests:
            _write(any_index, digest, blob)

        for blob, digest in blobs_digests:
            _write(any_index, digest, blob)

    digests = [digest for _, digest in blobs_digests]
    large_digests = [digest for _, digest in large_blobs_digests]
    all_digests = digests + large_digests
    delete_size = 2000

    bytes_deleted = any_index.delete_n_bytes(delete_size, protect_blobs_after=two_days_ago)
    assert bytes_deleted == 0
    # Use has_blob instead to not update timestamps like missing_blobs does
    missing_blobs = []
    for digest in all_digests:
        if not any_index.has_blob(digest):
            missing_blobs.append(digest)

    assert missing_blobs == []

    # Set the lifetime of large blobs to be only one day and validate only those blobs are deleted
    bytes_deleted = any_index.delete_n_bytes(
        delete_size, protect_blobs_after=two_days_ago, large_blob_threshold=20, large_blob_lifetime=one_day_ago
    )
    assert bytes_deleted > 0
    missing_blobs = []
    for digest in all_digests:
        if not any_index.has_blob(digest):
            missing_blobs.append(digest)

    assert sorted_digests(large_digests) == sorted_digests(missing_blobs)


def test_delete_n_bytes_verify_order(any_index, blobs_digests, extra_blobs_digests):
    two_days_ago = datetime.utcnow() - timedelta(days=2)

    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    # Mock datetime.utcnow() so that it posts time from two days ago
    # Blobs in blobs_digests will appear as if they were last accessed
    # 2 days ago within the index
    with (
        mock.patch(
            "buildgrid.server.cas.storage.index.sql_dialect_delegates.postgresqldelegate.datetime"
        ) as mock_date_pstgr,
        mock.patch(
            "buildgrid.server.cas.storage.index.sql_dialect_delegates.sqlitedelegate.datetime"
        ) as mock_date_sqlite,
        mock.patch("buildgrid.server.cas.storage.index.sql.datetime") as mock_date_generic,
    ):
        mock_date_pstgr.utcnow.return_value = two_days_ago
        mock_date_sqlite.utcnow.return_value = two_days_ago
        mock_date_generic.utcnow.return_value = two_days_ago
        for blob, digest in extra_blobs_digests:
            _write(any_index, digest, blob)

    # The digests in extra_blobs_digests have older timestamps and should be deleted first
    digests = [digest for _, digest in blobs_digests]
    extra_digests = [digest for _, digest in extra_blobs_digests]
    delete_size = 7
    _ = any_index.delete_n_bytes(delete_size)
    deleted_digests = sorted_digests(any_index.missing_blobs(digests + extra_digests))
    assert deleted_digests == sorted_digests(extra_digests)

    # Bumping up the delete size removes the rest of the blobs
    delete_size_all = 20
    _ = any_index.delete_n_bytes(delete_size_all)
    deleted_digests = sorted_digests(any_index.missing_blobs(digests + extra_digests))
    assert deleted_digests == sorted_digests([x[1] for x in (extra_blobs_digests + blobs_digests)])


def test_delete_n_bytes_renew_windows(any_index, blobs_digests, extra_blobs_digests):
    for blob, digest in blobs_digests + extra_blobs_digests:
        _write(any_index, digest, blob)

    # Test 1: At start, and without doing cleanup, there should be no LRU windows.
    # A terminology note: "any_index._queue_of_whereclauses" will be referred as "windows"
    assert len(any_index._queue_of_whereclauses) == 0

    # Test 2: Given window_size==2, by deleting a single digest, there should
    # be 3 windows (of whereclauses) constructed upon invoking cleanup
    any_index._all_blobs_window_size = 2
    request_size = 1
    any_index.delete_n_bytes(request_size)
    assert len(any_index._queue_of_whereclauses) == 3

    # Test 3: Deleting another digest should exhaust the first window (two digests), but not
    # discard the window (not stepping into next window), hence 3 windows.
    request_size = 1
    any_index.delete_n_bytes(request_size)
    # assert digests == sorted_digests(DIGESTS[:2])
    assert len(any_index._queue_of_whereclauses) == 3

    # Test 4: Another delete should exhaust the first window (two digests), discard it, and
    # step into the 2nd whereclause, hence 2 windows.
    request_size = 1
    any_index.delete_n_bytes(request_size)
    assert len(any_index._queue_of_whereclauses) == 2

    # Test 5: Bumping up the request size to delete the rest of the blobs, consuming all the
    # remaining windows.
    request_size = 10
    any_index.delete_n_bytes(request_size)
    assert len(any_index._queue_of_whereclauses) == 0

    # Test 6: Readd some blobs and verify that a new queue of whereclauses is generated
    for blob, digest in blobs_digests + extra_blobs_digests:
        _write(any_index, digest, blob)
    request_size = 1
    any_index.delete_n_bytes(request_size)
    assert len(any_index._queue_of_whereclauses) == 3


def test_delete_n_bytes_premarked_blobs(any_index, blobs_digests, extra_blobs_digests):
    for blob, digest in blobs_digests + extra_blobs_digests:
        _write(any_index, digest, blob)

    marked_blob, marked_digest = extra_blobs_digests[0]
    # Mark some blobs as deleted and verify those are deleted
    with any_index._sql.session() as session:
        session.execute(update(IndexEntry).where(IndexEntry.digest_hash == marked_digest.hash).values(deleted=True))

    any_index.delete_n_bytes(1)
    assert any_index._delete_premarked_blobs is False
    assert any_index.has_blob(marked_digest) is False

    # Rewrite the deleted blob, mark it deleted, and verify that it isn't deleted next
    _write(any_index, marked_digest, marked_blob)
    with any_index._sql.session() as session:
        session.execute(update(IndexEntry).where(IndexEntry.digest_hash == marked_digest.hash).values(deleted=True))

    any_index.delete_n_bytes(1)
    assert any_index.has_blob(marked_digest) is True


def _query_for_entry_by_digest(digest):
    return select(IndexEntry).where(IndexEntry.digest_hash == digest.hash)


def test_bulk_delete(any_index, blobs_digests):
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    # Pick a subset of digests to delete``
    all_digests = [digest for blob, digest in blobs_digests]
    digests_to_delete = all_digests[:3]
    deleted_hashes = set([x.hash for x in digests_to_delete])
    any_index.bulk_delete(digests_to_delete)

    for digest in digests_to_delete:
        statement1 = _query_for_entry_by_digest(digest)
    # Check that only the right things were deleted
    diff = [digest for digest in all_digests if digest.hash not in deleted_hashes]
    for digest in diff:
        statement2 = _query_for_entry_by_digest(digest)

    entry1 = []
    entry2 = []
    with any_index._sql.session() as session:
        entry1 = session.execute(statement1).scalars().all()
        entry2 = session.execute(statement2).scalars().first()

    assert not entry1
    assert entry2

    # Finally check that the deleted digests are not present in the backing storage
    missing_blobs = any_index._storage.missing_blobs(digests_to_delete)
    assert missing_blobs == digests_to_delete


def test_bulk_delete_test_inclause(any_index, blobs_digests):
    for blob, digest in blobs_digests:
        _write(any_index, digest, blob)

    # make inclause smaller than length of blob_digests
    any_index._inclause_limit = 2

    # Pick a subset of digests to delete
    all_digests = [digest for blob, digest in blobs_digests]
    digests_to_delete = all_digests[:3]
    deleted_hashes = set([x.hash for x in digests_to_delete])
    any_index.bulk_delete(digests_to_delete)

    for digest in digests_to_delete:
        statement1 = _query_for_entry_by_digest(digest)
    # Check that only the right things were deleted
    diff = [digest for digest in all_digests if digest.hash not in deleted_hashes]
    for digest in diff:
        statement2 = _query_for_entry_by_digest(digest)

    entry1 = []
    entry2 = []
    with any_index._sql.session() as session:
        entry1 = session.execute(statement1).scalars().all()
        entry2 = session.execute(statement2).scalars().first()

    assert not entry1
    assert entry2

    # Finally check that the deleted digests are not present in the backing storage
    missing_blobs = any_index._storage.missing_blobs(digests_to_delete)
    assert missing_blobs == digests_to_delete
