# Copyright (C) 2024 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from datetime import datetime, timedelta, timezone
from typing import Iterable

import pytest

from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Digest
from buildgrid._protos.google.rpc import code_pb2
from buildgrid.server.cas.storage.index.redis import RedisIndex
from buildgrid.server.cas.storage.lru_memory_cache import LRUMemoryCache
from buildgrid.server.cas.storage.storage_abc import create_write_session
from buildgrid.server.redis.provider import RedisProvider
from buildgrid.server.utils.digests import create_digest

BLOBS = [b"abc", b"defg", b"hijk", b"", b"lmno", b"pqr"]
DIGESTS = [create_digest(blob) for blob in BLOBS]


@pytest.fixture()
def digests_blobs():
    return list(zip(DIGESTS, BLOBS))


def get_hash(x: Digest) -> str:
    return x.hash


def sorted_digests(items: Iterable[Digest]) -> list[Digest]:
    return sorted(items, key=get_hash)


def sorted_digest_lists(arrays: Iterable[list[Digest]]) -> list[Digest]:
    return sorted((d for a in arrays for d in a), key=get_hash)


@pytest.fixture()
def redis_index(fake_redis: RedisProvider):
    storage = LRUMemoryCache(8129)
    yield RedisIndex(redis=fake_redis, storage=storage)


def _write(storage, digest, blob):
    with create_write_session(digest) as session:
        session.write(blob)
        storage.commit_write(digest, session)


def test_redis_key_format(redis_index: RedisIndex, digests_blobs: list[tuple[Digest, bytes]]):
    """Validate that keys have the expected format so any changes cause tests to fail"""
    redis = redis_index._redis
    digest, blob = digests_blobs[0]
    _write(redis_index, digest, blob)

    def validate_keys(r: "redis.Redis[bytes]") -> None:
        assert r.exists("total_size")
        redis_key = f"A:{digest.hash}_{digest.size_bytes}"
        assert r.exists(redis_key)

    redis.execute_ro(validate_keys)


def test_redis_configurable_prefix_key_format(fake_redis: RedisProvider, digests_blobs: list[tuple[Digest, bytes]]):
    """Validate that keys have the expected format so any changes cause tests to fail"""
    storage = LRUMemoryCache(8129)
    redis_index = RedisIndex(redis=fake_redis, storage=storage, prefix="B")
    redis = redis_index._redis
    digest, blob = digests_blobs[0]
    _write(redis_index, digest, blob)

    def validate_keys(r: "redis.Redis[bytes]") -> None:
        assert r.exists("B:total_size")
        redis_key = f"B:{digest.hash}_{digest.size_bytes}"
        assert r.exists(redis_key)

    redis.execute_ro(validate_keys)


def test_writes_update_total_size(redis_index: RedisIndex, digests_blobs: list[tuple[Digest, bytes]]):
    """The index should accurately reflect its contents with has_blob."""

    expected_size = 0
    for digest, blob in digests_blobs:
        assert redis_index.has_blob(digest) is False
        _write(redis_index, digest, blob)
        expected_size += digest.size_bytes

    assert redis_index.get_total_size() == expected_size
    assert redis_index.get_blob_count() == len(digests_blobs)

    # Writing an already existing blob shouldn't increase the total_size
    repeat_digest, repeat_blob = digests_blobs[0]
    _write(redis_index, repeat_digest, repeat_blob)
    assert redis_index.get_total_size() == expected_size


def test_bulk_update_updates_total_size(redis_index: RedisIndex, digests_blobs: list[tuple[Digest, bytes]]):
    expected_size = 0
    for digest, _ in digests_blobs:
        assert redis_index.has_blob(digest) is False
        expected_size += digest.size_bytes

    results = redis_index.bulk_update_blobs(digests_blobs)
    for result in results:
        assert result.code == code_pb2.OK

    assert redis_index.get_total_size() == expected_size
    assert redis_index.get_blob_count() == len(digests_blobs)

    # Repeating the bulk_write shouldn't update total_size
    results2 = redis_index.bulk_update_blobs(digests_blobs)
    for result in results2:
        assert result.code == code_pb2.OK

    assert redis_index.get_total_size() == expected_size
    assert redis_index.get_blob_count() == len(digests_blobs)


def test_bulk_delete_updates_total_size(redis_index: RedisIndex, digests_blobs: list[tuple[Digest, bytes]]):
    # Add an extra blob to the test set

    extra_blob = b"foobar"
    extra_digest = create_digest(extra_blob)

    expected_size = 0
    for digest, _ in digests_blobs:
        assert redis_index.has_blob(digest) is False
        expected_size += digest.size_bytes

    results = redis_index.bulk_update_blobs(digests_blobs)
    for result in results:
        assert result.code == code_pb2.OK
    expected_size += extra_digest.size_bytes
    _write(redis_index, extra_digest, extra_blob)

    assert redis_index.get_total_size() == expected_size
    assert redis_index.get_blob_count() == len(digests_blobs) + 1

    # Now delete all but the extra blob we created
    delete_result = redis_index.bulk_delete([digest for digest, _ in digests_blobs])
    assert len(delete_result) == 0
    assert redis_index.get_total_size() == extra_digest.size_bytes
    assert redis_index.get_blob_count() == 1

    # Make sure that attempting to delete digests which have already been
    # deleted doesn't change total_size
    delete_result = redis_index.bulk_delete([digest for digest, _ in digests_blobs])
    assert len(delete_result) == 0
    assert redis_index.get_total_size() == extra_digest.size_bytes
    assert redis_index.get_blob_count() == 1


def test_delete_n_bytes(redis_index: RedisIndex, digests_blobs: list[tuple[Digest, bytes]]):
    expected_size = 0
    for digest, _ in digests_blobs:
        expected_size += digest.size_bytes

    results = redis_index.bulk_update_blobs(digests_blobs)
    for result in results:
        assert result.code == code_pb2.OK

    # Run in dry-run mode to verify that no bytes have been deleted
    bytes_deleted = redis_index.delete_n_bytes(expected_size, dry_run=True)
    assert redis_index.get_total_size() == expected_size
    assert redis_index.get_blob_count() == len(digests_blobs)
    # All data would have been deleted
    assert bytes_deleted == expected_size

    # Set protect_blobs_after time in the past to verify that
    # no blobs are deleted as all blobs are newer
    bytes_deleted = redis_index.delete_n_bytes(
        expected_size, protect_blobs_after=datetime.now(timezone.utc) - timedelta(days=1)
    )
    assert bytes_deleted == 0
    assert redis_index.get_total_size() == expected_size
    assert redis_index.get_blob_count() == len(digests_blobs)

    # Verify that some blobs are deleted.
    bytes_deleted = redis_index.delete_n_bytes(1)
    assert bytes_deleted > 0
    expected_size -= bytes_deleted
    assert redis_index.get_total_size() == expected_size

    # Make sure only a single blob was deleted
    num_missing = redis_index.missing_blobs([digest for digest, _ in digests_blobs])
    assert len(num_missing) == 1
    assert redis_index.get_blob_count() == len(digests_blobs) - 1

    # Pass a large value and verify all digests are removed
    bytes_deleted = redis_index.delete_n_bytes(expected_size + 1000)
    assert bytes_deleted == expected_size
    assert redis_index.get_total_size() == 0
    assert redis_index.get_blob_count() == 0


def test_delete_n_bytes_multiple_prefixes(
    fake_redis: RedisProvider, redis_index: RedisIndex, digests_blobs: list[tuple[Digest, bytes]]
):
    secondStorage = LRUMemoryCache(8129)
    bIndex = RedisIndex(redis=fake_redis, storage=secondStorage, prefix="B")
    expected_size = 0
    for digest, _ in digests_blobs:
        expected_size += digest.size_bytes

    results = redis_index.bulk_update_blobs(digests_blobs)
    for result in results:
        assert result.code == code_pb2.OK

    # Write the blobs again to the 2nd redis instance sharing the same cache
    results = bIndex.bulk_update_blobs(digests_blobs)
    for result in results:
        assert result.code == code_pb2.OK

    # This is the total set of keys in redis, which includes
    # two copies of each digest under different prefixes and two
    # different total_size keys
    total_keys = fake_redis.execute_ro(lambda r: r.dbsize())
    assert total_keys == (len(digests_blobs) * 2) + 2

    # Verify that some blobs are deleted, but only from the first index.
    bytes_deleted = redis_index.delete_n_bytes(1)
    assert bytes_deleted > 0
    expected_size -= bytes_deleted
    assert redis_index.get_total_size() == expected_size

    # Make sure only a single blob was deleted
    num_missing = redis_index.missing_blobs([digest for digest, _ in digests_blobs])
    assert len(num_missing) == 1
    assert len(bIndex.missing_blobs([digest for digest, _ in digests_blobs])) == 0
    # Total keys - total_size key for this index - the actual removed blob
    assert redis_index.get_blob_count() == total_keys - 2

    # Pass a large value and verify all digests are removed, again only from the
    # first index
    bytes_deleted = redis_index.delete_n_bytes(expected_size + 1000)
    assert bytes_deleted == expected_size
    assert redis_index.get_total_size() == 0
    assert redis_index.get_blob_count() == total_keys - len(digests_blobs) - 1

    assert len(bIndex.missing_blobs([digest for digest, _ in digests_blobs])) == 0
    # The total_size key from the first index still exists, but no other keys do
    assert bIndex.get_blob_count() == len(digests_blobs) + 1


def test_delete_n_bytes_large_blobs(redis_index: RedisIndex, digests_blobs: list[tuple[Digest, bytes]]):
    expected_size = 0
    for digest, _ in digests_blobs:
        expected_size += digest.size_bytes

    results = redis_index.bulk_update_blobs(digests_blobs)
    for result in results:
        assert result.code == code_pb2.OK

    # Add a new blob which is double the size of all existing blobs
    largeBlob = b"0" * expected_size * 2
    largeBlobDigest = create_digest(largeBlob)
    assert largeBlobDigest.size_bytes > expected_size
    results = redis_index.bulk_update_blobs([(largeBlobDigest, largeBlob)])
    for result in results:
        assert result.code == code_pb2.OK
    expected_size += largeBlobDigest.size_bytes

    # Validate that no blobs are eligable for deletion with a now-1 day protection
    bytes_deleted = redis_index.delete_n_bytes(
        expected_size, protect_blobs_after=datetime.now(timezone.utc) - timedelta(days=1)
    )
    assert redis_index.get_total_size() == expected_size
    assert redis_index.get_blob_count() == len(digests_blobs) + 1
    assert bytes_deleted == 0
    # Validate that the large blob is deleted the large blob lifetime is set to 1 day in the future
    bytes_deleted = redis_index.delete_n_bytes(
        expected_size,
        protect_blobs_after=datetime.now(timezone.utc) - timedelta(days=1),
        large_blob_threshold=largeBlobDigest.size_bytes - 1,
        large_blob_lifetime=datetime.now(timezone.utc) + timedelta(days=1),
    )
    assert redis_index.get_total_size() == expected_size - bytes_deleted
    assert redis_index.get_blob_count() == len(digests_blobs)
    assert bytes_deleted == largeBlobDigest.size_bytes


def test_fmb_sets_ttl(redis_index: RedisIndex, digests_blobs: list[tuple[Digest, bytes]]):
    redis = redis_index._redis
    results = redis_index.bulk_update_blobs(digests_blobs)
    for result in results:
        assert result.code == code_pb2.OK
    digests = [digest for digest, _ in digests_blobs]

    def get_initial_ttls(r: "redis.Redis[bytes]") -> None:
        for digest in digests:
            digest_key = redis_index._construct_key(digest)
            assert r.ttl(digest_key) > 0

    redis.execute_ro(get_initial_ttls)

    # Set the TTL of all blobs to a lower value
    redis.execute_rw(lambda r: [r.expire(redis_index._construct_key(digest), 3600) for digest in digests])

    # Now call FMB on some of the blobs to verify that the ttl was updated
    results = redis_index.missing_blobs(digests)
    assert len(results) == 0

    def validate_updated_ttls(r: "redis.Redis[bytes]") -> None:
        for digest in digests:
            digest_key = redis_index._construct_key(digest)
            assert r.ttl(digest_key) > 3600

    redis.execute_ro(validate_updated_ttls)
