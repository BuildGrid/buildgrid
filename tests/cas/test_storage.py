# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import errno
import itertools
import math
import random
import tempfile
import time
from contextlib import contextmanager
from random import shuffle
from unittest import mock
from unittest.mock import patch

import boto3
import psycopg2
import pycurl
import pytest
from botocore.exceptions import ClientError
from sqlalchemy.exc import DBAPIError

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid._protos.google.rpc import code_pb2
from buildgrid._protos.google.rpc.status_pb2 import Status
from buildgrid.server.cas.storage.disk import DiskStorage
from buildgrid.server.cas.storage.index.redis import RedisIndex
from buildgrid.server.cas.storage.index.sql import SQLIndex
from buildgrid.server.cas.storage.lru_memory_cache import LRUMemoryCache
from buildgrid.server.cas.storage.redis import RedisStorage
from buildgrid.server.cas.storage.remote import RemoteStorage
from buildgrid.server.cas.storage.replicated import ReplicatedStorage
from buildgrid.server.cas.storage.s3 import S3Storage
from buildgrid.server.cas.storage.sharded import ShardedStorage
from buildgrid.server.cas.storage.size_differentiated import SizeDifferentiatedStorage
from buildgrid.server.cas.storage.sql import SQLStorage
from buildgrid.server.cas.storage.storage_abc import create_write_session
from buildgrid.server.cas.storage.with_cache import WithCacheStorage
from buildgrid.server.context import instance_context
from buildgrid.server.exceptions import NotFoundError, StorageFullError
from buildgrid.server.s3 import s3utils
from buildgrid.server.settings import HASH, MAX_IN_MEMORY_BLOB_SIZE_BYTES, S3_MAX_UPLOAD_SIZE, S3_MULTIPART_PART_SIZE
from buildgrid.server.sql.provider import SqlProvider
from buildgrid.server.utils.digests import create_digest
from tests.utils.fixtures import pg_schema_provider

from ..utils.cas import serve_cas
from ..utils.fixtures import Moto

BLOBS = [(b"abc", b"defg", b"hijk", b"", b"abcdefghijk")]
BLOBS_DIGESTS = [
    tuple([remote_execution_pb2.Digest(hash=HASH(blob).hexdigest(), size_bytes=len(blob)) for blob in blobs])
    for blobs in BLOBS
]
EMPTY_BLOB_DIGEST = remote_execution_pb2.Digest(hash=HASH(b"").hexdigest(), size_bytes=len(b""))
STORAGE_TYPES = [
    "lru",
    "disk",
    "s3",
    "lru_disk",
    "lru_disk_deferred",
    "lru_disk_size_differentiated",
    "lru_disk_size_differentiated_threadpool",
    "disk_s3",
    "remote",
    "remote-context",
    "redis",
    "redis_index",
    "sql_index",
    "sql_index_inlined",
    "postgresql_sql_index_inlined",
    "tiny_lru_cache_s3",
    "s3_in_mem_lru_index",
    "disk_with_unavailable_cache",
    "sharded",
    "sharded_threadpool",
    "sqlite",
    "postgresql",
    "replicated",
    "replicated_fmb_replication",
]


@pytest.fixture(params=STORAGE_TYPES)
def any_storage(request, moto_server: Moto, fake_redis, postgres):
    @contextmanager
    def factory():
        if request.param == "lru":
            yield LRUMemoryCache(8129)

        elif request.param == "disk":
            with tempfile.TemporaryDirectory() as path:
                yield DiskStorage(path)

        elif request.param == "s3":
            nums = [str(i) for i in range(10)]
            lets = [chr(i) for i in range(ord("a"), ord("g"))]
            hexvals = nums + lets
            for i in range(len(hexvals)):
                bucket_name = "testing--" + hexvals[i]
                boto3.resource("s3", **moto_server.opts).create_bucket(Bucket=moto_server.bucket(bucket_name))
            # limit page size to test functionality
            yield S3Storage(moto_server.bucket("testing--{digest[0]}"), page_size=3, **moto_server.opts)

        elif request.param == "lru_disk":
            # LRU cache with a uselessly small limit, so requests always fall back
            with tempfile.TemporaryDirectory() as path:
                yield WithCacheStorage(LRUMemoryCache(1), DiskStorage(path))

        elif request.param == "lru_disk_size_differentiated":
            with tempfile.TemporaryDirectory() as path:
                lru = LRUMemoryCache(10)
                disk = DiskStorage(path)
                yield SizeDifferentiatedStorage([{"storage": lru, "max_size": 3}], disk)

        elif request.param == "lru_disk_size_differentiated_threadpool":
            with tempfile.TemporaryDirectory() as path:
                lru = LRUMemoryCache(10)
                disk = DiskStorage(path)
                yield SizeDifferentiatedStorage([{"storage": lru, "max_size": 3}], disk, 2)

        elif request.param == "lru_disk_deferred":
            # LRU cache with a disk fallback, with writes to the fallback deferred
            # to a background thread to increase write request performance.
            with tempfile.TemporaryDirectory() as path:
                yield WithCacheStorage(LRUMemoryCache(2048), DiskStorage(path), defer_fallback_writes=True)

        elif request.param == "disk_s3":
            # Disk-based cache of S3, but we don't delete files, so requests
            # are always handled by the cache
            with tempfile.TemporaryDirectory() as path:
                boto3.resource("s3", **moto_server.opts).create_bucket(Bucket=moto_server.bucket("testing"))
                yield WithCacheStorage(DiskStorage(path), S3Storage(moto_server.bucket("testing"), **moto_server.opts))

        elif request.param == "remote":
            with serve_cas(["testing"]) as server:
                yield RemoteStorage(server.remote, "testing", max_backoff=0)

        elif request.param == "remote-context":
            with serve_cas(["testing"]) as server:
                with instance_context("testing"):
                    yield RemoteStorage(server.remote, max_backoff=0)

        elif request.param == "remote_without_bytestream":
            # CAS server without a Bytestream service to verify use of
            # batch requests. Not enabled by default.
            with serve_cas(["testing"], bytestream=False) as server:
                yield RemoteStorage(server.remote, "testing", max_backoff=0)

        elif request.param == "redis":
            yield RedisStorage(fake_redis)

        elif request.param == "redis_index":
            storage = LRUMemoryCache(8129)
            yield RedisIndex(redis=fake_redis, storage=storage)

        elif request.param == "sql_index":
            storage = LRUMemoryCache(8129)
            with tempfile.NamedTemporaryFile() as db:
                sql_provider = SqlProvider(connection_string=f"sqlite:///{db.name}", automigrate=True)
                yield SQLIndex(sql_provider=sql_provider, storage=storage)

        elif request.param == "sql_index_inlined":
            storage = LRUMemoryCache(8129)
            with tempfile.NamedTemporaryFile() as db:
                sql_provider = SqlProvider(connection_string=f"sqlite:///{db.name}", automigrate=True)
                yield SQLIndex(sql_provider=sql_provider, storage=storage, max_inline_blob_size=256)

        elif request.param == "tiny_lru_cache_s3":
            # LRU in-memory cache of S3, but the LRU size is set to 1 so all
            # requests are handled by the fallback
            boto3.resource("s3", **moto_server.opts).create_bucket(Bucket=moto_server.bucket("testing"))
            yield WithCacheStorage(LRUMemoryCache(1), S3Storage(moto_server.bucket("testing"), **moto_server.opts))

        elif request.param == "s3_in_mem_lru_index":
            # index pointing to s3+in_mem_lru cache storage, with the lru sized to handle
            # some blobs but not others
            boto3.resource("s3", **moto_server.opts).create_bucket(Bucket=moto_server.bucket("testing"))
            storage = WithCacheStorage(LRUMemoryCache(8), S3Storage(moto_server.bucket("testing"), **moto_server.opts))
            with tempfile.NamedTemporaryFile() as db:
                sql_provider = SqlProvider(connection_string=f"sqlite:///{db.name}", automigrate=True)
                yield SQLIndex(sql_provider=sql_provider, storage=storage)

        elif request.param == "disk_with_unavailable_cache":
            with tempfile.TemporaryDirectory() as path:
                # Domains in the `invalid` TLD can be assumed to not exist (RFC 6761)
                remote = RemoteStorage("http://unavailable.invalid", "testing", request_timeout=0, max_backoff=0)
                yield WithCacheStorage(remote, DiskStorage(path))

        elif request.param == "sharded":
            yield ShardedStorage({"A": LRUMemoryCache(8129), "B": LRUMemoryCache(8129), "C": LRUMemoryCache(8129)})

        elif request.param == "sharded_threadpool":
            yield ShardedStorage({"A": LRUMemoryCache(8129), "B": LRUMemoryCache(8129), "C": LRUMemoryCache(8129)}, 2)

        elif request.param == "sqlite":
            with tempfile.NamedTemporaryFile() as db:
                sql_provider = SqlProvider(connection_string=f"sqlite:///{db.name}", automigrate=True)
                yield SQLStorage(sql_provider=sql_provider)

        elif request.param == "postgresql":
            with pg_schema_provider(postgres) as sql_provider:
                yield SQLStorage(sql_provider=sql_provider)

        elif request.param == "postgresql_sql_index_inlined":
            with pg_schema_provider(postgres) as sql_provider:
                storage = LRUMemoryCache(8129)
                yield SQLIndex(sql_provider=sql_provider, storage=storage, max_inline_blob_size=256)

        elif request.param == "replicated":
            yield ReplicatedStorage([LRUMemoryCache(8129), LRUMemoryCache(8129)])

        elif request.param == "replicated_fmb_replication":
            yield ReplicatedStorage([LRUMemoryCache(8129), LRUMemoryCache(8129)], replication_queue_size=1000)

    with factory() as storage:
        with storage:
            storage.start()
            yield storage
            storage.stop()


def write(storage, digest, blob):
    with create_write_session(digest) as session:
        session.write(blob)
        storage.commit_write(digest, session)


@pytest.mark.parametrize("blobs_digests", zip(BLOBS, BLOBS_DIGESTS))
def test_initially_empty(any_storage, blobs_digests):
    _, digests = blobs_digests
    for digest in digests:
        # Remote storage will always have the empty digest
        if isinstance(any_storage, RemoteStorage) and digest == EMPTY_BLOB_DIGEST:
            assert any_storage.has_blob(digest)
        else:
            assert not any_storage.has_blob(digest)


@pytest.mark.parametrize("blobs_digests", zip(BLOBS, BLOBS_DIGESTS))
def test_basic_write_read(any_storage, blobs_digests):
    blobs, digests = blobs_digests

    for blob, digest in zip(blobs, digests):
        if isinstance(any_storage, RemoteStorage) and digest == EMPTY_BLOB_DIGEST:
            assert any_storage.has_blob(digest)
        else:
            assert not any_storage.has_blob(digest)
        write(any_storage, digest, blob)
        assert any_storage.has_blob(digest)
        assert any_storage.get_blob(digest).read() == blob

        # Try seeking on that blob
        read_head = any_storage.get_blob(digest)
        read_head.seek(1)
        assert read_head.read() == blob[1:]

        # Try writing the same digest again (since it's valid to do that)
        write(any_storage, digest, blob)
        assert any_storage.has_blob(digest)
        assert any_storage.get_blob(digest).read() == blob


@pytest.mark.parametrize("any_storage", ["s3", "remote"], indirect=["any_storage"])
def test_basic_write_read_large_blob(any_storage):
    # Some storages use an intermediary temporary file when reading files
    # larger than `MAX_IN_MEMORY_BLOB_SIZE_BYTES`; exercising that code path.
    large_blob = b"large blob" + b"." * MAX_IN_MEMORY_BLOB_SIZE_BYTES
    large_blob_hash = HASH(large_blob).hexdigest()
    large_blob_digest = remote_execution_pb2.Digest(hash=large_blob_hash, size_bytes=len(large_blob))

    assert not any_storage.has_blob(large_blob_digest)
    write(any_storage, large_blob_digest, large_blob)
    assert any_storage.has_blob(large_blob_digest)
    assert any_storage.get_blob(large_blob_digest).read() == large_blob


@pytest.mark.parametrize("any_storage", ["s3"], indirect=["any_storage"])
@pytest.mark.parametrize("blob_size", [2 * S3_MULTIPART_PART_SIZE, 2 * S3_MULTIPART_PART_SIZE + 10])
def test_multipart_upload(any_storage, blob_size):
    large_blob = random.getrandbits(blob_size * 8).to_bytes(blob_size, "little")
    large_blob_hash = HASH(large_blob).hexdigest()
    large_blob_digest = remote_execution_pb2.Digest(hash=large_blob_hash, size_bytes=len(large_blob))

    # Wrap the multipart upload code path so we can check the call
    call_count = 0
    orig = s3utils.start_multipart_upload

    def _wrapped_multipart_upload(*args, **kwargs):
        nonlocal call_count
        call_count += 1
        return orig(*args, **kwargs)

    with mock.patch("buildgrid.server.cas.storage.s3.s3utils.start_multipart_upload", new=_wrapped_multipart_upload):
        assert not any_storage.has_blob(large_blob_digest)
        write(any_storage, large_blob_digest, large_blob)
        assert any_storage.has_blob(large_blob_digest)

    # Check that we actually performed a multipart upload
    assert call_count == 1

    # Check that the blob was uploaded correctly
    response = any_storage._s3.get_object(
        Bucket=any_storage._get_bucket_name(large_blob_digest.hash), Key=any_storage._construct_key(large_blob_digest)
    )
    assert response["Body"].read() == large_blob

    # Check that the blob was uploaded in the expected number of parts
    response = any_storage._s3.head_object(
        Bucket=any_storage._get_bucket_name(large_blob_digest.hash),
        Key=any_storage._construct_key(large_blob_digest),
        PartNumber=1,
    )
    assert response["PartsCount"] == math.ceil(len(large_blob) / S3_MULTIPART_PART_SIZE)


@pytest.mark.parametrize("any_storage", ["s3"], indirect=["any_storage"])
def test_multipart_abort(any_storage):
    large_blob = b"large blob" + b"." * S3_MAX_UPLOAD_SIZE
    large_blob_hash = HASH(large_blob).hexdigest()
    large_blob_digest = remote_execution_pb2.Digest(hash=large_blob_hash, size_bytes=len(large_blob))

    abort_call_count = 0
    orig_abort = s3utils.abort_multipart_upload

    def _wrapped_abort_upload(*args, **kwargs):
        nonlocal abort_call_count
        abort_call_count += 1
        return orig_abort(*args, **kwargs)

    with mock.patch("buildgrid.server.cas.storage.s3.s3utils.abort_multipart_upload", new=_wrapped_abort_upload):
        with mock.patch("buildgrid.server.cas.storage.s3.s3utils.upload_parts", side_effect=Exception("Bad upload")):
            assert not any_storage.has_blob(large_blob_digest)
            with pytest.raises(Exception):
                write(any_storage, large_blob_digest, large_blob)

    assert abort_call_count == 1


@pytest.mark.parametrize("any_storage", ["s3"], indirect=["any_storage"])
def test_key_prefixing(any_storage):
    # digest hash: ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad
    # digest_size: 3
    # Using no prefixing
    any_storage._s3_hash_prefix_size = None
    any_storage._s3_path_prefix_string = None
    prefixed_key = any_storage._construct_key_with_prefix(BLOBS_DIGESTS[0][0])
    assert prefixed_key == "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad_3"
    # Using only hash prefix
    any_storage._s3_hash_prefix_size = 4
    prefixed_key = any_storage._construct_key_with_prefix(BLOBS_DIGESTS[0][0])
    assert prefixed_key == "ba78/16bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad_3"
    # Using only path prefix string
    any_storage._s3_hash_prefix_size = None
    any_storage._s3_path_prefix_string = "path/prefix"
    prefixed_key = any_storage._construct_key_with_prefix(BLOBS_DIGESTS[0][0])
    assert prefixed_key == "path/prefix/ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad_3"
    # Using both hash prefix and path prefix string
    any_storage._s3_hash_prefix_size = 4
    any_storage._s3_path_prefix_string = "path/prefix"
    prefixed_key = any_storage._construct_key_with_prefix(BLOBS_DIGESTS[0][0])
    assert prefixed_key == "path/prefix/ba78/16bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad_3"


@pytest.mark.parametrize("any_storage", ["s3"], indirect=["any_storage"])
def test_deconstruct_key_with_prefixing(any_storage):
    # digest hash: ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad
    # digest_size: 3
    any_storage._s3_hash_prefix_size = 4
    any_storage._s3_path_prefix_string = "path/prefix"
    prefixed_key = any_storage._construct_key_with_prefix(BLOBS_DIGESTS[0][0])
    assert prefixed_key == "path/prefix/ba78/16bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad_3"
    deconstructed_key = any_storage._deconstruct_key(prefixed_key)
    assert deconstructed_key[0] == "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad"
    assert deconstructed_key[1] == 3
    # Only using hash prefix
    any_storage._s3_hash_prefix_size = 4
    any_storage._s3_path_prefix_string = None
    prefixed_key = any_storage._construct_key_with_prefix(BLOBS_DIGESTS[0][0])
    deconstructed_key = any_storage._deconstruct_key(prefixed_key)
    assert deconstructed_key[0] == "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad"
    assert deconstructed_key[1] == 3
    # Only using path prefix string
    any_storage._s3_hash_prefix_size = None
    any_storage._s3_path_prefix_string = "path/prefix"
    prefixed_key = any_storage._construct_key_with_prefix(BLOBS_DIGESTS[0][0])
    deconstructed_key = any_storage._deconstruct_key(prefixed_key)
    assert deconstructed_key[0] == "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad"
    assert deconstructed_key[1] == 3


@pytest.mark.parametrize("blobs_digests", zip(BLOBS, BLOBS_DIGESTS))
def test_write_if_storage_is_full(any_storage, blobs_digests):
    blobs, digests = blobs_digests

    def __test_basic_write(any_storage, blobs, digests):
        with pytest.raises(StorageFullError):
            for blob, digest in zip(blobs, digests):
                write(any_storage, digest, blob)

    if isinstance(any_storage, S3Storage):
        with patch("buildgrid.server.s3.s3utils.put_objects") as m:
            response = {"Error": {"Code": "QuotaExceededException"}}
            err = ClientError(response, "Error")

            def mock_put_objects(s3, objects, *args, **kwargs):
                for s3object in objects:
                    s3object.error = err

            m.side_effect = mock_put_objects
            __test_basic_write(any_storage, blobs, digests)

    elif isinstance(any_storage, SQLIndex):
        with patch("buildgrid.server.cas.storage.index.sql.SQLIndex._save_digests_to_index") as m:
            orig = psycopg2.errors.DiskFull()
            error = DBAPIError("error", None, orig)
            m.side_effect = error
            __test_basic_write(any_storage, blobs, digests)

        with patch("buildgrid.server.cas.storage.index.sql.SQLIndex._save_digests_to_index") as m:
            orig = psycopg2.errors.OutOfMemory()
            error = DBAPIError("error", None, orig)
            m.side_effect = error
            __test_basic_write(any_storage, blobs, digests)

    elif isinstance(any_storage, DiskStorage):
        with patch("os.makedirs") as m:
            error = OSError()
            error.errno = errno.ENOSPC
            m.side_effect = error
            __test_basic_write(any_storage, blobs, digests)

        with patch("os.link") as m:
            error = OSError()
            error.errno = errno.EFBIG
            m.side_effect = error
            __test_basic_write(any_storage, blobs, digests)


@pytest.mark.parametrize("blobs_digests", zip(BLOBS, BLOBS_DIGESTS))
def test_write_if_storage_errors_not_related_to_full(any_storage, blobs_digests):
    blobs, digests = blobs_digests

    def __test_basic_write(any_storage, blobs, digests):
        try:
            for blob, digest in zip(blobs, digests):
                write(any_storage, digest, blob)
        except StorageFullError:
            pytest.fail("StorageFullError should not have been raised")
        except Exception:
            pass
        else:
            pytest.fail("Exception expected")

    if isinstance(any_storage, S3Storage):
        with patch("buildgrid.server.s3.s3utils.put_objects") as m:
            response = {"Error": {"Code": "LimitExceededException"}}
            err = ClientError(response, "Error")

            def mock_put_objects(s3, objects):
                for s3object in objects:
                    s3object.error = err

            m.side_effect = mock_put_objects
            __test_basic_write(any_storage, blobs, digests)

    elif isinstance(any_storage, SQLIndex):
        with patch("buildgrid.server.cas.storage.index.sql.SQLIndex._save_digests_to_index") as m:
            # Invalid SQL Statement Name
            orig = psycopg2.errors.InvalidSqlStatementName()
            error = DBAPIError("error", None, orig)
            m.side_effect = error
            __test_basic_write(any_storage, blobs, digests)

    elif isinstance(any_storage, DiskStorage):
        with patch("os.makedirs") as m:
            error = OSError()
            # Permission Denied Error
            error.errno = errno.EACCES
            m.side_effect = error
            __test_basic_write(any_storage, blobs, digests)


@pytest.mark.parametrize("blobs_digests", zip(BLOBS, BLOBS_DIGESTS))
def test_deletes(any_storage, blobs_digests):
    """Test the functionality of deletes.

    Deleting a blob should cause has_blob to return False and
    get_blob to return None.
    """

    # RemoteStorage does not implement delete
    if isinstance(any_storage, RemoteStorage):
        return

    blobs, digests = blobs_digests

    for blob, digest in zip(blobs, digests):
        write(any_storage, digest, blob)

    for blob, digest in zip(blobs, digests):
        assert any_storage.has_blob(digest)
        assert any_storage.get_blob(digest).read() == blob

    first_digest, *_ = digests

    any_storage.delete_blob(first_digest)

    for blob, digest in zip(blobs, digests):
        if digest != first_digest:
            assert any_storage.has_blob(digest)
            assert any_storage.get_blob(digest).read() == blob
        else:
            assert not any_storage.has_blob(digest)
            assert any_storage.get_blob(digest) is None

    # There shouldn't be any issue with deleting a blob that isn't there
    missing_digest = remote_execution_pb2.Digest(
        hash=HASH(b"missing_blob").hexdigest(), size_bytes=len(b"missing_blob")
    )
    assert not any_storage.has_blob(missing_digest)
    assert any_storage.get_blob(missing_digest) is None
    any_storage.delete_blob(missing_digest)
    assert not any_storage.has_blob(missing_digest)
    assert any_storage.get_blob(missing_digest) is None


def test_bulk_deletes(any_storage):
    """Test the functionality of deletes.

    Deleting a blob should cause has_blob to return False and
    get_blob to return None.
    """

    # RemoteStorage does not implement delete
    if isinstance(any_storage, RemoteStorage):
        return

    permutations = list(itertools.permutations([b"a", b"b", b"c", b"d", b"e"]))
    blobs = [b"".join(x) for x in permutations]
    digests = [remote_execution_pb2.Digest(hash=HASH(blob).hexdigest(), size_bytes=len(blob)) for blob in blobs]

    results = any_storage.bulk_update_blobs(list(zip(digests, blobs)))
    assert all(status.code == code_pb2.OK for status in results)

    # Test all S3 bulk_delete code paths by deleting
    # digests with different corresponding bucket sizes
    deleted_blobs = [
        b"abecfd",  # bucket 4f
        b"abcdfe",
        b"bcadfe",  # bucket 98
        b"abcedf",
        b"adcfeb",
        b"defcba",  # bucket 99
        b"abcdef",
        b"bdecfa",
        b"cdfbea",  # bucket be (also contains b'ebcdaf')
        b"abfced",
        b"bdeacf",
        b"cbedfa",
        b"facdeb",
        b"febcda",  # bucket 3d
        b"abdfce",
        b"adefcb",
        b"becadf",
        b"bfdaec",
        b"dbeacf",
        b"dfaebc",  # bucket d9
        b"acdbef",
        b"bedacf",
        b"befacd",
        b"defbac",
        b"ecbdaf",
        b"fadceb",
        b"fdecab",
    ]  # bucket ac

    # Shuffle list to catch any bugs related to ordering
    shuffle(deleted_blobs)

    # Get digests to delete
    deleted_hashes = set([HASH(x).hexdigest() for x in deleted_blobs])
    deleted_digests = [digest for digest in digests if digest.hash in deleted_hashes]

    # digests not in storage
    missing_blobs = [b"wxyz", b"zyxw"]
    missing_digests = [
        remote_execution_pb2.Digest(hash=HASH(blob).hexdigest(), size_bytes=len(blob)) for blob in missing_blobs
    ]
    for missing_digest in missing_digests:
        assert not any_storage.has_blob(missing_digest)

    deleted_digests += missing_digests
    failed = any_storage.bulk_delete(deleted_digests)

    # bulk_delete returns a list of digests that failed to
    # be deleted. This list should only include actual failures
    # rather than blobs that were missing. We don't expect any
    # real failures, despite having missing blobs in the
    # delete request
    assert len(failed) == 0

    # Check that digests in storage were deleted
    for digest in digests:
        if digest.hash not in deleted_hashes:
            assert any_storage.has_blob(digest)
        else:
            assert not any_storage.has_blob(digest)

    # Verify the missing blobs are still missing
    for missing_digest in missing_digests:
        assert not any_storage.has_blob(missing_digest)


@pytest.mark.parametrize("blobs_digests", zip(BLOBS, BLOBS_DIGESTS))
def test_bulk_write_read(any_storage, blobs_digests):
    blobs, digests = blobs_digests

    missing_digests = any_storage.missing_blobs(digests)
    if isinstance(any_storage, RemoteStorage):
        assert len(missing_digests) == len(digests) - 1
    else:
        assert len(missing_digests) == len(digests)
    for digest in digests:
        if isinstance(any_storage, RemoteStorage) and digest == EMPTY_BLOB_DIGEST:
            assert digest not in missing_digests
        else:
            assert digest in missing_digests

    faulty_blobs = list(blobs)
    faulty_blobs[-1] = b"this-is-not-matching"

    results = any_storage.bulk_update_blobs(list(zip(digests, faulty_blobs)))
    assert len(results) == len(digests)
    for result, blob, digest in zip(results[:-1], faulty_blobs[:-1], digests[:-1]):
        assert result.code == 0
        assert any_storage.get_blob(digest).read() == blob
    assert results[-1].code != 0

    missing_digests = any_storage.missing_blobs(digests)
    assert len(missing_digests) == 1
    assert missing_digests[0] == digests[-1]

    blobmap = any_storage.bulk_read_blobs(digests)
    for blob, digest in zip(blobs[:-1], digests[:-1]):
        assert blobmap[digest.hash] == blob
    assert digests[-1].hash not in blobmap


@pytest.mark.parametrize("any_storage", STORAGE_TYPES + ["remote_without_bytestream"], indirect=["any_storage"])
@pytest.mark.parametrize("blobs_digests", zip(BLOBS, BLOBS_DIGESTS))
def test_duplicate_bulk_read(any_storage, blobs_digests):
    blobs, digests = blobs_digests

    # Duplicate the blobs and digests
    duplicate_digests = digests + digests

    any_storage.bulk_update_blobs(list(zip(digests, blobs)))

    blobmap = any_storage.bulk_read_blobs(duplicate_digests)
    assert len(blobmap) == len(blobs)
    for blob, digest in zip(blobs, digests):
        assert blobmap[digest.hash] == blob


@pytest.mark.parametrize("any_storage", STORAGE_TYPES + ["remote_without_bytestream"], indirect=["any_storage"])
@pytest.mark.parametrize("blobs_digests", zip(BLOBS, BLOBS_DIGESTS))
def test_repeated_bulk_write(any_storage, blobs_digests):
    blobs, digests = blobs_digests

    results = any_storage.bulk_update_blobs(list(zip(digests, blobs)))
    assert len(results) == len(digests)
    for result in results:
        assert result.code == 0

    results2 = any_storage.bulk_update_blobs(list(zip(digests, blobs)))
    assert len(results2) == len(digests)
    for result in results2:
        assert result.code == 0

    blobmap = any_storage.bulk_read_blobs(digests)
    assert len(blobmap) == len(blobs)
    for blob, digest in zip(blobs, digests):
        assert blobmap[digest.hash] == blob


@pytest.mark.parametrize("any_storage", STORAGE_TYPES + ["remote_without_bytestream"], indirect=["any_storage"])
@pytest.mark.parametrize("blobs_digests", zip(BLOBS, BLOBS_DIGESTS))
def test_repeated_bulk_write_invalid_and_new_digest(any_storage, blobs_digests):
    blobs, digests = blobs_digests

    results = any_storage.bulk_update_blobs(list(zip(digests, blobs)))
    assert len(results) == len(digests)
    for result in results:
        assert result.code == 0

    blobs2 = list(blobs)
    digests2 = list(digests)

    # Attempt to upload an already existing digest again
    # but with a non-matching blob
    bad_blob = b"this data does not match this data"
    blobs2[0] = bad_blob

    # Add a new blob which wasn't uploaded in the previous batch
    new_digest = create_digest(b"data does match")
    new_blob = b"data does match"
    blobs2.append(new_blob)
    digests2.append(new_digest)

    results2 = any_storage.bulk_update_blobs(list(zip(digests2, blobs2)))
    assert len(results2) == len(digests2)
    for result in results2:
        # The first digest was the invalid one, and should have a non-ok status
        if results2[0] == result:
            assert result.code != code_pb2.OK
        else:
            assert result.code == code_pb2.OK

    # Ensure all the valid blobs, including the newly uploaded one, exist in storage
    blobmap = any_storage.bulk_read_blobs(digests2)
    assert len(blobmap) == len(blobs2)
    for blob, digest in zip(blobs2, digests2):
        if blob == bad_blob:
            assert blob not in blobmap
        else:
            assert blobmap[digest.hash] == blob


@pytest.mark.parametrize("blobs_digests", zip(BLOBS, BLOBS_DIGESTS))
def test_nonexistent_read(any_storage, blobs_digests):
    _, digests = blobs_digests

    for digest in digests:
        if digest == EMPTY_BLOB_DIGEST and isinstance(any_storage, RemoteStorage):
            assert any_storage.get_blob(digest) is not None
        else:
            assert any_storage.get_blob(digest) is None

    # Testing a blob that exceeds the limit to be kept completely in memory:
    large_blob_digest = remote_execution_pb2.Digest(
        hash=HASH(b"").hexdigest(), size_bytes=MAX_IN_MEMORY_BLOB_SIZE_BYTES * 2
    )
    assert any_storage.get_blob(large_blob_digest) is None


def test_get_tree(any_storage):
    """Directory Structure:
    |--root
       |--subEmptyDir
       |--subParentDir
          |--subChildDir
          |--foo.txt
    """
    file_content = b"foo"
    file_digest = create_digest(file_content)
    empty_dir = remote_execution_pb2.Directory()
    empty_digest = create_digest(b"")
    parent_dir = remote_execution_pb2.Directory(
        directories=[remote_execution_pb2.DirectoryNode(name="child_dir", digest=empty_digest)],
        files=[remote_execution_pb2.FileNode(name="foo.txt", digest=file_digest)],
    )
    parent_digest = create_digest(parent_dir.SerializeToString())
    root_dir = remote_execution_pb2.Directory(
        directories=[
            remote_execution_pb2.DirectoryNode(name="empty_dir", digest=empty_digest),
            remote_execution_pb2.DirectoryNode(name="parent_dir", digest=parent_digest),
        ]
    )
    root_digest = create_digest(root_dir.SerializeToString())
    res = any_storage.bulk_update_blobs(
        [
            (empty_digest, b""),
            (parent_digest, parent_dir.SerializeToString()),
            (root_digest, root_dir.SerializeToString()),
            (file_digest, file_content),
        ]
    )
    assert len(res) == 4

    result = [d for d in any_storage.get_tree(root_digest)]
    assert len(result) == 4
    # GetTree isn't guaranteed to be ordered so neither are the yielded directories.
    expected_dirs = [root_dir, empty_dir, parent_dir, empty_dir]
    for dir in expected_dirs:
        assert dir in result
        result.remove(dir)


def test_get_tree_missing_digest(any_storage):
    """Directory Structure:
    |--root
       |--subEmptyDir
       |--subParentDir <- Missing in this test
          |--subChildDir
          |--foo.txt
    """
    file_content = b"foo"
    file_digest = create_digest(file_content)
    empty_dir = remote_execution_pb2.Directory()
    empty_digest = create_digest(b"")
    parent_dir = remote_execution_pb2.Directory(
        directories=[remote_execution_pb2.DirectoryNode(name="child_dir", digest=empty_digest)],
        files=[remote_execution_pb2.FileNode(name="foo.txt", digest=file_digest)],
    )
    parent_digest = create_digest(parent_dir.SerializeToString())
    root_dir = remote_execution_pb2.Directory(
        directories=[
            remote_execution_pb2.DirectoryNode(name="empty_dir", digest=empty_digest),
            remote_execution_pb2.DirectoryNode(name="parent_dir", digest=parent_digest),
        ]
    )
    root_digest = create_digest(root_dir.SerializeToString())
    res = any_storage.bulk_update_blobs(
        [
            (empty_digest, b""),
            (root_digest, root_dir.SerializeToString()),
            (file_digest, file_content),
        ]
    )
    assert len(res) == 3

    result = [d for d in any_storage.get_tree(root_digest)]
    assert len(result) == 2
    # GetTree isn't guaranteed to be ordered so neither are the yielded directories.
    expected_dirs = [root_dir, empty_dir]
    for dir in expected_dirs:
        assert dir in result
        result.remove(dir)

    # Use get_tree with raise_on_missing_subdir option and validate it does throw
    with pytest.raises(NotFoundError):
        result = [d for d in any_storage.get_tree(root_digest, raise_on_missing_subdir=True)]


def test_get_tree_missing_root(any_storage):
    # If the root digest is missing NotFoundError is thrown regardless
    # of the value of raise_on_missing_subdir
    with pytest.raises(NotFoundError):
        [d for d in any_storage.get_tree(create_digest(b"non-existing-digest"), raise_on_missing_subdir=False)]

    with pytest.raises(NotFoundError):
        [d for d in any_storage.get_tree(create_digest(b"non-existing-digest"), raise_on_missing_subdir=True)]


@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_lru_eviction(blobs_digests, moto_server):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, *_ = blobs
    digest1, digest2, digest3, *_ = digests

    lru = LRUMemoryCache(8)
    write(lru, digest1, blob1)
    write(lru, digest2, blob2)
    assert lru.has_blob(digest1)
    assert lru.has_blob(digest2)

    write(lru, digest3, blob3)
    # Check that the LRU evicted blob1 (it was written first)
    assert not lru.has_blob(digest1)
    assert lru.has_blob(digest2)
    assert lru.has_blob(digest3)

    assert lru.get_blob(digest2).read() == blob2
    write(lru, digest1, blob1)
    # Check that the LRU evicted blob3 (since we just read blob2)
    assert lru.has_blob(digest1)
    assert lru.has_blob(digest2)
    assert not lru.has_blob(digest3)

    write(lru, digest3, blob3)
    # Check that the LRU evicted blob1 (since we just checked blob3)
    assert not lru.has_blob(digest1)
    assert lru.has_blob(digest2)
    assert lru.has_blob(digest3)

    # Write the older blob (blob3) into the cache again,
    # and verify nothing is evicted.
    write(lru, digest3, blob3)
    assert not lru.has_blob(digest1)
    assert lru.has_blob(digest2)
    assert lru.has_blob(digest3)

    # deleting a blob and readding it shouldn't trigger eviction
    lru.delete_blob(digest3)
    write(lru, digest3, blob3)
    assert not lru.has_blob(digest1)
    assert lru.has_blob(digest2)
    assert lru.has_blob(digest3)


@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_with_cache(blobs_digests, moto_server):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, *_ = blobs
    digest1, digest2, digest3, *_ = digests

    cache = LRUMemoryCache(1024)
    fallback = LRUMemoryCache(1024)
    with_cache_storage = WithCacheStorage(cache, fallback)

    assert not with_cache_storage.has_blob(digest1)
    write(with_cache_storage, digest1, blob1)
    assert cache.has_blob(digest1)
    assert fallback.has_blob(digest1)
    assert with_cache_storage.get_blob(digest1).read() == blob1

    # Even if a blob is in cache, we still need to check if the fallback
    # has it.
    write(cache, digest2, blob2)
    assert not with_cache_storage.has_blob(digest2)
    write(fallback, digest2, blob2)
    assert with_cache_storage.has_blob(digest2)

    # When a blob is in the fallback but not the cache, reading it should
    # put it into the cache.
    write(fallback, digest3, blob3)
    assert with_cache_storage.get_blob(digest3).read() == blob3
    assert cache.has_blob(digest3)
    assert cache.get_blob(digest3).read() == blob3


@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_bulk_read_with_cache(blobs_digests, moto_server):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, *_ = blobs
    digest1, digest2, digest3, *_ = digests

    cache = LRUMemoryCache(1024)
    fallback = LRUMemoryCache(1024)
    with_cache_storage = WithCacheStorage(cache, fallback)

    write(with_cache_storage, digest1, blob1)
    write(cache, digest2, blob2)

    # When a blob is in the fallback but not the cache, bulk reading blobs
    # should put it into the cache.
    write(fallback, digest3, blob3)
    assert with_cache_storage.bulk_read_blobs(digests)
    assert cache.has_blob(digest3)
    assert cache.get_blob(digest3).read() == blob3


@pytest.mark.parametrize("blobs_digests", zip(BLOBS, BLOBS_DIGESTS))
@pytest.mark.parametrize("fallback", ["disk", "s3"])
def test_with_cache_deferred_writes(blobs_digests, fallback, moto_server: Moto):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, *_ = blobs
    digest1, digest2, digest3, *_ = digests

    cache = LRUMemoryCache(1024)

    def _test_deferred_writes(fallback_storage):
        with WithCacheStorage(cache, fallback_storage, defer_fallback_writes=True) as with_cache_storage:
            assert not with_cache_storage.has_blob(digest1)
            write(with_cache_storage, digest1, blob1)

            assert cache.has_blob(digest1)
            assert with_cache_storage.get_blob(digest1).read() == blob1

            # Check that the fallback eventually has the blob
            attempts = 0
            while attempts <= 10:
                attempts += 1
                try:
                    assert fallback_storage.has_blob(digest1)
                except:  # noqa: E722
                    if attempts < 10:
                        time.sleep(0.1)
                        continue
                    raise

            # With deferred writes, `has_blob` should return True if the blob is in
            # the cache layer without checking the fallback
            write(cache, digest2, blob2)
            assert with_cache_storage.has_blob(digest2)
            write(fallback_storage, digest2, blob2)
            assert with_cache_storage.has_blob(digest2)

            # When a blob is in the fallback but not the cache, reading it should
            # put it into the cache.
            write(fallback_storage, digest3, blob3)
            assert with_cache_storage.get_blob(digest3).read() == blob3
            assert cache.has_blob(digest3)
            assert cache.get_blob(digest3).read() == blob3

    if fallback == "disk":
        with tempfile.TemporaryDirectory() as path:
            _test_deferred_writes(DiskStorage(path))

    elif fallback == "s3":
        boto3.resource("s3", **moto_server.opts).create_bucket(Bucket=moto_server.bucket("testing"))
        _test_deferred_writes(S3Storage(moto_server.bucket("testing"), **moto_server.opts))


@pytest.mark.parametrize("blobs_digests", zip(BLOBS, BLOBS_DIGESTS))
@pytest.mark.parametrize("fallback", ["disk"])
def test_with_cache_deferred_writes_shuts_down_pool(blobs_digests, fallback):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, *_ = blobs
    digest1, digest2, digest3, *_ = digests

    cache = LRUMemoryCache(1024)

    def _test_deferred_writes(fallback_storage):
        with WithCacheStorage(cache, fallback_storage, defer_fallback_writes=True) as with_cache_storage:
            assert not with_cache_storage.has_blob(digest1)
            write(with_cache_storage, digest1, blob1)

            assert cache.has_blob(digest1)
            assert with_cache_storage.get_blob(digest1).read() == blob1

            # With deferred writes, `has_blob` should return True if the blob is in
            # the cache layer without checking the fallback
            write(cache, digest2, blob2)
            assert with_cache_storage.has_blob(digest2)
            write(fallback_storage, digest2, blob2)
            assert with_cache_storage.has_blob(digest2)

            # When a blob is in the fallback but not the cache, reading it should
            # put it into the cache.
            write(fallback_storage, digest3, blob3)
            assert with_cache_storage.get_blob(digest3).read() == blob3
            assert cache.has_blob(digest3)
            assert cache.get_blob(digest3).read() == blob3

    # DON'T ACTUALLY WRITE THE BLOBS to avoid flakiness
    # with the thread writing blob after the contextmanager exits
    with patch("buildgrid.server.cas.storage.with_cache.ContextThreadPoolExecutor.shutdown") as shutdown:
        with patch("buildgrid.server.cas.storage.with_cache.ContextThreadPoolExecutor.submit") as submit:
            with tempfile.TemporaryDirectory() as path:
                _test_deferred_writes(DiskStorage(path))
                # This should've been called once to write the blob
                submit.assert_called_once()
        shutdown.assert_called_once()


@pytest.mark.parametrize("blobs_digests", zip(BLOBS, BLOBS_DIGESTS))
def test_remote_timeout(blobs_digests):
    _, digests = blobs_digests

    with serve_cas(["testing"], delay=5) as server:
        with pytest.raises(TimeoutError):
            with RemoteStorage(server.remote, "testing", request_timeout=0.5) as storage:
                storage.get_blob(digests[0])


def test_s3_get_blob_timeout_by_size():
    obj = s3utils.S3Object("bucket", "hash/1234567")
    obj.filesize = 1234567
    s3 = mock.MagicMock()

    def mock_curl(objects, handler):
        for obj in objects:
            handler(obj)

    with (
        mock.patch("buildgrid.server.s3.s3utils._curl_multi_run") as mock_curl_run,
        mock.patch("buildgrid.server.s3.s3utils.pycurl.Curl") as mock_pycurl_cls,
    ):
        mock_curl_run.side_effect = mock_curl
        mock_pycurl = mock.MagicMock()
        mock_pycurl_cls.return_value = mock_pycurl

        s3utils.get_objects(s3, [obj], 0.01, 1)

        mock_pycurl.setopt.assert_called_with(pycurl.TIMEOUT, 12)


@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_sharding(blobs_digests):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, blob4, *_ = blobs
    digest1, digest2, digest3, digest4, *_ = digests

    storage_a = LRUMemoryCache(8192)
    storage_b = LRUMemoryCache(8192)
    sharded = ShardedStorage({"A": storage_a, "B": storage_b})

    # given shard names A and B the test blobs end up on A, B, B, A
    write(sharded, digest1, blob1)
    write(sharded, digest2, blob2)
    write(sharded, digest3, blob3)
    write(sharded, digest4, blob4)
    assert sharded.has_blob(digest1)
    assert sharded.has_blob(digest2)
    assert sharded.has_blob(digest3)
    assert sharded.has_blob(digest4)

    assert storage_a.has_blob(digest1)
    assert not storage_b.has_blob(digest1)

    assert not storage_a.has_blob(digest2)
    assert storage_b.has_blob(digest2)

    assert not storage_a.has_blob(digest3)
    assert storage_b.has_blob(digest3)

    assert storage_a.has_blob(digest4)
    assert not storage_b.has_blob(digest4)


@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_sharding_with_bulk_read_failures(blobs_digests):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, blob4, *_ = blobs
    digest1, digest2, digest3, digest4, *_ = digests

    storage_a = LRUMemoryCache(8192)

    class FailableStorage(LRUMemoryCache):
        def __init__(self):
            super().__init__(8192)

        def bulk_read_blobs(self, digests):
            return {}

        def get_blob(self, digest):
            return None

    storage_b = FailableStorage()
    with ShardedStorage({"A": storage_a, "B": storage_b}) as sharded:
        # given shard names A and B the test blobs end up on A, B, B, A
        write(sharded, digest1, blob1)
        write(sharded, digest2, blob2)
        write(sharded, digest3, blob3)
        write(sharded, digest4, blob4)

        results = sharded.bulk_read_blobs([digest1, digest2, digest3, digest4])
        assert results[digest1.hash] == blob1
        assert digest2.hash not in results
        assert digest3.hash not in results
        assert results[digest4.hash] == blob4


@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_sharding_with_bulk_update_failures(blobs_digests):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, blob4, *_ = blobs
    digest1, digest2, digest3, digest4, *_ = digests

    storage_a = LRUMemoryCache(8192)

    class FailableStorage(LRUMemoryCache):
        def __init__(self):
            super().__init__(8192)

        def bulk_update_blobs(self, blobs):
            results = []
            for _ in blobs:
                results.append(Status(code=code_pb2.UNKNOWN, message="failed"))
            return results

    storage_b = FailableStorage()
    with ShardedStorage({"A": storage_a, "B": storage_b}) as sharded:
        # given shard names A and B the test blobs end up on A, B, B, A
        results = sharded.bulk_update_blobs([(digest1, blob1), (digest2, blob2), (digest3, blob3), (digest4, blob4)])

        # results should come back in the order of the inputs regardless of sharding
        assert results[0].code == code_pb2.OK
        assert results[1].code == code_pb2.UNKNOWN
        assert results[2].code == code_pb2.UNKNOWN
        assert results[3].code == code_pb2.OK


@pytest.mark.parametrize("num_of_storages", [2, 3, 4])
@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_replicated_bulk_writes_to_all_storages(num_of_storages, blobs_digests):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, blob4, *_ = blobs
    digest1, digest2, digest3, digest4, *_ = digests
    storages = []
    for _ in range(num_of_storages):
        storages.append(LRUMemoryCache(8192))
    replicated_storage = ReplicatedStorage(storages)
    results = replicated_storage.bulk_update_blobs(
        [(digest1, blob1), (digest2, blob2), (digest3, blob3), (digest4, blob4)]
    )
    for result in results:
        assert result.code == 0
    for storage in storages:
        assert len(storage.missing_blobs([digest1, digest2, digest3, digest4])) == 0
        # Empty out the storage for the next run of the loop
        storage._storage.clear()


@pytest.mark.parametrize("num_of_storages", [2, 3, 4])
@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_replicated_bulk_writes_failure_to_some_storages(num_of_storages, blobs_digests):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, blob4, *_ = blobs
    digest1, digest2, digest3, digest4, *_ = digests

    class FailableStorage(LRUMemoryCache):
        def __init__(self):
            super().__init__(8192)

        def bulk_update_blobs(self, blobs):
            results = []
            for _ in blobs:
                results.append(Status(code=code_pb2.UNKNOWN, message="failed"))
            return results

        def commit_write(self, digest, write_session):
            return None

    storages = [FailableStorage()]
    for _ in range(num_of_storages - 1):
        storages.append(LRUMemoryCache(8192))
    for storage_permutations in itertools.permutations(storages):
        replicated_storage = ReplicatedStorage(storage_permutations)
        results = replicated_storage.bulk_update_blobs(
            [(digest1, blob1), (digest2, blob2), (digest3, blob3), (digest4, blob4)]
        )
        for result in results:
            assert result.code == 0
        for storage in storage_permutations:
            if not isinstance(storage, FailableStorage):
                assert len(storage.missing_blobs([digest1, digest2, digest3, digest4])) == 0
            # Empty out the storage for the next run of the loop
            storage._storage.clear()


@pytest.mark.parametrize("num_of_storages", [2, 3, 4])
@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_replicated_bytestream_write_to_all_storages(num_of_storages, blobs_digests):
    blobs, digests = blobs_digests
    blob0, digest0 = blobs[0], digests[0]
    storages = []
    for _ in range(num_of_storages):
        storages.append(LRUMemoryCache(8192))
    replicated_storage = ReplicatedStorage(storages)
    with create_write_session(digest0) as session:
        session.write(blob0)
        replicated_storage.commit_write(digest0, session)
    for storage in storages:
        assert storage.has_blob(digest0)
        # Empty out the storage for the next run of the loop
        storage._storage.clear()


@pytest.mark.parametrize("num_of_storages", [2, 3, 4])
@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_replicated_bytestream_write_failure_to_some_storages(num_of_storages, blobs_digests):
    blobs, digests = blobs_digests
    blob0, digest0 = blobs[0], digests[0]

    class FailableStorage(LRUMemoryCache):
        def __init__(self):
            super().__init__(8192)

        def commit_write(self, digest, write_session):
            raise StorageFullError("Can't write to storage")

    storages = [FailableStorage()]
    for _ in range(num_of_storages - 1):
        storages.append(LRUMemoryCache(8192))
    for storage_permutations in itertools.permutations(storages):
        replicated_storage = ReplicatedStorage(storage_permutations)
        with create_write_session(digest0) as session:
            session.write(blob0)
            replicated_storage.commit_write(digest0, session)
        for storage in storage_permutations:
            if not isinstance(storage, FailableStorage):
                assert storage.has_blob(digest0)
            # Empty out the storage for the next run of the loop
            storage._storage.clear()


@pytest.mark.parametrize("num_of_storages", [2, 3, 4])
@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_replicated_bytestream_write_failure_for_all_storages(num_of_storages, blobs_digests):
    blobs, digests = blobs_digests
    blob0, digest0 = blobs[0], digests[0]

    class FailableStorage(LRUMemoryCache):
        def __init__(self):
            super().__init__(8192)

        def commit_write(self, digest, write_session):
            raise IOError("Failed to write")

    storages = []
    for _ in range(num_of_storages):
        storages.append(FailableStorage())
    replicated_storage = ReplicatedStorage(storages)
    with create_write_session(digest0) as session:
        session.write(blob0)
        with pytest.raises(RuntimeError):
            replicated_storage.commit_write(digest0, session)


@pytest.mark.parametrize("num_of_storages", [2, 3, 4])
@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_replicated_find_missing_blobs(num_of_storages, blobs_digests):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, blob4, *_ = blobs
    digest1, digest2, digest3, digest4, *_ = digests
    storages = []
    for _ in range(num_of_storages):
        storages.append(LRUMemoryCache(8192))
    replicated_storage = ReplicatedStorage(storages)
    for storage_permutations in itertools.permutations(storages):
        write_storage = storage_permutations[0]
        results = write_storage.bulk_update_blobs(
            [(digest1, blob1), (digest2, blob2), (digest3, blob3), (digest4, blob4)]
        )
        for result in results:
            assert result.code == 0
        assert len(write_storage.missing_blobs([digest1, digest2, digest3, digest4])) == 0
        for storage in storage_permutations[1:]:
            assert len(storage.missing_blobs([digest1, digest2, digest3, digest4])) == 4

        # FindMissingBlobs should report that all the blobs exist because they are in one of the storages
        assert len(replicated_storage.missing_blobs([digest1, digest2, digest3, digest4])) == 0

        # The storages which were previously missing the blobs are still missing
        for storage in storage_permutations[1:]:
            assert len(storage.missing_blobs([digest1, digest2, digest3, digest4])) == 4

        for storage in storage_permutations:
            # Empty out the storage for the next run of the loop
            storage._storage.clear()


@pytest.mark.parametrize("num_of_storages", [2, 3, 4])
@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_replicated_find_missing_blobs_replication_queue(num_of_storages, blobs_digests):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, blob4, *_ = blobs
    digest1, digest2, digest3, digest4, *_ = digests
    storages = []
    for _ in range(num_of_storages):
        storages.append(LRUMemoryCache(8192))
    replicated_storage = ReplicatedStorage(storages, replication_queue_size=10)
    # Start the replication thread
    replicated_storage.start()
    for storage_permutations in itertools.permutations(storages):
        write_storage = storage_permutations[0]
        results = write_storage.bulk_update_blobs(
            [(digest1, blob1), (digest2, blob2), (digest3, blob3), (digest4, blob4)]
        )
        for result in results:
            assert result.code == 0
        assert len(write_storage.missing_blobs([digest1, digest2, digest3, digest4])) == 0
        for storage in storage_permutations[1:]:
            assert len(storage.missing_blobs([digest1, digest2, digest3, digest4])) == 4

        # FindMissingBlobs should report that all the blobs exist because they are in one of the storages
        assert len(replicated_storage.missing_blobs([digest1, digest2, digest3, digest4])) == 0

        # Give the replication thread time to do the replication (with a 60s upper bound)
        iterations = 0
        while iterations < 120 and replicated_storage._replica_queue.qsize() > 0:
            time.sleep(0.5)
            iterations += 1
        if iterations >= 120:
            pytest.fail(
                f"Replication queue still has {replicated_storage._replica_queue.qsize()} items after hitting timeout"
            )
        # Replication task was picked up, wait for it to be completed
        replicated_storage._replica_queue.join()

        # The storages which were previously missing blobs had them successfully replicated
        for storage in storage_permutations[1:]:
            assert len(storage.missing_blobs([digest1, digest2, digest3, digest4])) == 0

        for storage in storage_permutations:
            # Empty out the storage for the next run of the loop
            storage._storage.clear()


@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_replicated_find_missing_blobs_full_queue(blobs_digests):
    # Write the blobs to only one storage and call FMB, which should result
    # in two elements being placed on the replication queue. Since the queue
    # size is capped one of the two replications should be dropped
    blobs, digests = blobs_digests
    blob1, blob2, blob3, blob4, *_ = blobs
    digest1, digest2, digest3, digest4, *_ = digests
    storages = [LRUMemoryCache(8192), LRUMemoryCache(8192), LRUMemoryCache(8192)]

    # Intentionally not starting the replication thread
    replicated_storage = ReplicatedStorage(storages, replication_queue_size=1)

    write_storage = storages[0]
    results = write_storage.bulk_update_blobs([(digest1, blob1), (digest2, blob2), (digest3, blob3), (digest4, blob4)])
    for result in results:
        assert result.code == 0
    assert len(write_storage.missing_blobs([digest1, digest2, digest3, digest4])) == 0
    for storage in storages[1:]:
        assert len(storage.missing_blobs([digest1, digest2, digest3, digest4])) == 4

    # Call FMB on the first two digests which should fill up the replication queue
    assert len(replicated_storage.missing_blobs([digest1, digest2])) == 0
    assert replicated_storage._replica_queue.full()

    # Call FMB on the remaining  digests which should succeed even though the queue is still full
    assert len(replicated_storage.missing_blobs([digest3, digest4])) == 0
    assert replicated_storage._replica_queue.full()

    # Now start the replication thread and verify one of the storages gets blobs written to it
    replicated_storage.start()
    # Give the replication thread time to do the replication (with a 60s upper bound)
    iterations = 0
    while iterations < 120 and replicated_storage._replica_queue.qsize() > 0:
        time.sleep(0.5)
        iterations += 1
    if iterations >= 120:
        pytest.fail(
            f"Replication queue still has {replicated_storage._replica_queue.qsize()} items after hitting timeout"
        )
    # Replication task was picked up, wait for it to be completed
    replicated_storage._replica_queue.join()

    # Both storages should have digest1 and digest2, but not the rest
    for storage in storages[1:]:
        assert len(storage.missing_blobs([digest1, digest2])) == 0
        assert len(storage.missing_blobs([digest3, digest4])) == 2


@pytest.mark.parametrize("num_of_storages", [2, 3, 4])
@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_replicated_batch_read_replicates_blobs(num_of_storages, blobs_digests):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, blob4, *_ = blobs
    digest1, digest2, digest3, digest4, *_ = digests
    storages = []
    for _ in range(num_of_storages):
        storages.append(LRUMemoryCache(8192))
    replicated_storage = ReplicatedStorage(storages)
    for storage_permutations in itertools.permutations(storages):
        write_storage = storage_permutations[0]
        results = write_storage.bulk_update_blobs(
            [(digest1, blob1), (digest2, blob2), (digest3, blob3), (digest4, blob4)]
        )
        for result in results:
            assert result.code == 0
        assert len(write_storage.missing_blobs([digest1, digest2, digest3, digest4])) == 0
        for storage in storage_permutations[1:]:
            assert len(storage.missing_blobs([digest1, digest2, digest3, digest4])) == 4

        # Read from replicated_storage and verify that the data is now in all storages
        results = replicated_storage.bulk_read_blobs([digest1, digest2, digest3, digest4])
        for storage in storage_permutations:
            assert len(storage.missing_blobs([digest1, digest2, digest3, digest4])) == 0
            # Empty out the storage for the next run of the loop
            storage._storage.clear()


@pytest.mark.parametrize("num_of_storages", [2, 3, 4])
@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_replicated_batch_read_some_replication_failures(num_of_storages, blobs_digests):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, blob4, *_ = blobs
    digest1, digest2, digest3, digest4, *_ = digests

    class FailableStorage(LRUMemoryCache):
        def __init__(self):
            super().__init__(8192)

        def bulk_update_blobs(self, blobs):
            results = []
            for _ in blobs:
                results.append(Status(code=code_pb2.UNKNOWN, message="failed"))
            return results

        def commit_write(self, digest, write_session):
            raise IOError("Failed to write")

    storages = [FailableStorage()]
    for _ in range(num_of_storages - 1):
        storages.append(LRUMemoryCache(8192))
    # Ordering shouldn't matter when it comes to what storage is erroring
    # and what storage data is initially written to.
    for storage_permutations in itertools.permutations(storages):
        replicated_storage = ReplicatedStorage(storage_permutations)
        for write_storage in storage_permutations:
            if isinstance(write_storage, FailableStorage):
                continue
            results = write_storage.bulk_update_blobs(
                [(digest1, blob1), (digest2, blob2), (digest3, blob3), (digest4, blob4)]
            )
            for result in results:
                assert result.code == 0
            assert len(write_storage.missing_blobs([digest1, digest2, digest3, digest4])) == 0
            for storage in storage_permutations:
                if storage is not write_storage:
                    assert len(storage.missing_blobs([digest1, digest2, digest3, digest4])) == 4

            # Read from replicated_storage and verify that the data is now in all storages
            # other than the FailableStorage
            results = replicated_storage.bulk_read_blobs([digest1, digest2, digest3, digest4])
            for storage in storage_permutations:
                if not isinstance(storage, FailableStorage):
                    assert len(storage.missing_blobs([digest1, digest2, digest3, digest4])) == 0
                # Empty out the storage for the next run of the loop
                storage._storage.clear()


@pytest.mark.parametrize("num_of_storages", [2, 3, 4])
@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_replicated_bytestream_read_replicates_blobs(num_of_storages, blobs_digests):
    blobs, digests = blobs_digests
    blob0, digest0 = blobs[0], digests[0]
    storages = []
    for _ in range(num_of_storages):
        storages.append(LRUMemoryCache(8192))
    replicated_storage = ReplicatedStorage(storages)
    for storage_permutations in itertools.permutations(storages):
        write_storage = storage_permutations[0]

        with create_write_session(digest0) as session:
            session.write(blob0)
            write_storage.commit_write(digest0, session)
        assert write_storage.get_blob(digest0).read() == blob0
        for storage in storage_permutations[1:]:
            assert storage.get_blob(digest0) is None

        assert replicated_storage.get_blob(digest0).read() == blob0
        for storage in storage_permutations:
            assert storage.get_blob(digest0).read() == blob0
            # Empty out the storage for the next run of the loop
            storage._storage.clear()


@pytest.mark.parametrize("blobs_digests", [(BLOBS[0], BLOBS_DIGESTS[0])])
def test_sql_storage_sorted_bulk_insert(blobs_digests):
    blobs, digests = blobs_digests
    blob1, blob2, blob3, blob4, *_ = blobs
    digest1, digest2, digest3, digest4, *_ = digests
    with tempfile.NamedTemporaryFile() as db:
        sql_provider = SqlProvider(connection_string=f"sqlite:///{db.name}", automigrate=True)
        with patch("buildgrid.server.cas.storage.sql.SQLStorage._sqlite_bulk_insert") as mock_insert:
            sql_storage = SQLStorage(sql_provider=sql_provider)
            sql_storage.bulk_update_blobs([(digest1, blob1), (digest2, blob2), (digest3, blob3), (digest4, blob4)])
            assert mock_insert.called
            call1 = mock_insert.call_args
            mock_insert.reset_mock()

            # Call again in reverse order and verify the arguments to _bolk_insert are passed in the same order
            sql_storage.bulk_update_blobs([(digest4, blob4), (digest3, blob3), (digest2, blob2), (digest1, blob1)])
            assert mock_insert.called
            call2 = mock_insert.call_args
            assert call1 == call2
