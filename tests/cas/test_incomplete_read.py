import os
from tempfile import TemporaryDirectory

import grpc
import pytest

from buildgrid._protos.google.bytestream.bytestream_pb2 import ReadRequest
from buildgrid._protos.google.devtools.remoteworkers.v1test2.command_pb2 import Digest
from buildgrid.server.cas.instance import ByteStreamInstance
from buildgrid.server.cas.service import ByteStreamService
from buildgrid.server.cas.storage.disk import DiskStorage
from buildgrid.server.settings import HASH
from tests.utils.mocks import mock_context


def test_incomplete_read():
    """
    Place a blob in cas which has missing data in it. In this scenario, attempting to
    read the blob should result in a DATA_LOSS.
    """

    with TemporaryDirectory() as tmpdir:
        storage = DiskStorage(tmpdir)
        service = ByteStreamService()
        service.add_instance("", ByteStreamInstance(storage))

        data = b"test1234"
        digest = Digest(hash=HASH(data).hexdigest(), size_bytes=len(data))

        object_path = storage._get_object_path(digest)
        os.makedirs(os.path.dirname(object_path), exist_ok=True)
        with open(storage._get_object_path(digest), "wb") as f:
            f.write(data[: len(data) // 2])

        resource_name = f"blobs/{digest.hash}/{digest.size_bytes}"

        context = mock_context()
        with pytest.raises(Exception):
            for _ in service.Read(ReadRequest(resource_name=resource_name), context):
                pass

        assert context.code() == grpc.StatusCode.DATA_LOSS
