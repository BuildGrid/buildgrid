# Copyright (C) 2024 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import tempfile
from unittest.mock import patch

import pytest

from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Digest
from buildgrid.server.cas.storage.index.index_abc import IndexABC
from buildgrid.server.cas.storage.index.redis import RedisIndex
from buildgrid.server.cas.storage.index.sql import SQLIndex
from buildgrid.server.cas.storage.sql import SQLStorage
from buildgrid.server.cas.storage.storage_abc import StorageABC, create_write_session
from buildgrid.server.cleanup.janitor.config import RedisConfig, SQLStorageConfig
from buildgrid.server.cleanup.janitor.index import RedisIndexLookup, SqlIndexLookup
from buildgrid.server.cleanup.janitor.sql import SQLJanitor, get_sha256_buckets
from buildgrid.server.settings import HASH
from buildgrid.server.sql.provider import SqlProvider
from tests.utils.fixtures import ThreadSafeFakeRedis, pg_schema_provider_pair

JANITOR_INDEX_TYPES = ["redis", "sql"]


@pytest.fixture
def pg_schema_pair(postgres):
    with pg_schema_provider_pair(postgres) as pair:
        yield pair


@pytest.fixture(params=zip(JANITOR_INDEX_TYPES))
def janitor(request, pg_schema_pair):
    sql_provider, sql_ro_provider = pg_schema_pair
    sql_storage_config = SQLStorageConfig(
        sql=sql_provider,
        sql_ro=sql_ro_provider,
        sleep_interval=1,
        batch_size=4,
    )

    if request.param[0] == "sql":
        with tempfile.NamedTemporaryFile() as db:
            connection_string = f"sqlite:///{db.name}"

            # Make a provider here to migrate the database
            SqlProvider(automigrate=True, connection_string=connection_string)
            index = SqlIndexLookup(connection_string)
            sqlJanitor = SQLJanitor(sql_storage_config, index)
            yield sqlJanitor
    else:
        with patch("buildgrid.server.redis.provider.redis.Redis", ThreadSafeFakeRedis):
            index = RedisIndexLookup(
                RedisConfig(host="localhost", port=8080, db=0, index_prefix="A:", key_batch_size=1000)
            )
            sqlJanitor = SQLJanitor(sql_storage_config, index)
            yield sqlJanitor


@pytest.fixture
def storage(pg_schema_pair) -> SQLStorage:
    sql_provider, _ = pg_schema_pair
    return SQLStorage(
        sql_provider=sql_provider,
    )


@pytest.fixture
def index(janitor: SQLJanitor, storage: SQLStorage) -> IndexABC:
    if isinstance(janitor._index, SqlIndexLookup):
        return SQLIndex(janitor._index._sql, storage)

    elif isinstance(janitor._index, RedisIndexLookup):
        return RedisIndex(janitor._index._redis, storage)

    else:
        raise ValueError("Unexpected Janitor type")


def create_test_blob(storage: StorageABC, data: str) -> Digest:
    blob = data.encode("utf-8")
    hash = HASH(blob).hexdigest()
    size_bytes = len(blob)
    digest = Digest(hash=hash, size_bytes=size_bytes)

    with create_write_session(digest) as writer:
        writer.write(blob)
        storage.commit_write(digest, writer)

    return digest


@pytest.mark.parametrize(
    "data, expected_deleted",
    [
        (["hello", "world"], 2),
        (["foo"], 1),
        ([], 0),
    ],
)
def test_delete_digests(janitor: SQLJanitor, storage: SQLStorage, data: list[str], expected_deleted: int):
    digests = set()
    for item in data:
        digest = create_test_blob(storage, item)
        digests.add(digest.hash)
    for digest in digests:
        assert storage.has_blob(Digest(hash=digest, size_bytes=len(digest)))

    deleted_count = janitor.delete_digests(digests)

    assert deleted_count == expected_deleted
    for digest in digests:
        assert not storage.has_blob(Digest(hash=digest, size_bytes=len(digest)))


def test_cleanup_single_digest(janitor: SQLJanitor, storage: SQLStorage):
    data = "hello"
    digest = create_test_blob(storage, data)
    assert storage.has_blob(digest)

    # Determine the bucket based on the first two letters of the digest hash
    bucket_boundaries = get_sha256_buckets()
    bucket = next(bucket for bucket in bucket_boundaries if bucket[0] <= digest.hash[:2] < bucket[1])

    janitor.cleanup_bucket(bucket)

    assert not storage.has_blob(digest)


@pytest.mark.parametrize(
    "data",
    [
        ["hello", "world"],
        ["foo", "bar", "baz"],
        ["test1", "test2", "test3", "test4"],
    ],
)
def test_cleanup_buckets_all_deleted(janitor: SQLJanitor, storage: SQLStorage, data: list[str]):
    bucket_boundaries = get_sha256_buckets()

    digests = set()
    for item in data:
        digest = create_test_blob(storage, item)
        digests.add(digest.hash)
    for digest in digests:
        assert storage.has_blob(Digest(hash=digest, size_bytes=len(digest)))

    for bucket in bucket_boundaries:
        janitor.cleanup_bucket(bucket)

    for digest in digests:
        assert not storage.has_blob(Digest(hash=digest, size_bytes=len(digest)))


@pytest.mark.parametrize(
    "storage_data, index_data",
    [
        (["hello", "world"], ["foo"]),
        (["foo", "bar"], ["baz", "qux"]),
        (["test1", "test2"], ["test3", "test4"]),
    ],
)
def test_cleanup_buckets_partial_deleted(
    janitor: SQLJanitor, storage: SQLStorage, index: IndexABC, storage_data: list[str], index_data: list[str]
):
    storage_digests = set()
    index_digests = set()

    for item in storage_data:
        digest = create_test_blob(storage, item)
        storage_digests.add(digest.hash)

    for item in index_data:
        digest = create_test_blob(index, item)
        index_digests.add(digest.hash)

    all_digests = storage_digests.union(index_digests)
    for digest in all_digests:
        assert storage.has_blob(Digest(hash=digest, size_bytes=len(digest)))

    bucket_boundaries = get_sha256_buckets()
    for bucket in bucket_boundaries:
        janitor.cleanup_bucket(bucket)

    for digest in storage_digests:
        assert not storage.has_blob(Digest(hash=digest, size_bytes=len(digest)))

    for digest in index_digests:
        assert storage.has_blob(Digest(hash=digest, size_bytes=len(digest)))


@pytest.mark.parametrize(
    "data, first_blob_hash_prefix",
    [
        (["a", "b", "c", "d"], "ca"),  # "a" hashes to "ca..."
        (["hnihr", "b", "c", "d"], "ff"),  # "hnihr" hashes to "ff..."
    ],
)
def test_cleanup_specific_bucket(
    janitor: SQLJanitor, storage: SQLStorage, data: list[str], first_blob_hash_prefix: str
):
    digests = set()
    for item in data:
        digest = create_test_blob(storage, item)
        digests.add(digest.hash)
    for digest in digests:
        assert storage.has_blob(Digest(hash=digest, size_bytes=len(digest)))

    # Determine the bucket based on the first two letters of the first blob's digest hash
    bucket_boundaries = get_sha256_buckets()
    bucket = next(bucket for bucket in bucket_boundaries if bucket[0] <= first_blob_hash_prefix < bucket[1])

    janitor.cleanup_bucket(bucket)

    # Verify that only the digests matching the bucket's range have been cleaned up
    for digest in digests:
        if bucket[0] <= digest[:2] < bucket[1]:
            assert not storage.has_blob(Digest(hash=digest, size_bytes=len(digest)))
        else:
            assert storage.has_blob(Digest(hash=digest, size_bytes=len(digest)))
