# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import io
import os
import shutil
import tempfile
from copy import deepcopy
from urllib.parse import urlparse

import grpc
import pytest

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid.server.client.cas import download, upload
from buildgrid.server.utils.digests import create_digest

from ..utils.cas import serve_cas

SYMLINK_TEST_DIR = "symlink_example_dir"
SYMLINK_EXAMPLES = [
    ("../hello", "symlink_hello"),
    ("/dir_does_not_exist", "symlink_absolute"),
    ("../recursive_test", "recursive_test/loop_into_parent"),
]

INSTANCES = ["", "instance"]
BLOBS = [(b"",), (b"test-string",), (b"test", b"string"), (b"duplicate_blob", b"duplicate_blob")]
MESSAGES = [
    (remote_execution_pb2.Directory(),),
    (remote_execution_pb2.SymlinkNode(name="name", target="target"),),
    (remote_execution_pb2.Action(do_not_cache=True), remote_execution_pb2.ActionResult(exit_code=12)),
    (remote_execution_pb2.Action(do_not_cache=True), remote_execution_pb2.Action(do_not_cache=True)),
]
DATA_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data")
FILES = [
    ("void",),
    ("hello.cc",),
    ("symlink-example.h",),
    (
        os.path.join("hello", "hello.c"),
        os.path.join("hello", "hello.h"),
        os.path.join("hello", "hello.sh"),
    ),
    (os.path.join("hello", "docs", "reference", "api.xml"),),
]
FOLDERS = [
    ("",),
    (os.path.join("hello"),),
    (os.path.join("hello", "docs"),),
    (os.path.join("hello", "utils"),),
    (SYMLINK_TEST_DIR,),
    (os.path.join("hello", "docs", "reference"),),
]
DIRECTORIES = [("",), ("hello",)]
NUM_FAILURES = [0, 1]


@pytest.fixture(autouse=True)
def data_dir():
    with tempfile.TemporaryDirectory() as dir:
        data_dir = os.path.join(dir, "data")
        symlink_test_path = os.path.join(data_dir, SYMLINK_TEST_DIR)
        shutil.copytree(DATA_DIR, data_dir)
        try:
            os.mkdir(symlink_test_path)
        except FileExistsError:
            pass
        for src, dest in SYMLINK_EXAMPLES:
            abs_dest = os.path.join(symlink_test_path, dest)
            parent_dir = abs_dest.rfind("/")
            if parent_dir != -1:
                try:
                    os.makedirs(abs_dest[:parent_dir])
                except FileExistsError:
                    pass
            os.symlink(src, abs_dest)
        yield data_dir


@pytest.mark.parametrize("blobs", BLOBS)
@pytest.mark.parametrize("instance", INSTANCES)
@pytest.mark.parametrize("num_failures", NUM_FAILURES)
def test_upload_blob(instance, blobs, num_failures):
    def __test_upload_blob(remote, instance, blobs):
        # Open a channel to the remote CAS server:
        remote = urlparse(remote)
        remote = f"{remote.hostname}:{remote.port}"
        channel = grpc.insecure_channel(remote)

        digests = []
        with upload(channel, instance, retries=5, should_backoff=False) as uploader:
            if len(blobs) > 1:
                for blob in blobs:
                    digest = uploader.put_blob(io.BytesIO(blob), queue=True)
                    digests.append(digest.SerializeToString())
            else:
                digest = uploader.put_blob(io.BytesIO(blobs[0]), queue=False)
                digests.append(digest.SerializeToString())

        return digests

    with serve_cas([instance], num_failures) as server:
        digests = __test_upload_blob(server.remote, instance, blobs)
        assert digests is not None

        for blob, digest_blob in zip(blobs, digests):
            digest = remote_execution_pb2.Digest()
            digest.ParseFromString(digest_blob)

            assert server.has(digest)
            assert server.compare_blobs(digest, blob)


@pytest.mark.parametrize("messages", MESSAGES)
@pytest.mark.parametrize("instance", INSTANCES)
@pytest.mark.parametrize("num_failures", NUM_FAILURES)
def test_upload_message(instance, messages, num_failures):
    def __test_upload_message(remote, instance, messages):
        # Open a channel to the remote CAS server:
        remote = urlparse(remote)
        remote = f"{remote.hostname}:{remote.port}"
        channel = grpc.insecure_channel(remote)

        digests = []
        with upload(channel, instance, retries=5, should_backoff=False) as uploader:
            if len(messages) > 1:
                for message in messages:
                    digest = uploader.put_message(message, queue=True)
                    digests.append(digest.SerializeToString())
            else:
                digest = uploader.put_message(messages[0], queue=False)
                digests.append(digest.SerializeToString())

        return digests

    with serve_cas([instance], num_failures) as server:
        digests = __test_upload_message(server.remote, instance, messages)
        assert digests is not None

        for message, digest_blob in zip(messages, digests):
            digest = remote_execution_pb2.Digest()
            digest.ParseFromString(digest_blob)

            assert server.has(digest)
            assert server.compare_messages(digest, message)


@pytest.mark.parametrize("file_paths", FILES)
@pytest.mark.parametrize("instance", INSTANCES)
@pytest.mark.parametrize("num_failures", NUM_FAILURES)
def test_upload_file(instance, file_paths, num_failures, data_dir):
    file_paths = [os.path.join(data_dir, path) for path in file_paths]

    def __test_upload_file(remote, instance, file_paths):
        # Open a channel to the remote CAS server:
        remote = urlparse(remote)
        remote = f"{remote.hostname}:{remote.port}"
        channel = grpc.insecure_channel(remote)

        digests = []
        with upload(channel, instance, retries=5, should_backoff=False) as uploader:
            if len(file_paths) > 1:
                for file_path in file_paths:
                    digest = uploader.upload_file(file_path, queue=True)
                    digests.append(digest.SerializeToString())
            else:
                digest = uploader.upload_file(file_paths[0], queue=False)
                digests.append(digest.SerializeToString())

        return digests

    with serve_cas([instance], num_failures) as server:
        digests = __test_upload_file(server.remote, instance, file_paths)
        assert digests is not None

        for file_path, digest_blob in zip(file_paths, digests):
            digest = remote_execution_pb2.Digest()
            digest.ParseFromString(digest_blob)

            assert server.has(digest)
            assert server.compare_files(digest, file_path)


@pytest.mark.parametrize("directory_paths", DIRECTORIES)
@pytest.mark.parametrize("instance", INSTANCES)
@pytest.mark.parametrize("num_failures", NUM_FAILURES)
def test_upload_directory(instance, directory_paths, num_failures, data_dir):
    directory_paths = [os.path.join(data_dir, path) for path in directory_paths]

    def __test_upload_directory(remote, instance, directory_paths):
        # Open a channel to the remote CAS server:
        remote = urlparse(remote)
        remote = f"{remote.hostname}:{remote.port}"
        channel = grpc.insecure_channel(remote)

        digests = []
        with upload(channel, instance, retries=5, should_backoff=False) as uploader:
            if len(directory_paths) > 1:
                for directory_path in directory_paths:
                    digest = uploader.upload_directory(directory_path, queue=True)
                    digests.append(digest.SerializeToString())
            else:
                digest = uploader.upload_directory(directory_paths[0], queue=False)
                digests.append(digest.SerializeToString())

        return digests

    with serve_cas([instance], num_failures) as server:
        digests = __test_upload_directory(server.remote, instance, directory_paths)
        assert digests is not None

        for directory_path, digest_blob in zip(directory_paths, digests):
            digest = remote_execution_pb2.Digest()
            digest.ParseFromString(digest_blob)

            assert server.compare_directories(digest, directory_path)


@pytest.mark.parametrize("directory_paths", DIRECTORIES)
@pytest.mark.parametrize("instance", INSTANCES)
@pytest.mark.parametrize("num_failures", NUM_FAILURES)
def test_upload_tree(instance, directory_paths, num_failures, data_dir):
    directory_paths = [os.path.join(data_dir, path) for path in directory_paths]

    def __test_upload_tree(remote, instance, directory_paths):
        # Open a channel to the remote CAS server:
        remote = urlparse(remote)
        remote = f"{remote.hostname}:{remote.port}"
        channel = grpc.insecure_channel(remote)

        digests = []
        with upload(channel, instance, retries=5, should_backoff=False) as uploader:
            if len(directory_paths) > 1:
                for directory_path in directory_paths:
                    digest = uploader.upload_tree(directory_path, queue=True)
                    digests.append(digest.SerializeToString())
            else:
                digest = uploader.upload_tree(directory_paths[0], queue=False)
                digests.append(digest.SerializeToString())

        return digests

    with serve_cas([instance], num_failures) as server:
        digests = __test_upload_tree(server.remote, instance, directory_paths)
        assert digests is not None

        for directory_path, digest_blob in zip(directory_paths, digests):
            digest = remote_execution_pb2.Digest()
            digest.ParseFromString(digest_blob)

            assert server.has(digest)

            tree = remote_execution_pb2.Tree()
            tree.ParseFromString(server.get(digest))

            directory_digest = create_digest(tree.root.SerializeToString())

            assert server.compare_directories(directory_digest, directory_path)


@pytest.mark.parametrize("blobs", BLOBS)
@pytest.mark.parametrize("instance", INSTANCES)
@pytest.mark.parametrize("num_failures", NUM_FAILURES)
def test_download_blob(instance, blobs, num_failures):
    def __test_download_blob(remote, instance, digests):
        # Open a channel to the remote CAS server:
        remote = urlparse(remote)
        remote = f"{remote.hostname}:{remote.port}"
        channel = grpc.insecure_channel(remote)

        blobs = []
        with download(channel, instance, retries=5, should_backoff=False) as downloader:
            if len(digests) > 1:
                blobs.extend(downloader.get_blobs(digests))
            else:
                blobs.append(downloader.get_blob(digests[0]))

        return blobs

    with serve_cas([instance], num_failures) as server:
        digests = []
        blob_digest_map = {}
        for blob in blobs:
            digest = server.store_blob(blob)
            digests.append(digest)
            # Convert to tuple to make hashable
            blob_digest_map[tuple(blob)] = digest

        blobs = __test_download_blob(server.remote, instance, digests)
        assert blobs is not None

        for blob in blobs:
            assert server.compare_blobs(blob_digest_map[tuple(blob)], blob)


@pytest.mark.parametrize("messages", MESSAGES)
@pytest.mark.parametrize("instance", INSTANCES)
@pytest.mark.parametrize("num_failures", NUM_FAILURES)
def test_download_message(instance, messages, num_failures):
    def __test_download_message(remote, instance, digests, empty_messages):
        # Open a channel to the remote CAS server:
        remote = urlparse(remote)
        remote = f"{remote.hostname}:{remote.port}"
        channel = grpc.insecure_channel(remote)

        messages = []
        with download(channel, instance, retries=5, should_backoff=False) as downloader:
            if len(digests) > 1:
                messages = downloader.get_messages(digests, empty_messages)
                messages = list([m.SerializeToString() for m in messages])
            else:
                message = downloader.get_message(digests[0], empty_messages[0])
                messages.append(message.SerializeToString())

        return messages

    with serve_cas([instance], num_failures) as server:
        empty_messages, digests = [], []
        message_digest_map = {}
        for message in messages:
            digest = server.store_message(message)
            digests.append(digest)
            message_digest_map[message.SerializeToString()] = digest

            empty_message = deepcopy(message)
            empty_message.Clear()
            empty_messages.append(empty_message)

        messages = __test_download_message(server.remote, instance, digests, empty_messages)
        assert messages is not None

        for message_blob, message in zip(messages, empty_messages):
            message.ParseFromString(message_blob)

            assert server.compare_messages(message_digest_map[message.SerializeToString()], message)


@pytest.mark.parametrize("file_paths", FILES)
@pytest.mark.parametrize("instance", INSTANCES)
@pytest.mark.parametrize("num_failures", NUM_FAILURES)
def test_download_file(instance, file_paths, num_failures, data_dir):
    file_paths = [os.path.join(data_dir, path) for path in file_paths]

    def __test_download_file(remote, instance, digests, paths):
        # Open a channel to the remote CAS server:
        remote = urlparse(remote)
        remote = f"{remote.hostname}:{remote.port}"
        channel = grpc.insecure_channel(remote)

        with download(channel, instance, retries=5, should_backoff=False) as downloader:
            if len(digests) > 1:
                for digest, path in zip(digests, paths):
                    downloader.download_file(digest, path, queue=False)
            else:
                downloader.download_file(digests[0], paths[0], queue=False)

        return True

    with serve_cas([instance], num_failures) as server:
        with tempfile.TemporaryDirectory() as temp_folder:
            paths, digests = [], []
            for file_path in file_paths:
                digest = server.store_file(file_path)
                digests.append(digest)

                path = os.path.relpath(file_path, start=DATA_DIR)
                path = os.path.join(temp_folder, path)
                paths.append(path)

                assert __test_download_file(server.remote, instance, digests, paths)

            for digest, path in zip(digests, paths):
                assert server.compare_files(digest, path)


@pytest.mark.parametrize("file_paths", FILES)
@pytest.mark.parametrize("instance", INSTANCES)
def test_download_file_failure(instance, file_paths, data_dir):
    file_paths = [os.path.join(data_dir, path) for path in file_paths]

    def __test_download_file(remote, instance, digests, paths):
        # Open a channel to the remote CAS server:
        remote = urlparse(remote)
        remote = f"{remote.hostname}:{remote.port}"
        channel = grpc.insecure_channel(remote)

        with download(channel, instance, retries=1, should_backoff=False) as downloader:
            if len(digests) > 1:
                for digest, path in zip(digests, paths):
                    downloader.download_file(digest, path, queue=False)
            else:
                downloader.download_file(digests[0], paths[0], queue=False)

        return True

    with serve_cas([instance], num_failures=5) as server:
        with tempfile.TemporaryDirectory() as temp_folder:
            paths, digests = [], []
            for file_path in file_paths:
                digest = server.store_file(file_path)
                digests.append(digest)

                path = os.path.relpath(file_path, start=DATA_DIR)
                path = os.path.join(temp_folder, path)
                paths.append(path)

                with pytest.raises(ConnectionError):
                    __test_download_file(server.remote, instance, digests, paths)


@pytest.mark.parametrize("folder_paths", FOLDERS)
@pytest.mark.parametrize("instance", INSTANCES)
@pytest.mark.parametrize("num_failures", NUM_FAILURES)
def test_download_directory(instance, folder_paths, num_failures, data_dir):
    folder_paths = [os.path.join(data_dir, path) for path in folder_paths]

    def __test_download_directory(remote, instance, digests, paths):
        # Open a channel to the remote CAS server:
        remote = urlparse(remote)
        remote = f"{remote.hostname}:{remote.port}"
        channel = grpc.insecure_channel(remote)

        with download(channel, instance, retries=5, should_backoff=False) as downloader:
            if len(digests) > 1:
                for digest, path in zip(digests, paths):
                    downloader.download_directory(digest, path)
            else:
                downloader.download_directory(digests[0], paths[0])

        return True

    with serve_cas([instance], num_failures) as server:
        with tempfile.TemporaryDirectory() as temp_folder:
            paths, digests = [], []
            for folder_path in folder_paths:
                digest = server.store_folder(folder_path)
                digests.append(digest)

                relpath = os.path.relpath(folder_path, start=data_dir)
                if relpath != ".":
                    path = os.path.join(temp_folder, relpath)
                else:
                    path = temp_folder
                paths.append(path)

            assert __test_download_directory(server.remote, instance, digests, paths)

            for digest, path in zip(digests, paths):
                assert server.compare_directories(digest, path)
