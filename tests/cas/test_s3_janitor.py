# Copyright (C) 2024 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import tempfile
from datetime import datetime, timedelta
from unittest import mock
from unittest.mock import patch

import boto3
import pytest

from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Digest
from buildgrid.server.cas.storage.index.index_abc import IndexABC
from buildgrid.server.cas.storage.index.redis import RedisIndex
from buildgrid.server.cas.storage.index.sql import SQLIndex
from buildgrid.server.cas.storage.s3 import S3Storage
from buildgrid.server.cas.storage.storage_abc import create_write_session
from buildgrid.server.cleanup.janitor.config import JanitorConfig, RedisConfig, S3Config
from buildgrid.server.cleanup.janitor.index import RedisIndexLookup, SqlIndexLookup
from buildgrid.server.cleanup.janitor.s3 import ListObjectResult, S3Janitor, publish_s3_object_metrics
from buildgrid.server.cleanup.janitor.utils import check_bucket_versioning
from buildgrid.server.metrics_names import METRIC
from buildgrid.server.monitoring import get_monitoring_bus, set_monitoring_bus
from buildgrid.server.settings import HASH
from buildgrid.server.sql.provider import SqlProvider
from tests.utils.fixtures import Moto, ThreadSafeFakeRedis
from tests.utils.metrics import mock_create_distribution_record

JANITOR_INDEX_TYPES = ["redis", "sql"]
PREFIXES = ["", "shard", "another/shard"]
HASH_PREFIX_SIZES = [0, 1, 2]
VERSIONED_BUCKET = [True, False]


@pytest.fixture
def bucket_name(moto_server: Moto) -> str:
    return moto_server.bucket("test")


@pytest.fixture(params=zip(PREFIXES, HASH_PREFIX_SIZES))
def s3_config(request, moto_server: Moto, bucket_name: str) -> S3Config:
    return S3Config(
        access_key=moto_server.opts["aws_access_key_id"],
        secret_key=moto_server.opts["aws_secret_access_key"],
        endpoint=moto_server.opts["endpoint_url"],
        bucket_regex=f"^{bucket_name}$",
        path_prefix=request.param[0],
        hash_prefix_size=request.param[1],
    )


@pytest.fixture(params=zip(JANITOR_INDEX_TYPES, VERSIONED_BUCKET))
def janitor(request, s3_config: S3Config, moto_server: Moto, bucket_name: str):
    s3 = boto3.client("s3", **moto_server.opts)
    s3.create_bucket(Bucket=bucket_name)
    if request.param[1]:
        s3.put_bucket_versioning(Bucket=bucket_name, VersioningConfiguration={"Status": "Enabled"})
    if request.param[0] == "sql":
        with tempfile.NamedTemporaryFile() as db:
            connection_string = f"sqlite:///{db.name}"

            # Make a provider here to migrate the database
            SqlProvider(automigrate=True, connection_string=connection_string)
            index = SqlIndexLookup(connection_string)

            config = JanitorConfig(
                s3=s3_config,
                sleep_interval=1,
                sql_connection_string=connection_string,
            )
            yield S3Janitor(config.s3, index)
    else:
        with patch("buildgrid.server.redis.provider.redis.Redis", ThreadSafeFakeRedis):
            config = JanitorConfig(
                s3=s3_config,
                sleep_interval=1,
                redis=RedisConfig(host="localhost", port=8080, db=0, index_prefix="A:", key_batch_size=1000),
            )
            index = RedisIndexLookup(config.redis)
            yield S3Janitor(config.s3, index)


def get_storage(moto_server: Moto, bucket_name: str, path_prefix: str, hash_prefix_size: int) -> S3Storage:
    return S3Storage(
        bucket=bucket_name, s3_path_prefix_string=path_prefix, s3_hash_prefix_size=hash_prefix_size, **moto_server.opts
    )


@pytest.fixture
def index(janitor: S3Janitor, moto_server: Moto, bucket_name: str) -> IndexABC:
    storage = get_storage(moto_server, bucket_name, janitor._path_prefix, janitor._hash_prefix_size)
    if isinstance(janitor._index, SqlIndexLookup):
        return SQLIndex(janitor._index._sql, storage)

    elif isinstance(janitor._index, RedisIndexLookup):
        return RedisIndex(janitor._index._redis, storage)

    else:
        raise ValueError("Unexpected Janitor type")


def create_test_blob(storage: S3Storage, bucket_name: str, data: str) -> tuple[Digest, str, str]:
    blob = data.encode("utf-8")
    hash = HASH(blob).hexdigest()
    size_bytes = len(blob)
    digest = Digest(hash=hash, size_bytes=size_bytes)

    with create_write_session(digest) as writer:
        writer.write(blob)
        storage.commit_write(digest, writer)

    key = storage._construct_key_with_prefix(digest)
    version_id = storage._s3.head_object(Bucket=bucket_name, Key=key).get("VersionId", "")

    return digest, key, version_id


def test_check_versioning(moto_server: Moto):
    s3 = boto3.client("s3", **moto_server.opts)
    s3.create_bucket(Bucket=moto_server.bucket("test-unversioned"))
    s3.create_bucket(Bucket=moto_server.bucket("test-versioned"))
    s3.put_bucket_versioning(
        Bucket=moto_server.bucket("test-versioned"), VersioningConfiguration={"Status": "Enabled"}
    )

    assert check_bucket_versioning(s3, moto_server.bucket("test-versioned"))
    assert not check_bucket_versioning(s3, moto_server.bucket("test-unversioned"))


@mock.patch("buildgrid.server.metrics_utils.create_distribution_record", new=mock_create_distribution_record)
@mock.patch("buildgrid.server.cleanup.janitor.s3.datetime")
def test_s3_object_metrics(mock_date, janitor: S3Janitor, moto_server: Moto, bucket_name: str):
    storage = get_storage(moto_server, bucket_name, janitor._path_prefix, janitor._hash_prefix_size)
    digest, key, version = create_test_blob(storage, bucket_name, "hello world")

    # Get last_modified_time manually since it is set by S3
    last_modified_time = janitor._s3.head_object(Bucket=bucket_name, Key=key).get("LastModified")

    # Set now to be 10 minutes after last modified
    mock_date.now.return_value = last_modified_time + timedelta(minutes=10)

    set_monitoring_bus(mock.Mock())
    test_objects = {ListObjectResult(key, version, last_modified_time, digest.size_bytes)}
    publish_s3_object_metrics(test_objects)

    expected_age = 600000  # 10 minutes in milliseconds
    expected_age_range = "0_TO_60"
    expected_size = digest.size_bytes
    expected_size_range = "0_TO_2000"

    mock_calls = [
        mock.call(
            mock_create_distribution_record(
                METRIC.CLEANUP.JANITOR.BLOB_AGE,
                expected_age,
                objectAgeRange=expected_age_range,
                objectSizeRange=expected_size_range,
            )
        ),
        mock.call(
            mock_create_distribution_record(
                METRIC.CLEANUP.JANITOR.BLOB_BYTES,
                expected_size,
                objectAgeRange=expected_age_range,
                objectSizeRange=expected_size_range,
            )
        ),
    ]

    get_monitoring_bus().send_record_nowait.assert_has_calls(mock_calls, any_order=True)


def test_bucket_enumeration(janitor: S3Janitor, moto_server: Moto, bucket_name: str):
    storage = get_storage(moto_server, bucket_name, janitor._path_prefix, janitor._hash_prefix_size)
    _, key, version = create_test_blob(storage, bucket_name, "hello world")
    key_prefix = "/".join(key.split("/")[:-1])
    # Verify that the key is found under it's prefix but not under a different one
    if check_bucket_versioning(janitor._s3, bucket_name):
        assert any(
            obj.key == key and obj.version_id == version
            for page in janitor.enumerate_versioned_bucket(bucket_name, prefix=key_prefix)
            for obj in page
        )
        assert not any(
            obj.key == key and obj.version_id == version
            for page in janitor.enumerate_versioned_bucket(bucket_name, prefix="fakeprefix")
            for obj in page
        )
    else:
        assert any(
            obj.key == key and obj.version_id == version
            for page in janitor.enumerate_unversioned_bucket(bucket_name, prefix=key_prefix)
            for obj in page
        )
        assert not any(
            obj.key == key and obj.version_id == version
            for page in janitor.enumerate_unversioned_bucket(bucket_name, prefix="fakeprefix")
            for obj in page
        )


def test_s3_deletion(janitor: S3Janitor, moto_server: Moto, bucket_name: str):
    storage = get_storage(moto_server, bucket_name, janitor._path_prefix, janitor._hash_prefix_size)
    _, key, version = create_test_blob(storage, bucket_name, "hello world")
    test_time = datetime.now()
    test_size = 11
    test_list_object_result = ListObjectResult(key, version, test_time, test_size)
    assert key in janitor.delete_s3_entries(bucket_name, [test_list_object_result])


def test_cleanup_bucket(janitor: S3Janitor, moto_server: Moto, bucket_name: str):
    storage = get_storage(moto_server, bucket_name, janitor._path_prefix, janitor._hash_prefix_size)
    create_test_blob(storage, bucket_name, "hello world")
    count = janitor.cleanup_bucket(bucket_name)
    assert count == 1


def test_get_missing_digests(janitor: S3Janitor, index: IndexABC, moto_server: Moto, bucket_name: str):
    storage = get_storage(moto_server, bucket_name, janitor._path_prefix, janitor._hash_prefix_size)
    digest, _, _ = create_test_blob(storage, bucket_name, "hello world")
    digest_str = f"{digest.hash}_{digest.size_bytes}"
    missing = janitor._index.get_missing_digests({digest_str})
    assert digest_str in missing

    # Now insert the test blob into the index too
    with create_write_session(digest) as writer:
        writer.write("hello world".encode("utf-8"))
        index.commit_write(digest, writer)
    assert index.has_blob(digest)

    missing = janitor._index.get_missing_digests({digest_str})
    assert digest_str not in missing
