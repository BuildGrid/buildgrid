# Copyright (C) 2024 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import typing
from datetime import datetime, timedelta
from unittest import mock

import boto3
import botocore.exceptions
import pytest

from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Digest
from buildgrid.server.cas.storage.s3 import S3Storage
from buildgrid.server.cas.storage.storage_abc import create_write_session
from buildgrid.server.metrics_names import METRIC
from buildgrid.server.monitoring import get_monitoring_bus, set_monitoring_bus
from buildgrid.server.settings import HASH
from tests.utils.fixtures import Moto
from tests.utils.metrics import mock_create_distribution_record

if typing.TYPE_CHECKING:
    from mypy_boto3_s3 import Client as S3Client


@pytest.fixture
def bucket_name(moto_server: Moto) -> str:
    return moto_server.bucket("test-bucket")


@pytest.fixture
def s3(moto_server: Moto, bucket_name: str) -> typing.Iterable["S3Client"]:
    s3 = boto3.client("s3", **moto_server.opts)
    s3.create_bucket(Bucket=bucket_name)
    s3.put_bucket_versioning(Bucket=bucket_name, VersioningConfiguration={"Status": "Enabled"})
    yield s3


def non_versioned_deletes_storage(moto_server: Moto, bucket_name: str) -> S3Storage:
    return S3Storage(bucket=bucket_name, s3_versioned_deletes=False, **moto_server.opts)


def versioned_delete_storage(moto_server: Moto, bucket_name: str) -> S3Storage:
    return S3Storage(bucket=bucket_name, s3_versioned_deletes=True, **moto_server.opts)


def create_test_blob(storage: S3Storage, bucket_name, data: str) -> tuple[Digest, str, str]:
    blob = data.encode("utf-8")
    hash = HASH(blob).hexdigest()
    size_bytes = len(blob)
    digest = Digest(hash=hash, size_bytes=size_bytes)

    with create_write_session(digest) as writer:
        writer.write(blob)
        storage.commit_write(digest, writer)

    key = storage._construct_key(digest)
    version_id = storage._s3.head_object(Bucket=bucket_name, Key=key)["VersionId"]
    assert version_id

    return digest, key, version_id


def test_s3_versioned_deletes_mocks_only(s3: "S3Client", bucket_name: str) -> None:
    """
    Before testing with our CAS wrapper, validate assumptions using the boto mocks alone.
    """

    # Create a versioned bucket with two objects.
    s3.put_object(Bucket=bucket_name, Key="foo", Body="foo")
    s3.put_object(Bucket=bucket_name, Key="bar", Body="bar")

    # Test that an object deleted without a version id uses non-versioned deletes
    foo = s3.head_object(Bucket=bucket_name, Key="foo")
    s3.delete_object(Bucket=bucket_name, Key="foo")
    s3.head_object(Bucket=bucket_name, Key="foo", VersionId=foo["VersionId"])

    # Test that an object deleted with a version id uses versioned deletes
    bar = s3.head_object(Bucket=bucket_name, Key="bar")

    s3.delete_object(Bucket=bucket_name, Key="bar", VersionId=bar["VersionId"])

    with pytest.raises(botocore.exceptions.ClientError) as e:
        s3.head_object(Bucket=bucket_name, Key="bar", VersionId=bar["VersionId"])
    assert e.value.response["Error"] == {"Code": "404", "Message": "Not Found"}


def test_s3_non_versioned_deletes(s3: "S3Client", moto_server: Moto, bucket_name: str) -> None:
    """
    Test that when performing non-versioned deletes on a versioned S3 bucket, that the object remains
    under the non-current version id, but is not queryable from a standard call without version id.
    """

    storage = non_versioned_deletes_storage(moto_server, bucket_name)

    digest, key, version_id = create_test_blob(storage, bucket_name, "foo")

    storage.delete_blob(digest)

    with pytest.raises(botocore.exceptions.ClientError) as e:
        s3.head_object(Bucket=bucket_name, Key=key)
    assert e.value.response["Error"] == {"Code": "404", "Message": "Not Found"}
    s3.head_object(Bucket=bucket_name, Key=key, VersionId=version_id)


def test_s3_versioned_deletes(s3: "S3Client", moto_server: Moto, bucket_name: str) -> None:
    """
    Test that when performing versioned deletes on a versioned S3 bucket, that the object and
    the current version id are both deleted immediately
    """

    storage = versioned_delete_storage(moto_server, bucket_name)

    digest, key, version_id = create_test_blob(storage, bucket_name, "foo")

    storage.delete_blob(digest)

    with pytest.raises(botocore.exceptions.ClientError) as e:
        s3.head_object(Bucket=bucket_name, Key=key)
    assert e.value.response["Error"] == {"Code": "404", "Message": "Not Found"}

    with pytest.raises(botocore.exceptions.ClientError) as e:
        s3.head_object(Bucket=bucket_name, Key=key, VersionId=version_id)
    assert e.value.response["Error"] == {"Code": "404", "Message": "Not Found"}


def test_s3_non_versioned_bulk_deletes(s3: "S3Client", moto_server: Moto, bucket_name: str) -> None:
    """
    Same as the above case for non-versioned deletes, but tests the bulk delete variant.
    """

    storage = non_versioned_deletes_storage(moto_server, bucket_name)

    digest0, key0, version_id0 = create_test_blob(storage, bucket_name, "foo")
    digest1, key1, version_id1 = create_test_blob(storage, bucket_name, "bar")

    storage.bulk_delete([digest0, digest1])

    for digest, key, version_id in [(digest0, key0, version_id0), (digest1, key1, version_id1)]:
        with pytest.raises(botocore.exceptions.ClientError) as e:
            s3.head_object(Bucket=bucket_name, Key=key)
        assert e.value.response["Error"] == {"Code": "404", "Message": "Not Found"}
        s3.head_object(Bucket=bucket_name, Key=key, VersionId=version_id)


def test_s3_versioned_bulk_deletes(s3: "S3Client", moto_server: Moto, bucket_name: str) -> None:
    """
    Same as the above case for versioned deletes, but tests the bulk delete variant.
    """

    storage = versioned_delete_storage(moto_server, bucket_name)

    digest0, key0, version_id0 = create_test_blob(storage, bucket_name, "foo")
    digest1, key1, version_id1 = create_test_blob(storage, bucket_name, "bar")

    storage.bulk_delete([digest0, digest1])

    for digest, key, version_id in [(digest0, key0, version_id0), (digest1, key1, version_id1)]:
        with pytest.raises(botocore.exceptions.ClientError) as e:
            s3.head_object(Bucket=bucket_name, Key=key)
        assert e.value.response["Error"] == {"Code": "404", "Message": "Not Found"}

        with pytest.raises(botocore.exceptions.ClientError) as e:
            s3.head_object(Bucket=bucket_name, Key=key, VersionId=version_id)
        assert e.value.response["Error"] == {"Code": "404", "Message": "Not Found"}


def test_s3_versioned_bulk_deletes_identical_blobs(s3: "S3Client", moto_server: Moto, bucket_name: str) -> None:
    """
    Checks versioned and unversioned deletes occur
    """

    storage = versioned_delete_storage(moto_server, bucket_name)

    digest0, key0, version_id0 = create_test_blob(storage, bucket_name, "foo")
    digest1, key1, version_id1 = create_test_blob(storage, bucket_name, "foo")
    assert (digest0, key0) == (digest1, key1)
    assert version_id0 != version_id1

    # Both versions exist
    s3.head_object(Bucket=bucket_name, Key=key0)
    s3.head_object(Bucket=bucket_name, Key=key0, VersionId=version_id0)
    s3.head_object(Bucket=bucket_name, Key=key0, VersionId=version_id1)

    storage.bulk_delete([digest0, digest1])

    # Non version entry 404 with delete marker
    with pytest.raises(botocore.exceptions.ClientError) as e:
        s3.head_object(Bucket=bucket_name, Key=key0)
    assert e.value.response["Error"] == {"Code": "404", "Message": "Not Found"}

    # Latest version id is now deleted
    with pytest.raises(botocore.exceptions.ClientError) as e:
        s3.head_object(Bucket=bucket_name, Key=key0, VersionId=version_id1)
    assert e.value.response["Error"] == {"Code": "404", "Message": "Not Found"}

    # Previous id present but non-current
    s3.head_object(Bucket=bucket_name, Key=key0, VersionId=version_id0)


def test_s3_bulk_delete_deleted_digest(s3: "S3Client", moto_server: Moto, bucket_name: str) -> None:
    """
    Ensuring normal behavior when deleting a digest that has already been deleted
    """

    storage = versioned_delete_storage(moto_server, bucket_name)

    digest0, key0, version_id0 = create_test_blob(storage, bucket_name, "foo")
    digest1, key1, version_id1 = create_test_blob(storage, bucket_name, "bar")

    failed_deletions1 = storage.bulk_delete([digest0])
    failed_deletions2 = storage.bulk_delete([digest0, digest1])

    assert failed_deletions1 == []
    assert failed_deletions2 == []


@mock.patch("buildgrid.server.metrics_utils.create_distribution_record", new=mock_create_distribution_record)
@mock.patch("buildgrid.server.cas.storage.s3.datetime", wraps=datetime)
def test_s3_bulk_delete_metrics(mock_date, s3: "S3Client", moto_server: Moto, bucket_name: str) -> None:
    """
    Checks that metrics are sent when deleting a blob
    """
    storage = non_versioned_deletes_storage(moto_server, bucket_name)
    digest, key, _ = create_test_blob(storage, bucket_name, "foo")

    # Get last_modified_time manually since it is set by S3
    last_modified_time = s3.head_object(Bucket=bucket_name, Key=key)["LastModified"]

    # Set now to be 10 minutes after last modified
    mock_date.now.return_value = last_modified_time + timedelta(minutes=10)

    set_monitoring_bus(mock.Mock())
    storage.bulk_delete([digest])

    expected_age = 600000  # 10 minutes in milliseconds
    expected_age_range = "0_TO_60"
    expected_size = digest.size_bytes
    expected_size_range = "0_TO_2000"

    mock_calls = [
        mock.call(
            mock_create_distribution_record(
                METRIC.STORAGE.S3.BLOB_BYTES,
                expected_size,
                objectAgeRange=expected_age_range,
                objectSizeRange=expected_size_range,
            )
        ),
        mock.call(
            mock_create_distribution_record(
                METRIC.STORAGE.S3.BLOB_AGE,
                expected_age,
                objectAgeRange=expected_age_range,
                objectSizeRange=expected_size_range,
            )
        ),
    ]

    get_monitoring_bus().send_record_nowait.assert_has_calls(mock_calls, any_order=True)
