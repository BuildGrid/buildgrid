# Copyright (C) 2024 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import tempfile
from unittest.mock import patch

import yaml

from buildgrid.server.cleanup.janitor.config import (
    JanitorConfig,
    RedisConfig,
    S3Config,
    SQLStorageConfig,
    parse_janitor_config,
)
from buildgrid.server.sql.provider import SqlProvider


def test_parse_janitor_config_s3():
    config_data = {
        "s3": {
            "access_key": "test-access-key",
            "bucket_regex": "test-bucket-regex",
            "endpoint": "test-endpoint",
            "path_prefix": "test-path-prefix",
            "hash_prefix_size": 2,
            "secret_key": "test-secret-key",
            "sleep_interval": 10,
        }
    }
    with tempfile.NamedTemporaryFile(delete=False, mode="w") as temp_config:
        yaml.dump(config_data, temp_config)
        temp_config_path = temp_config.name

    config = parse_janitor_config(temp_config_path)
    os.remove(temp_config_path)

    assert isinstance(config, JanitorConfig)
    assert isinstance(config.s3, S3Config)
    assert config.s3.access_key == "test-access-key"
    assert config.s3.bucket_regex == "test-bucket-regex"
    assert config.s3.endpoint == "test-endpoint"
    assert config.s3.path_prefix == "test-path-prefix"
    assert config.s3.hash_prefix_size == 2
    assert config.s3.secret_key == "test-secret-key"
    assert config.s3.sleep_interval == 10


@patch("buildgrid.server.sql.provider.SqlProvider._create_sqlalchemy_engine")
def test_parse_janitor_config_sql(mock_create_engine):
    config_data = {
        "sql_storage_config": {
            "sql": {"connection_string": "sqlite:///test.db"},
            "sql_ro": {"connection_string": "sqlite:///test_ro.db"},
            "sleep_interval": 20,
            "batch_size": 100,
        }
    }
    with tempfile.NamedTemporaryFile(delete=False, mode="w") as temp_config:
        yaml.dump(config_data, temp_config)
        temp_config_path = temp_config.name

    config = parse_janitor_config(temp_config_path)
    os.remove(temp_config_path)

    assert isinstance(config, JanitorConfig)
    assert isinstance(config.sql_storage_config, SQLStorageConfig)
    assert isinstance(config.sql_storage_config.sql, SqlProvider)
    assert isinstance(config.sql_storage_config.sql_ro, SqlProvider)

    assert config.sql_storage_config.sleep_interval == 20
    assert config.sql_storage_config.batch_size == 100


def test_parse_janitor_config_redis():
    config_data = {
        "redis": {
            "db": 1,
            "dns_srv_record": "test-srv-record",
            "index_prefix": "test-index-prefix",
            "key_batch_size": 50,
            "password": "test-password",
            "host": "test-host",
            "port": 6379,
            "sentinel_master_name": "test-sentinel-master",
        }
    }
    with tempfile.NamedTemporaryFile(delete=False, mode="w") as temp_config:
        yaml.dump(config_data, temp_config)
        temp_config_path = temp_config.name

    config = parse_janitor_config(temp_config_path)
    os.remove(temp_config_path)

    assert isinstance(config, JanitorConfig)
    assert isinstance(config.redis, RedisConfig)
    assert config.redis.db == 1
    assert config.redis.dns_srv_record == "test-srv-record"
    assert config.redis.index_prefix == "test-index-prefix"
    assert config.redis.key_batch_size == 50
    assert config.redis.password == "test-password"
    assert config.redis.host == "test-host"
    assert config.redis.port == 6379
    assert config.redis.sentinel_master_name == "test-sentinel-master"


@patch("buildgrid.server.cleanup.janitor.config.set_monitoring_bus")
def test_parse_janitor_config_monitoring(mock_set_monitoring_bus):
    config_data = {
        "monitoring": {
            "enabled": True,
            "endpoint-type": "file",
            "endpoint-location": "test-endpoint-location",
            "serialization-format": "json",
            "metric_prefix": "test-metric-prefix",
        }
    }
    with tempfile.NamedTemporaryFile(delete=False, mode="w") as temp_config:
        yaml.dump(config_data, temp_config)
        temp_config_path = temp_config.name

    config = parse_janitor_config(temp_config_path)
    os.remove(temp_config_path)

    assert isinstance(config, JanitorConfig)
    mock_set_monitoring_bus.assert_called_once()
