# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import logging
from unittest import mock

import grpc
import pytest

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2 as re_pb2
from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Digest
from buildgrid._protos.google.bytestream import bytestream_pb2
from buildgrid._protos.google.rpc import code_pb2
from buildgrid.server.cas.instance import ByteStreamInstance, ContentAddressableStorageInstance
from buildgrid.server.cas.service import ByteStreamService, ContentAddressableStorageService
from buildgrid.server.cas.storage.lru_memory_cache import LRUMemoryCache
from buildgrid.server.cas.storage.storage_abc import StorageABC, create_write_session
from buildgrid.server.metrics_names import METRIC
from buildgrid.server.monitoring import get_monitoring_bus, set_monitoring_bus
from buildgrid.server.settings import HASH
from buildgrid.server.utils.digests import create_digest
from tests.utils.metrics import mock_create_counter_record, mock_create_distribution_record, mock_create_timer_record
from tests.utils.mocks import mock_context, rpc_context


def simple_storage(existing_data: list[bytes] | None = None) -> StorageABC:
    storage = LRUMemoryCache(1024 * 1024 * 8)
    if existing_data:
        storage.bulk_update_blobs(
            [(Digest(hash=HASH(data).hexdigest(), size_bytes=len(data)), data) for data in existing_data]
        )
    return storage


test_strings = [b"", b"hij"]
instances = ["", "test_inst"]


def setup_bytestream_service(storage, instance_name, *, read_only=False):
    bs_instance = ByteStreamInstance(storage, read_only=read_only)
    bs_instance.start()

    bs_service = ByteStreamService()
    bs_service.add_instance(instance_name, bs_instance)

    return bs_service


def setup_cas_service(storage, instance_name, *, read_only=False, tree_cache_size: int | None = None):
    cas_instance = ContentAddressableStorageInstance(storage, read_only=read_only, tree_cache_size=tree_cache_size)
    cas_instance.start()

    cas_service = ContentAddressableStorageService()
    cas_service.add_instance(instance_name, cas_instance)

    return cas_service


@pytest.mark.parametrize("instance_name", instances)
def test_empty_blob_bytestream_read_but_not_in_storage(instance_name):
    storage = simple_storage([])
    servicer = setup_bytestream_service(storage, instance_name)
    empty_blob = b""
    request = bytestream_pb2.ReadRequest()
    if instance_name != "":
        request.resource_name = instance_name + "/"
    request.resource_name += f"blobs/{HASH(empty_blob).hexdigest()}/{len(empty_blob)}"
    count = 0
    data = b""
    for response in servicer.Read(request, mock_context()):
        data = response.data
        count = count + 1
    assert count == 1
    assert data == empty_blob


@pytest.mark.parametrize("data_to_read", test_strings)
@pytest.mark.parametrize("instance_name", instances)
@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
@mock.patch("buildgrid.server.metrics_utils.create_counter_record", new=mock_create_counter_record)
@mock.patch("buildgrid.server.metrics_utils.create_distribution_record", new=mock_create_distribution_record)
def test_bytestream_read(data_to_read, instance_name):
    storage = simple_storage([b"abc", b"defg", data_to_read])

    set_monitoring_bus(mock.Mock())

    bs_service = setup_bytestream_service(storage, instance_name)

    request = bytestream_pb2.ReadRequest()
    if instance_name != "":
        request.resource_name = instance_name + "/"
    request.resource_name += f"blobs/{HASH(data_to_read).hexdigest()}/{len(data_to_read)}"

    data = b""
    output_bytes = 0
    for response in bs_service.Read(request, mock_context()):
        output_bytes += response.ByteSize()
        data += response.data
    assert data == data_to_read

    with rpc_context("ByteStream", "Read", instance_name):
        call_list = [
            mock.call(mock_create_timer_record(METRIC.RPC.DURATION, status="OK")),
            mock.call(mock_create_counter_record(METRIC.RPC.OUTPUT_BYTES, output_bytes, status="OK")),
        ]
        if len(data_to_read) > 0:
            call_list.append(mock.call(mock_create_distribution_record(METRIC.CAS.BLOB_BYTES, len(data_to_read))))

        get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


@pytest.mark.parametrize("instance_name", instances)
def test_bytestream_read_shows_requestmetadata(caplog, instance_name):
    caplog.set_level(logging.INFO)
    storage = simple_storage([b"abc", b"defg"])
    bs_service = setup_bytestream_service(storage, instance_name)
    request = bytestream_pb2.ReadRequest()
    if instance_name != "":
        request.resource_name = instance_name + "/"
    request.resource_name += f"blobs/{HASH(b'abc').hexdigest()}/{len(b'abc')}"

    data = b""
    for response in bs_service.Read(request, mock_context()):
        data += response.data

    expected_substrings = ["tool_name", "tool_version", "action_id", "tool_invocation_id", "correlated_invocations_id"]
    for exp in expected_substrings:
        assert exp in caplog.text


@pytest.mark.parametrize("instance_name", instances)
def test_bytestream_write_shows_requestmetadata(caplog, instance_name):
    caplog.set_level(logging.INFO)
    storage = simple_storage()
    bs_service = setup_bytestream_service(storage, instance_name)
    resource_source = ""
    if instance_name != "":
        resource_source = instance_name + "/"
    hash_ = HASH(b"abcdef").hexdigest()
    resource_name = resource_source + f"uploads/UUID-HERE/blobs/{hash_}/6"

    requests = [
        bytestream_pb2.WriteRequest(resource_name=resource_name, data=b"abc"),
        bytestream_pb2.WriteRequest(data=b"def", write_offset=3, finish_write=True),
    ]

    bs_service.Write(iter(requests), mock_context())
    expected_substrings = ["tool_name", "tool_version", "action_id", "tool_invocation_id", "correlated_invocations_id"]
    for exp in expected_substrings:
        assert exp in caplog.text


@pytest.mark.parametrize("instance_name", instances)
def test_bytestream_read_many(instance_name):
    data_to_read = b"testing" * 10000

    storage = simple_storage([b"abc", b"defg", data_to_read])

    bs_service = setup_bytestream_service(storage, instance_name)

    request = bytestream_pb2.ReadRequest()
    resource_source = ""
    if instance_name != "":
        resource_source = instance_name + "/"
    request.resource_name = resource_source + f"blobs/{HASH(data_to_read).hexdigest()}/{len(data_to_read)}"

    data = b""
    for response in bs_service.Read(request, mock_context()):
        data += response.data
    assert data == data_to_read

    # if we try and read a bad digest we should get an invalid argument error
    bad_request = bytestream_pb2.ReadRequest()
    bad_request.resource_name = resource_source + "blobs/nothashed/2"
    context = mock_context()

    with pytest.raises(Exception):
        for response in bs_service.Read(bad_request, context):
            pass

    assert context.code() == grpc.StatusCode.INVALID_ARGUMENT


@pytest.mark.parametrize("instance_name", instances)
@pytest.mark.parametrize("extra_data", ["", "/", "/extra/data"])
@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
@mock.patch("buildgrid.server.metrics_utils.create_counter_record", new=mock_create_counter_record)
@mock.patch("buildgrid.server.metrics_utils.create_distribution_record", new=mock_create_distribution_record)
def test_bytestream_write(instance_name, extra_data):
    context = mock_context()
    storage = simple_storage()

    set_monitoring_bus(mock.Mock())

    bs_service = setup_bytestream_service(storage, instance_name)

    resource_source = ""
    if instance_name != "":
        resource_source = instance_name + "/"
    hash_ = HASH(b"abcdef").hexdigest()
    resource_name = resource_source + f"uploads/UUID-HERE/blobs/{hash_}/6"
    resource_name += extra_data
    requests = [
        bytestream_pb2.WriteRequest(resource_name=resource_name, data=b"abc"),
        bytestream_pb2.WriteRequest(data=b"def", write_offset=3, finish_write=True),
    ]

    response = bs_service.Write(iter(requests), context)
    assert response.committed_size == 6

    # Now do the same request again without hashing and we should get an exception
    # because the hash will be invalid
    resource_name = resource_source + "uploads/UUID-HERE/blobs/nothashed/6"
    resource_name += extra_data

    with pytest.raises(Exception):
        bs_service.Write(iter([bytestream_pb2.WriteRequest(resource_name=resource_name, data=b"abc")]), context)
    assert context.code() == grpc.StatusCode.INVALID_ARGUMENT

    with rpc_context("ByteStream", "Write", instance_name):
        # We call the method twice, but the second call throws an exception in the instance that's
        # handled by the service, so only the service method generates a metric the second time
        call_list = [
            mock.call(mock_create_timer_record(METRIC.RPC.DURATION, status="OK")),
            mock.call(mock_create_timer_record(METRIC.RPC.DURATION, status="INVALID_ARGUMENT")),
            mock.call(mock_create_distribution_record(METRIC.CAS.BLOB_BYTES, 6)),
        ]

    get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


def test_bytestream_write_rejects_wrong_hash():
    context = mock_context()
    storage = simple_storage()

    bs_instance = ByteStreamInstance(storage)
    servicer = ByteStreamService()
    servicer.add_instance("", bs_instance)

    data = b"some data"
    wrong_hash = HASH(b"incorrect").hexdigest()
    resource_name = f"uploads/UUID-HERE/blobs/{wrong_hash}/9"
    requests = [bytestream_pb2.WriteRequest(resource_name=resource_name, data=data, finish_write=True)]

    with pytest.raises(Exception):
        servicer.Write(iter(requests), context)
    assert context.code() == grpc.StatusCode.INVALID_ARGUMENT

    wrong_digest = re_pb2.Digest(hash=wrong_hash, size_bytes=len(data))
    assert not storage.has_blob(wrong_digest)


def test_bytestream_write_single_request():
    storage = simple_storage()

    with ByteStreamInstance(storage) as bs_instance:
        servicer = ByteStreamService()
        servicer.add_instance("", bs_instance)

        data = b"some data"
        hash_ = HASH(data).hexdigest()
        resource_name = f"uploads/UUID-HERE/blobs/{hash_}/9"
        requests = [bytestream_pb2.WriteRequest(resource_name=resource_name, data=data, finish_write=True)]

        response = servicer.Write(iter(requests), mock_context())
        assert response.committed_size == 9


@pytest.mark.parametrize("disable_early_return", [False, True])
def test_bytestream_write_existing_digest(disable_early_return):
    storage = simple_storage()

    with ByteStreamInstance(storage, disable_overwrite_early_return=disable_early_return) as bs_instance:
        servicer = ByteStreamService()
        servicer.add_instance("", bs_instance)

        blob_part_1 = b"Part1"
        blob_part_2 = b"Part2"
        data = blob_part_1 + blob_part_2
        hash_ = HASH(data).hexdigest()

        digest = re_pb2.Digest(hash=hash_, size_bytes=len(data))

        with create_write_session(digest) as write_session:
            write_session.write(data)
            storage.commit_write(digest, write_session)
        assert storage.has_blob(digest)

        resource_name = f"uploads/UUID-HERE/blobs/{digest.hash}/{digest.size_bytes}"

        requests = [
            bytestream_pb2.WriteRequest(resource_name=resource_name, data=blob_part_1, finish_write=False),
            bytestream_pb2.WriteRequest(resource_name=resource_name, data=blob_part_2, finish_write=True),
        ]

        response = servicer.Write(iter(requests), mock_context())
        assert response.committed_size == digest.size_bytes


def test_bytestream_read_only_write_fails():
    context = mock_context()
    storage = simple_storage()

    with ByteStreamInstance(storage, read_only=True) as bs_instance:
        servicer = ByteStreamService()
        servicer.add_instance("", bs_instance)

        data = b"some data"
        hash_ = HASH(b"some data").hexdigest()
        resource_name = f"uploads/UUID-HERE/blobs/{hash_}/9"
        requests = [bytestream_pb2.WriteRequest(resource_name=resource_name, data=data, finish_write=True)]

        with pytest.raises(Exception):
            servicer.Write(iter(requests), context)
        assert context.code() == grpc.StatusCode.PERMISSION_DENIED

        digest = re_pb2.Digest(hash=hash_, size_bytes=len(data))
        assert not storage.has_blob(digest)


@pytest.mark.parametrize("instance_name", instances)
def test_empty_blob_find_missing_blob_but_not_in_storage(instance_name):
    storage = simple_storage([])
    cas_service = setup_cas_service(storage, instance_name)

    digests = [re_pb2.Digest(hash=HASH(b"").hexdigest(), size_bytes=0)]
    request = re_pb2.FindMissingBlobsRequest(instance_name=instance_name, blob_digests=digests)
    response = cas_service.FindMissingBlobs(request, mock_context())
    assert len(response.missing_blob_digests) == 0


@pytest.mark.parametrize("instance_name", instances)
@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
@mock.patch("buildgrid.server.metrics_utils.create_distribution_record", new=mock_create_distribution_record)
def test_cas_find_missing_blobs(instance_name):
    storage = simple_storage([b"abc", b"def"])

    set_monitoring_bus(mock.Mock())

    cas_service = setup_cas_service(storage, instance_name)

    digests = [
        re_pb2.Digest(hash=HASH(b"def").hexdigest(), size_bytes=3),
        re_pb2.Digest(hash=HASH(b"def").hexdigest(), size_bytes=3),  # duplicate request
        re_pb2.Digest(hash=HASH(b"ghij").hexdigest(), size_bytes=4),
    ]
    request = re_pb2.FindMissingBlobsRequest(instance_name=instance_name, blob_digests=digests)
    response = cas_service.FindMissingBlobs(request, mock_context())
    assert len(response.missing_blob_digests) == 1
    assert response.missing_blob_digests[0] == digests[2]

    with rpc_context("ContentAddressableStorage", "FindMissingBlobs", instance_name):
        call_list = [
            mock.call(mock_create_timer_record(METRIC.RPC.DURATION, status="OK")),
            mock.call(mock_create_distribution_record(METRIC.CAS.BLOBS_COUNT, 2.0)),
            mock.call(mock_create_distribution_record(METRIC.CAS.BLOB_BYTES, 3)),
            mock.call(mock_create_distribution_record(METRIC.CAS.BLOB_BYTES, 4)),
            mock.call(mock_create_distribution_record(METRIC.CAS.BLOBS_MISSING_COUNT, 1)),
            mock.call(mock_create_distribution_record(METRIC.CAS.BLOBS_MISSING_PERCENT, 50.00)),
        ]
    get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


@pytest.mark.parametrize("instance_name", instances)
@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
@mock.patch("buildgrid.server.metrics_utils.create_counter_record", new=mock_create_counter_record)
@mock.patch("buildgrid.server.metrics_utils.create_distribution_record", new=mock_create_distribution_record)
def test_cas_batch_update_blobs(instance_name):
    storage = simple_storage()

    set_monitoring_bus(mock.Mock())

    cas_service = setup_cas_service(storage, instance_name)

    update_requests = [
        re_pb2.BatchUpdateBlobsRequest.Request(
            digest=re_pb2.Digest(hash=HASH(b"abc").hexdigest(), size_bytes=3), data=b"abc"
        ),
        re_pb2.BatchUpdateBlobsRequest.Request(  # duplicate request
            digest=re_pb2.Digest(hash=HASH(b"abc").hexdigest(), size_bytes=3), data=b"abc"
        ),
        re_pb2.BatchUpdateBlobsRequest.Request(
            digest=re_pb2.Digest(hash="invalid digest!", size_bytes=1000), data=b"wrong data"
        ),
    ]

    request = re_pb2.BatchUpdateBlobsRequest(instance_name=instance_name, requests=update_requests)

    response = cas_service.BatchUpdateBlobs(request, mock_context())

    assert len(response.responses) == 2

    for blob_response in response.responses:
        if blob_response.digest == update_requests[0].digest:
            assert blob_response.status.code == 0

        elif blob_response.digest == update_requests[2].digest:
            assert blob_response.status.code != 0

        else:
            raise Exception("Unexpected blob response")

    with rpc_context("ContentAddressableStorage", "BatchUpdateBlobs", instance_name):
        call_list = [
            mock.call(mock_create_timer_record(METRIC.RPC.DURATION, status="OK")),
            mock.call(mock_create_counter_record(METRIC.RPC.INPUT_BYTES, request.ByteSize(), status="OK")),
            mock.call(mock_create_counter_record(METRIC.RPC.OUTPUT_BYTES, response.ByteSize(), status="OK")),
            mock.call(mock_create_distribution_record(METRIC.CAS.BLOB_BYTES, 3)),
            mock.call(mock_create_distribution_record(METRIC.CAS.BLOB_BYTES, 1000)),
        ]
    get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


@pytest.mark.parametrize("instance", instances)
def test_cas_read_only_update_blobs(instance):
    context = mock_context()
    storage = simple_storage()

    with ContentAddressableStorageInstance(storage, read_only=True) as cas_instance:
        servicer = ContentAddressableStorageService()
        servicer.add_instance(instance, cas_instance)

        update_requests = [
            re_pb2.BatchUpdateBlobsRequest.Request(
                digest=re_pb2.Digest(hash=HASH(b"abc").hexdigest(), size_bytes=3), data=b"abc"
            ),
            re_pb2.BatchUpdateBlobsRequest.Request(
                digest=re_pb2.Digest(hash="invalid digest!", size_bytes=1000), data=b"wrong data"
            ),
        ]

        request = re_pb2.BatchUpdateBlobsRequest(instance_name=instance, requests=update_requests)

        with pytest.raises(Exception):
            servicer.BatchUpdateBlobs(request, context)
        assert context.code() == grpc.StatusCode.PERMISSION_DENIED

        assert not storage.has_blob(update_requests[0].digest)
        assert not storage.has_blob(update_requests[1].digest)


@pytest.mark.parametrize("instance_name", instances)
@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
@mock.patch("buildgrid.server.metrics_utils.create_counter_record", new=mock_create_counter_record)
@mock.patch("buildgrid.server.metrics_utils.create_distribution_record", new=mock_create_distribution_record)
def test_cas_batch_read_blobs(instance_name):
    data = {b"abc", b"defg", b"hij", b"klmnop", b"duplicate blob"}
    data_hashes = {HASH(blob).hexdigest() for blob in data}
    storage = simple_storage(data)

    set_monitoring_bus(mock.Mock())

    cas_service = setup_cas_service(storage, instance_name)

    bloblists_to_request = [
        [b"abc", b"defg"],
        [b"defg", b"missing_blob"],
        [b"missing_blob"],
        [b"duplicate blob", b"duplicate blob"],
    ]

    digest_lists = [
        [re_pb2.Digest(hash=HASH(blob).hexdigest(), size_bytes=len(blob)) for blob in bloblist]
        for bloblist in bloblists_to_request
    ]

    read_requests = [
        re_pb2.BatchReadBlobsRequest(instance_name=instance_name, digests=digest_list) for digest_list in digest_lists
    ]

    for request, bloblist in zip(read_requests, bloblists_to_request):
        responses = cas_service.BatchReadBlobs(request, mock_context())
        response_blobs = set()

        for response in responses.responses:
            if response.digest.hash in data_hashes:
                assert response.status.code == code_pb2.OK
                response_blobs.add(response.data)
            else:
                assert response.status.code == code_pb2.NOT_FOUND

        assert response_blobs == set(bloblist) & data

        requested_sizes = set(len(blob) for blob in set(bloblist) if blob in data)

        with rpc_context("ContentAddressableStorage", "BatchReadBlobs", instance_name):
            call_list = [
                mock.call(mock_create_timer_record(METRIC.RPC.DURATION, status="OK")),
                mock.call(mock_create_counter_record(METRIC.RPC.INPUT_BYTES, request.ByteSize(), status="OK")),
                mock.call(mock_create_counter_record(METRIC.RPC.OUTPUT_BYTES, responses.ByteSize(), status="OK")),
            ]
            for size in requested_sizes:
                call_list.append(mock.call(mock_create_distribution_record(METRIC.CAS.BLOB_BYTES, size)))

        get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)
        get_monitoring_bus().reset_mock()


@pytest.mark.parametrize("instance_name", instances)
def test_cas_batch_bad_digest(instance_name):
    data = {b"abc", b"def"}
    # 'aaa' is an invalid hash and should be rejected.
    data_hashes = {HASH(blob).hexdigest() for blob in data}
    data_hashes.add("aaa")

    storage = simple_storage(data)

    bs_service = setup_cas_service(storage, instance_name)
    read_request = re_pb2.BatchReadBlobsRequest(
        instance_name=instance_name, digests=[re_pb2.Digest(hash=h, size_bytes=3) for h in data_hashes]
    )

    # Batch Read
    batched_responses = bs_service.BatchReadBlobs(read_request, mock_context())
    for response in batched_responses.responses:
        if response.digest.hash == "aaa":
            assert response.status.code == code_pb2.INVALID_ARGUMENT
        else:
            assert response.status.code == code_pb2.OK

    # Batch update
    update_request = re_pb2.BatchUpdateBlobsRequest(
        instance_name=instance_name,
        requests=[
            re_pb2.BatchUpdateBlobsRequest.Request(
                digest=re_pb2.Digest(hash=HASH(b"foo").hexdigest(), size_bytes=3), data=b"foo"
            ),
            re_pb2.BatchUpdateBlobsRequest.Request(digest=re_pb2.Digest(hash="notadigest", size_bytes=3), data=b"abc"),
        ],
    )
    response_list = bs_service.BatchUpdateBlobs(update_request, mock_context())
    for response in response_list.responses:
        if response.digest.hash == "notadigest":
            assert response.status.code == code_pb2.INVALID_ARGUMENT
        else:
            assert response.status.code == code_pb2.OK


def register_directory(
    storage: StorageABC, sub_directories: dict[str, re_pb2.Digest] | None = None
) -> tuple[re_pb2.Directory, re_pb2.Digest]:
    directory = re_pb2.Directory(
        directories=[re_pb2.DirectoryNode(name=key, digest=value) for key, value in (sub_directories or {}).items()]
    )
    data = directory.SerializeToString()
    digest = re_pb2.Digest(hash=HASH(data).hexdigest(), size_bytes=len(data))
    storage.bulk_update_blobs([(digest, data)])
    return directory, digest


@pytest.mark.parametrize("instance_name", instances)
@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
@mock.patch("buildgrid.server.metrics_utils.create_counter_record", new=mock_create_counter_record)
def test_cas_get_tree(instance_name):
    """Directory Structure:
    |--root
       |--subEmptyDir
       |--subParentDir
          |--subChildDir
    """
    storage = LRUMemoryCache(1 << 32)

    child_dir, child_digest = register_directory(storage)
    parent_dir, parent_digest = register_directory(storage, {"child_dir": child_digest})
    empty_dir, empty_digest = register_directory(storage)
    root_dir, root_digest = register_directory(storage, {"empty_dir": empty_digest, "parent_dir": parent_digest})

    set_monitoring_bus(mock.Mock())

    cas_service = setup_cas_service(storage, instance_name)

    request = re_pb2.GetTreeRequest(instance_name=instance_name, root_digest=root_digest)
    responses = list(cas_service.GetTree(request, mock_context()))
    responses_total_size = sum(d.ByteSize() for d in responses)
    result = [d for res in responses for d in res.directories]
    assert result == [root_dir, empty_dir, parent_dir, child_dir]

    with rpc_context("ContentAddressableStorage", "GetTree", instance_name):
        call_list = [
            mock.call(mock_create_timer_record(METRIC.RPC.DURATION, status="OK")),
            mock.call(mock_create_counter_record(METRIC.RPC.OUTPUT_BYTES, responses_total_size, status="OK")),
        ]
    get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


@pytest.mark.parametrize("instance_name", instances)
def test_cas_get_tree_missing_root(instance_name):
    """
    Test that when the root digest is missing that a NOT_FOUND status is returned.
    """

    storage = LRUMemoryCache(1 << 32)

    missing_digest = re_pb2.Digest(hash=HASH("missing".encode()).hexdigest(), size_bytes=len("missing"))

    cas_service = setup_cas_service(storage, instance_name)

    request = re_pb2.GetTreeRequest(instance_name=instance_name, root_digest=missing_digest)
    context = mock_context()

    with pytest.raises(Exception):
        for _ in cas_service.GetTree(request, context):
            pass
    assert context.code() == grpc.StatusCode.NOT_FOUND


@pytest.mark.parametrize("instance_name", instances)
def test_cas_get_tree_page_size(instance_name):
    """
    Test that when a page size is requested, that we return results in the correct sizings.
    """
    storage = LRUMemoryCache(1 << 32)

    dir0, digest0 = register_directory(storage)
    dir1, digest1 = register_directory(storage, {"0": digest0})
    dir2, digest2 = register_directory(storage, {"1": digest1})
    dir3, digest3 = register_directory(storage, {"2": digest2})

    cas_service = setup_cas_service(storage, instance_name)

    request = re_pb2.GetTreeRequest(instance_name=instance_name, root_digest=digest3, page_size=2)

    results = [list(res.directories) for res in cas_service.GetTree(request, mock_context())]
    assert results == [[dir3, dir2], [dir1, dir0]]


@pytest.mark.parametrize("instance_name", instances)
def test_cas_get_tree_missing_child_dir(instance_name):
    """
    Test that a missing directory in the tree does not result in a failure, and is also not returned.
    """
    storage = LRUMemoryCache(1 << 32)

    missing_digest = re_pb2.Digest(hash=HASH("missing".encode()).hexdigest(), size_bytes=len("missing"))

    dir0, digest0 = register_directory(storage, {"missing": missing_digest})
    dir1, digest1 = register_directory(storage, {"0": digest0})
    dir2, digest2 = register_directory(storage, {"1": digest1})
    dir3, digest3 = register_directory(storage, {"2": digest2})

    cas_service = setup_cas_service(storage, instance_name)

    request = re_pb2.GetTreeRequest(instance_name=instance_name, root_digest=digest3)

    results = [d for res in cas_service.GetTree(request, mock_context()) for d in res.directories]
    assert results == [dir3, dir2, dir1, dir0]


@pytest.mark.parametrize("instance_name", instances)
def test_cas_get_tree_cache(instance_name: str) -> None:
    storage = LRUMemoryCache(1 << 32)
    dir0, digest0 = register_directory(storage)
    dir1, digest1 = register_directory(storage, {"0": digest0})
    dir2, digest2 = register_directory(storage, {"1": digest1})
    dir3, digest3 = register_directory(storage, {"2": digest2})

    cas_service = setup_cas_service(storage, instance_name, tree_cache_size=1)
    request = re_pb2.GetTreeRequest(instance_name=instance_name, root_digest=digest3, page_size=5)
    responses1 = [res for res in cas_service.GetTree(request, mock_context())]
    responses2 = [res for res in cas_service.GetTree(request, mock_context())]
    dirs1 = [d.directories for d in responses1[0].directories]
    dirs2 = [d.directories for d in responses2[0].directories]

    assert len(dirs1) == 4
    assert dirs1 == dirs2
    tree = re_pb2.Tree(root=dir3, children=[dir2, dir1, dir0])
    tree_digest = create_digest(tree.SerializeToString())
    # GetTreeResponse is cached in CAS
    response_in_storage = storage.get_message(tree_digest, re_pb2.Tree)
    assert response_in_storage is not None


@pytest.mark.parametrize("instance_name", instances)
def test_cas_get_tree_cache_page_size(instance_name: str) -> None:
    storage = LRUMemoryCache(1 << 32)
    dir0, digest0 = register_directory(storage)
    dir1, digest1 = register_directory(storage, {"0": digest0})
    dir2, digest2 = register_directory(storage, {"1": digest1})
    dir3, digest3 = register_directory(storage, {"2": digest2})

    cas_service = setup_cas_service(storage, instance_name, tree_cache_size=1)
    request = re_pb2.GetTreeRequest(instance_name=instance_name, root_digest=digest3, page_size=2)
    responses1 = [res for res in cas_service.GetTree(request, mock_context())]
    responses2 = [res for res in cas_service.GetTree(request, mock_context())]

    assert len(responses1) == len(responses2) == 2
    # For some reason we append an empty response at the end
    tree = re_pb2.Tree(root=dir3, children=[dir2, dir1, dir0])
    tree_digest = create_digest(tree.SerializeToString())
    # GetTreeResponse is cached in CAS
    response_in_storage = storage.get_message(tree_digest, re_pb2.Tree)
    assert response_in_storage is not None


@pytest.mark.parametrize("instance_name", instances)
def test_cas_get_tree_cache_miss(instance_name: str) -> None:
    storage = LRUMemoryCache(1 << 32)
    _, digest0 = register_directory(storage)
    _, digest1 = register_directory(storage, {"0": digest0})
    _, digest2 = register_directory(storage, {"1": digest1})
    _, digest3 = register_directory(storage, {"2": digest2})

    cas_service = setup_cas_service(storage, instance_name, tree_cache_size=1)
    request = re_pb2.GetTreeRequest(instance_name=instance_name, root_digest=digest3, page_size=5)
    responses1 = [res for res in cas_service.GetTree(request, mock_context())]
    response_message = responses1[0].SerializeToString()
    response_digest = create_digest(response_message)
    # Delete cached GetTreeResponse in CAS
    storage.delete_blob(response_digest)
    responses2 = [res for res in cas_service.GetTree(request, mock_context())]
    dirs1 = [d.directories for d in responses1[0].directories]
    dirs2 = [d.directories for d in responses2[0].directories]

    assert len(dirs1) == 4
    assert dirs1 == dirs2
