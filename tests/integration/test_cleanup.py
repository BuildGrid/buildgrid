# Copyright (C) 2019 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from datetime import timedelta
from unittest import mock

import boto3
import pytest

from buildgrid.server.app.settings.config import CleanupConfig
from buildgrid.server.cas.storage.index.sql import SQLIndex
from buildgrid.server.cas.storage.s3 import S3Storage
from buildgrid.server.cleanup.cleanup import CASCleanUp
from buildgrid.server.monitoring import set_monitoring_bus
from buildgrid.server.sql.provider import SqlProvider
from buildgrid.server.utils.digests import create_digest
from tests.utils.fixtures import Moto

BLOBS = [(f"{i}" * 5).encode() for i in range(10)]
BLOBS_DIGESTS = [create_digest(blob) for blob in BLOBS]


@pytest.fixture(autouse=True)
def monitoring():
    set_monitoring_bus(mock.Mock())


@pytest.fixture
def index(moto_server: Moto):
    boto3.resource("s3", **moto_server.opts).create_bucket(Bucket=moto_server.bucket("testing"))
    # limit page size to test functionality
    storage = S3Storage(moto_server.bucket("testing"), page_size=3, **moto_server.opts)
    return SQLIndex(SqlProvider(), storage)


def test_cleanup(index):
    """
    Set the high watermark to 0. See that all blobs added are deleted.
    """
    config = CleanupConfig(
        name="test",
        index=index,
        batch_size=10,
        high_watermark=0,
        low_watermark=0,
        high_blob_count_watermark=None,
        low_blob_count_watermark=None,
        only_if_unused_for=timedelta(0),
        large_blob_lifetime=None,
        large_blob_threshold=None,
        retry_limit=1,
    )
    cleanup = CASCleanUp(
        cleanup_configs=[config],
        dry_run=False,
        sleep_interval=0.1,
        monitor=False,
    )
    index.bulk_update_blobs(list(zip(BLOBS_DIGESTS, BLOBS)))
    assert index.missing_blobs(BLOBS_DIGESTS) == []
    try:
        cleanup.start(timeout=1)
    finally:
        cleanup.stop()
    assert index.missing_blobs(BLOBS_DIGESTS) == BLOBS_DIGESTS


def test_cleanup_not_yet_expired(index):
    """
    Set the high watermark to 0. See we don't delete any blobs
    """
    config = CleanupConfig(
        name="test",
        index=index,
        batch_size=10,
        high_watermark=0,
        low_watermark=0,
        high_blob_count_watermark=None,
        low_blob_count_watermark=None,
        only_if_unused_for=timedelta(days=1),
        large_blob_lifetime=None,
        large_blob_threshold=None,
        retry_limit=1,
    )
    cleanup = CASCleanUp(
        dry_run=False,
        cleanup_configs=[config],
        sleep_interval=0.1,
        monitor=False,
    )
    index.bulk_update_blobs(list(zip(BLOBS_DIGESTS, BLOBS)))
    assert index.missing_blobs(BLOBS_DIGESTS) == []
    try:
        cleanup.start(timeout=1)
    finally:
        cleanup.stop()
    assert index.missing_blobs(BLOBS_DIGESTS) == []


def test_cleanup_half_the_data(index):
    """
    Test that we stop at the low watermark when deleting.
    """
    index.bulk_update_blobs(list(zip(BLOBS_DIGESTS, BLOBS)))
    assert index.missing_blobs(BLOBS_DIGESTS) == []
    start_size = index.get_total_size()

    config = CleanupConfig(
        name="test",
        index=index,
        batch_size=5,
        high_watermark=start_size - 1,
        low_watermark=start_size / 2,
        high_blob_count_watermark=None,
        low_blob_count_watermark=None,
        only_if_unused_for=timedelta(),
        large_blob_lifetime=None,
        large_blob_threshold=None,
        retry_limit=1,
    )
    cleanup = CASCleanUp(
        dry_run=False,
        sleep_interval=0.1,
        cleanup_configs=[config],
        monitor=False,
    )
    try:
        cleanup.start(timeout=1)
    finally:
        cleanup.stop()
    assert index.get_total_size() == start_size / 2


def test_cleanup_large_blob_lifetime(index):
    """
    Test that the large blob lifetime options allow cleaning up large blobs
    without touching smaller blobs
    """
    # Add an additional blob larger than the rest
    large_blob = ("1" * 25).encode()
    large_blob_digest = create_digest(large_blob)

    index.bulk_update_blobs(list(zip(BLOBS_DIGESTS + [large_blob_digest], BLOBS + [large_blob])))
    assert index.missing_blobs(BLOBS_DIGESTS) == []
    start_size = index.get_total_size()

    config = CleanupConfig(
        name="test",
        index=index,
        batch_size=5,
        high_watermark=start_size - 1,
        low_watermark=0,
        high_blob_count_watermark=None,
        low_blob_count_watermark=None,
        only_if_unused_for=timedelta(days=1),
        large_blob_lifetime=timedelta(),
        large_blob_threshold=large_blob_digest.size_bytes - 3,
        retry_limit=1,
    )
    cleanup = CASCleanUp(
        dry_run=False,
        sleep_interval=0.1,
        cleanup_configs=[config],
        monitor=False,
    )
    try:
        cleanup.start(timeout=1)
    finally:
        cleanup.stop()
    assert index.get_total_size() == start_size - large_blob_digest.size_bytes


def test_cleanup_half_the_data_multiple_instances(index, moto_server: Moto):
    """
    Test that we stop at the low watermark when deleting and cleanup
    works with multiple instances
    """
    index.bulk_update_blobs(list(zip(BLOBS_DIGESTS, BLOBS)))
    assert index.missing_blobs(BLOBS_DIGESTS) == []
    start_size = index.get_total_size()

    # Create a second storage and index:
    boto3.resource("s3", **moto_server.opts).create_bucket(Bucket=moto_server.bucket("testing2"))
    # limit page size to test functionality
    storage2 = S3Storage(moto_server.bucket("testing2"), page_size=3, **moto_server.opts)
    index2 = SQLIndex(SqlProvider(), storage2)
    index2.bulk_update_blobs(list(zip(BLOBS_DIGESTS, BLOBS)))
    assert index2.missing_blobs(BLOBS_DIGESTS) == []
    assert start_size == index2.get_total_size()

    config = CleanupConfig(
        name="test",
        index=index,
        batch_size=5,
        high_watermark=start_size - 1,
        low_watermark=start_size / 2,
        high_blob_count_watermark=None,
        low_blob_count_watermark=None,
        only_if_unused_for=timedelta(),
        large_blob_lifetime=None,
        large_blob_threshold=None,
        retry_limit=1,
    )
    config2 = CleanupConfig(
        name="test2",
        index=index2,
        batch_size=5,
        high_watermark=start_size - 1,
        low_watermark=start_size / 2,
        high_blob_count_watermark=None,
        low_blob_count_watermark=None,
        only_if_unused_for=timedelta(),
        large_blob_lifetime=None,
        large_blob_threshold=None,
        retry_limit=1,
    )
    cleanup = CASCleanUp(
        dry_run=False,
        sleep_interval=0.1,
        cleanup_configs=[config, config2],
        monitor=False,
    )
    try:
        cleanup.start(timeout=1)
    finally:
        cleanup.stop()
    assert index.get_total_size() == start_size / 2
    assert index2.get_total_size() == start_size / 2


def test_blob_count_cleanup(index):
    """
    Set the high blob count watermark to 0. See that all blobs added are deleted.
    """
    config = CleanupConfig(
        name="test",
        index=index,
        batch_size=10,
        high_watermark=1024**3,
        low_watermark=1024**3,
        high_blob_count_watermark=0,
        low_blob_count_watermark=0,
        only_if_unused_for=timedelta(0),
        large_blob_lifetime=None,
        large_blob_threshold=None,
        retry_limit=1,
    )
    cleanup = CASCleanUp(
        cleanup_configs=[config],
        dry_run=False,
        sleep_interval=0.1,
        monitor=False,
    )
    index.bulk_update_blobs(list(zip(BLOBS_DIGESTS, BLOBS)))
    assert index.missing_blobs(BLOBS_DIGESTS) == []
    try:
        cleanup.start(timeout=1)
    finally:
        cleanup.stop()
    assert index.missing_blobs(BLOBS_DIGESTS) == BLOBS_DIGESTS


def test_cleanup_half_the_blobs(index):
    """
    Test that we stop at the low blob count watermark when deleting.
    """
    index.bulk_update_blobs(list(zip(BLOBS_DIGESTS, BLOBS)))
    assert index.missing_blobs(BLOBS_DIGESTS) == []
    start_count = index.get_blob_count()

    config = CleanupConfig(
        name="test",
        index=index,
        batch_size=1,
        high_watermark=1024**3,
        low_watermark=1024**3,
        high_blob_count_watermark=start_count - 1,
        low_blob_count_watermark=start_count // 2,
        only_if_unused_for=timedelta(),
        large_blob_lifetime=None,
        large_blob_threshold=None,
        retry_limit=1,
    )
    cleanup = CASCleanUp(
        dry_run=False,
        sleep_interval=0.1,
        cleanup_configs=[config],
        monitor=False,
    )
    try:
        cleanup.start(timeout=1)
    finally:
        cleanup.stop()
    assert index.get_blob_count() == start_count // 2
