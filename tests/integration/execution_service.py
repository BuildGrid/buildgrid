# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import tempfile
import time
import uuid
from contextlib import ExitStack
from unittest import mock

import grpc
import pytest

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid._protos.google.longrunning import operations_pb2
from buildgrid.server.actioncache.caches.lru_cache import LruActionCache
from buildgrid.server.cas.storage import lru_memory_cache
from buildgrid.server.cas.storage.storage_abc import create_write_session
from buildgrid.server.context import instance_context
from buildgrid.server.controller import ExecutionController
from buildgrid.server.enums import BotStatus, LeaseState, OperationStage
from buildgrid.server.execution.service import ExecutionService
from buildgrid.server.metrics_names import METRIC
from buildgrid.server.monitoring import get_monitoring_bus, set_monitoring_bus
from buildgrid.server.scheduler import Scheduler
from buildgrid.server.scheduler.properties import hash_from_dict
from buildgrid.server.sql import models
from buildgrid.server.sql.provider import SqlProvider
from buildgrid.server.utils.digests import create_digest
from tests.utils.fixtures import pg_schema_provider
from tests.utils.metrics import mock_create_counter_record, mock_create_timer_record
from tests.utils.mocks import (
    MOCK_ACTOR,
    MOCK_CORRELATED_INVOCATIONS_ID,
    MOCK_INVOCATION_ID,
    MOCK_SUBJECT,
    MOCK_TOOL_NAME,
    MOCK_TOOL_VERSION,
    MOCK_WORKFLOW,
    mock_context,
    mock_extract_client_identity,
    mock_extract_request_metadata,
    rpc_context,
)

server = mock.create_autospec(grpc.Server)

command = remote_execution_pb2.Command()
command.platform.properties.add(name="OSFamily", value="linux")
command.platform.properties.add(name="ISA", value="x86")
command_digest = create_digest(command.SerializeToString())

action = remote_execution_pb2.Action(command_digest=command_digest, do_not_cache=True)
action_digest = create_digest(action.SerializeToString())


@pytest.fixture()
def sql_provider():
    db = tempfile.NamedTemporaryFile().name
    yield SqlProvider(connection_string=f"sqlite:///{db}", automigrate=True)


PARAMS = [(impl, use_cache) for impl in ["sqlite", "postgresql"] for use_cache in ["action-cache", "no-action-cache"]]


@pytest.fixture(params=("sqlite", "postgresql"))
def scheduler(request, postgres, sql_provider, common_props):
    impl = request.param
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)

    with create_write_session(command_digest) as write_session:
        write_session.write(command.SerializeToString())
        storage.commit_write(command_digest, write_session)

    with create_write_session(action_digest) as write_session:
        write_session.write(action.SerializeToString())
        storage.commit_write(action_digest, write_session)

    if impl == "sqlite":
        yield Scheduler(
            sql_provider,
            storage,
            poll_interval=0.01,
            property_set=common_props,
            job_assignment_interval=0.1,
        )
    elif impl == "postgresql":
        with pg_schema_provider(postgres) as sql_provider:
            yield Scheduler(
                sql_provider,
                storage,
                poll_interval=0.01,
                property_set=common_props,
                job_assignment_interval=0.1,
            )


@pytest.fixture(params=("action-cache", "no-action-cache", "keepalive"))
def controller(request, scheduler, sql_provider):
    with ExitStack() as stack:
        controller = ExecutionController(scheduler)
        stack.enter_context(controller.execution_instance)
        stack.enter_context(controller.bots_interface)
        stack.enter_context(controller.operations_instance)

        if request.param == "action-cache":
            scheduler.cache = LruActionCache(scheduler.storage, 50)
        elif request.param == "no-action-cache":
            pass
        elif request.param == "keepalive":
            controller.operations_instance._max_list_operations_page_size = 1
        else:
            assert False, "Unexpected fixture param"

        yield controller


# Instance to test
@pytest.fixture()
def instance(controller, request):
    execution_service = ExecutionService()
    execution_service.add_instance("", controller.execution_instance)
    yield execution_service


def mock_queue_job_action(scheduler, action=action, action_digest=action_digest, priority=0):
    with instance_context(""):
        bot_id = "test-worker"
        bot_name = scheduler.add_bot_entry(bot_session_id=bot_id, bot_session_status=BotStatus.OK.value)
        operation_name = scheduler.queue_job_action(
            action=action,
            action_digest=action_digest,
            command=command,
            platform_requirements={},
            property_label="unknown",
            priority=priority,
            skip_cache_lookup=True,
        )
        scheduler.assign_n_leases_by_priority(capability_hash=hash_from_dict({}), bot_names=[bot_name])
        job_name = scheduler.get_operation_job_name(operation_name)
        return bot_name, bot_id, operation_name, job_name


@pytest.mark.parametrize("skip_cache_lookup", [True, False])
@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
@mock.patch("buildgrid.server.metrics_utils.create_counter_record", new=mock_create_counter_record)
@mock.patch("buildgrid.server.execution.service.extract_request_metadata", new=mock_extract_request_metadata)
@mock.patch("buildgrid.server.execution.service.extract_client_identity", new=mock_extract_client_identity)
def test_execute(scheduler, skip_cache_lookup, instance, controller):
    set_monitoring_bus(mock.Mock())

    request = remote_execution_pb2.ExecuteRequest(
        instance_name="", action_digest=action_digest, skip_cache_lookup=skip_cache_lookup
    )
    response = instance.Execute(request, mock_context())

    result = next(response)
    assert isinstance(result, operations_pb2.Operation)
    metadata = remote_execution_pb2.ExecuteOperationMetadata()
    result.metadata.Unpack(metadata)
    assert metadata.stage == OperationStage.QUEUED.value
    operation_uuid = result.name.split("/")[-1]
    assert uuid.UUID(operation_uuid, version=4)
    assert result.done is False

    with scheduler._sql.session() as session:
        operation: models.OperationEntry = session.query(models.OperationEntry).filter_by(name=operation_uuid).one()
        # assert execute request metadata
        assert operation.request_metadata.tool_name == MOCK_TOOL_NAME
        assert operation.request_metadata.tool_version == MOCK_TOOL_VERSION
        assert operation.request_metadata.invocation_id == MOCK_INVOCATION_ID
        assert operation.request_metadata.correlated_invocations_id == MOCK_CORRELATED_INVOCATIONS_ID
        # assert client identity
        assert operation.client_identity.instance == ""
        assert operation.client_identity.workflow == MOCK_WORKFLOW
        assert operation.client_identity.actor == MOCK_ACTOR
        assert operation.client_identity.subject == MOCK_SUBJECT

        capability_hash = operation.job.platform_requirements

    if controller.execution_instance._operation_stream_keepalive_timeout is not None:
        # If keepalive is enabled, we can get a keepalive message before the job
        # completes. If keepalive was disabled, this would hang.
        keepalive_message = next(response)
        assert not keepalive_message.done

    time.sleep(scheduler.poll_interval * 2)

    # Assign a bot and move the job to completed.
    bot_id = "test-worker"
    bot_name = scheduler.add_bot_entry(bot_session_id=bot_id, bot_session_status=BotStatus.OK.value)
    scheduler.assign_n_leases_by_priority(capability_hash=capability_hash, bot_names=[bot_name])

    # Fetch the new lease
    lease = scheduler.synchronize_bot_lease(bot_name, bot_id, BotStatus.OK.value, None)
    assert lease.state == LeaseState.PENDING.value

    # Ack the pending lease into active
    lease.state = LeaseState.ACTIVE.value
    lease = scheduler.synchronize_bot_lease(bot_name, bot_id, BotStatus.OK.value, lease)
    assert lease.state == LeaseState.ACTIVE.value

    # Send a keepalive
    lease = scheduler.synchronize_bot_lease(bot_name, bot_id, BotStatus.OK.value, lease)
    assert lease.state == LeaseState.ACTIVE.value

    # Complete the lease
    lease.state = LeaseState.COMPLETED.value
    lease = scheduler.synchronize_bot_lease(bot_name, bot_id, BotStatus.OK.value, lease)
    assert lease is None

    # We just moved the job to completed, so we should just have one item reported... the done state.
    assert len(list(response)) == 1

    # Asserting that metrics were collected:
    with rpc_context("Execution", "Execute", ""):
        call_list = [
            mock.call(mock_create_timer_record(METRIC.RPC.DURATION, status="OK")),
        ]
    get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


def test_wrong_execute_instance(instance):
    context = mock_context()
    request = remote_execution_pb2.ExecuteRequest(instance_name="blade")

    with pytest.raises(Exception):
        response = instance.Execute(request, context)
        list(response)
    assert context.code() == grpc.StatusCode.INVALID_ARGUMENT


def test_no_action_digest_in_storage(instance):
    context = mock_context()
    request = remote_execution_pb2.ExecuteRequest(instance_name="", skip_cache_lookup=True)

    with pytest.raises(Exception):
        response = instance.Execute(request, context)
        next(response)
    assert context.code() == grpc.StatusCode.FAILED_PRECONDITION


@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
@mock.patch("buildgrid.server.metrics_utils.create_counter_record", new=mock_create_counter_record)
def test_wait_execution(instance, controller):
    set_monitoring_bus(mock.Mock())
    scheduler = controller.execution_instance.scheduler

    scheduler.max_job_attempts = 0
    bot_name, bot_id, operation_name, job_name = mock_queue_job_action(scheduler)
    with instance_context(""):
        scheduler.close_bot_sessions(bot_name)

    request = remote_execution_pb2.WaitExecutionRequest(name=operation_name)

    response = instance.WaitExecution(request, mock_context())

    result = next(response)

    assert isinstance(result, operations_pb2.Operation)
    metadata = remote_execution_pb2.ExecuteOperationMetadata()
    result.metadata.Unpack(metadata)
    assert metadata.stage == OperationStage.COMPLETED.value
    assert result.done is True

    with scheduler._sql.session() as session:
        record = session.query(models.JobEntry).filter_by(name=job_name).first()
        assert record is not None
        assert record.stage == OperationStage.COMPLETED.value
        assert record.operations

    try:
        # This should raise StopIteration. If something changes in the internals of the
        # operation update streaming, this may hang indefinitely.
        next(response)
    except StopIteration:
        # Good, this is what we wanted to happen. We need to use up the whole generator
        # here so that the duration metric actually gets published.
        pass

    # Asserting that metrics were collected:
    with rpc_context("Execution", "WaitExecution", ""):
        call_list = [mock.call(mock_create_timer_record(METRIC.RPC.DURATION, status="OK"))]
    get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


def test_wrong_instance_wait_execution(instance):
    context = mock_context()

    with pytest.raises(Exception):
        request = remote_execution_pb2.WaitExecutionRequest(name="blade/operation")
        next(instance.WaitExecution(request, context))

    assert context.code() == grpc.StatusCode.INVALID_ARGUMENT


def test_missing_operation_wait_execution(instance):
    context = mock_context()

    with pytest.raises(Exception):
        request = remote_execution_pb2.WaitExecutionRequest(name="/missing-operation")
        next(instance.WaitExecution(request, context))

    assert context.code() == grpc.StatusCode.NOT_FOUND


def test_job_deduplication_in_scheduling(instance, controller):
    scheduler = controller.execution_instance.scheduler

    action = remote_execution_pb2.Action(command_digest=command_digest, do_not_cache=False)
    action_digest = create_digest(action.SerializeToString())

    bot_name1, bot_id1, operation_name1, job_name1 = mock_queue_job_action(
        scheduler, action=action, action_digest=action_digest
    )
    bot_name2, bot_id2, operation_name2, job_name2 = mock_queue_job_action(
        scheduler, action=action, action_digest=action_digest
    )

    # The jobs are be deduplicated, but separate operations are created
    assert job_name1 == job_name2
    assert operation_name1 != operation_name2

    with scheduler._sql.session() as session:
        query = session.query(models.JobEntry)
        job_count = query.filter_by(name=job_name1).count()
        assert job_count == 1
        query = session.query(models.OperationEntry)
        operation_count = query.filter_by(job_name=job_name1).count()
        assert operation_count == 2


def test_job_reprioritisation(instance, controller):
    scheduler = controller.execution_instance.scheduler

    action = remote_execution_pb2.Action(command_digest=command_digest)
    action_digest = create_digest(action.SerializeToString())

    bot_name1, bot_id1, operation_name1, job_name1 = mock_queue_job_action(
        scheduler, action=action, action_digest=action_digest, priority=10
    )

    with scheduler._sql.session() as session, instance_context(""):
        assert scheduler._get_job(job_name1, session).priority == 10

    bot_name2, bot_id2, operation_name2, job_name2 = mock_queue_job_action(
        scheduler, action=action, action_digest=action_digest, priority=1
    )

    assert job_name1 == job_name2
    with scheduler._sql.session() as session, instance_context(""):
        assert scheduler._get_job(job_name1, session).priority == 1


@pytest.mark.parametrize("do_not_cache", [True, False])
def test_do_not_cache_no_deduplication(do_not_cache, instance, controller):
    scheduler = controller.execution_instance.scheduler

    # The default action already has do_not_cache set, so use that
    bot_name1, bot_id1, operation_name1, job_name1 = mock_queue_job_action(scheduler)

    action2 = remote_execution_pb2.Action(command_digest=command_digest, do_not_cache=do_not_cache)
    action_digest2 = create_digest(action2.SerializeToString())

    bot_name2, bot_id2, operation_name2, job_name2 = mock_queue_job_action(
        scheduler, action=action2, action_digest=action_digest2
    )

    # The jobs are not be deduplicated because of do_not_cache,
    # and two operations are created
    assert job_name1 != job_name2
    assert operation_name1 != operation_name2

    with scheduler._sql.session() as session:
        job_count = session.query(models.JobEntry).count()
        assert job_count == 2

        operation_count = session.query(models.OperationEntry).count()
        assert operation_count == 2


@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_cancel_execution(instance, controller):
    set_monitoring_bus(mock.Mock())
    # Verifying that the `Job.worker_completed_timestamp` field is set
    # for cancelled jobs.
    scheduler = controller.execution_instance.scheduler
    bot_name, bot_id, operation_name, job_name = mock_queue_job_action(scheduler)

    with instance_context(""):
        scheduler.cancel_operation(operation_name)

    with scheduler._sql.session() as session:
        record = session.query(models.JobEntry).filter_by(name=job_name).first()
        assert record is not None
        assert record.stage == OperationStage.COMPLETED.value
        assert record.operations
        assert record.worker_completed_timestamp is not None
