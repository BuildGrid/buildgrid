# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import tempfile
from unittest import mock

import grpc
import pytest
from google.protobuf import any_pb2

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid._protos.google.longrunning import operations_pb2
from buildgrid.server.cas.storage import lru_memory_cache
from buildgrid.server.cas.storage.storage_abc import create_write_session
from buildgrid.server.context import instance_context
from buildgrid.server.controller import ExecutionController
from buildgrid.server.enums import OperationStage
from buildgrid.server.metrics_names import METRIC
from buildgrid.server.monitoring import get_monitoring_bus, set_monitoring_bus
from buildgrid.server.operations.service import OperationsService
from buildgrid.server.scheduler import Scheduler
from buildgrid.server.sql.provider import SqlProvider
from buildgrid.server.utils.digests import create_digest
from tests.utils.metrics import mock_create_timer_record
from tests.utils.mocks import mock_context, rpc_context

server = mock.MagicMock()
instance_name = "blade"

basic_command = remote_execution_pb2.Command()
basic_command.platform.properties.add(name="OSFamily", value="linux")
basic_command.platform.properties.add(name="ISA", value="x86")
basic_command_digest = create_digest(basic_command.SerializeToString())

basic_action = remote_execution_pb2.Action(command_digest=basic_command_digest, do_not_cache=True)
basic_action_digest = create_digest(basic_action.SerializeToString())

fake_action_browser_url = "https://action-browser"


@pytest.fixture
def commands():
    os_families = ["aix", "freebsd", "linux", "macos", "sunos"]
    isas = ["x86-32", "x86-64"]
    command_digest_pairs = []
    for i in range(0, 10):
        command = remote_execution_pb2.Command()
        command.platform.properties.add(name="OSFamily", value=os_families[i % len(os_families)])
        command.platform.properties.add(name="ISA", value=isas[i % len(isas)])
        command.arguments.extend(["test", "-i", f"{i % 2}"])
        command_digest_pairs.append((command, create_digest(command.SerializeToString())))
    return command_digest_pairs


@pytest.fixture
def actions(commands):
    action_digest_pairs = []
    for _, digest in commands:
        action = remote_execution_pb2.Action(command_digest=digest, do_not_cache=True)
        action_digest_pairs.append((action, create_digest(action.SerializeToString())))
    return action_digest_pairs


# Requests to make
@pytest.fixture
def execute_request():
    yield remote_execution_pb2.ExecuteRequest(
        instance_name="", action_digest=basic_action_digest, skip_cache_lookup=True
    )


@pytest.fixture
def controller(commands, actions, common_props):
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)
    db = tempfile.NamedTemporaryFile().name
    sql_provider = SqlProvider(connection_string=f"sqlite:///{db}", automigrate=True)

    with create_write_session(basic_command_digest) as write_session:
        write_session.write(basic_command.SerializeToString())
        storage.commit_write(basic_command_digest, write_session)

    with create_write_session(basic_action_digest) as write_session:
        write_session.write(basic_action.SerializeToString())
        storage.commit_write(basic_action_digest, write_session)

    for command, digest in commands:
        with create_write_session(digest) as write_session:
            write_session.write(command.SerializeToString())
            storage.commit_write(digest, write_session)

    for action, digest in actions:
        with create_write_session(digest) as write_session:
            write_session.write(action.SerializeToString())
            storage.commit_write(digest, write_session)

    with Scheduler(
        sql_provider, storage, property_set=common_props, action_browser_url=fake_action_browser_url
    ) as scheduler:
        yield ExecutionController(scheduler, max_list_operations_page_size=10)


# Instance to test
@pytest.fixture
def instance(controller):
    operation_service = OperationsService()
    operation_service.add_instance(instance_name, controller.operations_instance)
    yield operation_service


# Blank instance
@pytest.fixture
def blank_instance(controller, request):
    operation_service = OperationsService()
    operation_service.add_instance("", controller.operations_instance)

    yield operation_service


# Queue an execution, get operation corresponding to that request
@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_get_operation(instance, controller, execute_request):
    set_monitoring_bus(mock.Mock())

    with instance_context(instance_name):
        operation_name = controller.execution_instance.execute(
            action_digest=execute_request.action_digest,
            skip_cache_lookup=execute_request.skip_cache_lookup,
            priority=0,
        )

    request = operations_pb2.GetOperationRequest()

    # The execution instance name is normally set in add_instance, but since
    # we're manually creating the instance here, it doesn't get a name.
    # Therefore we need to manually add the instance name to the operation
    # name in the GetOperation request.
    request.name = f"{instance_name}/{operation_name}"

    response = instance.GetOperation(request, mock_context())
    assert response.name == f"{instance_name}/{operation_name}"

    with rpc_context("Operations", "GetOperation", instance_name):
        call_list = [mock.call(mock_create_timer_record(METRIC.RPC.DURATION, status="OK"))]
    get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


# Queue an execution, get operation corresponding to that request
def test_get_operation_blank(blank_instance, controller, execute_request):
    with instance_context(""):
        operation_name = controller.execution_instance.execute(
            action_digest=execute_request.action_digest,
            skip_cache_lookup=execute_request.skip_cache_lookup,
            priority=0,
        )

    request = operations_pb2.GetOperationRequest()

    request.name = operation_name

    response = blank_instance.GetOperation(request, mock_context())
    assert response.name == operation_name


def test_get_operation_browser_url(instance, controller):
    #
    # Submit operation manually, as buildgrid does not have code to
    # submit operation that adds execute_response (only workers do)
    #
    scheduler = controller.execution_instance.scheduler
    with instance_context(instance_name):
        operation_name = scheduler.create_operation_for_new_job(
            action=basic_action,
            action_digest=basic_action_digest,
            command=basic_command,
            execute_response=remote_execution_pb2.ExecuteResponse(),
            platform_requirements={},
            property_label="unknown",
            priority=0,
        )

    request = operations_pb2.GetOperationRequest()
    request.name = f"{instance_name}/{operation_name}"  # See comment in test_get_operation()
    operation = instance.GetOperation(request, mock_context())

    metadata = remote_execution_pb2.ExecuteOperationMetadata()
    operation.metadata.Unpack(metadata)
    digest = metadata.action_digest

    exec_response = remote_execution_pb2.ExecuteResponse()
    operation.response.Unpack(exec_response)

    assert exec_response.message == f"{fake_action_browser_url}/action/{digest.hash}/{digest.size_bytes}/"


def test_get_operation_fail(instance):
    context = mock_context()
    request = operations_pb2.GetOperationRequest()

    request.name = f"{instance_name}/runner"

    with pytest.raises(Exception):
        instance.GetOperation(request, context)

    assert context.code() == grpc.StatusCode.NOT_FOUND


def test_get_operation_instance_fail(instance):
    context = mock_context()
    request = operations_pb2.GetOperationRequest()

    with pytest.raises(Exception):
        instance.GetOperation(request, context)

    assert context.code() == grpc.StatusCode.INVALID_ARGUMENT


@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_list_operations(instance, controller, execute_request):
    set_monitoring_bus(mock.Mock())

    with instance_context(instance_name):
        operation_name = controller.execution_instance.execute(
            action_digest=execute_request.action_digest,
            skip_cache_lookup=execute_request.skip_cache_lookup,
            priority=0,
        )

    request = operations_pb2.ListOperationsRequest(name=instance_name)
    response = instance.ListOperations(request, mock_context())

    names = response.operations[0].name.split("/")
    assert names[0] == instance_name
    assert names[1] == operation_name

    with rpc_context("Operations", "ListOperations", instance_name):
        call_list = [mock.call(mock_create_timer_record(METRIC.RPC.DURATION, status="OK"))]
    get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


def test_list_operations_blank(blank_instance, controller, execute_request):
    with instance_context(""):
        operation_name = controller.execution_instance.execute(
            action_digest=execute_request.action_digest,
            skip_cache_lookup=execute_request.skip_cache_lookup,
            priority=0,
        )

    request = operations_pb2.ListOperationsRequest(name="")
    response = blank_instance.ListOperations(request, mock_context())

    assert response.operations[0].name.split("/")[-1] == operation_name


def test_list_operations_instance_fail(instance, controller, execute_request):
    context = mock_context()

    with instance_context(instance_name):
        controller.execution_instance.execute(
            action_digest=execute_request.action_digest,
            skip_cache_lookup=execute_request.skip_cache_lookup,
            priority=0,
        )

    request = operations_pb2.ListOperationsRequest()

    with pytest.raises(Exception):
        instance.ListOperations(request, context)

    assert context.code() == grpc.StatusCode.INVALID_ARGUMENT


def test_list_operations_empty(instance):
    request = operations_pb2.ListOperationsRequest(name=instance_name)

    response = instance.ListOperations(request, mock_context())

    assert not response.operations


def test_list_operations_mem_pagination_unsupported(instance, controller):
    context = mock_context()
    request = operations_pb2.ListOperationsRequest(name=instance_name)
    request.page_token = "some-page-token"

    with pytest.raises(Exception):
        instance.ListOperations(request, context)
    assert context.code() == grpc.StatusCode.INVALID_ARGUMENT


def test_list_operations_paginated_sql_only(instance, controller, execute_request):
    # Register a few peers, simulating repeats of the same request to generate multiple operations
    for _ in range(20):
        with instance_context(instance_name):
            controller.execution_instance.execute(
                action_digest=execute_request.action_digest,
                skip_cache_lookup=execute_request.skip_cache_lookup,
                priority=0,
            )
    request = operations_pb2.ListOperationsRequest(name=instance_name)
    response = instance.ListOperations(request, mock_context())

    assert response.next_page_token
    next_request = operations_pb2.ListOperationsRequest(name=instance_name, page_token=response.next_page_token)
    next_response = instance.ListOperations(next_request, mock_context())
    assert not next_response.next_page_token


def test_list_operations_page_size_too_large(instance):
    context = mock_context()
    request = operations_pb2.ListOperationsRequest(name=instance_name)
    request.page_size = 999999

    with pytest.raises(Exception):
        instance.ListOperations(request, context)
    assert request.page_size > instance.get_instance(instance_name)._max_list_operations_page_size
    assert context.code() == grpc.StatusCode.INVALID_ARGUMENT


def test_list_operations_filter(instance, controller):
    # Make a few execute requests
    for _ in range(10):
        execute_request = remote_execution_pb2.ExecuteRequest(
            instance_name=instance_name, action_digest=basic_action_digest, skip_cache_lookup=True
        )

        with instance_context(instance_name):
            controller.execution_instance.execute(
                action_digest=execute_request.action_digest,
                skip_cache_lookup=execute_request.skip_cache_lookup,
                priority=0,
            )

    request = operations_pb2.ListOperationsRequest(name=instance_name)
    response = instance.ListOperations(request, mock_context())
    assert len(response.operations) == 10
    operation_names = []
    for operation in response.operations:
        operation_names.append(operation.name)

    middle_operation = sorted(operation_names)[len(operation_names) // 2]
    # The operation names displayed are unfortunately prepended with the instance name,
    # so strip that away for use in the filtering
    middle_operation = middle_operation[middle_operation.find("/") + 1 :]

    request = operations_pb2.ListOperationsRequest(name=instance_name, filter=f"name >= {middle_operation}")
    response = instance.ListOperations(request, mock_context())
    assert len(response.operations) == 5


def test_list_operations_platform_filter(instance, controller, actions):
    for _, digest in actions:
        execute_request = remote_execution_pb2.ExecuteRequest(
            instance_name=instance_name, action_digest=digest, skip_cache_lookup=True
        )
        with instance_context(instance_name):
            controller.execution_instance.execute(
                action_digest=execute_request.action_digest,
                skip_cache_lookup=execute_request.skip_cache_lookup,
                priority=0,
            )

    request = operations_pb2.ListOperationsRequest(name=instance_name, filter="platform = OSFamily:linux")
    response = instance.ListOperations(request, mock_context())

    assert len(response.operations) == 2

    request = operations_pb2.ListOperationsRequest(name=instance_name, filter="platform != OSFamily:linux")
    response = instance.ListOperations(request, mock_context())

    assert len(response.operations) == 8


def test_list_operations_command_filter(instance, controller, actions):
    for _, digest in actions:
        execute_request = remote_execution_pb2.ExecuteRequest(
            instance_name=instance_name, action_digest=digest, skip_cache_lookup=True
        )
        with instance_context(instance_name):
            controller.execution_instance.execute(
                action_digest=execute_request.action_digest,
                skip_cache_lookup=execute_request.skip_cache_lookup,
                priority=0,
            )

    request = operations_pb2.ListOperationsRequest(name=instance_name, filter="command = test")
    response = instance.ListOperations(request, mock_context())

    assert len(response.operations) == 10

    request = operations_pb2.ListOperationsRequest(name=instance_name, filter="command = 0")
    response = instance.ListOperations(request, mock_context())

    assert len(response.operations) == 5


@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_delete_operation_unsupported(instance):
    """DeleteOperation is not supported."""
    context = mock_context()
    set_monitoring_bus(mock.Mock())
    request = operations_pb2.DeleteOperationRequest()
    request.name = f"{instance_name}/runner"
    instance.DeleteOperation(request, context)

    assert context.code() == grpc.StatusCode.UNIMPLEMENTED


@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_cancel_operation(instance, controller, execute_request):
    set_monitoring_bus(mock.Mock())

    with instance_context(instance_name):
        operation_name = controller.execution_instance.execute(
            action_digest=execute_request.action_digest,
            skip_cache_lookup=execute_request.skip_cache_lookup,
            priority=0,
        )

    request = operations_pb2.CancelOperationRequest()
    request.name = f"{instance_name}/{operation_name}"

    instance.CancelOperation(request, mock_context())

    request = operations_pb2.GetOperationRequest()
    request.name = f"{instance_name}/{operation_name}"
    response = instance.GetOperation(request, mock_context())

    operation_metadata = remote_execution_pb2.ExecuteOperationMetadata()
    response.metadata.Unpack(operation_metadata)
    assert operation_metadata.stage == OperationStage.COMPLETED.value

    with rpc_context("Operations", "CancelOperation", instance_name):
        call_list = [mock.call(mock_create_timer_record(METRIC.RPC.DURATION, status="OK"))]
    get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


def test_cancel_operation_blank(blank_instance):
    context = mock_context()
    request = operations_pb2.CancelOperationRequest()
    request.name = "runner"

    with pytest.raises(Exception):
        blank_instance.CancelOperation(request, context)

    assert context.code() == grpc.StatusCode.INVALID_ARGUMENT


def test_cancel_operation__fail(instance):
    context = mock_context()
    request = operations_pb2.CancelOperationRequest()

    with pytest.raises(Exception):
        instance.CancelOperation(request, context)

    assert context.code() == grpc.StatusCode.INVALID_ARGUMENT


def _pack_any(pack):
    some_any = any_pb2.Any()
    some_any.Pack(pack)
    return some_any
