# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import random
import time
from collections import Counter
from datetime import datetime, timedelta
from typing import Iterator
from unittest import mock

import grpc
import pytest
from flaky import flaky
from google.protobuf import any_pb2
from grpc._server import _Context

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import ExecuteOperationMetadata
from buildgrid._protos.google.devtools.remoteworkers.v1test2 import bots_pb2
from buildgrid._protos.google.devtools.remoteworkers.v1test2.bots_pb2 import BotSession
from buildgrid._protos.google.devtools.remoteworkers.v1test2.worker_pb2 import Device, Worker
from buildgrid._protos.google.rpc import code_pb2, status_pb2
from buildgrid.server.bots.instance import BotsInterface
from buildgrid.server.bots.service import BotsService
from buildgrid.server.cas.storage.lru_memory_cache import LRUMemoryCache
from buildgrid.server.context import instance_context
from buildgrid.server.enums import BotStatus, LeaseState, OperationStage
from buildgrid.server.metrics_names import METRIC
from buildgrid.server.monitoring import get_monitoring_bus, set_monitoring_bus
from buildgrid.server.scheduler import PropertyLabel, PropertySet, Scheduler, StaticPropertySet
from buildgrid.server.sql.models import BotEntry, JobEntry
from buildgrid.server.sql.provider import SqlProvider
from tests.utils.fixtures import mock_logstream_client, pg_schema_provider
from tests.utils.metrics import mock_create_timer_record
from tests.utils.mocks import mock_context, rpc_context

server = mock.create_autospec(grpc.Server)


TIME_REMAINING = 0.3
BOT_SESSION_KEEPALIVE_DELAYS = [TIME_REMAINING / 4, TIME_REMAINING / 2]
BOT_SESSION_KEEPALIVE_TIMEOUT_OPTIONS = [TIME_REMAINING / 2, TIME_REMAINING]


# GRPC context
@pytest.fixture
def context() -> Iterator[_Context]:
    return mock_context(time_remaining=0.5)


@pytest.fixture
def bot_session() -> Iterator[bots_pb2.BotSession]:
    bot = bots_pb2.BotSession()
    bot.bot_id = "ana"
    bot.status = BotStatus.OK.value
    yield bot


@pytest.fixture()
def sql_provider(postgres) -> Iterator[SqlProvider]:
    with pg_schema_provider(postgres) as sql_provider:
        yield sql_provider


@pytest.fixture
def storage() -> LRUMemoryCache:
    return LRUMemoryCache(1024 * 1024)


@pytest.fixture(params=BOT_SESSION_KEEPALIVE_TIMEOUT_OPTIONS)
def scheduler(request, sql_provider: SqlProvider, storage: LRUMemoryCache, common_props: PropertySet) -> Scheduler:
    set_monitoring_bus(mock.Mock())
    timeout = request.param
    return Scheduler(
        sql_provider,
        storage,
        bot_session_keepalive_timeout=timeout,
        job_assignment_interval=10,
        property_set=common_props,
    )


@pytest.fixture
def bots_interface(scheduler: Scheduler) -> Iterator[BotsInterface]:
    # Set the job assigner to some large value. Each new bot session added wakes it up.
    instance = BotsInterface(scheduler)
    with scheduler.job_assigner:
        yield instance


@pytest.fixture
def oldest_first_bots_interface(scheduler: Scheduler) -> Iterator[BotsInterface]:
    # Set priority_assignment_percentage to 0, meaning all work will be assigned in age order
    scheduler.job_assigner.priority_percentage = 0.0
    instance = BotsInterface(scheduler)
    with scheduler.job_assigner:
        yield instance


# Instance to test
@pytest.fixture
def bots_service(bots_interface: BotsInterface) -> Iterator[BotsService]:
    bots_service = BotsService()
    bots_service.add_instance("", bots_interface)
    yield bots_service


# Instance to test with monitoring enabled
@pytest.fixture
def bots_service_monitored(bots_interface: BotsInterface) -> Iterator[BotsService]:
    bots_service = BotsService()
    bots_service.add_instance("", bots_interface)
    yield bots_service


@pytest.fixture
def oldest_first_bots_service(oldest_first_bots_interface: BotsInterface) -> Iterator[BotsService]:
    bots_service = BotsService()
    bots_service.add_instance("", oldest_first_bots_interface)
    yield bots_service


def _inject_work(
    storage: LRUMemoryCache,
    scheduler: Scheduler,
    platform_requirements: dict[str, list[str]] | None = None,
    priority: int = 0,
) -> tuple[remote_execution_pb2.Action, str]:
    command = remote_execution_pb2.Command(arguments=["test", f"command-{priority}"])
    command_digest = storage.put_message(command)

    action = remote_execution_pb2.Action(command_digest=command_digest)
    action_digest = storage.put_message(action)

    with instance_context(""):
        operation_name = scheduler.queue_job_action(
            action=action,
            action_digest=action_digest,
            command=command,
            priority=priority,
            platform_requirements=platform_requirements or {},
            property_label="unknown",
            skip_cache_lookup=True,
        )

    return action, operation_name


BOT_COUNT = [2]
HISTORY_LENGTH = [3]

BOT_SESSION_HISTORY = [(bot_count, history_len) for bot_count in BOT_COUNT for history_len in HISTORY_LENGTH]


@pytest.fixture(params=BOT_SESSION_HISTORY)
def update_bot_session_history(request) -> Iterator[tuple[int, int]]:
    yield request.param


def check_bot_session_request_response_and_assigned_expiry(
    scheduler: Scheduler,
    request: bots_pb2.BotSession,
    response: bots_pb2.BotSession,
    request_time: datetime,
):
    assert isinstance(response, bots_pb2.BotSession)

    assert request.bot_id == response.bot_id

    # See if expiry time was set (when enabled)
    assert response.expire_time.IsInitialized()
    # See if it was set to an expected time
    earliest_expire_time = request_time + timedelta(seconds=scheduler.bot_session_keepalive_timeout)
    # We don't want to do strict comparisons because we are not looking for
    # exact time precision (e.g. in ms/ns)
    response_expire_time = response.expire_time.ToDatetime()
    assert response_expire_time >= earliest_expire_time


@flaky(max_runs=3)
@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_create_bot_session(
    scheduler: Scheduler, bot_session: bots_pb2.BotSession, context: _Context, bots_service: BotsService
):
    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = bots_service.CreateBotSession(request, context)

    check_bot_session_request_response_and_assigned_expiry(scheduler, bot_session, response, request_time)

    # Asserting that metrics were collected:
    with rpc_context("Bots", "CreateBotSession", ""):
        call_list = [mock.call(mock_create_timer_record(METRIC.RPC.DURATION, status="OK"))]
    get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_lease_assignment_creates_logstream(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):
    scheduler.logstream_channel = mock.MagicMock()
    scheduler.logstream_instance = "testing"

    _, operation_name = _inject_work(storage, scheduler)

    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    with mock.patch("buildgrid.server.scheduler.impl.logstream_client", new=mock_logstream_client()):
        response = bots_service.CreateBotSession(request, context)

    # Make sure a lease was assigned
    assert len(response.leases) == 1

    # The metadata packed into a lease should have the write stream names.
    lease = response.leases[0]
    with instance_context(""):
        [[_, lease_metadata]] = scheduler.get_metadata_for_leases([lease])
    metadata = ExecuteOperationMetadata()
    metadata.ParseFromString(lease_metadata)

    assert metadata.stdout_stream_name
    assert metadata.stdout_stream_name.endswith("stdout/mock-logstream/write")
    assert metadata.stderr_stream_name
    assert metadata.stderr_stream_name.endswith("stderr/mock-logstream/write")

    # The metadata packed into an operation should have the read stream names.
    with instance_context(""):
        op = scheduler.load_operation(operation_name)
    metadata = ExecuteOperationMetadata()
    op.metadata.Unpack(metadata)

    assert metadata.stdout_stream_name
    assert metadata.stdout_stream_name.endswith("stdout/mock-logstream")
    assert metadata.stderr_stream_name
    assert metadata.stderr_stream_name.endswith("stderr/mock-logstream")


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_lease_retry_creates_new_logstream(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):
    scheduler.logstream_channel = mock.MagicMock()
    scheduler.logstream_instance = "testing"

    _inject_work(storage, scheduler)

    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    with mock.patch("buildgrid.server.scheduler.impl.logstream_client", new=mock_logstream_client()):
        response = bots_service.CreateBotSession(request, context)

        # Make sure a lease was assigned
        assert len(response.leases) == 1

        lease = response.leases[0]
        with instance_context(""):
            [[_, lease_metadata]] = scheduler.get_metadata_for_leases([lease])
        metadata = ExecuteOperationMetadata()
        metadata.ParseFromString(lease_metadata)

        assert metadata.stdout_stream_name
        assert metadata.stdout_stream_name.endswith("stdout/mock-logstream/write")
        assert metadata.stderr_stream_name
        assert metadata.stderr_stream_name.endswith("stderr/mock-logstream/write")

        with instance_context(""):
            scheduler.close_bot_sessions(response.name)

    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    with mock.patch("buildgrid.server.scheduler.impl.logstream_client", new=mock_logstream_client(suffix="-second")):
        response = bots_service.CreateBotSession(request, context)

        # Make sure a lease was assigned
        assert len(response.leases) == 1

        lease = response.leases[0]
        with instance_context(""):
            [[_, lease_metadata]] = scheduler.get_metadata_for_leases([lease])
        metadata = ExecuteOperationMetadata()
        metadata.ParseFromString(lease_metadata)

        assert metadata.stdout_stream_name
        assert metadata.stdout_stream_name.endswith("stdout/mock-logstream-second/write")
        assert metadata.stderr_stream_name
        assert metadata.stderr_stream_name.endswith("stderr/mock-logstream-second/write")


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_update_bot_session_retryable_not_ok_lease_status(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):
    # Create BotSession and expect a lease to be assigned
    _inject_work(storage, scheduler)
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, context)

    # Both bot has received 1 lease, and BotsInterface knows about it
    assert len(bot.leases) == 1

    with scheduler._sql.session() as session:
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == bot.name).count()
        assert leases == 1

    # Make the bot call UpdateBotSession with a COMPLETED lease
    # Expect the BotsInterface to remove the lease from the BotSession
    bot.leases[0].state = bots_pb2.LeaseState.Value("COMPLETED")
    bot.leases[0].status.CopyFrom(status_pb2.Status(code=code_pb2.UNAVAILABLE))
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bot = bots_service.UpdateBotSession(request, context)

    # Send another UpdateBotSession since sending a COMPLETED lease
    # means we don't get any new leases in this request
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bot = bots_service.UpdateBotSession(request, context)

    # The lease should have been rescheduled, and since
    # this is the only bot connected it'll get the lease again
    assert len(bot.leases) == 1
    with scheduler._sql.session() as session:
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == bot.name).count()
        assert leases == 1
    assert bot.leases[0].state == LeaseState.PENDING.value


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_update_bot_session_non_retryable_lease_status(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):
    # Create BotSession and expect a lease to be assigned
    _inject_work(storage, scheduler)
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, context)

    # Both bot has received 1 lease, and BotsInterface knows about it
    assert len(bot.leases) == 1
    with scheduler._sql.session() as session:
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == bot.name).count()
        assert leases == 1
    leaseId = bot.leases[0].id

    # Make the bot call UpdateBotSession with a COMPLETED lease
    # Expect the BotsInterface to remove the lease from the BotSession
    bot.leases[0].state = bots_pb2.LeaseState.Value("COMPLETED")
    bot.leases[0].status.CopyFrom(status_pb2.Status(code=code_pb2.FAILED_PRECONDITION))
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bot = bots_service.UpdateBotSession(request, context)

    # The lease should not have been rescheduled due to the status
    # not being retryable
    assert len(bot.leases) == 0
    with scheduler._sql.session() as session:
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == bot.name).scalar()
        assert leases is None
        with instance_context(""):
            assert len(scheduler._get_job(leaseId, session).active_leases) == 0


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_update_bot_session_non_ok_retries_exhausted(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):
    scheduler.max_job_attempts = 2

    # Create BotSession and expect a lease to be assigned
    _inject_work(storage, scheduler)
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, context)

    # Both bot has received 1 lease, and BotsInterface knows about it
    assert len(bot.leases) == 1
    with scheduler._sql.session() as session:
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == bot.name).count()
        assert leases == 1
    leaseId = bot.leases[0].id

    # Make the bot call UpdateBotSession with a COMPLETED lease
    # Expect the BotsInterface to remove the lease from the BotSession
    bot.leases[0].state = bots_pb2.LeaseState.Value("ACTIVE")
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bot = bots_service.UpdateBotSession(request, context)

    with scheduler._sql.session() as session, instance_context(""):
        assert scheduler._get_job(leaseId, session).leases[0].state == LeaseState.ACTIVE.value

    # Make the bot call UpdateBotSession with a COMPLETED lease
    # Expect the BotsInterface to remove the lease from the BotSession
    bot.leases[0].state = bots_pb2.LeaseState.Value("COMPLETED")
    bot.leases[0].status.CopyFrom(status_pb2.Status(code=code_pb2.UNAVAILABLE))
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bot = bots_service.UpdateBotSession(request, context)

    assert len(bot.leases) == 0

    # Send another UpdateBotSession since sending a COMPLETED lease
    # means we don't get any new leases in this request
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bot = bots_service.UpdateBotSession(request, context)

    # The lease should have been rescheduled, and since
    # this is the only bot connected it'll get the lease again
    assert len(bot.leases) == 1
    assert bot.leases[0].id == leaseId
    with scheduler._sql.session() as session:
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == bot.name).count()
        assert leases == 1
    assert bot.leases[0].state == LeaseState.PENDING.value

    with scheduler._sql.session() as session, instance_context(""):
        assert scheduler._get_job(leaseId, session).leases[0].state == LeaseState.PENDING.value

    bot.leases[0].state = bots_pb2.LeaseState.Value("ACTIVE")
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bot = bots_service.UpdateBotSession(request, context)

    with scheduler._sql.session() as session, instance_context(""):
        assert scheduler._get_job(leaseId, session).leases[0].state == LeaseState.ACTIVE.value

    # Send a non-OK lease status again, and verify it doesn't get
    # rescheduled
    bot.leases[0].state = bots_pb2.LeaseState.Value("COMPLETED")
    bot.leases[0].status.CopyFrom(status_pb2.Status(code=code_pb2.UNAVAILABLE))
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bot = bots_service.UpdateBotSession(request, context)

    # Send another UpdateBotSession since sending a COMPLETED lease
    # means we don't get any new leases in this request no matter what
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bot = bots_service.UpdateBotSession(request, context)

    assert len(bot.leases) == 0
    with scheduler._sql.session() as session:
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == bot.name).scalar()
        assert leases is None

    with scheduler._sql.session() as session, instance_context(""):
        assert len(scheduler._get_job(leaseId, session).active_leases) == 0


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_update_bot_session_complete_lease(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):
    # Create BotSession and expect a lease to be assigned
    _inject_work(storage, scheduler)
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, context)

    # Both bot has received 1 lease, and BotsInterface knows about it
    assert len(bot.leases) == 1
    with scheduler._sql.session() as session:
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == bot.name).count()
        assert leases == 1
    lease_id = bot.leases[0].id

    # Make the bot call UpdateBotSession with a COMPLETED lease
    # Expect the BotsInterface to remove the lease from the BotSession
    bot.leases[0].state = bots_pb2.LeaseState.Value("ACTIVE")
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bot = bots_service.UpdateBotSession(request, context)

    with scheduler._sql.session() as session, instance_context(""):
        assert scheduler._get_job(lease_id, session).leases[0].state == LeaseState.ACTIVE.value

    # Make the bot call UpdateBotSession with a COMPLETED lease with
    # a dummy ActionResult in the payload
    action_result = remote_execution_pb2.ActionResult()
    action_result_in_any_proto = any_pb2.Any()
    action_result_in_any_proto.Pack(action_result)

    bot.leases[0].state = bots_pb2.LeaseState.Value("COMPLETED")
    bot.leases[0].result.CopyFrom(action_result_in_any_proto)
    bot.leases[0].status.CopyFrom(status_pb2.Status(code=code_pb2.OK))

    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bot = bots_service.UpdateBotSession(request, context)

    # Verify that there are no other leases assigned to the bot
    # and that the existing lease has been removed
    assert len(bot.leases) == 0
    with scheduler._sql.session() as session, instance_context(""):
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == bot.name).scalar()
        assert leases is None
        assert len(scheduler._get_job(lease_id, session).active_leases) == 0


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_job_assigner_queue_empty_after_create_update_bot_session_no_work(
    scheduler: Scheduler, bot_session: bots_pb2.BotSession, context: _Context, bots_service: BotsService
):
    """
    This test verifies that a worker which calls CreateBotSession or UpdateBotSession
    with no work available doesn't leave any entries in the job_assigner
    """
    job_assigner = scheduler.job_assigner

    # Create BotSession and expect no lease to be assigned as there's no work
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, context)
    assert len(bot.leases) == 0
    with scheduler._sql.session() as session:
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == bot.name).scalar()
        assert leases is None

    # Verify that the JobAssigner has no entry for this worker
    assert job_assigner.listener_count() == 0

    # Make the bot call UpdateBotSession and also verify there's no entries
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bots_service.UpdateBotSession(request, context)

    # Verify that the JobAssigner has no entry for this worker
    assert job_assigner.listener_count() == 0


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_create_bot_session_bot_id_fail(context: _Context, bots_service: BotsService):
    bot = bots_pb2.BotSession()

    request = bots_pb2.CreateBotSessionRequest(parent="", bot_session=bot)

    with pytest.raises(Exception):
        bots_service.CreateBotSession(request, context)

    assert context.code() == grpc.StatusCode.INVALID_ARGUMENT


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_recreate_bot_session(
    scheduler: Scheduler, bot_session: bots_pb2.BotSession, context: _Context, bots_service: BotsService
):
    # This test simulates bots re-connecting due to e.g. restarting suddenly
    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = bots_service.CreateBotSession(request, context)
    initial_botsession_bot_id = response.bot_id
    initial_botsession_name = response.name

    check_bot_session_request_response_and_assigned_expiry(scheduler, bot_session, response, request_time)

    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = bots_service.CreateBotSession(request, context)
    new_botsession_bot_id = response.bot_id
    new_botsession_name = response.name

    # New BotSession should be for the same BotID
    assert initial_botsession_bot_id == new_botsession_bot_id
    # New BotSession should have a new name!
    assert initial_botsession_name != new_botsession_name

    check_bot_session_request_response_and_assigned_expiry(scheduler, bot_session, response, request_time)

    # The previous botsession for this bot must have gotten closed
    with scheduler._sql.session() as session:
        assert session.query(BotEntry).count() == 1


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_recreate_bot_session_as_initial_bot_session_expires(
    scheduler: Scheduler, bot_session: bots_pb2.BotSession, context: _Context, bots_service: BotsService
):
    # This tests edge cases in which a botsession is re-created as it expires.
    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = bots_service.CreateBotSession(request, context)
    initial_botsession_bot_id = response.bot_id
    initial_botsession_name = response.name

    check_bot_session_request_response_and_assigned_expiry(scheduler, bot_session, response, request_time)

    # Simulate simultaneous expiry of earlier botsession
    # and recreation of a new BotSession with a `CreateBotSession` request

    # Re-create botsession (e.g. bot crashed)
    # Causes a call to `_close_bot_session`
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    response = bots_service.CreateBotSession(request, context)
    new_botsession_bot_id = response.bot_id
    new_botsession_name = response.name

    with instance_context(""):
        scheduler.reap_expired_sessions()

    # New BotSession should be for the same BotID
    assert initial_botsession_bot_id == new_botsession_bot_id
    # New BotSession should have a new name!
    assert initial_botsession_name != new_botsession_name


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_recreate_bot_session_after_expiry(
    scheduler: Scheduler, bot_session: bots_pb2.BotSession, context: _Context, bots_service: BotsService
):
    # Test that a bot is able to successfully create a new botsession after
    # their BotSession expires.
    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = bots_service.CreateBotSession(request, context)
    initial_botsession_bot_id = response.bot_id
    initial_botsession_name = response.name

    check_bot_session_request_response_and_assigned_expiry(scheduler, bot_session, response, request_time)

    # Force expiry
    with instance_context(""):
        scheduler.reap_expired_sessions()

    # Bot returns and creates a new session
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    response = bots_service.CreateBotSession(request, context)
    new_botsession_bot_id = response.bot_id
    new_botsession_name = response.name

    # New BotSession should be for the same BotID
    assert initial_botsession_bot_id == new_botsession_bot_id
    # New BotSession should have a new name!
    assert initial_botsession_name != new_botsession_name


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_recreated_bot_session_after_update_expiry_receives_leases(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):
    # Test that a bot is able to successfully create a new botsession after
    # their BotSession expires.
    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = bots_service.CreateBotSession(request, context)
    initial_botsession = response
    initial_botsession_bot_id = initial_botsession.bot_id
    initial_botsession_name = initial_botsession.name

    check_bot_session_request_response_and_assigned_expiry(scheduler, bot_session, response, request_time)

    # Send an UpdateBotSession
    request = bots_pb2.UpdateBotSessionRequest(name=initial_botsession_name, bot_session=initial_botsession)
    response = bots_service.UpdateBotSession(request, context)

    # Force expiry
    with instance_context(""):
        scheduler.reap_expired_sessions()

    # Add a job to be picked up by the reconnecting bot
    _inject_work(storage, scheduler)

    # Bot returns and creates a new session
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    response = bots_service.CreateBotSession(request, context)
    new_botsession_bot_id = response.bot_id
    new_botsession_name = response.name

    # New BotSession should be for the same BotID
    assert initial_botsession_bot_id == new_botsession_bot_id
    # New BotSession should have a new name!
    assert initial_botsession_name != new_botsession_name
    # Assigned a job under the new botsession name
    assert len(response.leases) == 1
    with scheduler._sql.session() as session:
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == response.name).count()
        assert leases == 1


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_update_bot_session_zombie(bot_session: bots_pb2.BotSession, context: _Context, bots_service: BotsService):
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, context)
    # Update server with incorrect UUID by rotating it
    bot.name = bot.name[len(bot.name) : 0]

    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)

    with pytest.raises(Exception):
        bots_service.UpdateBotSession(request, context)

    assert context.code() == grpc.StatusCode.INVALID_ARGUMENT


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_update_bot_session_bot_id_fail(
    bot_session: bots_pb2.BotSession, context: _Context, bots_service: BotsService
):
    request = bots_pb2.UpdateBotSessionRequest(bot_session=bot_session)

    with pytest.raises(Exception):
        bots_service.UpdateBotSession(request, context)

    assert context.code() == grpc.StatusCode.INVALID_ARGUMENT


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_update_bot_session_after_expiry(
    scheduler: Scheduler, bot_session: bots_pb2.BotSession, context: _Context, bots_service: BotsService
):
    # Test that a bot is able to successfully create a new botsession after
    # their BotSession expires.
    request_time = datetime.utcnow()
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)

    response = bots_service.CreateBotSession(request, context)
    initial_botsession = response
    initial_botsession_name = response.name

    check_bot_session_request_response_and_assigned_expiry(scheduler, bot_session, response, request_time)

    # Close bot session (e.g. expiry)
    with instance_context(""):
        scheduler.close_bot_sessions(initial_botsession_name)

    # Bot now sends an `UpdateBotSession` request
    request = bots_pb2.UpdateBotSessionRequest(name=initial_botsession_name, bot_session=initial_botsession)

    with pytest.raises(Exception):
        bots_service.UpdateBotSession(request, context)

    # The response should contain the relevant error code
    assert context.code() == grpc.StatusCode.INVALID_ARGUMENT


@flaky(max_runs=3)
@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_update_bot_session(
    scheduler: Scheduler, bot_session: bots_pb2.BotSession, context: _Context, bots_service: BotsService
):
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, context)

    request_time = datetime.utcnow()
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)

    response = bots_service.UpdateBotSession(request, context)

    check_bot_session_request_response_and_assigned_expiry(scheduler, bot_session, response, request_time)

    # Asserting that metrics were collected:
    with rpc_context("Bots", "UpdateBotSession", ""):
        call_list = [mock.call(mock_create_timer_record(METRIC.RPC.DURATION, status="OK"))]
    with instance_context(""):
        call_list += [mock.call(mock_create_timer_record(METRIC.SCHEDULER.ASSIGNMENT_DURATION))]

    get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_update_bot_session_completed_no_request_leases(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):
    """When a Lease is completed, we shouldn't ask for new work in the same request.

    This behaviour avoids an issue where the Bots service going down whilst waiting
    for new work after removing a completed Lease leads to the worker attempting to
    update the completed Lease in future requests.

    """
    bots_interface = bots_service.get_instance("")

    # Create BotSession and expect a lease to be assigned
    _inject_work(storage, scheduler)
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, context)

    # Both bot has received 1 lease, and BotsInterface knows about it
    assert len(bot.leases) == 1
    with scheduler._sql.session() as session:
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == bot.name).count()
        assert leases == 1

    # Have the bot complete the job
    lease = bot.leases[0]
    lease.state = bots_pb2.LeaseState.Value("COMPLETED")
    bots_interface._request_leases = mock.MagicMock()

    # We provide a deadline of 10s, but the deadline used is None
    with instance_context(""):
        bots_interface.update_bot_session(bot, context, deadline=10)

    bots_interface._request_leases.assert_not_called()


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_job_lease_creation_with_preexisting_worker_timestamps(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):
    """If a job has no lease but already has worker timestamps set, verify
    leases can still be created and the job assigned

    This generally doesn't happen during normal operation, but if it does happen
    the job assigner used to deadlock. Verify it doesn't and that the worker
    timestamps are as expected
    """
    # Inject work and manually add a worker_start/worker_completed timestamp to the job
    _, operation_name = _inject_work(storage, scheduler)

    with instance_context(""):
        with scheduler._sql.session() as session:
            job = scheduler._get_operation(operation_name, session).job
            job.worker_start_timestamp = datetime.utcnow() - timedelta(minutes=10)
            job.worker_completed_timestamp = datetime.utcnow()

    # Send a CreateBotSessionRequest and expect it to return a lease
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, context)

    # Both bot has received 1 lease, and BotsInterface knows about it
    assert len(bot.leases) == 1
    with scheduler._sql.session() as session:
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == bot.name).count()
        assert leases == 1


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_update_bot_session_bot_not_reporting_lease(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):

    # Create BotSession and expect a lease to be assigned
    _inject_work(storage, scheduler)
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot_session = bots_service.CreateBotSession(request, context)

    # Bot has received 1 lease, and database knows about it
    assert len(bot_session.leases) == 1
    with scheduler._sql.session() as session:
        lease_id = session.query(BotEntry.lease_id).scalar()
        assert lease_id == bot_session.leases[0].id
        job = session.query(JobEntry).scalar()
        assert job.name == bot_session.leases[0].id
        assert job.n_tries == 1
        assert job.assigned
        assert job.stage == OperationStage.EXECUTING.value

    # Move the job into the active state.
    bot_session.leases[0].state = LeaseState.ACTIVE.value
    request = bots_pb2.UpdateBotSessionRequest(name=bot_session.name, bot_session=bot_session)
    bots_service.UpdateBotSession(request, context)

    assert len(bot_session.leases) == 1
    with scheduler._sql.session() as session:
        lease_id = session.query(BotEntry.lease_id).scalar()
        assert lease_id == bot_session.leases[0].id
        job = session.query(JobEntry).scalar()
        assert job.name == bot_session.leases[0].id
        assert job.n_tries == 1
        assert job.assigned
        assert job.stage == OperationStage.EXECUTING.value

    # Make the bot call UpdateBotSession without the lease
    # Expect the BotsInterface to retry the lease and unassign
    bot_session.leases.pop()
    context = mock_context(time_remaining=0)
    request = bots_pb2.UpdateBotSessionRequest(name=bot_session.name, bot_session=bot_session)

    with pytest.raises(Exception):
        bots_service.UpdateBotSession(request, context)
    assert context.code() == grpc.StatusCode.DATA_LOSS

    # Bot session should be scrapped and the job retried.
    with scheduler._sql.session() as session:
        bot_count = session.query(BotEntry).count()
        assert bot_count == 0
        job = session.query(JobEntry).scalar()
        assert job.n_tries == 2
        assert not job.assigned
        assert job.stage == OperationStage.QUEUED.value


@flaky(max_runs=3)
@pytest.mark.parametrize("sleep_duration", BOT_SESSION_KEEPALIVE_DELAYS)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_update_bot_session_and_wait_for_expiry(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    sleep_duration: float,
):
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, context)

    request_time = datetime.utcnow()
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)

    response = bots_service.UpdateBotSession(request, context)

    check_bot_session_request_response_and_assigned_expiry(scheduler, bot_session, response, request_time)

    with instance_context(""):
        scheduler.reap_expired_sessions()
    with scheduler._sql.session() as session:
        sessions = session.query(BotEntry).count()
        assert sessions == 1

    time.sleep(sleep_duration)

    with instance_context(""):
        scheduler.reap_expired_sessions()
    with scheduler._sql.session() as session:
        sessions = session.query(BotEntry).count()
        if scheduler.bot_session_keepalive_timeout <= sleep_duration:
            assert sessions == 0
        else:
            assert sessions == 1


@flaky(max_runs=3)
@pytest.mark.parametrize("sleep_duration", BOT_SESSION_KEEPALIVE_DELAYS)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_botsession_expiry_cleanup(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    sleep_duration: float,
):
    bots_interface = bots_service.get_instance("")

    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, context)

    request_time = datetime.utcnow()

    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    response = bots_service.UpdateBotSession(request, context)

    check_bot_session_request_response_and_assigned_expiry(scheduler, bot_session, response, request_time)

    with scheduler._sql.session() as session:
        sessions = session.query(BotEntry).count()
        assert sessions == 1

    time.sleep(sleep_duration)
    # Call this manually for more deterministic testing

    with instance_context(""):
        scheduler.reap_expired_sessions()

    if scheduler.bot_session_keepalive_timeout <= sleep_duration:
        # Should have no tracked expiries now
        with bots_interface.scheduler._sql.session() as session:
            sessions = session.query(BotEntry).count()
            assert sessions == 0


@flaky(max_runs=3)
@pytest.mark.parametrize("sleep_duration", BOT_SESSION_KEEPALIVE_DELAYS)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_botsession_expiry_cleanup_two_entries(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    sleep_duration: float,
):

    # Create two bots around the same time
    for bot_num in range(2):
        new_bot_session = bot_session
        new_bot_session.bot_id = f"{bot_session.bot_id}-{bot_num}"
        request = bots_pb2.CreateBotSessionRequest(bot_session=new_bot_session)
        bot = bots_service.CreateBotSession(request, context)

        request_time = datetime.utcnow()
        request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)

        response = bots_service.UpdateBotSession(request, context)

        check_bot_session_request_response_and_assigned_expiry(scheduler, bot_session, response, request_time)

    if scheduler.bot_session_keepalive_timeout:
        with scheduler._sql.session() as session:
            sessions = session.query(BotEntry).count()
            assert sessions == 2

    time.sleep(sleep_duration)
    # Call this manually for more deterministic testing
    with instance_context(""):
        scheduler.reap_expired_sessions()

    if scheduler.bot_session_keepalive_timeout <= sleep_duration:
        # Verify both expired bot sessions were reaped with the single call
        with scheduler._sql.session() as session:
            sessions = session.query(BotEntry).count()
            assert sessions == 0


@flaky(max_runs=3)
@pytest.mark.parametrize("number_of_jobs", [0, 1, 3, 200])
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_number_of_leases(
    scheduler: Scheduler,
    number_of_jobs: int,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):
    for _ in range(0, number_of_jobs):
        _inject_work(storage, scheduler)

    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    response = bots_service.CreateBotSession(request, context)

    assert len(response.leases) == min(number_of_jobs, 1)


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_createbotsession_leases_with_work(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):
    action, _ = _inject_work(storage, scheduler)

    request = bots_pb2.CreateBotSessionRequest(parent="", bot_session=bot_session)
    response = bots_service.CreateBotSession(request, context)
    assert isinstance(response, bots_pb2.BotSession)

    assert len(response.leases) == 1
    assert response.leases[0].state == LeaseState.PENDING.value

    response_action = remote_execution_pb2.Action()
    response.leases[0].payload.Unpack(response_action)
    assert response_action == action


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_updatebotsession_leases_with_work(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):
    # CreateBotSession
    request = bots_pb2.CreateBotSessionRequest(parent="", bot_session=bot_session)

    response = bots_service.CreateBotSession(request, context)
    assert len(response.leases) == 0

    # Work queued on the server
    action, _ = _inject_work(storage, scheduler)

    bot_session = response  # the server has assigned a session name
    # UpdateBotSession should receive a lease
    request = bots_pb2.UpdateBotSessionRequest(name=bot_session.name, bot_session=bot_session)

    response = bots_service.UpdateBotSession(request, context)
    assert isinstance(response, bots_pb2.BotSession)

    assert len(response.leases) == 1

    execute_operation_metadata = ExecuteOperationMetadata()
    execute_operation_metadata.ParseFromString(context.trailing_metadata()[0][1])
    assert ExecuteOperationMetadata(
        stage=execute_operation_metadata.stage,
        action_digest=execute_operation_metadata.action_digest,
    ) == ExecuteOperationMetadata(
        stage=OperationStage.EXECUTING.value,
        action_digest=storage.put_message(action),
    )

    response_action = remote_execution_pb2.Action()
    response.leases[0].payload.Unpack(response_action)

    assert response.leases[0].state == LeaseState.PENDING.value
    assert response_action == action


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_lease_reassignment_after_botsession_close(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):
    request = bots_pb2.CreateBotSessionRequest(parent="", bot_session=bot_session)

    action, _ = _inject_work(storage, scheduler)

    response = bots_service.CreateBotSession(request, context)
    assert isinstance(response, bots_pb2.BotSession)

    assert len(response.leases) == 1
    response_action = remote_execution_pb2.Action()
    response.leases[0].payload.Unpack(response_action)

    assert response.leases[0].state == LeaseState.PENDING.value
    assert response_action == action

    with scheduler._sql.session() as session:
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == response.name).count()
        assert leases == 1

    # Invoke `_close_bot_session`
    with instance_context(""):
        scheduler.close_bot_sessions(response.name)
    # Check whether the lease assignment has been cleaned up
    with scheduler._sql.session() as session:
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == response.name).scalar()
        assert leases is None

    request = bots_pb2.CreateBotSessionRequest(parent="", bot_session=bot_session)
    # Check whether a new botsession is able to pick up the re-queued lease
    response = bots_service.CreateBotSession(request, context)
    assert isinstance(response, bots_pb2.BotSession)

    assert len(response.leases) == 1
    response_action = remote_execution_pb2.Action()
    response.leases[0].payload.Unpack(response_action)

    assert response.leases[0].state == LeaseState.PENDING.value
    assert response_action == action

    with scheduler._sql.session() as session:
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == response.name).count()
        assert leases == 1


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_post_bot_event_temp(context: _Context, bots_service: BotsService):
    request = bots_pb2.PostBotEventTempRequest()
    bots_service.PostBotEventTemp(request, context)

    assert context.code() == grpc.StatusCode.UNIMPLEMENTED


@flaky(max_runs=3)
def test_unmet_platform_requirements(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):
    request = bots_pb2.CreateBotSessionRequest(parent="", bot_session=bot_session)

    _inject_work(
        storage,
        scheduler,
        platform_requirements={"OSFamily": ["wonderful-os"]},
    )

    response = bots_service.CreateBotSession(request, context)

    assert len(response.leases) == 0


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_unhealthy_bot(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):
    # set botstatus to unhealthy
    bot_session.status = BotStatus.UNHEALTHY.value
    request = bots_pb2.CreateBotSessionRequest(parent="", bot_session=bot_session)

    _inject_work(storage, scheduler)

    response = bots_service.CreateBotSession(request, context)

    # No leases should be given
    assert len(response.leases) == 0


def new_session(properties: set[tuple[str, str]]) -> BotSession:
    return BotSession(
        worker=Worker(
            devices=[Device(properties=[Device.Property(key=key, value=value) for key, value in properties])]
        )
    )


def _create_bot_session(name: str, bots_service: BotsService, context: _Context):
    bot = new_session({("OSFamily", "Linux"), ("ISA", "x86_64")})
    bot.status = BotStatus.OK.value
    bot.bot_id = name

    request = bots_pb2.CreateBotSessionRequest(parent="", bot_session=bot)
    new_bot_session = bots_service.CreateBotSession(request, context)
    return new_bot_session


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_update_bot_session_state_reflected_in_metrics(
    scheduler: Scheduler,
    update_bot_session_history: tuple[int, int],
    context: _Context,
    bots_service_monitored: BotsService,
):
    # Add a property set to the scheduler
    property_labels: list[PropertyLabel] = [
        PropertyLabel(label="Linux64", properties={("OSFamily", "Linux"), ("ISA", "x86_64")})
    ]
    scheduler.property_set = StaticPropertySet(
        property_labels=property_labels,
        wildcard_property_keys=set(),
    )

    TRACKED_BOT_STATUSES = [botstatus for botstatus in BotStatus]

    n_bots, history_len = update_bot_session_history
    # Generate history of desired length
    bots_history = []
    for i in range(history_len):
        bot_statuses = [random.choice(TRACKED_BOT_STATUSES) for i in range(n_bots)]
        # Calculate # of bots in each status
        total_bots_by_status = Counter()
        total_bots_by_status.update(bot_statuses)
        bots_history.append((bot_statuses, total_bots_by_status))

    # Create all the bot sessions
    bot_sessions = []
    for i in range(n_bots):
        bot_id = f"bot_{i}"
        new_bot_session = _create_bot_session(bot_id, bots_service_monitored, context)
        print(f"bot id=[{bot_id}] has name=[{new_bot_session.name}]")

        new_bot_session.worker.devices.append(
            Device(
                properties=[Device.Property(key="OSFamily", value="Linux"), Device.Property(key="ISA", value="x86_64")]
            )
        )
        bot_sessions.append(new_bot_session)

    process_order = [i for i in range(n_bots)]
    # Simulate UpdateBotSession calls from all bots and check metrics
    for iteration in range(history_len):
        print(f"Iteration: {iteration}")
        # Update all BotSessions (in random order for each iteraion)
        random.shuffle(process_order)  # Shuffle update order
        status_updates, total_bots_by_status = bots_history[iteration]
        for bot_index in process_order:
            current_bot_session = bot_sessions[bot_index]
            assert (f"bot_{bot_index}") == current_bot_session.bot_id
            # Update status
            current_bot_session.status = status_updates[bot_index].value
            request = bots_pb2.UpdateBotSessionRequest(name=current_bot_session.name, bot_session=current_bot_session)
            update_bot_session_response = bots_service_monitored.UpdateBotSession(request, context)
            assert (f"bot_{bot_index}") == update_bot_session_response.bot_id

        print("Bot statuses:")
        print([bot.status for bot in bot_sessions])

        # Make sure the total number of bots matches expected number
        with instance_context(""):
            bot_metrics = bots_service_monitored.get_bot_status_metrics("")
            print(f"bot_metrics: {bot_metrics}")
        assert n_bots == sum(bot_metrics["bots_total"].values())

        # Make sure the numbers of the tracked states match up
        for status in TRACKED_BOT_STATUSES:
            # Print this so that it is visible in case of errors
            label = "Linux64" if total_bots_by_status[status] else "unknown"
            print(f"Asserting expected numbers for status: {status} (value={status.value}) (label={label})")
            assert total_bots_by_status[status] == bot_metrics["bots_per_property_label"][status, label]

    # Simulate all bot sessions closing in a random order (e.g. due to expiry)
    process_order = [i for i in range(n_bots)]
    remaining_bots = n_bots
    # Start from the last iteration statuses and keep subtracting
    last_status_updates, last_total_bots_by_status = bots_history[-1]

    for bot_index in process_order:
        current_bot_session = bot_sessions[bot_index]
        assert (f"bot_{bot_index}") == current_bot_session.bot_id

        # Close bot session
        with instance_context(""):
            scheduler.close_bot_sessions(current_bot_session.name)

        # Remove this bot_session from the totals
        last_total_bots_by_status[last_status_updates[bot_index]] -= 1
        remaining_bots -= 1

        print("Bot statuses:")
        print([bot.status for bot in bot_sessions])

        # Make sure the total number of bots matches expected number
        with instance_context(""):
            bot_metrics = bots_service_monitored.get_bot_status_metrics("")
            print(f"bot_metrics: {bot_metrics}")
        assert remaining_bots == sum(bot_metrics["bots_total"].values())

        # Make sure the numbers of the tracked states match up
        for status in TRACKED_BOT_STATUSES:
            # Print this so that it is visible in case of errors
            label = "Linux64" if total_bots_by_status[status] else "unknown"
            print(f"Asserting expected numbers for status: {status} (value={status.value}) (label={label})")
            assert total_bots_by_status[status] == bot_metrics["bots_per_property_label"][status, label]


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_update_bot_session_complete_already_completed_lease(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):
    # Create BotSession and expect a lease to be assigned
    _inject_work(storage, scheduler)
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, context)

    # Both bot has received 1 lease, and BotsInterface knows about it
    assert len(bot.leases) == 1
    with scheduler._sql.session() as session:
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == bot.name).count()
        assert leases == 1

    # Mark the job as completed in the scheduler
    lease = bot.leases[0]
    lease.state = bots_pb2.LeaseState.Value("COMPLETED")
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bot = bots_service.UpdateBotSession(request, context)

    # The lease should be removed by the server
    assert len(bot.leases) == 0

    # Make the bot call UpdateBotSession with a COMPLETED lease
    # Expect the BotsInterface to remove the lease from the BotSession
    bot.leases.append(lease)
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bot = bots_service.UpdateBotSession(request, context)

    # The lease should be removed by the server
    assert len(bot.leases) == 0


@flaky(max_runs=3)
@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_update_bot_session_updating_already_completed_lease(
    scheduler: Scheduler,
    bot_session: bots_pb2.BotSession,
    context: _Context,
    bots_service: BotsService,
    storage: LRUMemoryCache,
):
    # Create BotSession and expect a lease to be assigned
    _inject_work(storage, scheduler)
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = bots_service.CreateBotSession(request, context)

    # Both bot has received 1 lease, and BotsInterface knows about it
    assert len(bot.leases) == 1
    with scheduler._sql.session() as session:
        leases = session.query(BotEntry.lease_id).filter(BotEntry.name == bot.name).count()
        assert leases == 1

    # Mark the job as completed in the scheduler
    lease = bot.leases[0]
    lease.state = bots_pb2.LeaseState.Value("COMPLETED")
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)
    bot = bots_service.UpdateBotSession(request, context)

    # The lease should be removed by the server
    assert len(bot.leases) == 0

    # Make the bot call UpdateBotSession with an ACTIVE lease
    # Expect the BotsInterface to remove the lease as
    # the bot completing an already completed lease isn't valid
    bot.leases.append(lease)
    bot.leases[0].state = bots_pb2.LeaseState.Value("ACTIVE")
    request = bots_pb2.UpdateBotSessionRequest(name=bot.name, bot_session=bot)

    with pytest.raises(Exception):
        bots_service.UpdateBotSession(request, context)

    assert context.code() == grpc.StatusCode.DATA_LOSS

    with rpc_context("Bots", "UpdateBotSession", ""):
        call_list = [
            mock.call(mock_create_timer_record(METRIC.RPC.DURATION, status="OK")),
            mock.call(mock_create_timer_record(METRIC.RPC.DURATION, status="DATA_LOSS")),
        ]
    get_monitoring_bus().send_record_nowait.assert_has_calls(call_list, any_order=True)


@flaky(max_runs=3)
@mock.patch("buildgrid.server.bots.instance.NETWORK_TIMEOUT", 0)
def test_priority_percentage(
    oldest_first_bots_service: BotsService,
    scheduler: Scheduler,
    storage: LRUMemoryCache,
    bot_session: bots_pb2.BotSession,
    context: _Context,
) -> None:
    # GIVEN two jobs in the queue, lowest priority oldest
    action, _ = _inject_work(storage, scheduler, priority=10)
    _inject_work(storage, scheduler)

    # WHEN a bot connects asking for work
    request = bots_pb2.CreateBotSessionRequest(bot_session=bot_session)
    bot = oldest_first_bots_service.CreateBotSession(request, context)

    # THEN the assigned lease is for the oldest job, despite the lower priority
    assert len(bot.leases) == 1
    lease = bot.leases[0]
    lease_action = remote_execution_pb2.Action()
    lease.payload.Unpack(lease_action)
    assert action == lease_action
