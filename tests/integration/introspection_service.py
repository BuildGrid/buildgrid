# Copyright (C) 2024 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from datetime import datetime, timedelta
from typing import Iterator
from unittest import mock

import grpc
import pytest

from buildgrid._protos.buildgrid.v2.introspection_pb2 import GetOperationFiltersRequest, ListWorkersRequest
from buildgrid.server.cas.storage import lru_memory_cache
from buildgrid.server.enums import OperationStage
from buildgrid.server.introspection.instance import IntrospectionInstance
from buildgrid.server.introspection.service import IntrospectionService
from buildgrid.server.monitoring import set_monitoring_bus
from buildgrid.server.operations.filtering.interpreter import VALID_OPERATION_FILTERS
from buildgrid.server.scheduler.impl import Scheduler
from buildgrid.server.sql.models import BotEntry, JobEntry, OperationEntry
from buildgrid.server.sql.provider import SqlProvider
from buildgrid.server.utils.digests import create_digest
from tests.utils.fixtures import pg_schema_provider
from tests.utils.mocks import mock_context

server = mock.create_autospec(grpc.Server)

TEST_DIGEST = create_digest(b"test")


@pytest.fixture
def sql_provider(postgres) -> Iterator[SqlProvider]:
    with pg_schema_provider(postgres) as sql:
        yield sql


@pytest.fixture
def scheduler(sql_provider, common_props):
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)

    yield Scheduler(
        sql_provider,
        storage,
        poll_interval=0.01,
        property_set=common_props,
        job_assignment_interval=0.1,
    )


@pytest.fixture()
def service(scheduler: Scheduler) -> Iterator[IntrospectionService]:
    service = IntrospectionService()
    instance = IntrospectionInstance(scheduler)
    service.add_instance("", instance)
    yield service


def add_workers(sql_provider: SqlProvider) -> None:
    with sql_provider.session() as session:
        now = datetime.utcnow()
        session.add_all(
            [
                JobEntry(
                    name="test-job",
                    instance_name="",
                    action_digest=f"{TEST_DIGEST.hash}/{TEST_DIGEST.size_bytes}",
                    priority=1,
                    stage=OperationStage.EXECUTING.value,
                    action=b"test",
                    platform_requirements="",
                    command="foo",
                    create_timestamp=now,
                    queued_timestamp=now,
                ),
                OperationEntry(name="test-operation", job_name="test-job"),
            ]
        )
        session.add_all(
            [
                BotEntry(
                    name="session-1",
                    bot_id="worker-1",
                    last_update_timestamp=datetime.utcnow(),
                    expiry_time=datetime.utcnow() + timedelta(minutes=10),
                    bot_status=1,
                    instance_name="",
                    lease_id="test-job",
                ),
                BotEntry(
                    name="session-2",
                    bot_id="worker-2",
                    last_update_timestamp=datetime.utcnow(),
                    expiry_time=datetime.utcnow() + timedelta(minutes=10),
                    bot_status=1,
                    instance_name="",
                ),
            ]
        )


def test_list_workers(sql_provider: SqlProvider, service: IntrospectionService):
    # GIVEN: an IntrospectionService with workers connected
    set_monitoring_bus(mock.Mock())
    add_workers(sql_provider)

    # WHEN: a ListWorkers request with a blank filter is sent
    context = mock_context()
    request = ListWorkersRequest(instance_name="", worker_name="")
    response = service.ListWorkers(request, context)

    # THEN: all workers are returned
    assert len(response.workers) == 2

    # WHEN: a ListWorkers request is sent with a filter to find a specific bot
    request = ListWorkersRequest(instance_name="", worker_name="worker-1")
    response = service.ListWorkers(request, context)

    # THEN: the expected worker is returned
    assert len(response.workers) == 1
    assert response.workers[0].worker_name == "worker-1"
    assert len(response.workers[0].operations) == 1
    assert response.workers[0].operations[0].operation_name == "test-operation"
    assert response.workers[0].action_digest == TEST_DIGEST

    # WHEN: a ListWorkers request is sent with a filter which doesn't match any bots
    request = ListWorkersRequest(instance_name="", worker_name="not-a-bot")
    response = service.ListWorkers(request, context)

    # THEN: no workers are returned
    assert len(response.workers) == 0

    # WHEN: a ListWorkers request is sent with pagination limits
    request = ListWorkersRequest(instance_name="", worker_name="", page_size=1)
    response = service.ListWorkers(request, context)

    # THEN: only `page_size` workers are returned
    assert len(response.workers) == 1
    assert response.total == 2
    assert response.workers[0].worker_name == "worker-1"

    # WHEN: a pagination offset is also set
    request = ListWorkersRequest(instance_name="", worker_name="", page_size=1, page=2)
    response = service.ListWorkers(request, context)

    # THEN: the expected page is returned
    assert len(response.workers) == 1
    assert response.total == 2
    assert response.page == 2
    assert response.workers[0].worker_name == "worker-2"


def test_get_operation_filters(service: IntrospectionService):
    # GIVEN: an IntrospectionService
    set_monitoring_bus(mock.Mock())

    # WHEN: a GetOperationFilters request is sent
    context = mock_context()
    request = GetOperationFiltersRequest(instance_name="")
    response = service.GetOperationFilters(request, context)

    # THEN: the returned filters match the filters defined in the query language parser
    assert set(filter.key for filter in response.filters) == set(VALID_OPERATION_FILTERS.keys())
