#!/bin/python
import socket
import sys

# This python script checks if a server
# has a specific port open
# Invoke using `python3 is-port-open.py server port`


def is_port_open(host, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        result = sock.connect_ex((host, port))
        if result == 0:
            sys.exit(0)
        else:
            sys.exit(1)
    except Exception:
        sys.exit(1)


if __name__ == "__main__":
    if len(sys.argv) == 3:
        is_port_open(sys.argv[1], int(sys.argv[2]))
    else:
        print("You need to invoke this with a HOST PORT args")
        sys.exit(2)
