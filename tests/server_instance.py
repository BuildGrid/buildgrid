# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import asyncio
import time

import grpc
import pytest
from grpc_health.v1 import health_pb2, health_pb2_grpc

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2, remote_execution_pb2_grpc
from buildgrid._protos.google.bytestream import bytestream_pb2, bytestream_pb2_grpc
from buildgrid._protos.google.devtools.remoteworkers.v1test2 import bots_pb2, bots_pb2_grpc
from buildgrid._protos.google.longrunning import operations_pb2, operations_pb2_grpc
from buildgrid.server.enums import BotStatus

from .utils.server import serve

CONFIGURATION = """
server:
  - !channel
    address: "[::]:50051"
    insecure-mode: true

description: |
  A single default instance

monitoring:
  enabled: MON_ENABLE_BOOL
  endpoint-type: file
  endpoint-location: MON_ENDPOINT_LOCATION

connections:
  - !sql-connection &sql
    connection-string: DB_CONN_STR
    automigrate: yes
    connection-timeout: 15

storages:
  - !lru-storage &storage
    size: 256mb

caches:
  - !lru-action-cache &cache
    storage: *storage
    max-cached-refs: 256
    allow-updates: true

schedulers:
  - !sql-scheduler &scheduler
    sql: *sql
    storage: *storage
    action-cache: *cache

instances:
  - name: main
    description: |
      The main server

    services:
      - !action-cache
        cache: *cache

      - !execution
        scheduler: *scheduler
        endpoints:
          - execution
          - operations

      - !bots
        scheduler: *scheduler

      - !cas
        storage: *storage

      - !bytestream
        storage: *storage
"""

NO_EXECUTE_CONFIG = """
server:
  - !channel
    address: "[::]:50051"
    insecure-mode: true

description: |
  A single default instance

monitoring:
  enabled: MON_ENABLE_BOOL
  endpoint-type: file
  endpoint-location: MON_ENDPOINT_LOCATION

connections:
  - !sql-connection &sql
    connection-string: DB_CONN_STR
    automigrate: yes
    connection-timeout: 15

storages:
  - !lru-storage &storage
    size: 256mb

caches:
  - !lru-action-cache &cache
    storage: *storage
    max-cached-refs: 256
    allow-updates: true

schedulers:
  - !sql-scheduler &scheduler
    sql: *sql
    storage: *storage
    action-cache: *cache

instances:
  - name: main
    description: |
      The main server

    services:
      - !action-cache
        cache: *cache

      - !bots
        scheduler: *scheduler

      - !cas
        storage: *storage

      - !bytestream
        storage: *storage
"""

ONLY_STORAGE_CONFIG = """
server:
  - !channel
    address: "[::]:50051"
    insecure-mode: true

description: |
  A single default instance

monitoring:
  enabled: MON_ENABLE_BOOL
  endpoint-type: file
  endpoint-location: MON_ENDPOINT_LOCATION

storages:
  - !lru-storage &storage
    size: 256mb

caches:
  - !lru-action-cache &cache
    storage: *storage
    max-cached-refs: 256
    allow-updates: true

instances:
  - name: main
    description: |
      The main server

    services:
      - !action-cache
        cache: *cache

      - !cas
        storage: *storage

      - !bytestream
        storage: *storage
"""

TAG_CONFIGURATION = """
server:
  - !channel
    address: "[::]:50051"
    insecure-mode: true

description: |
  A single default instance

monitoring:
  enabled: MON_ENABLE_BOOL
  endpoint-type: file
  endpoint-location: MON_ENDPOINT_LOCATION
  additional-tags:
    foo: bar

connections:
  - !sql-connection &sql
    connection-string: DB_CONN_STR
    automigrate: yes
    connection-timeout: 15

storages:
  - !lru-storage &storage
    size: 256mb

caches:
  - !lru-action-cache &cache
    storage: *storage
    max-cached-refs: 256
    allow-updates: true

schedulers:
  - !sql-scheduler &scheduler
    sql: *sql
    storage: *storage
    action-cache: *cache

instances:
  - name: main
    description: |
      The main server

    services:
      - !action-cache
        cache: *cache

      - !execution
        scheduler: *scheduler
        endpoints:
          - execution
          - operations

      - !bots
        scheduler: *scheduler

      - !cas
        storage: *storage

      - !bytestream
        storage: *storage
"""


@pytest.mark.parametrize("monitoring", [True, False])
def test_create_server(monitoring):
    # Actual test function, to be run in a subprocess:
    def __test_create_server(remote):
        # Open a channel to the remote server:
        channel = grpc.insecure_channel(remote)

        try:
            stub = remote_execution_pb2_grpc.ExecutionStub(channel)
            request = remote_execution_pb2.ExecuteRequest(instance_name="main")
            response = next(stub.Execute(request))

            assert response.DESCRIPTOR is operations_pb2.Operation.DESCRIPTOR

        except grpc.RpcError as e:
            assert e.code() != grpc.StatusCode.UNIMPLEMENTED

        try:
            stub = remote_execution_pb2_grpc.ActionCacheStub(channel)
            request = remote_execution_pb2.GetActionResultRequest(instance_name="main")
            response = stub.GetActionResult(request)

            assert response.DESCRIPTOR is remote_execution_pb2.ActionResult.DESCRIPTOR

        except grpc.RpcError as e:
            assert e.code() != grpc.StatusCode.UNIMPLEMENTED

        try:
            stub = remote_execution_pb2_grpc.ContentAddressableStorageStub(channel)
            request = remote_execution_pb2.BatchUpdateBlobsRequest(instance_name="main")
            response = stub.BatchUpdateBlobs(request)

            assert response.DESCRIPTOR is remote_execution_pb2.BatchUpdateBlobsResponse.DESCRIPTOR

        except grpc.RpcError as e:
            assert e.code() != grpc.StatusCode.UNIMPLEMENTED

        try:
            stub = bytestream_pb2_grpc.ByteStreamStub(channel)
            request = bytestream_pb2.ReadRequest()
            response = stub.Read(request)

            assert next(response).DESCRIPTOR is bytestream_pb2.ReadResponse.DESCRIPTOR

        except grpc.RpcError as e:
            assert e.code() != grpc.StatusCode.UNIMPLEMENTED

        try:
            stub = operations_pb2_grpc.OperationsStub(channel)
            request = operations_pb2.ListOperationsRequest(name="main")
            response = stub.ListOperations(request)

            assert response.DESCRIPTOR is operations_pb2.ListOperationsResponse.DESCRIPTOR

        except grpc.RpcError as e:
            assert e.code() != grpc.StatusCode.UNIMPLEMENTED

        try:
            session = bots_pb2.BotSession(leases=[], bot_id="test-bot", name="test-bot", status=BotStatus.OK.value)
            stub = bots_pb2_grpc.BotsStub(channel)
            request = bots_pb2.CreateBotSessionRequest(parent="main", bot_session=session)
            response = stub.CreateBotSession(request, timeout=0.5)

            assert response.DESCRIPTOR is bots_pb2.BotSession.DESCRIPTOR

        except grpc.RpcError as e:
            assert e.code() != grpc.StatusCode.UNIMPLEMENTED

        try:
            stub = health_pb2_grpc.HealthStub(channel)
            request = health_pb2.HealthCheckRequest(service="")
            response = stub.Check(request)
            assert response.status == health_pb2.HealthCheckResponse.SERVING

            request = health_pb2.HealthCheckRequest(
                service=remote_execution_pb2.DESCRIPTOR.services_by_name["Execution"].full_name
            )
            response = stub.Check(request)
            assert response.status == health_pb2.HealthCheckResponse.SERVING

        except grpc.RpcError as e:
            assert e.code() != grpc.StatusCode.UNIMPLEMENTED

        time.sleep(0.5)
        return True

    with serve(CONFIGURATION, monitoring) as (server, path):
        assert __test_create_server(server.remote)

        if monitoring:
            time.sleep(0.5)
            server.stop()
            with open(path) as f:
                metrics = [line.strip() for line in f.readlines()]
            # There should be a bots-count record from before we connected the bot,
            # and also one from after the bot had made its initial connection
            assert "scheduler.bots.count,instanceName=main,propertyLabel=unknown,state=OK:0.0|g" in metrics
            assert "scheduler.bots.count,instanceName=main,propertyLabel=unknown,state=OK:1.0|g" in metrics
            assert "connections.clients.count,instanceName=main:0.0|g" in metrics


def test_state_metrics_server_no_execute():
    with serve(NO_EXECUTE_CONFIG, True) as (server, path):
        # Give some time for the metrics monitor to tick.
        time.sleep(0.5)
        server.stop()
        with open(path) as f:
            metrics = [line.strip() for line in f.readlines()]
        # Bots service and a scheduler both exist, so relevant metrics should be published
        assert "scheduler.bots.count,instanceName=main,propertyLabel=unknown,state=OK:0.0|g" in metrics
        assert "scheduler.jobs.count,instanceName=main,propertyLabel=unknown,state=QUEUED:0.0|g" in metrics
        # This metric requires an execution service, so shouldn't be published
        assert "connections.clients.count,instanceName=main:0.0|g" not in metrics


def test_state_metrics_additional_tags():
    with serve(TAG_CONFIGURATION, True) as (server, path):
        # Give some time for the metrics monitor to tick.
        time.sleep(0.5)
        server.stop()
        with open(path) as f:
            metrics = [line.strip() for line in f.readlines()]
        # Bots service and a scheduler both exist, so relevant metrics should be published
        assert "scheduler.bots.count,foo=bar,instanceName=main,propertyLabel=unknown,state=OK:0.0|g" in metrics
        assert "scheduler.jobs.count,foo=bar,instanceName=main,propertyLabel=unknown,state=QUEUED:0.0|g" in metrics
        assert "connections.clients.count,foo=bar,instanceName=main:0.0|g" in metrics


def test_state_metrics_server_only_storage():
    # Actual test function, to be run in a subprocess:
    def __test_create_server(remote):
        # Open a channel to the remote server:
        grpc.insecure_channel(remote)
        # Sleep to allow a useful number of metrics to get reported
        asyncio.get_event_loop().run_until_complete(asyncio.sleep(1))
        return True

    with serve(ONLY_STORAGE_CONFIG, True) as (server, path):
        assert __test_create_server(server.remote)
        time.sleep(0.5)
        server.stop()

        with open(path) as f:
            metrics = [line.strip() for line in f.readlines()]

        # The state-monitoring-thread currently only has metrics for execution/bots/schedulers,
        # So no metrics should be published, but the server should run successfully
        assert metrics == []
