# Copyright (C) 2024 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import collections
import tempfile
from datetime import datetime, timedelta
from unittest import mock

import pytest
from google.protobuf.duration_pb2 import Duration
from grpc import StatusCode

from buildgrid._protos.google.rpc import status_pb2
from buildgrid.server.exceptions import RetriableDatabaseError
from buildgrid.server.settings import MIN_TIME_BETWEEN_SQL_POOL_DISPOSE_MINUTES
from buildgrid.server.sql import models
from buildgrid.server.sql.provider import SqlProvider
from tests.utils.fixtures import pg_schema_provider
from tests.utils.sql import add_test_job


class MockDBError(Exception):
    pass


@pytest.fixture(params=["sqlite", "postgresql"])
def sql(request, postgres):
    if request.param == "sqlite":
        with tempfile.NamedTemporaryFile() as db:
            sql_provider = SqlProvider(connection_string=f"sqlite:///{db.name}", automigrate=True)
            yield sql_provider
    elif request.param == "postgresql":
        with pg_schema_provider(postgres) as sql_provider:
            yield sql_provider


@pytest.mark.parametrize(
    "conn_str",
    [
        "sqlite:///file:memdb1?option=value&cache=shared&mode=memory",
        "sqlite:///file:memdb1?mode=memory&cache=shared",
        "sqlite:///file:memdb1?cache=shared&mode=memory",
        "sqlite:///file::memory:?cache=shared",
        "sqlite:///file::memory:",
        "sqlite:///:memory:",
        "sqlite:///",
        "sqlite://",
    ],
)
def test_is_sqlite_inmemory_connection_string(conn_str):
    with pytest.raises(ValueError):
        # Should raise ValueError when trying to instantiate
        _ = SqlProvider(connection_string=conn_str)


@pytest.mark.parametrize("conn_str", ["sqlite:///../../myfile.db", "sqlite:///./myfile.db", "sqlite:////myfile.db"])
def test_file_based_sqlite_db(conn_str):
    # Those should be OK and not raise anything during instantiation
    with mock.patch("buildgrid.server.sql.provider.create_engine") as create_engine:
        _ = SqlProvider(connection_string=conn_str)
        assert create_engine.call_count == 1


def test_no_connection_string():
    provider = SqlProvider()
    assert provider.dialect == "sqlite"
    assert provider._database_tempfile is not None


@pytest.mark.parametrize("conn_str", ["postgresql+psycopg2://example", "sqlite:////myfile.db"])
def test_dialect(conn_str):
    provider = SqlProvider(connection_string=conn_str)
    if conn_str.startswith("sqlite"):
        assert provider.dialect == "sqlite"
    else:
        assert provider.dialect == "postgresql"


def test_session_rollback(sql):
    with mock.patch("sqlalchemy.orm.session.Session.rollback") as rollback:
        with sql.session(exceptions_to_not_raise_on=[MockDBError]):
            raise MockDBError("Forced exception")
        rollback.assert_called_once()


def test_session_rollback_suppression(sql):
    with mock.patch("sqlalchemy.orm.session.Session.rollback") as rollback:
        with sql.session(exceptions_to_not_raise_on=[MockDBError], exceptions_to_not_rollback_on=[MockDBError]):
            raise MockDBError("Forced exception")
        rollback.assert_not_called()


def test_suppressed_rollback_committed(sql):
    job_name = "test-job"
    other_name = "other-job"
    add_test_job(job_name, sql)

    with sql.session(exceptions_to_not_raise_on=[MockDBError], exceptions_to_not_rollback_on=[MockDBError]) as session:
        job = session.query(models.JobEntry).filter_by(name=job_name).first()
        job.name = other_name
        raise MockDBError("Forced exception")

    with sql.session() as session:
        original_job = session.query(models.JobEntry).filter_by(name=job_name).first()
        assert original_job is None
        modified_job = session.query(models.JobEntry).filter_by(name=other_name).first()
        assert modified_job is not None


def test_session_exception_suppression(sql):
    # First check that re-raising exceptions works properly
    with pytest.raises(AssertionError):
        with sql.session():
            assert False

    # Now check that the same exception can be suppressed
    with sql.session(exceptions_to_not_raise_on=[AssertionError]):
        assert False


def test_scoped_session(sql):
    s = None

    # Get a scoped session
    with sql.scoped_session() as session:
        s = session

    # Get another scoped session (in the same thread)
    with sql.scoped_session() as session:
        # This should be the same Session object as before
        assert session == s

    # Remove the scoped session, and get another
    sql.remove_scoped_session()
    with sql.scoped_session() as session:
        # This should be a totally new Session
        assert session != s


def _get_disposal_exceptions(sql):
    dialect = sql.dialect
    assert dialect != ""

    exceptions = sql._sql_pool_dispose_helper._dispose_pool_on_exceptions
    if dialect == "postgresql":
        assert len(exceptions) > 0

    return exceptions or ()


def test_pool_disposal_exceptions(sql):
    disposal_exceptions = _get_disposal_exceptions(sql)
    sql._engine.dispose = mock.MagicMock()
    sql._sql_pool_dispose_helper._cooldown_time_in_secs = 10
    sql._sql_pool_dispose_helper._cooldown_jitter_base_in_secs = 5

    # Throw some random exceptions and ensure we don't dispose the pool
    for e in (ValueError, KeyError):
        try:
            with sql.session():
                raise MockDBError("Forced exception") from e()
        except MockDBError:
            pass
        sql._engine.dispose.assert_not_called()

    # Throw the exceptions we want to dispose the pool on
    # Ensure we only dispose the pool once in a short period of time
    for e in disposal_exceptions:
        with pytest.raises(RetriableDatabaseError) as exc_info:
            with sql.session():
                raise Exception("Disposal exception") from e()
        exc = exc_info.value
        assert exc.retry_info is not None

        # Validate that the retry delay is in the correct range
        minimum_dur = Duration(seconds=5).ToTimedelta()
        maximum_dur = Duration(seconds=15).ToTimedelta()
        assert exc.retry_info.retry_delay.ToTimedelta() <= maximum_dur
        assert exc.retry_info.retry_delay.ToTimedelta() >= minimum_dur

        # Validate the other exception fields
        retry_message = "Database connection was temporarily interrupted, please retry"
        assert exc.error_status.code == StatusCode.UNAVAILABLE
        assert exc.error_status.details == retry_message
        assert exc.error_status.trailing_metadata is not None

        # Construct the expected Status message
        expected_metadata = status_pb2.Status(code=14, message=retry_message)
        error_detail = expected_metadata.details.add()
        error_detail.Pack(exc.retry_info)
        expected_metadata_str = expected_metadata.SerializeToString()
        found_metadata = False
        # Go through and find all the 'grpc-status-details-bin' metadata keys and
        # verify the Status message is one of them and has the expected contents
        for trailing_metadata in exc.error_status.trailing_metadata:
            if trailing_metadata[0] == "grpc-status-details-bin" and trailing_metadata[1] == expected_metadata_str:
                found_metadata = True
        if not found_metadata:
            pytest.fail("Status message was not sent as part of trailing_metadata")
        # Should only call this once in a short period of time,
        # regardless of how many exceptions we have
        sql._engine.dispose.assert_called_once()


def test_pool_disposal_limit(sql):
    disposal_exceptions = _get_disposal_exceptions(sql)
    sql._engine.dispose = mock.MagicMock()
    sql._sql_pool_dispose_helper._cooldown_time_in_secs = 1

    # Throw the exceptions we want to dispose the pool on
    # Ensure we only dispose the pool once in a short period of time
    for e in disposal_exceptions:
        with pytest.raises(Exception):
            with sql.session():
                raise Exception("Forced exception") from e()
        # Should only call this once in a short period of time,
        # regardless of how many exceptions we have
        sql._engine.dispose.assert_called_once()

    # Now manually override the last disposal time and see if that triggers another disposal (once more)
    timestamp_disposed_a_while_back = datetime.utcnow() - 2 * timedelta(
        minutes=MIN_TIME_BETWEEN_SQL_POOL_DISPOSE_MINUTES
    )
    sql._sql_pool_dispose_helper._last_pool_dispose_time = timestamp_disposed_a_while_back

    # Throw the exceptions we want to dispose the pool on
    # Ensure we dispose the pool again, but only once more
    for e in disposal_exceptions:
        with pytest.raises(Exception):
            with sql.session():
                raise Exception("Forced exception") from e()
        # Should only call this once in a short period of time,
        # regardless of how many exceptions we have
        assert sql._engine.dispose.call_count == 2


def test_session_metrics(sql: SqlProvider) -> None:
    counter = collections.defaultdict(int)
    gauge = collections.defaultdict(int)

    def publish_counter(name, value):
        counter[name] += value

    def publish_gauge(name, value):
        gauge[name] = value

    # GIVEN
    with (
        mock.patch("buildgrid.server.sql.provider.publish_counter_metric") as mock_publish_counter,
        mock.patch("buildgrid.server.sql.provider.publish_gauge_metric") as mock_publish_gauge,
    ):
        mock_publish_counter.side_effect = publish_counter
        mock_publish_gauge.side_effect = publish_gauge

        # WHEN
        with sql.session():
            gauge1 = sum(gauge.values())
            with sql.session():
                gauge2 = sum(gauge.values())
        with sql.session():
            gauge3 = sum(gauge.values())

        assert gauge1 == 1
        assert gauge2 == 2
        assert gauge3 == 1
        assert sum(counter.values()) == 3
