# Copyright (C) 2024 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import operator
from datetime import datetime
from unittest import mock

import pytest
from sqlalchemy import Boolean, Column, DateTime, Integer, String
from sqlalchemy.sql.expression import and_, or_

from buildgrid.server.exceptions import InvalidArgumentError
from buildgrid.server.operations.filtering import DEFAULT_SORT_KEYS, FilterParser, OperationFilter, SortKey
from buildgrid.server.sql import models
from buildgrid.server.sql.utils import (
    DATETIME_FORMAT,
    build_page_filter,
    build_page_token,
    build_pagination_clause_for_sort_key,
    build_sort_column_list,
    convert_filter_to_sql_filter,
    dump_list_operations_token_value,
    extract_sort_keys,
    parse_list_operations_sort_value,
)


class FakeModel(models.Base):
    __tablename__ = "fakemodel"
    id = Column(String, primary_key=True)
    bool_column = Column(Boolean)
    datetime_column = Column(DateTime)
    integer_column = Column(Integer)
    string_column = Column(String)


def test_parse_list_operations_sort_value():
    """Basic tests for parsing various types out of a page token."""
    # Boolean
    assert parse_list_operations_sort_value("True", FakeModel.bool_column) is True
    assert parse_list_operations_sort_value("False", FakeModel.bool_column) is False
    # Datetime
    timestamp = datetime.now()
    timestamp_str = datetime.strftime(timestamp, DATETIME_FORMAT)
    assert parse_list_operations_sort_value(timestamp_str, FakeModel.datetime_column) == timestamp
    # Integer
    assert parse_list_operations_sort_value("42", FakeModel.integer_column) == 42
    assert parse_list_operations_sort_value("0", FakeModel.integer_column) == 0
    assert parse_list_operations_sort_value("-1", FakeModel.integer_column) == -1
    # String
    assert parse_list_operations_sort_value("test string", FakeModel.string_column) == "test string"
    assert parse_list_operations_sort_value("", FakeModel.string_column) == ""


def test_dump_list_operations_token_value():
    """Test changing a Python value into the corresponding string for the page token."""
    assert dump_list_operations_token_value("foo") == "foo"
    assert dump_list_operations_token_value(False) == "False"
    assert dump_list_operations_token_value(12345) == "12345"
    assert dump_list_operations_token_value(datetime(2020, 7, 30, 14, 37, 56)) == "2020-07-30-14-37-56-000000"


def test_build_pagination_clause_for_sort_key():
    """Test the helper function to build a single clause of the pagination filter."""
    sort_keys = [SortKey(name="stage", descending=False), SortKey(name="name", descending=True)]
    # Illegal case (previous_sort_values too long)
    with pytest.raises(ValueError):
        build_pagination_clause_for_sort_key(3, [1, "foo"], sort_keys)
    # Standard case (no prev)
    actual_clause = build_pagination_clause_for_sort_key(3, [], sort_keys)
    expected_clause = models.JobEntry.stage > 3
    assert actual_clause.compare(expected_clause)
    # Standard case (one prev)
    actual_clause = build_pagination_clause_for_sort_key("foo", [3], sort_keys)
    expected_clause = and_(models.JobEntry.stage == 3, models.OperationEntry.name < "foo")
    assert actual_clause.compare(expected_clause)


def test_build_page_filter():
    """Test building a page filter."""
    # Invalid page filters
    with pytest.raises(InvalidArgumentError):
        page_filter = "2020-07-21-21-59-43-489920|test-operation-100|extra-stuff"
        build_page_filter(page_filter, DEFAULT_SORT_KEYS)
    with pytest.raises(InvalidArgumentError):
        page_filter = "2020-07-21-21-59-43-489920"
        build_page_filter(page_filter, DEFAULT_SORT_KEYS)

    timestamp = datetime.utcnow()
    arbitrary_operation_name = "operation-name-13245"
    page_filter = f"{timestamp.strftime(DATETIME_FORMAT)}|{arbitrary_operation_name}"
    page_filter = build_page_filter(page_filter, DEFAULT_SORT_KEYS)

    expected_page_filter = or_(
        models.JobEntry.queued_timestamp > timestamp,
        and_(models.JobEntry.queued_timestamp == timestamp, models.OperationEntry.name > arbitrary_operation_name),
    )
    assert page_filter.compare(expected_page_filter)


def test_build_page_token():
    """Test building a page token."""
    operation = mock.Mock()
    operation.name = "my-operation"
    operation.job.queued_timestamp = datetime(2020, 7, 20, 1, 1, 1, 323040)
    assert build_page_token(operation, []) == ""
    sort_keys = [SortKey(name="queued_time", descending=True), SortKey(name="name", descending=False)]
    assert build_page_token(operation, sort_keys) == "2020-07-20-01-01-01-323040|my-operation"


def test_extract_sort_keys():
    """Test splitting the sort keys from the rest of the filters."""
    filters = FilterParser.parse_listoperations_filters("sort_order = foo & sort_order = bar(desc)")
    assert extract_sort_keys(filters) == (
        [
            SortKey(name="foo", descending=False),
            SortKey(name="bar", descending=True),
        ],
        [],
    )

    filters = FilterParser.parse_listoperations_filters("sort_order = name(desc) & sort_order = foo(asc)")
    assert extract_sort_keys(filters) == (
        [SortKey(name="name", descending=True), SortKey(name="foo", descending=False)],
        [],
    )

    # Make sure that non-sort filters are extracted properly
    filters = FilterParser.parse_listoperations_filters(
        "sort_order = queued_time & name > bar & sort_order = stage(desc)"
    )
    assert extract_sort_keys(filters) == (
        [SortKey(name="queued_time", descending=False), SortKey(name="stage", descending=True)],
        [OperationFilter(parameter="name", operator=operator.gt, value="bar")],
    )


def test_build_sort_column_list():
    """Test that the sort columns are properly produced."""
    # Empty list produces empty list
    assert build_sort_column_list([]) == []

    # Invalid sort keys produce an InvalidArgumentError
    with pytest.raises(InvalidArgumentError):
        build_sort_column_list([SortKey(name="foo", descending=False)])

    # Some normal sort column lists
    actual_sort_columns = build_sort_column_list([SortKey(name="queued_time", descending=True)])
    expected_sort_columns = [models.JobEntry.queued_timestamp.desc()]
    for actual, expected in zip(actual_sort_columns, expected_sort_columns):
        assert actual.compare(expected)

    actual_sort_columns = build_sort_column_list(
        [SortKey(name="name", descending=False), SortKey(name="start_time", descending=True)]
    )
    expected_sort_columns = [models.OperationEntry.name.asc(), models.JobEntry.worker_start_timestamp.desc()]
    for actual, expected in zip(actual_sort_columns, expected_sort_columns):
        assert actual.compare(expected)


def test_convert_filter_to_sql_filter():
    """Test converting filters to SQL clauses.
    Each supported operator should be properly handled."""
    operators = [operator.eq, operator.ne, operator.lt, operator.le, operator.gt, operator.ge]
    for op in operators:
        timestamp = datetime(2020, 8, 1)
        op_filter = OperationFilter(parameter="completed_time", operator=op, value=timestamp)
        expected_filter = op(models.JobEntry.worker_completed_timestamp, timestamp)
        actual_filter = convert_filter_to_sql_filter(op_filter)
        assert expected_filter.compare(actual_filter)
