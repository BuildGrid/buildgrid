# Copyright (C) 2019 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import tempfile
import time
import uuid
from datetime import datetime, timedelta
from time import sleep
from unittest import mock

import pytest
import sqlalchemy

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid._protos.google.rpc import code_pb2
from buildgrid.server.cas.storage import lru_memory_cache
from buildgrid.server.cas.storage.storage_abc import create_write_session
from buildgrid.server.context import current_instance, instance_context
from buildgrid.server.enums import BotStatus, LeaseState, OperationStage
from buildgrid.server.exceptions import DatabaseError, InvalidArgumentError
from buildgrid.server.monitoring import set_monitoring_bus
from buildgrid.server.operations.filtering import DEFAULT_OPERATION_FILTERS, FilterParser
from buildgrid.server.scheduler import AgedJobHandlerOptions, Scheduler
from buildgrid.server.scheduler.properties import hash_from_dict
from buildgrid.server.sql import models
from buildgrid.server.sql.provider import SqlProvider
from buildgrid.server.sql.utils import DATETIME_FORMAT
from tests.utils.fixtures import pg_schema_provider, pg_schema_provider_pair
from tests.utils.metrics import mock_create_timer_record
from tests.utils.sql import add_test_job, generate_dummy_action
from tests.utils.utils import poll_and_assert

TEST_JOB_PRUNING_PERIOD = timedelta(milliseconds=50)
INSTANCE_NAME = "test_instance"


def populated_operation_request_metadata_fields():
    """These are `Operation()` kwargs related to information that a
    client attaches in a RequestMetadata message.
    """
    return {
        "tool_name": "bgd",
        "tool_version": "v1.2.3",
        "invocation_id": "inv-id-0",
        "correlated_invocations_id": "corr-inv-id-1",
    }


@pytest.fixture(params=["sqlite", "postgresql"])
def scheduler(request, postgres, common_props):
    set_monitoring_bus(mock.Mock())
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)

    if request.param == "sqlite":
        with tempfile.NamedTemporaryFile() as db:
            sql_provider = SqlProvider(connection_string=f"sqlite:///{db.name}", automigrate=True)
            scheduler = Scheduler(sql_provider, storage, poll_interval=0.02, property_set=common_props)
            scheduler.action_browser_url = "https://localhost/"
            with instance_context(INSTANCE_NAME):
                yield scheduler

    elif request.param == "postgresql":
        with pg_schema_provider_pair(postgres) as (sql_provider, sql_ro_provider):
            scheduler = Scheduler(
                sql_provider,
                storage,
                sql_ro_provider=sql_ro_provider,
                poll_interval=0.02,
                property_set=common_props,
            )
            scheduler.action_browser_url = "https://localhost/"
            with instance_context(INSTANCE_NAME):
                yield scheduler


@pytest.fixture()
def database_pair():
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)
    with tempfile.NamedTemporaryFile() as db:
        sql_provider_1 = SqlProvider(connection_string=f"sqlite:///{db.name}", automigrate=True)
        sql_provider_2 = SqlProvider(connection_string=f"sqlite:///{db.name}", automigrate=False)

        scheduler_1 = Scheduler(sql_provider_1, storage, poll_interval=0.02)
        scheduler_2 = Scheduler(sql_provider_2, storage, poll_interval=0.02)

        with scheduler_1, scheduler_2:
            yield scheduler_1, scheduler_2


@pytest.fixture()
def database_pruning_enabled(postgres, common_props):
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)
    pruning_options = AgedJobHandlerOptions(
        job_max_age=timedelta(seconds=1),
        handling_period=TEST_JOB_PRUNING_PERIOD,
        max_handling_window=1000,
    )
    with pg_schema_provider(postgres) as sql_provider:
        scheduler = Scheduler(
            sql_provider, storage, poll_interval=0.2, pruning_options=pruning_options, property_set=common_props
        )
        scheduler.action_browser_url = "https://localhost/"
        with scheduler:
            yield scheduler


@pytest.fixture
def database_queue_timeout_enabled(postgres, common_props):
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)

    queue_timeout_options = AgedJobHandlerOptions(
        job_max_age=timedelta(seconds=60),
        handling_period=timedelta(seconds=0.01),
        max_handling_window=10,
    )
    with pg_schema_provider(postgres) as sql_provider:
        scheduler = Scheduler(
            sql_provider,
            storage,
            poll_interval=0.2,
            queue_timeout_options=queue_timeout_options,
            property_set=common_props,
        )
        scheduler.action_browser_url = "https://localhost/"
        with scheduler:
            yield scheduler


def add_test_blob(digest, data, storage):
    with create_write_session(digest) as session:
        session.write(data)
        storage.commit_write(digest, session)


def add_test_execute_response(scheduler):
    test_execute_response = remote_execution_pb2.ExecuteResponse(
        result=remote_execution_pb2.ActionResult(exit_code=12)
    )
    test_execute_response_digest = remote_execution_pb2.Digest(hash="finished-action-execute-response", size_bytes=123)
    add_test_blob(test_execute_response_digest, test_execute_response.SerializeToString(), scheduler.storage)
    return test_execute_response_digest


def populate_database(scheduler):
    test_execute_response_digest = add_test_execute_response(scheduler)
    with scheduler._sql.session() as session:
        now = datetime.utcnow()
        session.add_all(
            [
                models.RequestMetadataEntry(id=1, **populated_operation_request_metadata_fields(), target_id="test"),
                models.JobEntry(
                    name="test-job",
                    action=generate_dummy_action().SerializeToString(),
                    action_digest="test-action/100",
                    priority=1,
                    stage=OperationStage.QUEUED.value,
                    leases=[models.LeaseEntry(status=0, state=LeaseState.ACTIVE.value)],
                    operations=[
                        models.OperationEntry(
                            name="test-operation",
                            request_metadata_id=1,
                            **populated_operation_request_metadata_fields(),
                        )
                    ],
                    create_timestamp=now,
                    queued_timestamp=now,
                    platform_requirements=hash_from_dict({"OSFamily": ["sunos"]}),
                    property_label="sunos",
                    instance_name=INSTANCE_NAME,
                    command="foo",
                ),
                models.JobEntry(
                    name="other-job",
                    action=generate_dummy_action().SerializeToString(),
                    action_digest="other-action/10",
                    priority=5,
                    stage=OperationStage.QUEUED.value,
                    leases=[models.LeaseEntry(status=0, state=LeaseState.PENDING.value)],
                    operations=[
                        models.OperationEntry(
                            name="other-operation",
                            request_metadata_id=1,
                            **populated_operation_request_metadata_fields(),
                        )
                    ],
                    create_timestamp=now,
                    queued_timestamp=now,
                    platform_requirements=hash_from_dict({"OSFamily": ["linux"]}),
                    property_label="linux",
                    instance_name=INSTANCE_NAME,
                    command="foo",
                ),
                models.JobEntry(
                    name="linux-job",
                    action=generate_dummy_action().SerializeToString(),
                    action_digest="linux-action/10",
                    priority=7,
                    stage=OperationStage.QUEUED.value,
                    leases=[models.LeaseEntry(status=0, state=LeaseState.PENDING.value)],
                    operations=[
                        models.OperationEntry(
                            name="linux-operation",
                            request_metadata_id=1,
                            **populated_operation_request_metadata_fields(),
                        )
                    ],
                    create_timestamp=now,
                    queued_timestamp=now,
                    platform_requirements=hash_from_dict({"OSFamily": ["linux"]}),
                    property_label="linux",
                    instance_name=INSTANCE_NAME,
                    command="foo",
                ),
                models.JobEntry(
                    name="extra-job",
                    action=generate_dummy_action().SerializeToString(),
                    action_digest="extra-action/50",
                    priority=20,
                    stage=OperationStage.QUEUED.value,
                    leases=[models.LeaseEntry(status=0, state=LeaseState.PENDING.value)],
                    operations=[
                        models.OperationEntry(
                            name="extra-operation",
                            request_metadata_id=1,
                            **populated_operation_request_metadata_fields(),
                        )
                    ],
                    create_timestamp=now,
                    queued_timestamp=now,
                    platform_requirements=hash_from_dict({"OSFamily": ["linux"], "generic": ["requirement"]}),
                    property_label="linux",
                    instance_name=INSTANCE_NAME,
                    command="foo",
                ),
                models.JobEntry(
                    name="cancelled-job",
                    action=generate_dummy_action().SerializeToString(),
                    action_digest="cancelled-action/35",
                    priority=20,
                    stage=OperationStage.COMPLETED.value,
                    cancelled=True,
                    create_timestamp=now,
                    queued_timestamp=now,
                    queued_time_duration=60,
                    worker_start_timestamp=datetime(2019, 6, 1, minute=1),
                    leases=[models.LeaseEntry(status=0, state=LeaseState.CANCELLED.value)],
                    operations=[
                        models.OperationEntry(
                            name="cancelled-operation",
                            cancelled=True,
                            request_metadata_id=1,
                            **populated_operation_request_metadata_fields(),
                        )
                    ],
                    platform_requirements=hash_from_dict({"OSFamily": ["linux"]}),
                    property_label="linux",
                    instance_name=INSTANCE_NAME,
                    command="foo",
                ),
                models.JobEntry(
                    name="finished-job",
                    action=generate_dummy_action().SerializeToString(),
                    action_digest="finished-action/35",
                    result=f"{test_execute_response_digest.hash}/{test_execute_response_digest.size_bytes}",
                    priority=20,
                    stage=OperationStage.COMPLETED.value,
                    create_timestamp=datetime(2019, 6, 1),
                    queued_timestamp=datetime(2019, 6, 1),
                    queued_time_duration=10,
                    worker_start_timestamp=datetime(2019, 6, 1, second=10),
                    worker_completed_timestamp=datetime(2019, 6, 1, minute=1),
                    leases=[models.LeaseEntry(status=0, state=LeaseState.COMPLETED.value)],
                    operations=[
                        models.OperationEntry(
                            name="finished-operation",
                            request_metadata_id=1,
                            **populated_operation_request_metadata_fields(),
                        )
                    ],
                    platform_requirements=hash_from_dict({"OSFamily": ["linux"]}),
                    property_label="linux",
                    instance_name=INSTANCE_NAME,
                    command="foo",
                ),
                models.JobEntry(
                    name="platform-job",
                    action=generate_dummy_action().SerializeToString(),
                    action_digest="platform-action/10",
                    priority=5,
                    stage=OperationStage.QUEUED.value,
                    leases=[models.LeaseEntry(status=0, state=LeaseState.PENDING.value)],
                    operations=[
                        models.OperationEntry(
                            name="platform-operation",
                            request_metadata_id=1,
                            **populated_operation_request_metadata_fields(),
                        )
                    ],
                    create_timestamp=now,
                    queued_timestamp=now,
                    platform_requirements=hash_from_dict(
                        {"OSFamily": ["aix"], "generic": ["requirement", "requirement2"]}
                    ),
                    property_label="aix",
                    instance_name=INSTANCE_NAME,
                    command="foo",
                ),
                models.JobEntry(
                    name="old-queued-job",
                    action=generate_dummy_action().SerializeToString(),
                    action_digest="old-queued-job/10",
                    priority=10,
                    stage=OperationStage.QUEUED.value,
                    leases=[],
                    operations=[
                        models.OperationEntry(
                            name="old-queued-job-operation",
                            request_metadata_id=1,
                            **populated_operation_request_metadata_fields(),
                        )
                    ],
                    create_timestamp=now - timedelta(seconds=2),
                    queued_timestamp=now - timedelta(seconds=2),
                    platform_requirements=hash_from_dict({"OSFamily": ["linux"]}),
                    property_label="linux",
                    instance_name=INSTANCE_NAME,
                    command="foo",
                ),
            ]
        )


def completed_job(prefix, index, timestamp, platform, instance_name: str) -> models.JobEntry:
    now = timestamp - timedelta(seconds=10)
    return models.JobEntry(
        name=f"{prefix}-job/{index}",
        action=generate_dummy_action().SerializeToString(),
        action_digest=f"{prefix}-action/{index}",
        priority=20,
        stage=OperationStage.COMPLETED.value,
        create_timestamp=now,
        queued_timestamp=now,
        queued_time_duration=10,
        worker_start_timestamp=timestamp,
        worker_completed_timestamp=timestamp + timedelta(seconds=10),
        leases=[models.LeaseEntry(status=0, state=LeaseState.COMPLETED.value)],
        operations=[models.OperationEntry(name=f"{prefix}-operation/{index}")],
        platform_requirements=hash_from_dict({"OSFamily": ["linux"]}),
        platform=platform,
        property_label="linux",
        instance_name=instance_name,
        command="foo",
    )


def queued_job(prefix: str, index: int, timestamp: datetime, instance_name: str) -> models.JobEntry:
    return models.JobEntry(
        name=f"{prefix}-job/{index}",
        action=generate_dummy_action().SerializeToString(),
        action_digest=f"{prefix}-action/{index}",
        priority=20,
        stage=OperationStage.QUEUED.value,
        create_timestamp=timestamp,
        queued_timestamp=timestamp,
        leases=[],
        operations=[models.OperationEntry(name=f"{prefix}-operation/{index}")],
        platform_requirements=hash_from_dict({"OSFamily": ["linux"]}),
        property_label="linux",
        instance_name=instance_name,
        command="foo",
    )


def test_get_operation_request_metadata_by_name(scheduler):
    populate_database(scheduler)

    # Non-existing operation:
    assert scheduler.get_operation_request_metadata_by_name("notarealoperation") is None

    # Existing operation:
    metadata = scheduler.get_operation_request_metadata_by_name("extra-operation")
    assert metadata is not None

    # We get back the values that were pre-populated by this method:
    expected_values = populated_operation_request_metadata_fields()
    assert metadata.tool_details.tool_name == expected_values["tool_name"]
    assert metadata.tool_details.tool_version == expected_values["tool_version"]
    assert metadata.tool_invocation_id == expected_values["invocation_id"]
    assert metadata.correlated_invocations_id == expected_values["correlated_invocations_id"]
    assert metadata.target_id == "test"


def test_hash_from_dict():
    config = {"OSFamily": ["Linux"], "ISA": ["x86-32", "x86-64"]}
    assert hash_from_dict(config) == "2844e4e6d221f4205cdb70c344f51db79dc1bd80"


def compare_lists_of_dicts(list1, list2):
    for dictionary in list1:
        assert dictionary in list2
    for dictionary in list2:
        assert dictionary in list1


@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_list_operations(scheduler):
    populate_database(scheduler)
    operations, next_page_token = scheduler.list_operations()
    # Only incomplete operations are returned
    assert len(operations) == 8
    assert next_page_token == ""


def test_list_operations_filtered(scheduler):
    populate_database(scheduler)
    operation_filters = FilterParser.parse_listoperations_filters("stage != completed")
    operations, next_page_token = scheduler.list_operations(operation_filters=operation_filters)
    assert len(operations) == 6
    assert next_page_token == ""


def test_list_operations_sort_only(scheduler):
    populate_database(scheduler)
    operation_filters = FilterParser.parse_listoperations_filters("sort_order = name(asc)")
    operations, next_page_token = scheduler.list_operations(operation_filters)
    # Only incomplete operations are returned
    assert len(operations) == 8
    assert next_page_token == ""


def check_operation_order(operations, sort_keys, database):
    with database._sql.session() as session:
        results = session.query(models.OperationEntry).join(
            models.JobEntry, models.OperationEntry.job_name == models.JobEntry.name
        )
        results = results.order_by(*sort_keys)
        for expected_operation, actual_operation in zip(results, operations):
            if expected_operation.name != actual_operation.name:
                return False

    return True


def test_list_operations_sorted(scheduler):
    def check_sort_filters(filter_string, sort_columns):
        operation_filters = FilterParser.parse_listoperations_filters(filter_string)
        operations, _ = scheduler.list_operations(operation_filters=operation_filters)
        assert check_operation_order(operations, sort_columns, scheduler)

    populate_database(scheduler)
    # By default, operations are primarily sorted by queued_timestamp,
    check_sort_filters("", [models.JobEntry.queued_timestamp, models.OperationEntry.name])

    # When "name" is given as the "sort_order", the operations should
    # be ordered primarily by name in ascending order
    check_sort_filters("sort_order = name", [models.OperationEntry.name])

    # This is equivalent to putting "(asc)" at the end of the sort order value
    check_sort_filters("sort_order = name(asc)", [models.OperationEntry.name])

    # "(desc)" at the end of the sort_order value should produce descending order
    check_sort_filters("sort_order = name(desc)", [models.OperationEntry.name.desc()])

    # The parser requires that (desc) be attached to the sort key name.
    # Whitespace isn't supported.
    with pytest.raises(InvalidArgumentError):
        check_sort_filters("sort_order = name (desc)", [models.OperationEntry.name.desc()])


def test_large_list_operations_multi_job(scheduler):
    """With a large number of jobs and corresponding operations in the database,
    ensure that the results are properly returned in page form.

    We populate the database with 550 operations and ask for 250-operation pages.
    We should get back two 250-operation pages and one 50-operation page."""
    queued_timestamp = datetime.utcnow()
    total_jobs = 550
    with scheduler._sql.session() as session:
        jobs = [
            models.JobEntry(
                name=f"test-job-{i}",
                action=generate_dummy_action().SerializeToString(),
                action_digest=f"test-action-{i}/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.LeaseEntry(status=0, state=LeaseState.ACTIVE.value)],
                operations=[models.OperationEntry(name=f"test-operation-{i:04}")],
                create_timestamp=queued_timestamp + timedelta(seconds=i),
                queued_timestamp=queued_timestamp + timedelta(seconds=i),
                platform_requirements=hash_from_dict({"OSFamily": "linux"}),
                property_label="linux",
                instance_name=INSTANCE_NAME,
                command="foo",
            )
            for i in range(total_jobs)
        ]
        session.add_all(jobs)

    # Only the first 250 operations are returned the first time
    operations, next_token = scheduler.list_operations(page_size=250)
    assert len(operations) == 250
    token_timestamp = (queued_timestamp + timedelta(seconds=249)).strftime(DATETIME_FORMAT)
    assert next_token == f"{token_timestamp}|test-operation-0249"

    # Another 250 are returned the second time
    operations, next_token = scheduler.list_operations(page_size=250, page_token=next_token)
    assert len(operations) == 250
    token_timestamp = (queued_timestamp + timedelta(seconds=499)).strftime(DATETIME_FORMAT)
    assert next_token == f"{token_timestamp}|test-operation-0499"

    # The last 50 are returned the last time
    operations, next_token = scheduler.list_operations(page_size=250, page_token=next_token)
    assert len(operations) == 50
    assert next_token == ""


def test_large_list_operations_same_job(scheduler):
    """With a large number of operations in the database, ensure that the results
    are properly returned in page form. All of the operations are for a single job.

    We populate the database with 2500 operations and ask for 1000-operation pages.
    We should get back two 1000-operation pages and one 500-operation page."""
    queued_timestamp = datetime.utcnow()
    with scheduler._sql.session() as session:
        session.add(
            models.JobEntry(
                name="test-job",
                action=generate_dummy_action().SerializeToString(),
                action_digest="test-action/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.LeaseEntry(status=0, state=LeaseState.ACTIVE.value)],
                operations=[models.OperationEntry(name=f"test-operation-{i:04}") for i in range(2500)],
                create_timestamp=queued_timestamp,
                queued_timestamp=queued_timestamp,
                platform_requirements=hash_from_dict({"OSFamily": ["sunos"]}),
                property_label="sunos",
                instance_name=INSTANCE_NAME,
                command="foo",
            )
        )
    # Only the first 1000 operations are returned the first time
    operations, next_token = scheduler.list_operations(page_size=1000)
    assert len(operations) == 1000
    token_timestamp = queued_timestamp.strftime(DATETIME_FORMAT)
    assert next_token == f"{token_timestamp}|test-operation-0999"

    # Another 1000 are returned the second time
    operations, next_token = scheduler.list_operations(page_size=1000, page_token=next_token)
    assert len(operations) == 1000
    assert next_token == f"{token_timestamp}|test-operation-1999"

    # The last 500 are returned the last time
    operations, next_token = scheduler.list_operations(page_size=1000, page_token=next_token)
    assert len(operations) == 500
    assert next_token == ""


def test_large_list_operations_filtered(scheduler):
    """We populate the database with 1000 operations and filter for the operations
    [250, 750). We should get back those 500 operations."""
    queued_timestamp = datetime.utcnow()
    with scheduler._sql.session() as session:
        session.add(
            models.JobEntry(
                name="test-job",
                action=generate_dummy_action().SerializeToString(),
                action_digest="test-action/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.LeaseEntry(status=0, state=LeaseState.ACTIVE.value)],
                operations=[models.OperationEntry(name=f"test-operation-{i:03}") for i in range(1000)],
                create_timestamp=queued_timestamp,
                queued_timestamp=queued_timestamp,
                platform_requirements=hash_from_dict({"OSFamily": ["sunos"]}),
                property_label="sunos",
                instance_name=INSTANCE_NAME,
                command="foo",
            )
        )
    op_filters = FilterParser().parse_listoperations_filters("name >= test-operation-250 & name < test-operation-750")
    # Only the first 1000 operations are returned the first time
    operations, next_token = scheduler.list_operations(operation_filters=op_filters)
    assert len(operations) == 500
    for operation in operations:
        assert operation.name >= "test-operation-250" and operation.name < "test-operation-750"
    assert next_token == ""


def test_paginated_custom_sort(scheduler):
    """Demonstrate that custom sort orders work on a paginated result set."""
    queued_timestamp = datetime.utcnow()
    total_jobs = 100
    with scheduler._sql.session() as session:
        jobs = [
            models.JobEntry(
                name=f"test-job-{i}",
                action=generate_dummy_action().SerializeToString(),
                action_digest=f"test-action-{i}/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.LeaseEntry(status=0, state=LeaseState.ACTIVE.value)],
                operations=[models.OperationEntry(name=f"test-operation-{total_jobs - i - 1:04}")],
                create_timestamp=queued_timestamp + timedelta(seconds=i),
                queued_timestamp=queued_timestamp + timedelta(seconds=i),
                platform_requirements=hash_from_dict({"OSFamily": "linux"}),
                property_label="linux",
                instance_name=INSTANCE_NAME,
                command="foo",
            )
            for i in range(total_jobs)
        ]
        session.add_all(jobs)
    operation_filters = FilterParser.parse_listoperations_filters("sort_order = name")
    page_size = 10
    next_page_token = ""
    for _ in range(total_jobs // page_size):
        operations, next_page_token = scheduler.list_operations(
            operation_filters=operation_filters, page_size=page_size, page_token=next_page_token
        )
        assert len(operations) == page_size
        prev_operation = None
        for operation in operations:
            if prev_operation:
                assert prev_operation.name < operation.name
            prev_operation = operation


def test_large_list_operations_exact(scheduler):
    now = datetime.now()
    with scheduler._sql.session() as session:
        session.add(
            models.JobEntry(
                name="test-job",
                action=generate_dummy_action().SerializeToString(),
                action_digest="test-action/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.LeaseEntry(status=0, state=LeaseState.ACTIVE.value)],
                operations=[models.OperationEntry(name=f"test-operation-{i}") for i in range(1000)],
                platform_requirements=hash_from_dict({"OSFamily": ["sunos"]}),
                property_label="sunos",
                instance_name=INSTANCE_NAME,
                command="foo",
                create_timestamp=now,
                queued_timestamp=now,
            )
        )
    # All operations are returned
    operations, next_token = scheduler.list_operations(page_size=1000)
    assert len(operations) == 1000
    # There is no next_token since we've gotten all of the results
    assert next_token == ""


def test_expiry(scheduler):
    max_execution_timeout_seconds = timedelta(seconds=7200)
    queued_timestamp = datetime.utcnow() - 3 * max_execution_timeout_seconds
    old_worker_start_timestamp = datetime.utcnow() - 2 * max_execution_timeout_seconds
    new_worker_start_timestamp = datetime.utcnow()

    num_jobs_queued = 7
    # Add a bunch of jobs in queued state
    with scheduler._sql.session() as session:
        jobs = [
            models.JobEntry(
                name=f"test-queued-job-{i}",
                action=generate_dummy_action().SerializeToString(),
                action_digest=f"test-queued-action-{i}/100",
                priority=1,
                stage=OperationStage.QUEUED.value,
                leases=[models.LeaseEntry(status=0, state=LeaseState.ACTIVE.value)],
                operations=[models.OperationEntry(name=f"test-operation-queued-{i}")],
                create_timestamp=queued_timestamp,
                queued_timestamp=queued_timestamp,
                platform_requirements=hash_from_dict({"OSFamily": "linux"}),
                property_label="linux",
                instance_name=INSTANCE_NAME,
                command="foo",
            )
            for i in range(num_jobs_queued)
        ]
        session.add_all(jobs)

    # Add a bunch of jobs in executing state, for a long time
    num_jobs_old_executing = 11
    with scheduler._sql.session() as session:
        jobs = [
            models.JobEntry(
                name=f"test-executing-old-job-{i}",
                action=generate_dummy_action().SerializeToString(),
                action_digest=f"test-executing-old-action-{i}/100",
                priority=1,
                stage=OperationStage.EXECUTING.value,
                leases=[models.LeaseEntry(status=0, state=LeaseState.ACTIVE.value)],
                operations=[models.OperationEntry(name=f"test-operation-old-{i}")],
                create_timestamp=queued_timestamp,
                queued_timestamp=queued_timestamp,
                worker_start_timestamp=old_worker_start_timestamp,
                platform_requirements=hash_from_dict({"OSFamily": "linux"}),
                property_label="linux",
                instance_name=INSTANCE_NAME,
                command="foo",
            )
            for i in range(num_jobs_old_executing)
        ]
        session.add_all(jobs)

    # Add a bunch of jobs in executing state, for a short time
    num_jobs_new_executing = 13
    with scheduler._sql.session() as session:
        jobs = [
            models.JobEntry(
                name=f"test-executing-new-job-{i}",
                action=generate_dummy_action().SerializeToString(),
                action_digest=f"test-executing-new-action-{i}/100",
                priority=1,
                stage=OperationStage.EXECUTING.value,
                leases=[models.LeaseEntry(status=0, state=LeaseState.ACTIVE.value)],
                operations=[models.OperationEntry(name=f"test-operation-new-{i}")],
                create_timestamp=queued_timestamp,
                queued_timestamp=queued_timestamp,
                worker_start_timestamp=new_worker_start_timestamp,
                platform_requirements=hash_from_dict({"OSFamily": "linux"}),
                property_label="linux",
                instance_name=INSTANCE_NAME,
                command="foo",
            )
            for i in range(num_jobs_new_executing)
        ]
        session.add_all(jobs)

    # List all the operations
    # and make sure all the operations that exceed the
    # execution timeout are marked as cancelled

    # Mock the `notify_job_updated` method to inspect the call
    with mock.patch.object(scheduler, "_notify_job_updated", autospec=True) as mock_notify_fn:
        # Call list operations with the default filters (i.e. ignore COMPLETED operations)
        scheduler.cancel_jobs_exceeding_execution_timeout(max_execution_timeout_seconds.total_seconds())
        operations, _ = scheduler.list_operations(
            page_size=1000,
            operation_filters=DEFAULT_OPERATION_FILTERS,
        )

        # Make sure we did a notify for each of the cancelled jobs
        assert mock_notify_fn.call_count == num_jobs_old_executing

    # The queued and newly executing jobs should be in the list
    assert sum("test-operation-queued-" in op.name for op in operations) == num_jobs_queued
    assert sum("test-operation-new-" in op.name for op in operations) == num_jobs_new_executing

    # The jobs that have been executing for a while should be cancelled
    assert sum("test-operation-old-" in op.name for op in operations) == 0


@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_create_operation(scheduler):
    job_name = "test-job"
    add_test_job(job_name, scheduler._sql, INSTANCE_NAME)

    op_name = scheduler.create_operation(job_name=job_name)

    with scheduler._sql.session() as session:
        op = session.query(models.OperationEntry).filter_by(name=op_name).first()
        assert op is not None
        assert op.job.name == job_name
        assert op.name == op_name
        assert op.client_identity is None


@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_create_operation_with_existing_client_identity(scheduler):
    job_name = "test-job"
    add_test_job(job_name, scheduler._sql, INSTANCE_NAME)
    with scheduler._sql.session() as session:
        session.add(
            models.ClientIdentityEntry(instance="instance", workflow="workflow", actor="actor", subject="subject")
        )

    op_name = scheduler.create_operation(
        job_name=job_name,
        client_identity=models.ClientIdentityEntry(
            instance="instance", workflow="workflow", actor="actor", subject="subject"
        ),
    )

    with scheduler._sql.session() as session:
        op: models.OperationEntry = session.query(models.OperationEntry).filter_by(name=op_name).first()
        assert op is not None
        assert op.job.name == job_name
        assert op.name == op_name
        assert op.client_identity is not None
        assert op.client_identity.instance == "instance"
        assert op.client_identity.workflow == "workflow"
        assert op.client_identity.actor == "actor"
        assert op.client_identity.subject == "subject"


@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_create_operation_with_new_client_identity(scheduler):
    job_name = "test-job"
    add_test_job(job_name, scheduler._sql, INSTANCE_NAME)

    op_name = scheduler.create_operation(
        job_name=job_name,
        client_identity=models.ClientIdentityEntry(
            instance="instance", workflow="workflow", actor="actor", subject="subject"
        ),
    )

    with scheduler._sql.session() as session:
        op: models.OperationEntry = session.query(models.OperationEntry).filter_by(name=op_name).first()
        assert op is not None
        assert op.job.name == job_name
        assert op.name == op_name
        assert op.client_identity is not None
        assert op.client_identity.instance == "instance"
        assert op.client_identity.workflow == "workflow"
        assert op.client_identity.actor == "actor"
        assert op.client_identity.subject == "subject"


@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_assign_n_leases_by_priority(scheduler):
    populate_database(scheduler)

    bot_foo = scheduler.add_bot_entry(bot_session_id="foo", bot_session_status=BotStatus.OK.value)
    bot_bar = scheduler.add_bot_entry(bot_session_id="bar", bot_session_status=BotStatus.OK.value)
    bot_fizz = scheduler.add_bot_entry(bot_session_id="fizz", bot_session_status=BotStatus.OK.value)
    bot_buzz = scheduler.add_bot_entry(bot_session_id="buzz", bot_session_status=BotStatus.OK.value)

    # Request 2 Leases from the scheduler. The example data contains
    # 2 queued jobs which have `OSFamily: linux`, so we should get two
    # jobs passed to the assignment callback as we requested.
    capability = hash_from_dict({"OSFamily": ["linux"]})
    assigned_bots = scheduler.assign_n_leases_by_priority(
        capability_hash=capability, bot_names=[bot_foo, bot_bar, bot_fizz]
    )
    assert set(assigned_bots) == {bot_foo, bot_bar, bot_fizz}

    # Check that those jobs actually got assigned in the scheduler too.
    # There should be no matching jobs left when we ask for more leases.
    assigned_bots = scheduler.assign_n_leases_by_priority(
        capability_hash=capability, bot_names=[bot_foo, bot_bar, bot_fizz, bot_buzz]
    )
    assert assigned_bots == []


@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_assign_n_leases_by_priority_limits(scheduler):
    populate_database(scheduler)

    bot_foo = scheduler.add_bot_entry(bot_session_id="foo", bot_session_status=BotStatus.OK.value)
    bar_bots = [
        scheduler.add_bot_entry(bot_session_id=f"bar{i}", bot_session_status=BotStatus.OK.value) for i in range(10)
    ]

    # Request only 1 lease from the scheduler. The example data contains
    # 2 queued jobs matching `OSFamily: linux`, so we should get the job
    # with the highest priority (`other-job`).
    capability = hash_from_dict({"OSFamily": ["linux"]})
    assigned_bots = scheduler.assign_n_leases_by_priority(capability_hash=capability, bot_names=[bot_foo])
    assert len(assigned_bots) == 1

    with scheduler._sql.session() as session:
        assert scheduler._get_job("other-job", session).leases[0].worker_name == "foo"

    # Request many leases from the scheduler. There's only one job left
    # in the queue, so we should get just that job (`linux-job`).
    assigned_bots = scheduler.assign_n_leases_by_priority(capability_hash=capability, bot_names=[bot_foo] + bar_bots)
    assert len(assigned_bots) == 2

    with scheduler._sql.session() as session:
        assert scheduler._get_job("linux-job", session).leases[0].worker_name.startswith("bar")
        assert scheduler._get_job("old-queued-job", session).leases[0].worker_name.startswith("bar")


@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_assign_n_leases_by_age(scheduler):
    populate_database(scheduler)

    bot_foo = scheduler.add_bot_entry(bot_session_id="foo", bot_session_status=BotStatus.OK.value)
    bot_bar = scheduler.add_bot_entry(bot_session_id="bar", bot_session_status=BotStatus.OK.value)
    bot_fizz = scheduler.add_bot_entry(bot_session_id="fizz", bot_session_status=BotStatus.OK.value)
    bot_buzz = scheduler.add_bot_entry(bot_session_id="buzz", bot_session_status=BotStatus.OK.value)

    # Request 3 Leases from the scheduler. The example data contains
    # 3 queued jobs which have `OSFamily: linux`, so we should get two
    # jobs passed to the assignment callback as we requested.
    capability = hash_from_dict({"OSFamily": ["linux"]})
    assigned_bots = scheduler.assign_n_leases_by_age(
        capability_hash=capability, bot_names=[bot_foo, bot_bar, bot_fizz]
    )
    assert set(assigned_bots) == {bot_foo, bot_bar, bot_fizz}

    # Check that those jobs actually got assigned in the scheduler too.
    # There should be no matching jobs left when we ask for more leases.
    assigned_bots = scheduler.assign_n_leases_by_age(
        capability_hash=capability, bot_names=[bot_foo, bot_bar, bot_fizz, bot_buzz]
    )
    assert assigned_bots == []


@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_assign_n_leases_by_age_limits(scheduler):
    populate_database(scheduler)

    bot_foo = scheduler.add_bot_entry(bot_session_id="foo", bot_session_status=BotStatus.OK.value)
    bar_bots = [
        scheduler.add_bot_entry(bot_session_id=f"bar{i}", bot_session_status=BotStatus.OK.value) for i in range(10)
    ]

    # Request only 1 lease from the scheduler. The example data contains
    # 3 queued jobs matching `OSFamily: linux`, so we should get the oldest
    # job (`old-queued-job`).
    capability = hash_from_dict({"OSFamily": ["linux"]})
    assigned_bots = scheduler.assign_n_leases_by_age(capability_hash=capability, bot_names=[bot_foo])
    assert len(assigned_bots) == 1

    with scheduler._sql.session() as session:
        assert scheduler._get_job("old-queued-job", session).leases[0].worker_name == "foo"

    # Request many leases from the scheduler. There's only two jobs left
    # in the queue, so we should get both of them (`linux-job` and `other-job`).
    assigned_bots = scheduler.assign_n_leases_by_age(capability_hash=capability, bot_names=[bot_foo] + bar_bots)
    assert len(assigned_bots) == 2

    with scheduler._sql.session() as session:
        assert scheduler._get_job("linux-job", session).leases[0].worker_name.startswith("bar")
        assert scheduler._get_job("other-job", session).leases[0].worker_name.startswith("bar")


# Check that the metrics returned match what is
# populated in the test database
def test_get_metrics(scheduler):
    populate_database(scheduler)

    # Setup expected counts for each category
    expected_metrics = {
        (OperationStage.UNKNOWN.name, "unknown"): 0,
        (OperationStage.CACHE_CHECK.name, "unknown"): 0,
        (OperationStage.QUEUED.name, "unknown"): 0,
        (OperationStage.QUEUED.name, "aix"): 1,
        (OperationStage.QUEUED.name, "linux"): 4,
        (OperationStage.QUEUED.name, "sunos"): 1,
        (OperationStage.EXECUTING.name, "unknown"): 0,
    }

    returned_metrics = scheduler.get_metrics()
    assert returned_metrics != {}
    assert expected_metrics == returned_metrics["jobs"]


# Make it so connection to the database throws a
# DatabaseError, and ensure it's non-fatal
def test_get_metrics_error(scheduler):
    populate_database(scheduler)

    with mock.patch.object(
        scheduler._sql_ro, "session", autospec=True, side_effect=DatabaseError("TestException")
    ) as session_fn:
        returned_metrics = scheduler.get_metrics()
        assert session_fn.called
        assert returned_metrics == {}


def test_watcher_exits_after_flag_update(scheduler):
    with scheduler.ops_notifier:
        assert scheduler.ops_notifier.worker.is_alive()
    assert scheduler.ops_notifier.worker.is_alive() is False


@mock.patch("buildgrid.server.metrics_utils.create_timer_record", new=mock_create_timer_record)
def test_watcher_detects_changes(scheduler):
    with scheduler.ops_notifier:
        delta = scheduler.poll_interval * 2

        populate_database(scheduler)

        # Ensure the watcher is running
        assert scheduler.ops_notifier.worker.is_alive()

        # Start watching a job
        with scheduler.ops_notifier.subscription("other-job") as event:
            # Clear the first event. Some notifier impls notify immediately.
            time.sleep(delta)
            event.clear()

            scheduler.cancel_operation("other-operation")

            poll_and_assert(event.is_set, True, delta, 10.0)

        # Update flag to ask the watcher to stop after this iteration
        scheduler.ops_notifier.stop()

        # Assert that the watcher thread has exited
        assert scheduler.ops_notifier.worker.is_alive() is False


def test_queue_timeout_cancel_function(scheduler):
    num = 100
    limit = 10
    now = datetime.now()
    # populdate db
    with scheduler._sql.session() as session:
        session.add_all([queued_job("queued", x, now - timedelta(seconds=30), INSTANCE_NAME) for x in range(num)])

    # invoke queue timeout cancellation
    assert scheduler._timeout_queued_jobs_scheduled_before(now - timedelta(seconds=10), limit=limit) == limit

    # confirm remaining
    with scheduler._sql.session() as session:
        remaining = num - limit
        assert (
            session.query(models.JobEntry)
            .filter(
                models.JobEntry.stage == OperationStage.QUEUED.value,
                models.JobEntry.instance_name == INSTANCE_NAME,
            )
            .count()
        ) == remaining


def test_queue_timeout_thread(database_queue_timeout_enabled: Scheduler):
    num_timeout = 20
    num_active = 10
    now = datetime.now()
    # populdate db
    with database_queue_timeout_enabled._sql.session() as session:
        session.add_all([queued_job("stale", x, now - timedelta(days=999), INSTANCE_NAME) for x in range(num_timeout)])
        session.add_all([queued_job("active", x, now + timedelta(days=99), INSTANCE_NAME) for x in range(num_active)])

    # fetch remaining queued jobs
    def get_num_queued():
        with database_queue_timeout_enabled._sql.session() as session:
            return (
                session.query(models.JobEntry)
                .filter(
                    models.JobEntry.stage == OperationStage.QUEUED.value,
                    models.JobEntry.instance_name == INSTANCE_NAME,
                )
                .count()
            )

    # all jobs are eventually cancelled
    poll_and_assert(get_num_queued, num_active, 0.01, 10)


def test_pruner_delete_function(scheduler):
    # populate database
    NUM_ROWS_ADDED = 1000
    now = datetime.utcnow()
    prefix = "completed"
    with scheduler._sql.session() as session:
        # Insert the necessary platform_entry record and re-read it to pass to completed_job
        session.execute(sqlalchemy.insert(models.PlatformEntry).values(key="OSFamily", value="linux"))
        select_statement = sqlalchemy.select(models.PlatformEntry).where(
            sqlalchemy.and_(models.PlatformEntry.key == "OSFamily", models.PlatformEntry.value == "linux")
        )
        platform_entries = list(session.execute(select_statement).scalars())

        session.add_all(
            [
                completed_job(prefix, x, now - timedelta(minutes=1), platform_entries, INSTANCE_NAME)
                for x in range(NUM_ROWS_ADDED)
            ]
        )

    # confirm existence
    with scheduler._sql.session() as session:
        jobs = session.query(models.JobEntry).filter(models.JobEntry.name.like(f"{prefix}-job/%"))
        assert jobs is not None
        assert jobs.count() == NUM_ROWS_ADDED

    # invoke the deleter function confirming we only deleted TEST_LIMIT rows
    TEST_LIMIT = 100
    assert scheduler._delete_jobs_prior_to(now, TEST_LIMIT) == TEST_LIMIT

    # confirm remaining
    with scheduler._sql.session() as session:
        jobs = session.query(models.JobEntry).filter(models.JobEntry.name.like(f"{prefix}-job/%"))
        assert jobs.count() == (NUM_ROWS_ADDED - TEST_LIMIT)


def test_pruner_thread(database_pruning_enabled):
    prefix = "completed"
    now = datetime.utcnow()
    with database_pruning_enabled._sql.session() as session:
        # Insert the necessary platform_entry record and re-read it to pass to completed_job
        session.execute(sqlalchemy.insert(models.PlatformEntry).values(key="OSFamily", value="linux"))
        select_statement = sqlalchemy.select(models.PlatformEntry).where(
            sqlalchemy.and_(models.PlatformEntry.key == "OSFamily", models.PlatformEntry.value == "linux")
        )
        platform_entries = list(session.execute(select_statement).scalars())
        session.add(completed_job(prefix, 1, now - timedelta(minutes=1), platform_entries, INSTANCE_NAME))

    # confirm existence
    job_name = f"{prefix}-job/1"
    with database_pruning_enabled._sql.session() as session:
        job = session.query(models.JobEntry).filter_by(name=job_name, instance_name=INSTANCE_NAME).first()
        assert job is not None
        assert job.name == job_name

    # sleep for 2 cycles, allowing the background pruning thread time to delete the record
    sleep(TEST_JOB_PRUNING_PERIOD.total_seconds() * 2)

    # confirm non-existence
    with database_pruning_enabled._sql.session() as session:
        job = session.query(models.JobEntry).filter_by(name=job_name, instance_name=INSTANCE_NAME).first()
        assert job is None


def test_operation_to_proto(scheduler):
    action_result = remote_execution_pb2.ActionResult(exit_code=12, stdout_raw=b"hello!")
    action_result_digest = remote_execution_pb2.Digest(hash="finished-action-result", size_bytes=200)
    add_test_blob(action_result_digest, action_result.SerializeToString(), scheduler.storage)

    execute_response = remote_execution_pb2.ExecuteResponse(result=action_result)
    execute_response_digest = remote_execution_pb2.Digest(hash="execute-response-digest", size_bytes=123)
    add_test_blob(execute_response_digest, execute_response.SerializeToString(), scheduler.storage)

    # Adding a completed Job and associated Operation:
    operation_name = "finished-operation"
    with scheduler._sql.session() as session:
        now = datetime.now()
        job = models.JobEntry(
            name="a-finished-job",
            instance_name=current_instance(),
            action=generate_dummy_action().SerializeToString(),
            action_digest="a-finished-action/35",
            stage=OperationStage.COMPLETED.value,
            result=f"{execute_response_digest.hash}/{execute_response_digest.size_bytes}",
            operations=[models.OperationEntry(name=operation_name, job_name="a-finished-job")],
            platform_requirements="",
            property_label="unknown",
            command="foo",
            create_timestamp=now,
            queued_timestamp=now,
        )
        session.add(job)

    # Fetching back the Operation and converting it to protobuf:
    operation_proto = scheduler.load_operation(operation_name)
    assert operation_proto and operation_proto.done
    assert operation_proto.response.Is(remote_execution_pb2.ExecuteResponse().DESCRIPTOR)

    returned_execute_response = remote_execution_pb2.ExecuteResponse()
    operation_proto.response.Unpack(returned_execute_response)
    assert returned_execute_response.result == action_result


def test_operation_to_proto_with_missing_execute_response(scheduler):
    # Adding a completed Job and associated Operation:
    operation_name = "finished-operation2"
    execute_response_digest = remote_execution_pb2.Digest(hash="execute-response-digest-not-in-cas", size_bytes=1234)
    with scheduler._sql.session() as session:
        now = datetime.now()
        job = models.JobEntry(
            name="a-finished-job",
            instance_name=current_instance(),
            action=generate_dummy_action().SerializeToString(),
            action_digest="a-finished-action/35",
            stage=OperationStage.COMPLETED.value,
            result=f"{execute_response_digest.hash}/{execute_response_digest.size_bytes}",
            operations=[models.OperationEntry(name=operation_name, job_name="a-finished-job")],
            platform_requirements="",
            property_label="unknown",
            command="foo",
            create_timestamp=now,
            queued_timestamp=now,
        )
        session.add(job)

    assert not scheduler.storage.has_blob(execute_response_digest)

    # Fetching back the Operation and converting it to protobuf:
    operation_proto = scheduler.load_operation(operation_name)
    assert operation_proto and operation_proto.done
    assert operation_proto.error.code == code_pb2.DATA_LOSS


def test_job_leases_order_by_id(scheduler):
    # GIVEN
    operation_name = "test_operation_lease_id_order"
    job_name = "test_job_lease_id_order"
    with scheduler._sql.session() as session:
        job = models.JobEntry(
            name=job_name,
            action=generate_dummy_action().SerializeToString(),
            action_digest="some-action/35",
            priority=20,
            stage=OperationStage.COMPLETED.value,
            cancelled=True,
            create_timestamp=datetime(2019, 6, 1),
            queued_timestamp=datetime(2019, 6, 1),
            queued_time_duration=60,
            worker_start_timestamp=datetime(2019, 6, 1, minute=1),
            leases=[
                models.LeaseEntry(id=0, status=0, state=LeaseState.CANCELLED.value),
                models.LeaseEntry(id=1, status=0, state=LeaseState.ACTIVE.value),
            ],
            operations=[models.OperationEntry(name=operation_name, cancelled=True)],
            platform_requirements=hash_from_dict({"OSFamily": ["linux"]}),
            property_label="linux",
            instance_name="",
            command="foo",
        )
        session.add(job)

    with scheduler._sql.session() as session:
        # WHEN
        read_job = session.query(models.JobEntry).filter_by(name=job_name).first()

        # THEN
        assert len(read_job.active_leases) == 2
        assert read_job.active_leases[0].id == 1
        assert read_job.active_leases[1].id == 0
        assert read_job.active_leases[0].state == LeaseState.ACTIVE.value


def test_notifier_sends_updates(scheduler):
    """
    Test that an event is called the expected number of times for a database update.
    This digs into the internals to assert that the call count for setting the event is sane.
    """
    with scheduler.ops_notifier:
        job_name = "test-job"
        add_test_job(job_name, scheduler._sql, instance_name=INSTANCE_NAME)

        class FakeEvent:
            def __init__(self):
                self.called = 0

            def set(self):
                self.called += 1

        event = FakeEvent()
        with scheduler.ops_notifier._lock:
            scheduler.ops_notifier._listeners[job_name] = {str(uuid.uuid4()): event}

        # Give time for notifier to start up. And clear initial startup events.
        time.sleep(scheduler.poll_interval * 2)
        event.called = 0

        with scheduler._sql.session() as session:
            scheduler._get_job(job_name, session).stage = OperationStage.EXECUTING.value
            scheduler._notify_job_updated(job_name, session)

        time.sleep(scheduler.poll_interval * 2)
        assert event.called == 1

        with scheduler._sql.session() as session:
            scheduler._get_job(job_name, session).stage = OperationStage.COMPLETED.value
            scheduler._notify_job_updated(job_name, session)

        time.sleep(scheduler.poll_interval * 2)
        assert event.called == 2


def test_notifier_recovers_from_error(scheduler):
    """
    Test that in the case of an error, the notifier will be able to reset itself.
    This digs into the internals to assert that the call count for setting the event is sane.
    """
    with scheduler.ops_notifier:

        class FakeEvent:
            def __init__(self):
                self.needs_raise = False

            def set(self):
                if self.needs_raise:
                    self.needs_raise = False
                    raise Exception

        for i in range(3):
            job_name = f"test-job{i}"
            add_test_job(job_name, scheduler._sql, instance_name=INSTANCE_NAME)

            event = FakeEvent()
            with scheduler.ops_notifier._lock:
                scheduler.ops_notifier._listeners[job_name] = {str(uuid.uuid4()): event}

            # Give time for notifier to start up. And clear events
            time.sleep(scheduler.poll_interval * 2)
            event.needs_raise = True

            with scheduler._sql.session() as session:
                scheduler._get_job(job_name, session).stage = OperationStage.EXECUTING.value
                scheduler._notify_job_updated(job_name, session)

            time.sleep(scheduler.poll_interval * 2)
            assert not event.needs_raise

            # Give time for notifier to reset
            time.sleep(scheduler.poll_interval * 2)
            event.needs_raise = True

            with scheduler._sql.session() as session:
                scheduler._get_job(job_name, session).stage = OperationStage.COMPLETED.value
                scheduler._notify_job_updated(job_name, session)

            time.sleep(scheduler.poll_interval * 2)
            assert not event.needs_raise
