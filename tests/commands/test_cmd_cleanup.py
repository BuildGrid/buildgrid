# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from datetime import timedelta

import pytest

from buildgrid.server.app.commands.cmd_cleanup import parse_size, parse_time


def test_parse_size():
    """Test basic functionality."""
    bytes_zero = "0"
    assert parse_size(bytes_zero) == int(bytes_zero)

    bytes_str = "100000000"
    assert parse_size(bytes_str) == int(bytes_str)

    kilobytes_str = "1000000K"
    assert parse_size(kilobytes_str) == int(kilobytes_str[:-1]) * 1000

    megabytes_str = "1000000M"
    assert parse_size(megabytes_str) == int(megabytes_str[:-1]) * 1000000

    gigabytes_str = "1000000G"
    assert parse_size(gigabytes_str) == int(gigabytes_str[:-1]) * 1000000000

    terabytes_str = "1000000T"
    assert parse_size(terabytes_str) == int(terabytes_str[:-1]) * 1000000000000


def test_parse_size_capitalization():
    """Test if parse_size is agnostic to capitalization."""
    assert parse_size("1K") == parse_size("1k")
    assert parse_size("1M") == parse_size("1m")
    assert parse_size("1G") == parse_size("1g")
    assert parse_size("1T") == parse_size("1t")


def test_parse_size_decimal():
    """Test decimal values."""
    decimal = "1.8M"
    assert parse_size(decimal) == 1800000


def test_parse_size_throw():
    """Test malformed strings."""
    with pytest.raises(ValueError):
        petabytes_str = "100P"
        parse_size(petabytes_str)

    with pytest.raises(IndexError):
        empty_str = ""
        parse_size(empty_str)

    with pytest.raises(ValueError):
        m_str = "M"
        parse_size(m_str)

    with pytest.raises(ValueError):
        prefix_str = "G1"
        parse_size(prefix_str)

    with pytest.raises(ValueError):
        not_number_str = "Not A Number"
        parse_size(not_number_str)


def test_parse_time():
    """Test basic functionality for converting time."""
    zero_seconds = "0"
    assert parse_time(zero_seconds) == timedelta(seconds=0)

    ten_seconds = "10"
    assert parse_time(ten_seconds) == timedelta(seconds=10)

    one_minute = "1M"
    assert parse_time(one_minute) == timedelta(minutes=1)

    million_hours = "1000000H"
    assert parse_time(million_hours) == timedelta(hours=1000000)

    hundred_days = "100D"
    assert parse_time(hundred_days) == timedelta(days=100)

    year_in_weeks = "52W"
    assert parse_time(year_in_weeks) == timedelta(weeks=52)


def test_parse_time_capitalization():
    """Test if parse_time is agnostic to capitalization."""
    assert parse_time("1M") == parse_time("1m")
    assert parse_time("1H") == parse_time("1h")
    assert parse_time("1D") == parse_time("1d")
    assert parse_time("1W") == parse_time("1w")


def test_parse_time_decimal():
    """Test time decimal values."""
    decimal = "1.5D"
    assert parse_time(decimal) == timedelta(hours=36)


def test_parse_time_throw():
    """Test malformed time strings."""
    with pytest.raises(ValueError):
        years_str = "100Y"
        parse_time(years_str)

    with pytest.raises(IndexError):
        empty_str = ""
        parse_time(empty_str)

    with pytest.raises(ValueError):
        h_str = "H"
        parse_time(h_str)

    with pytest.raises(ValueError):
        prefix_str = "D1"
        parse_time(prefix_str)

    with pytest.raises(ValueError):
        not_number_str = "Not A Number"
        parse_time(not_number_str)
