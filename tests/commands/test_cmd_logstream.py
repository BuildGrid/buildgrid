# Copyright (C) 2024 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import pytest

from buildgrid.server.app.commands.cmd_logstream import instanced_resource_name


@pytest.mark.parametrize(
    "instance_name, resource, expected",
    [
        ("instance", "resource", "instance/resource"),
        ("", "resource", "resource"),
        ("instance", "", "instance/"),
        ("", "", ""),
    ],
)
def test_instanced_resource_name(instance_name, resource, expected):
    "Test basic functionality"
    assert instanced_resource_name(instance_name, resource) == expected
