# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import json
import os
from datetime import datetime
from typing import Any
from unittest import mock

import jwt
import pytest
from grpc._server import _Context
from jwcrypto import jwk

from buildgrid._protos.buildgrid.v2.identity_pb2 import ClientIdentity
from buildgrid.server.auth.config import Acl, InstanceAuthorizationConfig
from buildgrid.server.auth.enums import AuthMetadataAlgorithm
from buildgrid.server.auth.exceptions import SigningKeyNotFoundError
from buildgrid.server.auth.manager import (
    ContextClientIdentity,
    HeadersAuthManager,
    JWTAuthManager,
    JwtParser,
    set_auth_manager,
)
from buildgrid.server.decorators import instanced
from buildgrid.server.decorators.authorize import authorize
from buildgrid.server.exceptions import InvalidArgumentError

from ..utils.utils import read_file

DATA_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data")

TOKENS = [None, "not-a-token"]
SECRETS = [None, None]
JWKS_URL = "https://test.url.com"
ALGORITHMS = [AuthMetadataAlgorithm.UNSPECIFIED, AuthMetadataAlgorithm.UNSPECIFIED]
VALIDITIES = [False, False]
# Generic test data: token, secret, algorithm, validity:
DATA = zip(TOKENS, SECRETS, ALGORITHMS, VALIDITIES)

JWT_TOKENS = [
    "jwt-hs256-expired.token",
    "jwt-hs256-unbounded.token",
    "jwt-hs256-valid.token",
    "jwt-hs256-valid.token",
    "jwt-rs256-expired.token",
    "jwt-rs256-unbounded.token",
    "jwt-rs256-valid.token",
    "jwt-rs256-valid.token",
    "jwt-hs256-valid.token",
    "jwt-rs256-valid.token",
]
JWT_SECRETS = [
    "jwt-hs256-matching.secret",
    "jwt-hs256-matching.secret",
    "jwt-hs256-matching.secret",
    "jwt-hs256-conflicting.secret",
    "jwt-rs256-matching.pub.key",
    "jwt-rs256-matching.pub.key",
    "jwt-rs256-matching.pub.key",
    "jwt-rs256-conflicting.pub.key",
    "jwt-hs256-matching.secret",
    "jwt-rs256-matching.pub.key",
]
JWT_ALGORITHMS = [
    AuthMetadataAlgorithm.JWT_HS256,
    AuthMetadataAlgorithm.JWT_HS256,
    AuthMetadataAlgorithm.JWT_HS256,
    AuthMetadataAlgorithm.JWT_HS256,
    AuthMetadataAlgorithm.JWT_RS256,
    AuthMetadataAlgorithm.JWT_RS256,
    AuthMetadataAlgorithm.JWT_RS256,
    AuthMetadataAlgorithm.JWT_RS256,
    AuthMetadataAlgorithm.JWT_HS256,
    AuthMetadataAlgorithm.JWT_RS256,
]
JWT_AUDIENCE = [
    "buildgrid",
    "buildgrid",
    "buildgrid",
    "buildgrid",
    "buildgrid",
    "buildgrid",
    "buildgrid",
    "buildgrid",
    "invalid",
    "invalid",
]
JWT_VALIDITIES = [False, False, True, False, False, False, True, False, False, False]
# JWT test data: token, secret, algorithm, validity:
JWT_DATA = zip(JWT_TOKENS, JWT_SECRETS, JWT_ALGORITHMS, JWT_AUDIENCE, JWT_VALIDITIES)

INSTANCE_NAME = ""
TEST_ACTOR = "test-client"


class MockRequest:
    """Used to mock request object"""

    def __init__(self, json_object, status_code):
        self.ok = True
        self.json_object = json_object
        self.status_code = status_code
        self.call_count = 0

    def json(self):
        """Mock json method from request object"""
        return json.loads(self.json_object)

    def __call__(self, *args, **kwargs):
        self.call_count += 1
        return self


@pytest.fixture(autouse=True)
def reset_context_client_identity():
    try:
        yield
    finally:
        ContextClientIdentity.set(None)


def test_headers_authorization():
    acl_config = {INSTANCE_NAME: InstanceAuthorizationConfig(allow=[Acl(actor=TEST_ACTOR, requests=["Execute"])])}
    manager = HeadersAuthManager(acls=acl_config)
    context = mock.create_autospec(_Context, spec_set=True)
    context.invocation_metadata.return_value = (("x-request-actor", TEST_ACTOR),)
    assert manager.authorize(context, INSTANCE_NAME, "Execute")
    assert not manager.authorize(context, INSTANCE_NAME, "CreateBotSession")


def test_authorize_unary_unary():
    acl_config = {INSTANCE_NAME: InstanceAuthorizationConfig(allow=[Acl(actor=TEST_ACTOR, requests=["Execute"])])}
    set_auth_manager(HeadersAuthManager(acls=acl_config))

    def _abort(*args):
        raise Exception()

    context = mock.create_autospec(_Context, spec_set=True)
    context.invocation_metadata.return_value = (("x-request-actor", TEST_ACTOR),)

    # The implementation of `_Context.abort()` raises an Exception to interrupt the
    # RPC once the internal gRPC state is updated as required. Our mock needs the
    # same behaviour, otherwise the contextmanager doesn't yield in the error case.
    context.abort.side_effect = _abort

    @instanced(lambda r: INSTANCE_NAME)
    @authorize
    def Execute(cls, request, context):
        pass

    @instanced(lambda r: INSTANCE_NAME)
    @authorize
    def CreateBotSession(cls, request, context):
        pass

    Execute(mock.Mock(), mock.MagicMock(), context)
    context.abort.assert_not_called()

    with pytest.raises(Exception):
        CreateBotSession(mock.Mock(), mock.MagicMock(), context)
    context.abort.assert_called_once()


def test_authorize_unary_stream():
    acl_config = {INSTANCE_NAME: InstanceAuthorizationConfig(allow=[Acl(actor=TEST_ACTOR, requests=["Execute"])])}
    set_auth_manager(HeadersAuthManager(acls=acl_config))

    def _abort(*args):
        raise Exception()

    context = mock.create_autospec(_Context, spec_set=True)
    context.invocation_metadata.return_value = (("x-request-actor", TEST_ACTOR),)

    # The implementation of `_Context.abort()` raises an Exception to interrupt the
    # RPC once the internal gRPC state is updated as required. Our mock needs the
    # same behaviour, otherwise the contextmanager doesn't yield in the error case.
    context.abort.side_effect = _abort

    @instanced(lambda r: INSTANCE_NAME)
    @authorize
    def Execute(cls, request, context):
        yield

    @instanced(lambda r: INSTANCE_NAME)
    @authorize
    def CreateBotSession(cls, request, context):
        yield

    next(Execute(mock.Mock(), mock.MagicMock(), context))
    context.abort.assert_not_called()

    with pytest.raises(Exception):
        next(CreateBotSession(mock.Mock(), mock.MagicMock(), context))
    context.abort.assert_called_once()


def test_authorize_stream_unary():
    acl_config = {INSTANCE_NAME: InstanceAuthorizationConfig(allow=[Acl(actor=TEST_ACTOR, requests=["Execute"])])}
    set_auth_manager(HeadersAuthManager(acls=acl_config))

    def _abort(*args):
        raise Exception()

    context = mock.create_autospec(_Context, spec_set=True)
    context.invocation_metadata.return_value = (("x-request-actor", TEST_ACTOR),)

    # The implementation of `_Context.abort()` raises an Exception to interrupt the
    # RPC once the internal gRPC state is updated as required. Our mock needs the
    # same behaviour, otherwise the contextmanager doesn't yield in the error case.
    context.abort.side_effect = _abort

    @instanced(lambda r: INSTANCE_NAME)
    @authorize
    def Execute(cls, request, context):
        pass

    @instanced(lambda r: INSTANCE_NAME)
    @authorize
    def CreateBotSession(cls, request, context):
        pass

    def _requests():
        yield from [mock.MagicMock(), mock.MagicMock()]

    Execute(mock.Mock(), _requests(), context)
    context.abort.assert_not_called()

    with pytest.raises(Exception):
        CreateBotSession(mock.Mock(), _requests(), context)
    context.abort.assert_called_once()


def test_jwt_unspecified_algorithm():
    # JWT requires an algorithm to be specified, so using UNSPECIFIED should raise
    with pytest.raises(InvalidArgumentError):
        JWTAuthManager(secret="secret", algorithm=AuthMetadataAlgorithm.UNSPECIFIED)


# Claims for data/jwt-rs256-valid.token
# {
#   "sub": "BuildGrid Test",
#   "aud": "buildgrid",
#   "iat": 1355314332,
#   "exp": 2301999132
# }

# Claims for data/jwt-rs256-valid.token
# {
#   "sub": "BuildGrid Test",
#   "iat": 1355314332,
#   "exp": 2301999132,
#   "aud": [
#     "buildgrid"
#   ]
# }


@pytest.mark.parametrize("token,secret,algorithm,audience,validity", JWT_DATA)
def test_jwt_authorization(token, secret, algorithm, audience, validity):
    token = read_file(os.path.join(DATA_DIR, token), text_mode=True).strip()
    secret = read_file(os.path.join(DATA_DIR, secret), text_mode=True).strip()

    manager = JWTAuthManager(secret=secret, algorithm=algorithm, audiences=[audience])

    context = mock.create_autospec(_Context, spec_set=True)
    context.invocation_metadata.return_value = (("authorization", f"Bearer {token}"),)
    authorized = manager.authorize(context, INSTANCE_NAME, "Execute")

    if validity:
        assert authorized
    else:
        assert not authorized

    # Token should have been cached now, let's test authorization again:
    context = mock.create_autospec(_Context, spec_set=True)
    context.invocation_metadata.return_value = (("authorization", f"Bearer {token}"),)
    authorized = manager.authorize(context, INSTANCE_NAME, "Execute")

    if validity:
        assert authorized
    else:
        assert not authorized


def test_jwt_authorization_expiry():
    secret, algorithm = "your-256-bit-secret", AuthMetadataAlgorithm.JWT_HS256
    now = int(datetime.now().timestamp())
    payload = {"sub": "BuildGrid Expiry Test", "iat": now, "exp": now + 2}
    token = jwt.encode(payload, secret, algorithm=algorithm.value.upper())

    manager = JWTAuthManager(secret=secret, algorithm=algorithm)

    # First, test generated token validation:
    context = mock.create_autospec(_Context, spec_set=True)
    context.invocation_metadata.return_value = (("authorization", f"Bearer {token}"),)
    assert manager.authorize(context, INSTANCE_NAME, "Execute")

    # Second, ensure cached token validation:
    context = mock.create_autospec(_Context, spec_set=True)
    context.invocation_metadata.return_value = (("authorization", f"Bearer {token}"),)
    assert manager.authorize(context, INSTANCE_NAME, "Execute")

    # Now test with an expired token:
    payload = {"sub": "BuildGrid Expiry Test", "iat": now - 2, "exp": now - 1}
    token = jwt.encode(payload, secret, algorithm=algorithm.value.upper())

    # Finally, test for cached-token invalidation:
    context = mock.create_autospec(_Context, spec_set=True)
    context.invocation_metadata.return_value = (("authorization", f"Bearer {token}"),)
    assert not manager.authorize(context, INSTANCE_NAME, "Execute")


def test_jwt_jwk_authorization():
    token = read_file(os.path.join(DATA_DIR, "jwt-rs256-jwk-encrypted.token"), text_mode=True).strip()
    json_key_set = read_file(os.path.join(DATA_DIR, "jwks-valid.json"), text_mode=True).strip()
    key_set = json.loads(json_key_set)

    with mock.patch("buildgrid.server.auth.manager.jwt.PyJWKClient.fetch_data", return_value=key_set):
        manager = JWTAuthManager(jwks_urls=JWKS_URL, algorithm=AuthMetadataAlgorithm.JWT_RS256)

        context = mock.create_autospec(_Context, spec_set=True)
        context.invocation_metadata.return_value = (("authorization", f"Bearer {token}"),)
        assert manager.authorize(context, INSTANCE_NAME, "Execute")

        # Token should have been cached now, let's test authorization again:
        context = mock.create_autospec(_Context, spec_set=True)
        context.invocation_metadata.return_value = (("authorization", f"Bearer {token}"),)
        assert manager.authorize(context, INSTANCE_NAME, "Execute")


def test_jwt_jwk_authorization_errors():
    """Test jwk authorization errors"""

    token = read_file(os.path.join(DATA_DIR, "jwt-rs256-jwk-encrypted.token"), text_mode=True).strip()

    # Test JWKs request exception gets caught and re-raised during normal flow.
    with mock.patch(
        "buildgrid.server.auth.manager.jwt.PyJWKClient.fetch_data",
        side_effect=jwt.exceptions.PyJWKClientConnectionError,
    ):
        with pytest.raises(SigningKeyNotFoundError):
            manager = JWTAuthManager(jwks_urls=JWKS_URL, algorithm=AuthMetadataAlgorithm.JWT_RS256)
            context = mock.create_autospec(_Context, spec_set=True)
            context.invocation_metadata.return_value = (("authorization", f"Bearer {token}"),)
            manager._token_parser.parse(token)


@pytest.mark.parametrize(
    "audience,jwt_claims,expected_output",
    [
        (
            None,
            {"aud": "buildgrid", "sub": "johndoe", "act": "janedoe"},
            ClientIdentity(workflow="buildgrid", subject="johndoe", actor="janedoe"),
        ),
        (
            ["testaud"],
            {
                "aud": "buildgrid",
                "sub": "johndoe",
            },
            ClientIdentity(workflow="buildgrid", subject="johndoe", actor="johndoe"),
        ),
        (
            None,
            {
                "aud": ["buildgrid", "test"],
                "sub": "johndoe",
            },
            ClientIdentity(workflow="buildgrid", subject="johndoe", actor="johndoe"),
        ),
    ],
)
def test_identity_from_jwt_payload(audience: str | None, jwt_claims: dict[str, Any], expected_output: ClientIdentity):
    secret, algorithm = "your-256-bit-secret", AuthMetadataAlgorithm.JWT_HS256

    manager = JwtParser(secret=secret, algorithm=algorithm, audiences=audience)
    assert expected_output == manager.identity_from_jwt_payload(jwt_claims)


def test_multiple_allowed_audiences():
    secret = "secret"
    now = int(datetime.now().timestamp())
    payload1 = {"aud": "buildgrid", "sub": "sub", "act": "act", "exp": now + 10}
    payload2 = {"aud": "dev", "sub": "sub", "act": "act", "exp": now + 10}
    payload_invalid = {"aud": "invalid", "sub": "sub", "act": "act", "exp": now + 10}
    token1 = jwt.encode(payload1, secret)
    token2 = jwt.encode(payload2, secret)
    token_invalid = jwt.encode(payload_invalid, secret)

    context = mock.create_autospec(_Context, spec_set=True)
    context.invocation_metadata.side_effect = [
        (("authorization", f"Bearer {token1}"),),
        (("authorization", f"Bearer {token2}"),),
        (("authorization", f"Bearer {token_invalid}"),),
    ]

    manager = JWTAuthManager(secret=secret, algorithm=AuthMetadataAlgorithm.JWT_HS256, audiences=["buildgrid", "dev"])

    assert manager.authorize(context, INSTANCE_NAME, "Execute")
    assert manager.authorize(context, INSTANCE_NAME, "Execute")
    assert not manager.authorize(context, INSTANCE_NAME, "Execute")


def test_multiple_allowed_jkus():
    # GIVEN
    key1 = jwk.JWK.generate(kty="RSA", size=2048, alg="RS256", use="sig", kid="key1")
    key2 = jwk.JWK.generate(kty="RSA", size=2048, alg="RS256", use="sig", kid="key2")
    pubkey1 = {"keys": [key1.export_public(as_dict=True)]}
    pubkey2 = {"keys": [key2.export_public(as_dict=True)]}
    mock_jku1 = "https://mock-jku1"
    mock_jku2 = "https://mock-jku2"
    now = int(datetime.now().timestamp())
    payload = {"aud": "buildgrid", "sub": "sub", "act": "act", "exp": now + 10}

    token1 = jwt.encode(
        payload,
        key=key1.export_to_pem(private_key=True, password=None),
        headers={"kid": "key1"},
        algorithm="RS256",
    )
    token2 = jwt.encode(
        payload,
        key=key2.export_to_pem(private_key=True, password=None),
        headers={"kid": "key2"},
        algorithm="RS256",
    )

    context = mock.create_autospec(_Context, spec_set=True)
    context.invocation_metadata.side_effect = [
        (("authorization", f"Bearer {token1}"),),
        (("authorization", f"Bearer {token2}"),),
    ]

    def mock_fetch_jwks(self):
        data = {mock_jku1: pubkey1, mock_jku2: pubkey2}
        return data[self.uri]

    with mock.patch.object(jwt.PyJWKClient, "fetch_data", mock_fetch_jwks):
        manager = JWTAuthManager(
            jwks_urls=[mock_jku1, mock_jku2], algorithm=AuthMetadataAlgorithm.JWT_RS256, audiences="buildgrid"
        )
        assert manager.authorize(context, INSTANCE_NAME, "Execute")
        assert manager.authorize(context, INSTANCE_NAME, "Execute")
