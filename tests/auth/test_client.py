# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import tempfile
import time

import grpc
import pytest

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2, remote_execution_pb2_grpc
from buildgrid.server.client.auth_token_loader import AuthTokenLoader
from buildgrid.server.client.channel import setup_channel
from buildgrid.server.exceptions import InvalidArgumentError

from ..utils.server import serve

DATA_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data")
TOKEN = os.path.join(DATA_DIR, "jwt-hs256-valid.token")
INVALID_TOKEN = os.path.join(DATA_DIR, "jwt-hs256-expired.token")
SECRET = os.path.join(DATA_DIR, "jwt-hs256-matching.secret")


AUTH_CONFIG = f"""
server:
  - !channel
    address: "[::]:50051"
    insecure-mode: true

description: |
  A single default instance

authorization:
  method: jwt
  secret: {SECRET}
  audience: buildgrid
  algorithm: hs256
  allow-unauthorized-instances: ['']

storages:
    - !lru-storage &storage
      size: 256mb

instances:
  - name: ''
    services:
      - !cas
        storage: *storage

  - name: main
    services:
      - !cas
        storage: *storage
"""


@pytest.mark.parametrize(
    "instance_name,token,expected_status",
    [
        ("", TOKEN, grpc.StatusCode.OK),
        ("", None, grpc.StatusCode.OK),
        ("", INVALID_TOKEN, grpc.StatusCode.OK),
        ("main", TOKEN, grpc.StatusCode.OK),
        ("main", None, grpc.StatusCode.UNAUTHENTICATED),
        ("main", INVALID_TOKEN, grpc.StatusCode.UNAUTHENTICATED),
    ],
)
def test_channel_token_authorization(instance_name, token, expected_status):
    with serve(AUTH_CONFIG) as (server, _):
        channel, _ = setup_channel("http://" + server.remote, auth_token=token)
        stub = remote_execution_pb2_grpc.CapabilitiesStub(channel)
        request = remote_execution_pb2.GetCapabilitiesRequest(instance_name=instance_name)
        status = grpc.StatusCode.OK
        try:
            stub.GetCapabilities(request)
        except grpc.RpcError as e:
            status = e.code()
        assert status == expected_status


def test_token_refresh():
    temp_file = tempfile.NamedTemporaryFile(delete=False)
    with open(temp_file.name, "w") as f:
        f.write("test-token-1")
    auth_token_loader = AuthTokenLoader(token_path=temp_file.name, refresh_in_seconds=2)
    assert auth_token_loader.get_token() == "test-token-1"
    with open(temp_file.name, "w") as f:
        f.write("test-token-2")
    # Test to ensure that the token is not re read
    assert auth_token_loader.get_token() == "test-token-1"
    time.sleep(2)
    assert auth_token_loader.get_token() == "test-token-2"


def test_token_read_fail():
    random_file_name = "random-file.token"
    auth_token_loader = AuthTokenLoader(token_path=random_file_name, refresh_in_seconds=2)
    with pytest.raises(InvalidArgumentError, match="Cannot read token from filepath: random-file.token"):
        auth_token_loader.get_token()
