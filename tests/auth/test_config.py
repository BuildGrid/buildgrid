# Copyright (C) 2024 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os

import pytest

from buildgrid.server.auth.config import InstanceAuthorizationConfig, parse_auth_config

DATA_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data")
AUTH_CONFIG_PATH = os.path.join(DATA_DIR, "auth.yaml")


def test_parse_auth_config():
    config = parse_auth_config(AUTH_CONFIG_PATH)

    prod_config = config.get("prod")
    assert prod_config is not None
    assert isinstance(prod_config.allow, list)
    assert len(prod_config.allow) == 6

    dev_config = config.get("dev")
    assert dev_config is not None
    assert dev_config.allow == "all"


@pytest.fixture
def prod_config() -> InstanceAuthorizationConfig:
    config = parse_auth_config(AUTH_CONFIG_PATH)
    return config.get("prod")


@pytest.fixture
def dev_config() -> InstanceAuthorizationConfig:
    config = parse_auth_config(AUTH_CONFIG_PATH)
    return config.get("dev")


# The following tests all use the config parsed from tests/auth/data/auth.yaml,
# and assert that authorization is granted based on the rules defined there.
def test_regex_authorization(prod_config: InstanceAuthorizationConfig):
    assert prod_config.is_authorized("BatchUpdateBlobs", actor="prodbuilder")
    assert not prod_config.is_authorized("BatchUpdateBlobs", actor="devclient")


def test_specific_authorization(prod_config: InstanceAuthorizationConfig):
    assert prod_config.is_authorized("Execute", actor="prodbuilder")
    assert prod_config.is_authorized("CreateBotSession", actor="prodworker")
    assert not prod_config.is_authorized("Execute", actor="prodworker")

    assert not prod_config.is_authorized("CancelOperation", actor="prodbuilder")
    assert prod_config.is_authorized(
        "CancelOperation", actor="prodbuilder", subject="daily", workflow="production-builds"
    )


def test_over_specified_authorization(prod_config: InstanceAuthorizationConfig):
    assert prod_config.is_authorized("Execute", actor="prodbuilder", subject="daily", workflow="production-builds")


def test_catch_all_authorization(prod_config: InstanceAuthorizationConfig):
    assert prod_config.is_authorized("FindMissingBlobs", actor="prodbuilder")
    assert prod_config.is_authorized("FindMissingBlobs", actor="prodworker")
    assert prod_config.is_authorized("FindMissingBlobs", actor="devclient")
    assert prod_config.is_authorized("FindMissingBlobs", actor="bgd")
    assert prod_config.is_authorized("FindMissingBlobs")


def test_allow_all(dev_config: InstanceAuthorizationConfig):
    assert dev_config.is_authorized("Execute")
    assert dev_config.is_authorized("Execute", workflow="dev-builds")
    assert dev_config.is_authorized("Execute", actor="devclient", workflow="dev-builds")
