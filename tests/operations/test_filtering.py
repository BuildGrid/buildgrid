# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import operator
from datetime import datetime

import pytest

from buildgrid.server.exceptions import InvalidArgumentError
from buildgrid.server.operations.filtering import FilterParser, OperationFilter


def test_filtering_empty():
    """We should handle the empty string and all=whitespace strings."""
    filters = FilterParser.parse_listoperations_filters("")
    assert not filters

    filters = FilterParser.parse_listoperations_filters("  ")
    assert not filters


def test_filtering_one_item():
    """Baseline test."""
    filters = FilterParser.parse_listoperations_filters("name=blah")
    assert filters == [OperationFilter(parameter="name", operator=operator.eq, value="blah")]


def test_filtering_one_date_item():
    """Test a filter with a date value."""
    filters = FilterParser.parse_listoperations_filters("queued_time > 2020-04-01")
    assert filters == [
        OperationFilter(parameter="queued_time", operator=operator.gt, value=datetime(year=2020, month=4, day=1))
    ]


def test_filtering_invalid_parameter():
    """Parameter does not match the valid operation filters."""
    with pytest.raises(InvalidArgumentError):
        FilterParser.parse_listoperations_filters("foo=4")


def test_filtering_invalid_value():
    """Parameter has an invalid value."""
    with pytest.raises(InvalidArgumentError):
        FilterParser.parse_listoperations_filters("stage=bar")


def test_multi_filter():
    """Multiple filters ANDed together.

    The filters are returned in same order as the input clauses."""
    filters = FilterParser.parse_listoperations_filters("stage=completed&name>=foo")
    assert filters == [
        OperationFilter(parameter="stage", operator=operator.eq, value=4),
        OperationFilter(parameter="name", operator=operator.ge, value="foo"),
    ]

    filters = FilterParser.parse_listoperations_filters("name>=foo&stage=completed")
    assert filters == [
        OperationFilter(parameter="name", operator=operator.ge, value="foo"),
        OperationFilter(parameter="stage", operator=operator.eq, value=4),
    ]


def test_invalid_filters():
    """Some invalid filter cases."""
    with pytest.raises(InvalidArgumentError):
        # Parameter/value only
        FilterParser.parse_listoperations_filters("stage")

    with pytest.raises(InvalidArgumentError):
        # Operator only
        FilterParser.parse_listoperations_filters("=")

    with pytest.raises(InvalidArgumentError):
        # Missing value
        FilterParser.parse_listoperations_filters("stage=")

    with pytest.raises(InvalidArgumentError):
        # Missing parameter
        FilterParser.parse_listoperations_filters("=completed")

    with pytest.raises(InvalidArgumentError):
        # Invalid operator
        FilterParser.parse_listoperations_filters("stage==completed")


def test_filtering_whitespace_ignored():
    """Whitespace doesn't matter."""
    expected_filters = [OperationFilter(parameter="name", operator=operator.eq, value="blah")]
    assert FilterParser.parse_listoperations_filters("name=blah") == expected_filters
    assert FilterParser.parse_listoperations_filters("name = blah") == expected_filters
    assert FilterParser.parse_listoperations_filters("name   =blah") == expected_filters
    assert FilterParser.parse_listoperations_filters("name=   blah") == expected_filters
    assert FilterParser.parse_listoperations_filters("  name =   blah  ") == expected_filters

    expected_filters = [
        OperationFilter(parameter="stage", operator=operator.eq, value=4),
        OperationFilter(parameter="name", operator=operator.ge, value="foo"),
    ]
    assert FilterParser.parse_listoperations_filters("stage=completed&name>=foo") == expected_filters
    assert FilterParser.parse_listoperations_filters("stage = completed & name >= foo") == expected_filters
    assert FilterParser.parse_listoperations_filters("stage = completed&name >= foo") == expected_filters
    assert FilterParser.parse_listoperations_filters("   stage=completed &name   >=foo") == expected_filters
