# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from datetime import datetime

import pytest

from buildgrid.server.exceptions import InvalidArgumentError
from buildgrid.server.operations.filtering import SortKey
from buildgrid.server.operations.filtering.sanitizer import (
    DatetimeValueSanitizer,
    OperationStageValueSanitizer,
    RegexValueSanitizer,
    SortKeyValueSanitizer,
)


def test_regex_value_sanitizer():
    """Some general tests for the regex sanitizer."""
    # Make sure a full match is required
    apple_sanitizer = RegexValueSanitizer("apple_sanitizer", "apple")
    assert apple_sanitizer.sanitize("apple") == "apple"
    with pytest.raises(InvalidArgumentError):
        apple_sanitizer.sanitize("banana")
    with pytest.raises(InvalidArgumentError):
        assert apple_sanitizer.sanitize("cranapple") == "cranapple"
    with pytest.raises(InvalidArgumentError):
        assert apple_sanitizer.sanitize("applepie") == "applepie"

    # Arbitrary Python regexes are ok
    buffalo_sanitizer = RegexValueSanitizer("buffalo_sanitizer", "(buffalo)+")
    assert buffalo_sanitizer.sanitize("buffalo") == "buffalo"
    assert buffalo_sanitizer.sanitize("buffalobuffalo") == "buffalobuffalo"
    assert buffalo_sanitizer.sanitize("buffalobuffalobuffalo") == "buffalobuffalobuffalo"
    with pytest.raises(InvalidArgumentError):
        buffalo_sanitizer.sanitize("buffalobill")


def test_datetime_value_sanitizer():
    """Test the datetime sanitizer."""
    datetime_sanitizer = DatetimeValueSanitizer("datetime_sanitizer")

    # Random strings raise an exception
    with pytest.raises(InvalidArgumentError):
        datetime_sanitizer.sanitize("blahblahblah")
    with pytest.raises(InvalidArgumentError):
        datetime_sanitizer.sanitize("5")

    # Assume UTC by default
    assert datetime_sanitizer.sanitize("2020-07-30") == datetime(2020, 7, 30)

    assert datetime_sanitizer.sanitize("2020-07-30T14:30:27") == datetime(2020, 7, 30, 14, 30, 27)

    # Timezone-aware strings are allowed, but they are converted to UTC and stripped of timezone awareness
    assert datetime_sanitizer.sanitize("2020-07-30T14:30:27+00:00") == datetime(2020, 7, 30, 14, 30, 27)
    assert datetime_sanitizer.sanitize("2020-07-30T09:30:27-05:00") == datetime(2020, 7, 30, 14, 30, 27)


def test_operation_stage_value_sanitizer():
    """Test the sanitizer for operation stages."""
    stage_sanitizer = OperationStageValueSanitizer("stage_sanitizer")

    # Random strings raise an exception
    with pytest.raises(InvalidArgumentError):
        stage_sanitizer.sanitize("blahblahblah")
    with pytest.raises(InvalidArgumentError):
        stage_sanitizer.sanitize("5")

    # Match each of the stages (upper or lowercase)
    assert stage_sanitizer.sanitize("unknown") == 0
    assert stage_sanitizer.sanitize("UNKNOWN") == 0

    assert stage_sanitizer.sanitize("cache_check") == 1
    assert stage_sanitizer.sanitize("CACHE_CHECK") == 1

    assert stage_sanitizer.sanitize("queued") == 2
    assert stage_sanitizer.sanitize("QUEUED") == 2

    assert stage_sanitizer.sanitize("executing") == 3
    assert stage_sanitizer.sanitize("EXECUTING") == 3

    assert stage_sanitizer.sanitize("completed") == 4
    assert stage_sanitizer.sanitize("COMPLETED") == 4


def test_sort_key_value_sanitizer():
    sort_key_sanitizer = SortKeyValueSanitizer("sort_key_sanitizer")

    # Empty string throws an error
    with pytest.raises(InvalidArgumentError):
        sort_key_sanitizer.sanitize("")

    # Any other parseable string is matched.
    example_sort_keys = ["+32", "foo", "stage", "camelCaseItem" "+()asdjfio-jw"]
    for base_key in example_sort_keys:
        # Allow any casing
        for cased_key in [base_key, base_key.lower(), base_key.upper()]:
            # Allow key by itself, as well as key followed by "(asc)" and key followed by "(desc)"
            for key_name in [cased_key, cased_key + "(asc)", cased_key + "(desc)"]:
                sort_key = sort_key_sanitizer.sanitize(key_name)
                if key_name.endswith("(desc)"):
                    assert sort_key == SortKey(name=base_key.lower(), descending=True)
                else:
                    assert sort_key == SortKey(name=base_key.lower(), descending=False)
