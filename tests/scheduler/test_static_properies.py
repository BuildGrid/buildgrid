from dataclasses import dataclass

import pytest

from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import Platform
from buildgrid._protos.google.devtools.remoteworkers.v1test2.bots_pb2 import BotSession
from buildgrid._protos.google.devtools.remoteworkers.v1test2.worker_pb2 import Device, Worker
from buildgrid.server.exceptions import FailedPreconditionError
from buildgrid.server.scheduler import PropertyLabel, StaticPropertySet


def new_platform(properties: set[tuple[str, str]]) -> Platform:
    return Platform(properties=[Platform.Property(name=name, value=value) for name, value in properties])


def new_session(properties: set[tuple[str, str]]) -> BotSession:
    return BotSession(
        worker=Worker(
            devices=[Device(properties=[Device.Property(key=key, value=value) for key, value in properties])]
        )
    )


@dataclass
class ExecutionPropertiesTestcase:
    name: str
    property_set: StaticPropertySet
    execution_platform: Platform
    expected_label: str | None = None
    expected_properties: dict[str, list[str]] | None = None
    raises: type[Exception] | None = None


@pytest.mark.parametrize(
    "testcase",
    [
        # For a property set with no items, we get back an empty set and the test label for execution.
        ExecutionPropertiesTestcase(
            name="EmptyProperties",
            property_set=StaticPropertySet(
                property_labels=[PropertyLabel(label="test", properties=set())],
                wildcard_property_keys=set(),
            ),
            execution_platform=new_platform(set()),
            expected_label="test",
            expected_properties={},
        ),
        # For a property set with an unregistered key, raise a value error.
        ExecutionPropertiesTestcase(
            name="UnknownProperty",
            property_set=StaticPropertySet(
                property_labels=[PropertyLabel(label="test", properties=set())],
                wildcard_property_keys=set(),
            ),
            execution_platform=new_platform({("foo", "bar")}),
            raises=FailedPreconditionError,
        ),
        # If there are wildcard properties, we don't count them.
        ExecutionPropertiesTestcase(
            name="WildcardProperty",
            property_set=StaticPropertySet(
                property_labels=[PropertyLabel(label="test", properties=set())],
                wildcard_property_keys={"foo"},
            ),
            execution_platform=new_platform({("foo", "bar")}),
            expected_label="test",
            expected_properties={},
        ),
        # If a wildcard is listed with the correct key, but wrong value, fail
        ExecutionPropertiesTestcase(
            name="ValueMismatch1",
            property_set=StaticPropertySet(
                property_labels=[PropertyLabel(label="test", properties={("foo", "bar")})],
                wildcard_property_keys=set(),
            ),
            execution_platform=new_platform({("foo", "fizz")}),
            raises=FailedPreconditionError,
        ),
        ExecutionPropertiesTestcase(
            name="ValueMismatch2",
            property_set=StaticPropertySet(
                property_labels=[PropertyLabel(label="test", properties={("foo", "bar")})],
                wildcard_property_keys=set(),
            ),
            execution_platform=new_platform({("foo", "bar"), ("foo", "fizz")}),
            raises=FailedPreconditionError,
        ),
        # If properties are not set, we apply the first match possible.
        ExecutionPropertiesTestcase(
            name="AppliesFirstToEmpty",
            property_set=StaticPropertySet(
                property_labels=[
                    PropertyLabel(label="test1", properties={("foo", "bar")}),
                    PropertyLabel(label="test2", properties={("fizz", "buzz")}),
                ],
                wildcard_property_keys=set(),
            ),
            execution_platform=new_platform(set()),
            expected_label="test1",
            expected_properties={"foo": ["bar"]},
        ),
        # Allow posting one key with multiple values
        ExecutionPropertiesTestcase(
            name="MultiValueKey1",
            property_set=StaticPropertySet(
                property_labels=[
                    PropertyLabel(label="test1", properties={("foo", "bar1"), ("foo", "bar2")}),
                ],
                wildcard_property_keys=set(),
            ),
            execution_platform=new_platform(set()),
            expected_label="test1",
            expected_properties={"foo": ["bar1", "bar2"]},
        ),
        ExecutionPropertiesTestcase(
            name="MultiValueKey2",
            property_set=StaticPropertySet(
                property_labels=[
                    PropertyLabel(label="test1", properties={("foo", "bar1"), ("foo", "bar2")}),
                ],
                wildcard_property_keys=set(),
            ),
            execution_platform=new_platform({("foo", "bar1")}),
            expected_label="test1",
            expected_properties={"foo": ["bar1", "bar2"]},
        ),
        ExecutionPropertiesTestcase(
            name="MultiValueKey3",
            property_set=StaticPropertySet(
                property_labels=[
                    PropertyLabel(label="test1", properties={("foo", "bar1"), ("foo", "bar2")}),
                ],
                wildcard_property_keys=set(),
            ),
            execution_platform=new_platform({("foo", "bar2")}),
            expected_label="test1",
            expected_properties={"foo": ["bar1", "bar2"]},
        ),
        ExecutionPropertiesTestcase(
            name="MultiValueKey4",
            property_set=StaticPropertySet(
                property_labels=[
                    PropertyLabel(label="test1", properties={("foo", "bar1"), ("foo", "bar2")}),
                ],
                wildcard_property_keys=set(),
            ),
            execution_platform=new_platform({("foo", "bar1"), ("foo", "bar2")}),
            expected_label="test1",
            expected_properties={"foo": ["bar1", "bar2"]},
        ),
        ExecutionPropertiesTestcase(
            name="MultiValueKey5",
            property_set=StaticPropertySet(
                property_labels=[
                    PropertyLabel(label="test1", properties={("foo", "bar1"), ("foo", "bar2")}),
                ],
                wildcard_property_keys=set(),
            ),
            execution_platform=new_platform({("foo", "bar1"), ("foo", "bar2"), ("foo", "bar3")}),
            raises=FailedPreconditionError,
        ),
        # Skips non-matching sets to find first match
        ExecutionPropertiesTestcase(
            name="SkipsNonMatch",
            property_set=StaticPropertySet(
                property_labels=[
                    PropertyLabel(label="test1", properties={("foo", "bar")}),
                    PropertyLabel(label="test2", properties={("fizz", "buzz")}),
                ],
                wildcard_property_keys=set(),
            ),
            execution_platform=new_platform({("fizz", "buzz")}),
            expected_label="test2",
            expected_properties={"fizz": ["buzz"]},
        ),
        # Skips partial non-matching sets to find first match
        ExecutionPropertiesTestcase(
            name="SkipsPartialMatch1",
            property_set=StaticPropertySet(
                property_labels=[
                    PropertyLabel(label="test1", properties={("foo", "bar")}),
                    PropertyLabel(label="test2", properties={("foo", "bar"), ("fizz", "buzz")}),
                ],
                wildcard_property_keys=set(),
            ),
            execution_platform=new_platform({("foo", "bar"), ("fizz", "buzz")}),
            expected_label="test2",
            expected_properties={"foo": ["bar"], "fizz": ["buzz"]},
        ),
        ExecutionPropertiesTestcase(
            name="SkipsPartialMatch1",
            property_set=StaticPropertySet(
                property_labels=[
                    PropertyLabel(label="test1", properties={("foo", "bar"), ("fizz", "buzz2")}),
                    PropertyLabel(label="test2", properties={("foo", "bar"), ("fizz", "buzz1")}),
                ],
                wildcard_property_keys=set(),
            ),
            execution_platform=new_platform({("foo", "bar"), ("fizz", "buzz1")}),
            expected_label="test2",
            expected_properties={"foo": ["bar"], "fizz": ["buzz1"]},
        ),
        # Skips partial non-matching sets to find first match and ignores wildcards
        ExecutionPropertiesTestcase(
            name="MatchWithWildcards",
            property_set=StaticPropertySet(
                property_labels=[
                    PropertyLabel(label="test1", properties={("foo", "bar")}),
                    PropertyLabel(label="test2", properties={("foo", "bar"), ("fizz", "buzz")}),
                ],
                wildcard_property_keys={"xog"},
            ),
            execution_platform=new_platform({("foo", "bar"), ("fizz", "buzz"), ("xog", "xog")}),
            expected_label="test2",
            expected_properties={"foo": ["bar"], "fizz": ["buzz"]},
        ),
    ],
    ids=lambda tc: tc.name,
)
def test_static_execution_properties(testcase: ExecutionPropertiesTestcase):
    if testcase.raises:
        with pytest.raises(testcase.raises):
            testcase.property_set.execution_properties(testcase.execution_platform)
    else:
        label, values = testcase.property_set.execution_properties(testcase.execution_platform)
        assert label == testcase.expected_label
        assert values == testcase.expected_properties


@dataclass
class WorkerPropertiesTestcase:
    name: str
    property_set: StaticPropertySet
    bot_session: BotSession
    expected_properties: list[dict[str, list[str]]] | None = None
    raises: type[Exception] | None = None


@pytest.mark.parametrize(
    "testcase",
    [
        # For a property set with no items, we get back an empty set and the test label for execution.
        WorkerPropertiesTestcase(
            name="EmptyProperties",
            property_set=StaticPropertySet(
                property_labels=[PropertyLabel(label="test", properties=set())],
                wildcard_property_keys=set(),
            ),
            bot_session=new_session(set()),
            expected_properties=[{}],
        ),
        # Test that we can include unknown properties in the worker, which are simply ignored.
        # Workers are allowed to over specify
        WorkerPropertiesTestcase(
            name="UnknownProperties",
            property_set=StaticPropertySet(
                property_labels=[PropertyLabel(label="test", properties=set())],
                wildcard_property_keys=set(),
            ),
            bot_session=new_session({("foo", "bar")}),
            expected_properties=[{}],
        ),
        # Test that we raise if we can't match on any property listings.
        WorkerPropertiesTestcase(
            name="NoMatchingProperties",
            property_set=StaticPropertySet(
                property_labels=[PropertyLabel(label="test", properties={("foo", "bar")})],
                wildcard_property_keys=set(),
            ),
            bot_session=new_session(set()),
            raises=FailedPreconditionError,
        ),
        # Test that a worker can listen for multiple property labels.
        WorkerPropertiesTestcase(
            name="MultipleMatchingProperties",
            property_set=StaticPropertySet(
                property_labels=[
                    PropertyLabel(label="test", properties={("foo", "bar")}),
                    PropertyLabel(label="test", properties={("fizz", "buzz")}),
                ],
                wildcard_property_keys=set(),
            ),
            bot_session=new_session({("foo", "bar"), ("fizz", "buzz")}),
            expected_properties=[{"foo": ["bar"]}, {"fizz": ["buzz"]}],
        ),
        # Test that a worker can filter property labels.
        WorkerPropertiesTestcase(
            name="FilterProperties1",
            property_set=StaticPropertySet(
                property_labels=[
                    PropertyLabel(label="test", properties={("foo", "bar")}),
                    PropertyLabel(label="test", properties={("fizz", "buzz")}),
                ],
                wildcard_property_keys=set(),
            ),
            bot_session=new_session({("foo", "bar")}),
            expected_properties=[{"foo": ["bar"]}],
        ),
        WorkerPropertiesTestcase(
            name="FilterProperties2",
            property_set=StaticPropertySet(
                property_labels=[
                    PropertyLabel(label="test", properties={("foo", "bar")}),
                    PropertyLabel(label="test", properties={("fizz", "buzz")}),
                ],
                wildcard_property_keys=set(),
            ),
            bot_session=new_session({("fizz", "buzz")}),
            expected_properties=[{"fizz": ["buzz"]}],
        ),
    ],
    ids=lambda tc: tc.name,
)
def test_static_worker_properties(testcase: WorkerPropertiesTestcase):
    if testcase.raises:
        with pytest.raises(testcase.raises):
            testcase.property_set.worker_properties(testcase.bot_session)
    else:
        values = testcase.property_set.worker_properties(testcase.bot_session)
        assert values == testcase.expected_properties
