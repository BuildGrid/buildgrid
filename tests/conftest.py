# Copyright (C) 2019 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import asyncio
import logging

import pytest

from buildgrid.server.app.cli import RequestIdFilter
from buildgrid.server.auth.manager import get_auth_manager, set_auth_manager
from buildgrid.server.monitoring import get_monitoring_bus, set_monitoring_bus

from .utils.fixtures import common_props, fake_redis, moto_server, postgres  # noqa: F401 allow fixture inclusion

collect_ignore = ["utils"]

# Needed or else logging monitor in tests will throw.
logging.getLogger().addFilter(RequestIdFilter())


@pytest.fixture(autouse=True)
def prepare_test_environment():
    # We need to explicitly set an event loop here, since pytest-aiohttp
    # seems to close the default/implicit loop we rely on in our asyncio
    # code. Without explicitly creating a new loop, this leads to all tests
    # that touch asyncio-related code and run in the same test process as
    # the bgd-browser backend tests failing due to the lack of a running
    # event loop in the current thread.

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    monitoring_bus = get_monitoring_bus()
    auth_manager = get_auth_manager()
    try:
        yield
    finally:
        loop.close()
        set_monitoring_bus(monitoring_bus)
        set_auth_manager(auth_manager)
