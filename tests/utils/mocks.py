# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import contextlib
from unittest import mock

from grpc._server import _Context, _RPCState

from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import RequestMetadata
from buildgrid.server.context import instance_context, method_context, service_context
from buildgrid.server.sql.models import ClientIdentityEntry

MOCK_TOOL_NAME = "bgd-test-suite"
MOCK_TOOL_VERSION = "0.0.0"
MOCK_INVOCATION_ID = "test-invocation"
MOCK_CORRELATED_INVOCATIONS_ID = "test-correlation"

MOCK_WORKFLOW = "build"
MOCK_ACTOR = "tool"
MOCK_SUBJECT = "user"


def mock_extract_request_metadata(context) -> RequestMetadata:
    metadata = RequestMetadata()
    metadata.tool_invocation_id = MOCK_INVOCATION_ID
    metadata.correlated_invocations_id = MOCK_CORRELATED_INVOCATIONS_ID
    metadata.tool_details.tool_name = MOCK_TOOL_NAME
    metadata.tool_details.tool_version = MOCK_TOOL_VERSION
    return metadata


def mock_extract_client_identity(instance, metadata) -> ClientIdentityEntry:
    client_id = ClientIdentityEntry()
    client_id.instance = instance
    client_id.workflow = MOCK_WORKFLOW
    client_id.actor = MOCK_ACTOR
    client_id.subject = MOCK_SUBJECT

    return client_id


@contextlib.contextmanager
def rpc_context(service, method, instance):
    with service_context(service), method_context(method), instance_context(instance):
        yield


def mock_context(invocation_metadata=None, time_remaining=None) -> _Context:
    event = mock.Mock()
    event.invocation_metadata = invocation_metadata or ()
    context = _Context(event, _RPCState(), None)
    if time_remaining is not None:
        context.time_remaining = lambda: time_remaining
    return context
