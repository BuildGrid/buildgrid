# Copyright (C) 2023 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from datetime import datetime

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid.server.enums import OperationStage
from buildgrid.server.sql import models
from buildgrid.server.sql.provider import SqlProvider


def generate_dummy_action(do_not_cache=False):
    action_proto = remote_execution_pb2.Action()
    action_proto.command_digest.hash = "commandHash"
    action_proto.command_digest.size_bytes = 123
    action_proto.do_not_cache = do_not_cache
    return action_proto


def add_test_job(job_name: str, sql: SqlProvider, instance_name: str = ""):
    with sql.session() as session:
        now = datetime.now()
        session.add(
            models.JobEntry(
                name=job_name,
                action=generate_dummy_action().SerializeToString(),
                action_digest="test-action-digest/144",
                priority=10,
                stage=OperationStage.CACHE_CHECK.value,
                instance_name=instance_name,
                platform_requirements="",
                property_label="unknown",
                command="",
                create_timestamp=now,
                queued_timestamp=now,
            )
        )
