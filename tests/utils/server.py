# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import signal
import tempfile
from contextlib import contextmanager

from grpc import ServicerContext

from buildgrid._protos.build.bazel.remote.asset.v1.remote_asset_pb2 import (
    PushBlobRequest,
    PushBlobResponse,
    PushDirectoryRequest,
    PushDirectoryResponse,
)
from buildgrid._protos.build.bazel.remote.asset.v1.remote_asset_pb2_grpc import PushServicer
from buildgrid.server.app.cli import setup_logging
from buildgrid.server.app.settings.config import populate_buildgrid_config
from buildgrid.server.app.settings.parser import load_config_value
from buildgrid.server.auth.manager import set_auth_manager
from buildgrid.server.monitoring import set_monitoring_bus
from buildgrid.server.server import Server
from buildgrid.server.sql.models import digest_to_string


@contextmanager
def serve(configuration: str, monitor=False):
    with tempfile.NamedTemporaryFile() as db_file:
        configuration = configuration.replace("DB_CONN_STR", f"sqlite:///{db_file.name}")

        path = tempfile.NamedTemporaryFile().name
        if monitor:
            configuration = configuration.replace("MON_ENABLE_BOOL", "true")
        else:
            configuration = configuration.replace("MON_ENABLE_BOOL", "true")
        configuration = configuration.replace("MON_ENDPOINT_LOCATION", path)

        with TestServer(configuration) as server:
            yield server, path


class TestServer:
    def __init__(self, configuration):
        self.config = populate_buildgrid_config(load_config_value(configuration))

        setup_logging()
        self.server = Server(
            server_reflection=self.config.server_reflection,
            grpc_compression=self.config.grpc_compression,
            is_instrumented=self.config.monitoring is not None,
            max_workers=self.config.thread_pool_size,
            grpc_server_options=self.config.grpc_server_options,
            monitoring_period=0.1,
        )

        if monitoring_bus := self.config.monitoring:
            set_monitoring_bus(monitoring_bus)

        if auth := self.config.authorization:
            set_auth_manager(auth)

        for instance in self.config.instances:
            for service in instance.services:
                self.server.register_instance(instance.name, service)

        self.server.add_port("localhost:0", None)

        def _retrieve_port(port_map):
            self.port = port_map["localhost:0"]
            self.remote = f"localhost:{self.port}"

        self.server.start(port_assigned_callback=_retrieve_port, run_forever=False)

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs) -> None:
        self.stop()

    def stop(self):
        self.server.stop()
        signal.alarm(0)


class MockAssetPushServicer(PushServicer):
    def __init__(self) -> None:
        self.blobs: dict[str, PushBlobRequest] = {}
        self.directories: dict[str, PushDirectoryRequest] = {}

    def PushBlob(self, request: PushBlobRequest, context: ServicerContext) -> PushBlobResponse:
        self.blobs[digest_to_string(request.blob_digest)] = request
        return PushBlobResponse()

    def PushDirectory(self, request: PushDirectoryRequest, context: ServicerContext) -> PushDirectoryResponse:
        self.directories[digest_to_string(request.root_directory_digest)] = request
        return PushDirectoryResponse()
