# Copyright (C) 2019 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
from contextlib import ExitStack, contextmanager

import grpc

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid.server.actioncache.caches.lru_cache import LruActionCache
from buildgrid.server.actioncache.instance import ActionCache
from buildgrid.server.actioncache.service import ActionCacheService
from buildgrid.server.capabilities.instance import CapabilitiesInstance
from buildgrid.server.capabilities.service import CapabilitiesService
from buildgrid.server.cas.storage.remote import RemoteStorage
from buildgrid.server.threading import ContextThreadPoolExecutor

from .cas import serve_cas


@contextmanager
def serve_cache(instances, num_failures=0, allow_updates=True):
    with serve_cas(["testing"]) as cas:
        with TestServer(cas, instances, num_failures, allow_updates) as server:
            yield server


def should_fail(method_name, failure_dict, num_failures):
    if method_name not in failure_dict:
        failure_dict[method_name] = 0

    if failure_dict[method_name] >= num_failures:
        failure_dict[method_name] = 0
        return False
    else:
        failure_dict[method_name] += 1
        return True


class FailableActionCacheService(ActionCacheService):
    def __init__(self, num_failures):
        super().__init__()

        self.__failure_dict = {}
        # The number of times to fail each call before succeeding
        self.num_failures = num_failures

    def GetActionResult(self, request, context):
        method_name = "GetActionResult"
        if should_fail(method_name, self.__failure_dict, self.num_failures):
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            return remote_execution_pb2.ActionResult()
        else:
            return super().GetActionResult(request, context)

    def UpdateActionResult(self, request, context):
        method_name = "UpdateActionResult"
        if should_fail(method_name, self.__failure_dict, self.num_failures):
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            return remote_execution_pb2.ActionResult()
        else:
            return super().UpdateActionResult(request, context)


class TestServer:
    def __init__(self, cas, instances, num_failures, allow_updates):
        self.stack = ExitStack()
        # Use max_workers default from Python 3.4+
        max_workers = (os.cpu_count() or 1) * 5
        self.server = grpc.server(ContextThreadPoolExecutor(max_workers, immediate_copy=True))
        self.port = self.server.add_insecure_port("localhost:0")
        self.remote = f"http://localhost:{self.port}"

        storage = RemoteStorage(cas.remote, "testing")

        ac_service = FailableActionCacheService(num_failures=num_failures)
        capabilities_service = CapabilitiesService()
        for name in instances:
            cache = LruActionCache(storage, 256, allow_updates=allow_updates)
            ac_instance = self.stack.enter_context(ActionCache(cache))
            ac_service.add_instance(name, ac_instance)

            capabilities_instance = CapabilitiesInstance(action_cache_instance=ac_instance)
            capabilities_service.add_instance(name, capabilities_instance)

        ac_service.setup_grpc(self.server)
        capabilities_service.setup_grpc(self.server)
        self.server.start()
        self.stack.callback(self.server.stop, grace=1)

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs) -> None:
        self.stack.close()
