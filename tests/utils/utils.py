# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import socket
import time
from contextlib import closing
from typing import Any, Callable


def read_file(file_path, text_mode=False):
    with open(file_path, "r" if text_mode else "rb") as in_file:
        return in_file.read()


def poll_and_assert(func: Callable[[], Any], expected: Any, poll_interval: float, timeout: float) -> None:
    start = time.time()
    actual = None
    while time.time() - start <= timeout:
        actual = func()
        if actual == expected:
            return
        time.sleep(poll_interval)
    assert False, f"{actual} != {expected} after {timeout} seconds"


def find_free_port() -> str:
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(("", 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return s.getsockname()[1]
