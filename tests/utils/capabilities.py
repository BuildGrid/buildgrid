# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
from contextlib import ExitStack, contextmanager

import grpc

from buildgrid.server.capabilities.instance import CapabilitiesInstance
from buildgrid.server.capabilities.service import CapabilitiesService
from buildgrid.server.threading import ContextThreadPoolExecutor


@contextmanager
def serve_capabilities_service(instances, cas_instance=None, action_cache_instance=None, execution_instance=None):
    with TestServer(instances, cas_instance, action_cache_instance, execution_instance) as server:
        yield server


class TestServer:
    def __init__(self, instances, cas_instance=None, action_cache_instance=None, execution_instance=None):
        self.stack = ExitStack()
        self.instances = instances

        # Use max_workers default from Python 3.4+
        max_workers = (os.cpu_count() or 1) * 5
        self.server = grpc.server(ContextThreadPoolExecutor(max_workers, immediate_copy=True))
        self.port = self.server.add_insecure_port("localhost:0")
        self.remote = f"localhost:{self.port}"

        capabilities_service = CapabilitiesService()
        for name in instances:
            capabilities_instance = CapabilitiesInstance(cas_instance, action_cache_instance, execution_instance)
            capabilities_service.add_instance(name, capabilities_instance)

        if cas_instance is not None:
            self.stack.enter_context(cas_instance)
        if execution_instance is not None:
            self.stack.enter_context(execution_instance)
        if action_cache_instance is not None:
            self.stack.enter_context(action_cache_instance)

        capabilities_service.setup_grpc(self.server)
        self.server.start()
        self.stack.callback(self.server.stop, grace=1)

    def __enter__(self):
        return self

    def __exit__(self, *args, **kwargs) -> None:
        self.stack.close()
