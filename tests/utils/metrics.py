# Copyright (C) 2020 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from datetime import datetime, timedelta

from buildgrid._protos.buildgrid.v2.monitoring_pb2 import MetricRecord
from buildgrid.server.metrics_utils import create_counter_record, create_distribution_record, create_timer_record

MOCK_FIXED_DATETIME = datetime(2020, 3, 23)
MOCK_FIXED_DURATION = 5


def mock_create_timer_record(name, duration=timedelta(milliseconds=MOCK_FIXED_DURATION), **metadata) -> MetricRecord:
    """Wrapper around create_timer_record that fixes the created_timestamp and duration,
    allowing us to assert_called_once_with with a consistent input argument"""
    record = create_timer_record(name, duration, **metadata)
    record.creation_timestamp.FromDatetime(MOCK_FIXED_DATETIME)
    record.duration.FromMilliseconds(MOCK_FIXED_DURATION)
    return record


def mock_create_counter_record(name, count=1.0, **metadata) -> MetricRecord:
    """Wrapper around create_counter_record that fixes the created_timestamp,
    allowing us to assert_called_once_with with a consistent input argument"""
    record = create_counter_record(name, count, **metadata)
    record.creation_timestamp.FromDatetime(MOCK_FIXED_DATETIME)
    return record


def mock_create_distribution_record(name, value=0.0, **metadata) -> MetricRecord:
    """Wrapper around create_distribution_record"""
    record = create_distribution_record(name, value, **metadata)
    record.creation_timestamp.FromDatetime(MOCK_FIXED_DATETIME)
    return record
