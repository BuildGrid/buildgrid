# Copyright (C) 2022 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import dataclasses
import os
import socket
import typing
import uuid
from contextlib import contextmanager
from typing import Iterator
from unittest.mock import patch

import fakeredis
import pytest
import sqlalchemy
import testing.postgresql
from botocore.config import Config

from buildgrid._protos.build.bazel.remote.logstream.v1.remote_logstream_pb2 import LogStream
from buildgrid.server.redis.provider import RedisProvider
from buildgrid.server.scheduler import DynamicPropertySet
from buildgrid.server.sql.models import Base
from buildgrid.server.sql.provider import SqlProvider

initdb = os.environ.get("INITDB_EXEC")
Postgresql = testing.postgresql.PostgresqlFactory(cache_initialized_db=True, initdb=initdb)


@dataclasses.dataclass
class Moto:
    bucket_prefix: str
    endpoint: str

    def bucket(self, name: str) -> str:
        return f"{self.bucket_prefix}-{name}"

    @property
    def opts(self) -> dict[str, typing.Any]:
        return {
            "endpoint_url": self.endpoint,
            "aws_access_key_id": "access_key",
            "aws_secret_access_key": "secret_key",
            "config": Config(s3={"addressing_style": "path"}),
        }


@pytest.fixture
def moto_server() -> typing.Iterator[Moto]:
    """
    This fixture spawns "moto_server", which is a test server for S3.
    """

    # If we spawn the moto server before running pytest, just use that.
    # For xdist style tests, this cuts out about 5 minutes testing time.
    port = int(os.environ.get("MOTO_PORT", "5000"))
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        if s.connect_ex(("localhost", port)) != 0:
            raise Exception("Run moto_server prior to running tests")

    with patch.dict(os.environ, {"TEST_SERVER_MODE": "true"}):
        yield Moto(bucket_prefix=str(uuid.uuid4()), endpoint=f"http://localhost:{port}")


class ThreadSafeFakeRedis(fakeredis.FakeRedis):
    """
    The FakeRedis mocks use a global lookup for mock servers which for some reason
    is returning the same instance in every test. This prevents tests from running
    in parallel without forking the python process via xdist...
    """

    def __init__(self, *args, server=None, **kwargs):
        super().__init__(*args, server=fakeredis.FakeServer(), **kwargs)


@pytest.fixture
def common_props() -> DynamicPropertySet:
    return DynamicPropertySet(
        unique_property_keys={"OSFamily"},
        match_property_keys={"OSFamily", "ISA"},
        wildcard_property_keys=set(),
    )


@pytest.fixture
def fake_redis() -> Iterator[RedisProvider]:
    with patch("buildgrid.server.redis.provider.redis.Redis", ThreadSafeFakeRedis):
        yield RedisProvider(host="localhost", port=8080, db=0)


@pytest.fixture(scope="session")
def postgres():
    with Postgresql() as postgres:
        sql_provider = SqlProvider(connection_string=postgres.url(), automigrate=False)
        with sql_provider._engine.begin() as conn:
            conn.execute(sqlalchemy.sql.text("CREATE USER rouser WITH PASSWORD 'test';"))
            conn.execute(sqlalchemy.sql.text("GRANT CONNECT ON DATABASE test TO rouser;"))
        sql_provider._engine.dispose()
        yield postgres


@contextmanager
def pg_schema_provider(postgres) -> Iterator[SqlProvider]:
    schema = "test" + uuid.uuid4().hex

    sql_provider = SqlProvider(connection_string=postgres.url(), automigrate=False)
    sql_provider._engine.update_execution_options(schema_translate_map={None: schema})
    with sql_provider._engine.begin() as conn:
        conn.execute(sqlalchemy.schema.CreateSchema(schema))
        Base.metadata.create_all(conn)

    try:
        yield sql_provider
    finally:
        sql_provider._engine.dispose()


@contextmanager
def pg_schema_provider_pair(postgres) -> Iterator[tuple[SqlProvider, SqlProvider]]:
    schema = "test" + uuid.uuid4().hex

    rw_sql_provider = SqlProvider(connection_string=postgres.url(), automigrate=False)
    rw_sql_provider._engine.update_execution_options(schema_translate_map={None: schema})
    with rw_sql_provider._engine.begin() as conn:
        conn.execute(sqlalchemy.schema.CreateSchema(schema))
        Base.metadata.create_all(conn)
        conn.execute(sqlalchemy.sql.text(f"GRANT ALL PRIVILEGES ON SCHEMA {schema} TO rouser;"))
        conn.execute(sqlalchemy.sql.text(f"GRANT SELECT ON ALL TABLES IN SCHEMA {schema} TO rouser;"))

    ro_connection_string = postgres.url().replace("postgresql://", "postgresql://rouser:test")
    ro_sql_provider = SqlProvider(connection_string=ro_connection_string, automigrate=False)
    ro_sql_provider._engine.update_execution_options(schema_translate_map={None: schema})

    try:
        yield rw_sql_provider, ro_sql_provider
    finally:
        rw_sql_provider._engine.dispose()
        ro_sql_provider._engine.dispose()


def mock_logstream_client(suffix=""):
    @contextmanager
    def _mock_logstream_client(channel, instance_name):
        class LogStreamClient:
            def create(self, parent: str) -> LogStream:
                return LogStream(
                    name=f"{parent}/mock-logstream{suffix}",
                    write_resource_name=f"{parent}/mock-logstream{suffix}/write",
                )

            def close(self):
                pass

        yield LogStreamClient()

    return _mock_logstream_client
