# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import tempfile
import time
from contextlib import ExitStack, contextmanager

import grpc

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid._protos.google.bytestream import bytestream_pb2
from buildgrid.server.cas.instance import ByteStreamInstance, ContentAddressableStorageInstance
from buildgrid.server.cas.service import ByteStreamService, ContentAddressableStorageService
from buildgrid.server.cas.storage.disk import DiskStorage
from buildgrid.server.cas.storage.storage_abc import create_write_session
from buildgrid.server.client.cas import merkle_tree_maker
from buildgrid.server.threading import ContextThreadPoolExecutor
from buildgrid.server.utils.digests import create_digest


@contextmanager
def serve_cas(instances, num_failures=0, *, delay=None, bytestream=True):
    with TestServer(instances, num_failures, delay=delay, bytestream=bytestream) as server:
        yield server


def should_fail(method_name, failure_dict, num_failures):
    if method_name not in failure_dict:
        failure_dict[method_name] = 0

    if failure_dict[method_name] >= num_failures:
        failure_dict[method_name] = 0
        return False
    else:
        failure_dict[method_name] += 1
        return True


class FailableContentAddressableStorageService(ContentAddressableStorageService):
    def __init__(self, num_failures, delay):
        super().__init__()

        self.__failure_dict = {}
        # The number of times to fail each call before succeeding
        self.num_failures = num_failures
        # The delay in seconds before responding to a request
        self.delay = delay

    def FindMissingBlobs(self, request, context):
        if self.delay is not None:
            time.sleep(self.delay)
        method_name = "FindMissingBlobs"
        if should_fail(method_name, self.__failure_dict, self.num_failures):
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            return remote_execution_pb2.FindMissingBlobsResponse()
        else:
            return super().FindMissingBlobs(request, context)

    def BatchUpdateBlobs(self, request, context):
        if self.delay is not None:
            time.sleep(self.delay)
        method_name = "BatchUpdateBlobs"
        if should_fail(method_name, self.__failure_dict, self.num_failures):
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            return remote_execution_pb2.BatchUpdateBlobsResponse()
        else:
            return super().BatchUpdateBlobs(request, context)

    def BatchReadBlobs(self, request, context):
        if self.delay is not None:
            time.sleep(self.delay)
        method_name = "BatchReadBlobs"
        if should_fail(method_name, self.__failure_dict, self.num_failures):
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            return remote_execution_pb2.BatchReadBlobsResponse()
        else:
            return super().BatchReadBlobs(request, context)

    def GetTree(self, request, context):
        if self.delay is not None:
            time.sleep(self.delay)
        method_name = "GetTree"
        if should_fail(method_name, self.__failure_dict, self.num_failures):
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            return remote_execution_pb2.GetTreeResponse()
        else:
            return super().GetTree(request, context)


class FailableByteStreamService(ByteStreamService):
    def __init__(self, num_failures, delay):
        super().__init__()

        self.__failure_dict = {}
        # The number of times to fail each call before succeeding
        self.num_failures = num_failures
        # The delay in seconds before responding to a request
        self.delay = delay

    def Read(self, request, context):
        if self.delay is not None:
            time.sleep(self.delay)
        method_name = "Read"
        if should_fail(method_name, self.__failure_dict, self.num_failures):
            context.set_code(grpc.StatusCode.UNAVAILABLE)
        else:
            yield from super().Read(request, context)

    def Write(self, request, context):
        if self.delay is not None:
            time.sleep(self.delay)
        method_name = "Write"
        if should_fail(method_name, self.__failure_dict, self.num_failures):
            context.set_code(grpc.StatusCode.UNAVAILABLE)
            return bytestream_pb2.WriteResponse()
        else:
            return super().Write(request, context)


class TestServer:
    def __init__(self, instances, num_failures=0, *, delay=None, bytestream=True):
        self.stack = ExitStack()
        self.__storage_path = self.stack.enter_context(tempfile.TemporaryDirectory())
        self.__storage = DiskStorage(self.__storage_path)

        # Use max_workers default from Python 3.4+
        max_workers = (os.cpu_count() or 1) * 5
        self.server = grpc.server(ContextThreadPoolExecutor(max_workers, immediate_copy=True))
        self.port = self.server.add_insecure_port("localhost:0")
        self.remote = f"http://localhost:{self.port}"

        storage = DiskStorage(self.__storage_path)

        if bytestream:
            bs_service = FailableByteStreamService(num_failures=num_failures, delay=delay)
            for name in instances:
                bs_instance = self.stack.enter_context(ByteStreamInstance(storage))
                bs_service.add_instance(name, bs_instance)

            bs_service.setup_grpc(self.server)

        cas_service = FailableContentAddressableStorageService(num_failures=num_failures, delay=delay)
        for name in instances:
            cas_instance = self.stack.enter_context(ContentAddressableStorageInstance(storage))
            cas_service.add_instance(name, cas_instance)

        cas_service.setup_grpc(self.server)
        self.server.start()
        self.stack.callback(self.server.stop, grace=1)

        # The CAS instance will pre-insert the empty blob in the storage.
        # To simplify testing (i.e avoid having an edge case with the empty
        # blob), we remove it:
        storage.delete_blob(create_digest(b""))

    def has(self, digest):
        return self.__storage.has_blob(digest)

    def get(self, digest):
        return self.__storage.get_blob(digest).read()

    def store_blob(self, blob):
        digest = create_digest(blob)
        with create_write_session(digest) as write_buffer:
            write_buffer.write(blob)
            self.__storage.commit_write(digest, write_buffer)

        return digest

    def compare_blobs(self, digest, blob):
        if not self.__storage.has_blob(digest):
            return False

        stored_blob = self.__storage.get_blob(digest)
        stored_blob = stored_blob.read()

        return blob == stored_blob

    def store_message(self, message):
        message_blob = message.SerializeToString()
        message_digest = create_digest(message_blob)

        with create_write_session(message_digest) as write_buffer:
            write_buffer.write(message_blob)
            self.__storage.commit_write(message_digest, write_buffer)

        return message_digest

    def compare_messages(self, digest, message):
        if not self.__storage.has_blob(digest):
            return False

        message_blob = message.SerializeToString()

        stored_blob = self.__storage.get_blob(digest)
        stored_blob = stored_blob.read()

        return message_blob == stored_blob

    def store_file(self, file_path):
        with open(file_path, "rb") as file_bytes:
            file_blob = file_bytes.read()
        file_digest = create_digest(file_blob)

        with create_write_session(file_digest) as write_buffer:
            write_buffer.write(file_blob)
            self.__storage.commit_write(file_digest, write_buffer)

        return file_digest

    def compare_files(self, digest, file_path):
        if not self.__storage.has_blob(digest):
            return False

        with open(file_path, "rb") as file_bytes:
            file_blob = file_bytes.read()

        stored_blob = self.__storage.get_blob(digest)
        stored_blob = stored_blob.read()

        return file_blob == stored_blob

    def store_folder(self, folder_path):
        last_digest = None
        for node, blob, _ in merkle_tree_maker(folder_path):
            with create_write_session(node.digest) as write_buffer:
                write_buffer.write(blob.read())
                self.__storage.commit_write(node.digest, write_buffer)
            last_digest = node.digest
        return last_digest

    def compare_directories(self, digest, directory_path):
        if not self.__storage.has_blob(digest):
            return False
        elif not os.path.isdir(directory_path):
            return False

        def __compare_folders(digest, path):
            directory = remote_execution_pb2.Directory()
            directory.ParseFromString(self.__storage.get_blob(digest).read())

            files, directories, symlinks = [], [], []
            for entry in os.scandir(path):
                if entry.is_file(follow_symlinks=False):
                    files.append(entry.name)

                elif entry.is_dir(follow_symlinks=False):
                    directories.append(entry.name)

                elif os.path.islink(entry.path):
                    symlinks.append(entry.name)

            assert len(files) == len(directory.files)
            assert len(directories) == len(directory.directories)
            assert len(symlinks) == len(directory.symlinks)

            for file_node in directory.files:
                file_path = os.path.join(path, file_node.name)

                assert file_node.name in files
                assert os.path.isfile(file_path)
                assert not os.path.islink(file_path)
                if file_node.is_executable:
                    assert os.access(file_path, os.X_OK)

                assert self.compare_files(file_node.digest, file_path)

            for directory_node in directory.directories:
                directory_path = os.path.join(path, directory_node.name)

                assert directory_node.name in directories
                assert os.path.exists(directory_path)
                assert not os.path.islink(directory_path)

                assert __compare_folders(directory_node.digest, directory_path)

            for symlink_node in directory.symlinks:
                symlink_path = os.path.join(path, symlink_node.name)

                assert symlink_node.name in symlinks
                assert os.path.islink(symlink_path)
                try:
                    assert os.readlink(symlink_path) == symlink_node.target
                except AssertionError:
                    # Maybe the target was a relative path and readlink gave
                    # us an absolute path
                    assert os.readlink(symlink_path) == os.path.join(path, symlink_node.target)

            return True

        return __compare_folders(digest, directory_path)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stack.close()
