# Copyright (C) 2018 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import io
from datetime import datetime
from unittest import mock
from unittest.mock import patch

import boto3
import fakeredis
import pytest
from botocore.exceptions import ClientError

from buildgrid._protos.build.bazel.remote.execution.v2 import remote_execution_pb2
from buildgrid.server.actioncache.caches.lru_cache import LruActionCache
from buildgrid.server.actioncache.caches.redis_cache import RedisActionCache
from buildgrid.server.actioncache.caches.remote_cache import RemoteActionCache
from buildgrid.server.actioncache.caches.s3_cache import S3ActionCache
from buildgrid.server.actioncache.caches.sharded_cache import ShardedActionCache
from buildgrid.server.actioncache.caches.with_cache import WithCacheActionCache
from buildgrid.server.actioncache.caches.write_once_cache import WriteOnceActionCache
from buildgrid.server.actioncache.instance import ActionCache
from buildgrid.server.cas.instance import EMPTY_BLOB_DIGEST
from buildgrid.server.cas.storage import lru_memory_cache, storage_abc
from buildgrid.server.context import instance_context
from buildgrid.server.enums import ActionCacheEntryType
from buildgrid.server.exceptions import NotFoundError, StorageFullError
from buildgrid.server.metrics_names import METRIC
from buildgrid.server.monitoring import get_monitoring_bus, set_monitoring_bus
from buildgrid.server.s3 import s3utils
from buildgrid.server.utils.digests import create_digest
from tests.utils.metrics import mock_create_distribution_record

from .utils.action_cache import serve_cache
from .utils.fixtures import Moto

retries = 3
max_backoff = 0.1


@pytest.fixture
def cas():
    return lru_memory_cache.LRUMemoryCache(1024 * 1024)


FALLBACK_TYPES = ["lru", "s3", "redis", "remote", "remote-context", "sharded"]


@pytest.fixture(params=FALLBACK_TYPES)
def any_cache(request, moto_server: Moto):
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)

    if request.param == "lru":
        yield LruActionCache(storage, max_cached_refs=2, allow_updates=True)
    elif request.param == "remote":
        with serve_cache(["testing"]) as server:
            with RemoteActionCache(server.remote, "testing", retries=retries, max_backoff=max_backoff) as cache:
                yield cache
    elif request.param == "remote-context":
        with serve_cache(["testing"]) as server:
            with RemoteActionCache(server.remote, retries=retries, max_backoff=max_backoff) as cache:
                with instance_context("testing"):
                    yield cache
    elif request.param == "s3":
        boto3.resource("s3", **moto_server.opts).create_bucket(Bucket=moto_server.bucket("cachebucket"))
        yield S3ActionCache(
            storage,
            allow_updates=True,
            cache_failed_actions=True,
            bucket=moto_server.bucket("cachebucket"),
            **moto_server.opts,
        )
    elif request.param == "redis":
        with patch("buildgrid.server.redis.provider.redis.Redis", fakeredis.FakeRedis):
            from buildgrid.server.actioncache.caches.redis_cache import RedisActionCache
            from buildgrid.server.redis.provider import RedisProvider

            redis = RedisProvider(host="localhost", port=8000)
            yield RedisActionCache(storage, redis, True, True)
    elif request.param == "sharded":
        yield ShardedActionCache(
            {
                "a": LruActionCache(storage, max_cached_refs=2, allow_updates=True),
                "b": LruActionCache(storage, max_cached_refs=2, allow_updates=True),
            }
        )


def test_null_cas_action_cache(cas):
    cache = LruActionCache(cas, 0)

    action_digest1 = remote_execution_pb2.Digest(hash="alpha", size_bytes=4)
    dummy_result = remote_execution_pb2.ActionResult()

    cache.update_action_result(action_digest1, dummy_result)
    with pytest.raises(NotFoundError):
        cache.get_action_result(action_digest1)


def test_expiry(cas):
    cache = LruActionCache(cas, 2)

    action_digest1 = create_digest(b"alpha")
    action_digest2 = create_digest(b"bravo")
    action_digest3 = create_digest(b"charlie")
    dummy_result = remote_execution_pb2.ActionResult()

    cache.update_action_result(action_digest1, dummy_result)
    cache.update_action_result(action_digest2, dummy_result)

    # Get digest 1 (making 2 the least recently used)
    assert cache.get_action_result(action_digest1) is not None
    # Add digest 3 (so 2 gets removed from the cache)
    cache.update_action_result(action_digest3, dummy_result)

    assert cache.get_action_result(action_digest1) is not None
    with pytest.raises(NotFoundError):
        cache.get_action_result(action_digest2)

    assert cache.get_action_result(action_digest3) is not None


@mock.patch("buildgrid.server.metrics_utils.create_distribution_record", new=mock_create_distribution_record)
@mock.patch("buildgrid.server.actioncache.instance.datetime")
@mock.patch.object(LruActionCache, "get_action_result")
def test_result_age_metric_published(mock_lru_result, mock_date, cas):
    cache = LruActionCache(cas, 2)
    ac_instance = ActionCache(cache)
    digest = mock.Mock()

    set_monitoring_bus(mock.Mock())

    # Action with worker_completed_timestamp in the metadata should publish the metric
    time_completed_timestamp = datetime(2024, 10, 7)
    execution_metadata = remote_execution_pb2.ExecutedActionMetadata()
    execution_metadata.worker_completed_timestamp.FromDatetime(time_completed_timestamp)
    action_result = remote_execution_pb2.ActionResult(execution_metadata=execution_metadata)
    mock_lru_result.return_value = action_result

    # Mock the current date to be 1 day after the worker_completed_timestamp
    mock_date.now.return_value = datetime(2024, 10, 8)
    expected_age = 8.64e7  # 1 day in milliseconds

    ac_instance.get_action_result(digest)
    mock_record = mock_create_distribution_record(METRIC.ACTION_CACHE.RESULT_AGE, expected_age)
    get_monitoring_bus().send_record_nowait.assert_called_once_with(mock_record)


@mock.patch.object(LruActionCache, "get_action_result")
def test_result_age_metric_not_published(mock_lru_result, cas):
    cache = LruActionCache(cas, 2)
    ac_instance = ActionCache(cache)
    digest = mock.Mock()

    set_monitoring_bus(mock.Mock())

    action_result = remote_execution_pb2.ActionResult()
    mock_lru_result.return_value = action_result
    ac_instance.get_action_result(digest)
    get_monitoring_bus().send_record_nowait.assert_not_called()


def test_with_cache(any_cache, moto_server):
    storage = lru_memory_cache.LRUMemoryCache(1024 * 1024)
    local_cache = LruActionCache(storage, 50)

    if callable(any_cache):
        any_cache = any_cache()

    cache = WithCacheActionCache(local_cache, any_cache, allow_updates=True, cache_failed_actions=True)

    assert cache is not None

    action_digest1 = create_digest(b"alpha")
    action_digest2 = create_digest(b"bravo")
    action_digest3 = create_digest(b"charlie")
    dummy_result = remote_execution_pb2.ActionResult()

    with pytest.raises(NotFoundError):
        cache.get_action_result(action_digest1)

    # Populate only fallback storage with action result
    cache._fallback.update_action_result(action_digest1, dummy_result)
    assert cache.get_action_result(action_digest1) is not None

    # Ensure fallback storage is populated
    cache.update_action_result(action_digest2, dummy_result)
    cache.update_action_result(action_digest3, dummy_result)
    assert cache._fallback.get_action_result(action_digest2) is not None


@pytest.mark.parametrize("acType", ["memory", "s3", "write_once", "redis"])
def test_checks_cas(acType, cas, moto_server: Moto):
    if acType == "memory":
        cache = LruActionCache(cas, 50)
    elif acType == "s3":
        boto3.resource("s3", **moto_server.opts).create_bucket(Bucket=moto_server.bucket("cachebucket"))
        cache = S3ActionCache(
            cas,
            allow_updates=True,
            cache_failed_actions=True,
            bucket=moto_server.bucket("cachebucket"),
            **moto_server.opts,
        )
    elif acType == "write_once":
        underlying_cache = LruActionCache(cas, 50)
        cache = WriteOnceActionCache(underlying_cache)
    elif acType == "redis":
        with patch("buildgrid.server.redis.provider.redis.Redis", fakeredis.FakeRedis):
            from buildgrid.server.actioncache.caches.redis_cache import RedisActionCache
            from buildgrid.server.redis.provider import RedisProvider

            redis = RedisProvider(host="localhost", port=8000)
            cache = RedisActionCache(cas, redis, True, True)

    action1 = remote_execution_pb2.Action(salt=b"action1")
    action_digest1 = cas.put_message(action1)

    action2 = remote_execution_pb2.Action(salt=b"action2")
    action_digest2 = cas.put_message(action2)

    action3 = remote_execution_pb2.Action(salt=b"action3")
    action_digest3 = cas.put_message(action3)

    action4 = remote_execution_pb2.Action(salt=b"action4")
    action_digest4 = cas.put_message(action4)

    action5 = remote_execution_pb2.Action(salt=b"action5")
    action_digest5 = cas.put_message(action5)

    # Create a tree that actions digests in CAS
    sample_digest = cas.put_message(remote_execution_pb2.Command(arguments=["sample"]))
    subdir = remote_execution_pb2.Directory()
    subdir.files.add().digest.CopyFrom(sample_digest)
    subdir_digest = cas.put_message(subdir)
    tree = remote_execution_pb2.Tree()
    tree.root.files.add().digest.CopyFrom(sample_digest)
    tree.root.directories.add().digest.CopyFrom(subdir_digest)
    tree.children.add().CopyFrom(subdir)
    tree_digest = cas.put_message(tree)
    root_directory_digest = cas.put_message(tree.root)

    # Add an ActionResult that actions real digests to the cache (tree_digest)
    action_result1 = remote_execution_pb2.ActionResult()
    action_result1.output_directories.add().tree_digest.CopyFrom(tree_digest)
    action_result1.output_files.add().digest.CopyFrom(sample_digest)
    action_result1.stdout_digest.CopyFrom(sample_digest)
    action_result1.stderr_digest.CopyFrom(sample_digest)
    cache.update_action_result(action_digest1, action_result1)

    # Add ActionResults that action fake digests to the cache
    action_result2 = remote_execution_pb2.ActionResult()
    action_result2.output_directories.add().tree_digest.hash = "nonexistent"
    action_result2.output_directories[0].tree_digest.size_bytes = 8
    cache.update_action_result(action_digest2, action_result2)

    action_result3 = remote_execution_pb2.ActionResult()
    action_result3.stdout_digest.hash = "nonexistent"
    action_result3.stdout_digest.size_bytes = 8
    cache.update_action_result(action_digest3, action_result3)

    # Add action result with empty digest
    action_result4 = remote_execution_pb2.ActionResult()
    action_result4.stdout_digest.hash = EMPTY_BLOB_DIGEST.hash
    action_result4.stdout_digest.size_bytes = EMPTY_BLOB_DIGEST.size_bytes
    cache.update_action_result(action_digest4, action_result4)

    # Add an ActionResult that actions real digests to the cache (root_directory_digest)
    action_result5 = remote_execution_pb2.ActionResult()
    action_result5.output_directories.add().root_directory_digest.CopyFrom(root_directory_digest)
    action_result5.output_files.add().digest.CopyFrom(sample_digest)
    action_result5.stdout_digest.CopyFrom(sample_digest)
    action_result5.stderr_digest.CopyFrom(sample_digest)
    cache.update_action_result(action_digest5, action_result5)

    # Verify we can get the 1st, 4th and 5th ActionResult but not the others
    fetched_result1 = cache.get_action_result(action_digest1)
    assert fetched_result1.output_directories[0].tree_digest.hash == tree_digest.hash

    fetched_result4 = cache.get_action_result(action_digest4)
    assert fetched_result4.stdout_digest.hash == EMPTY_BLOB_DIGEST.hash
    assert fetched_result4.stdout_digest.size_bytes == EMPTY_BLOB_DIGEST.size_bytes

    fetched_result5 = cache.get_action_result(action_digest5)
    assert fetched_result5.output_directories[0].root_directory_digest.hash == root_directory_digest.hash

    with pytest.raises(NotFoundError):
        cache.get_action_result(action_digest2)
    with pytest.raises(NotFoundError):
        cache.get_action_result(action_digest3)


@pytest.mark.parametrize("acType", ["memory", "s3", "write_once", "redis"])
def test_get_action_result_fmb_action(acType, cas, moto_server: Moto):
    if acType == "memory":
        cache = LruActionCache(cas, 50)
    elif acType == "s3":
        boto3.resource("s3", **moto_server.opts).create_bucket(Bucket=moto_server.bucket("cachebucket"))
        cache = S3ActionCache(
            cas,
            allow_updates=True,
            cache_failed_actions=True,
            bucket=moto_server.bucket("cachebucket"),
            **moto_server.opts,
        )
    elif acType == "write_once":
        underlying_cache = LruActionCache(cas, 50)
        cache = WriteOnceActionCache(underlying_cache)
    elif acType == "redis":
        with patch("buildgrid.server.redis.provider.redis.Redis", fakeredis.FakeRedis):
            from buildgrid.server.actioncache.caches.redis_cache import RedisActionCache
            from buildgrid.server.redis.provider import RedisProvider

            redis = RedisProvider(host="localhost", port=8000)
            cache = RedisActionCache(cas, redis, True, True)

    command = remote_execution_pb2.Command(arguments=["/bin/echo", "hello", "world"])
    command_digest = create_digest(command.SerializeToString())
    action = remote_execution_pb2.Action(command_digest=command_digest)
    # Only uplaod the action but not the Command just yet
    action_digest = cas.put_message(action)

    sample_digest = create_digest(b"sample_digest")
    with storage_abc.create_write_session(sample_digest) as write_session:
        write_session.write(b"sample_digest")
        cas.commit_write(sample_digest, write_session)
    subdir = remote_execution_pb2.Directory()
    subdir.files.add().digest.CopyFrom(sample_digest)
    subdir_digest = cas.put_message(subdir)
    tree = remote_execution_pb2.Tree()
    tree.root.files.add().digest.CopyFrom(sample_digest)
    tree.root.directories.add().digest.CopyFrom(subdir_digest)
    tree.children.add().CopyFrom(subdir)
    tree_digest = cas.put_message(tree)
    _ = cas.put_message(tree.root)

    action_result = remote_execution_pb2.ActionResult()
    action_result.output_directories.add().tree_digest.CopyFrom(tree_digest)
    action_result.output_files.add().digest.CopyFrom(sample_digest)
    action_result.stdout_digest.CopyFrom(sample_digest)
    action_result.stderr_digest.CopyFrom(sample_digest)
    cache.update_action_result(action_digest, action_result)

    expected_fmb_blobs = [sample_digest, tree_digest, action_digest, command_digest]
    with patch.object(cas, "missing_blobs", autospec=True) as mock_storage:
        # The command digest was not uploaded to CAS, but it should
        # still be included in the FMB call, and it being reported
        # missing should not cause `get_action_result` to report missing
        fetched_result = cache.get_action_result(action_digest)
        assert fetched_result
        assert mock_storage.call_count == 1
        fmb_blobs = mock_storage.call_args.args[0]
        for digest in expected_fmb_blobs:
            assert digest in fmb_blobs

        # Now upload the command message and ensure that it still works
        _ = cas.put_message(command)
        mock_storage.reset_mock()
        fetched_result = cache.get_action_result(action_digest)
        assert fetched_result
        assert mock_storage.call_count == 1
        fmb_blobs = mock_storage.call_args.args[0]
        for digest in expected_fmb_blobs:
            assert digest in fmb_blobs


def test_remote_max_retries():
    with serve_cache(["testing"], num_failures=3) as server:
        with RemoteActionCache(server.remote, "testing", retries, max_backoff) as cache:
            action_digest = create_digest(b"alpha")
            result = remote_execution_pb2.ActionResult()
            cache.update_action_result(action_digest, result)

            fetched = cache.get_action_result(action_digest)
            assert result == fetched


def test_remote_retry_failure():
    with serve_cache(["testing"], num_failures=4) as server:
        with RemoteActionCache(server.remote, "testing", retries, max_backoff) as cache:
            action_digest = create_digest(b"alpha")
            result = remote_execution_pb2.ActionResult()

            with pytest.raises(ConnectionError):
                cache.update_action_result(action_digest, result)

            with pytest.raises(ConnectionError):
                cache.get_action_result(action_digest)


def test_remote_update():
    with serve_cache(["testing"]) as server:
        with RemoteActionCache(server.remote, "testing") as cache:
            action_digest = create_digest(b"alpha")
            result = remote_execution_pb2.ActionResult()
            cache.update_action_result(action_digest, result)

            fetched = cache.get_action_result(action_digest)
            assert result == fetched


def test_remote_update_disallowed():
    with serve_cache(["testing"], allow_updates=False) as server:
        with RemoteActionCache(server.remote, "testing") as cache:
            action_digest = remote_execution_pb2.Digest(hash="alpha", size_bytes=4)
            result = remote_execution_pb2.ActionResult()
            with pytest.raises(NotImplementedError, match="Updating cache not allowed"):
                cache.update_action_result(action_digest, result)


def test_remote_get_missing():
    with serve_cache(["testing"]) as server:
        with RemoteActionCache(server.remote, "testing") as cache:
            action_digest = remote_execution_pb2.Digest(hash="alpha", size_bytes=4)
            with pytest.raises(NotFoundError):
                cache.get_action_result(action_digest)


def test_full_s3_action_cache(cas, moto_server: Moto):
    def __test_action_cache_write():
        action_digest = remote_execution_pb2.Digest(hash="alpha", size_bytes=4)
        result = remote_execution_pb2.ActionResult()
        with pytest.raises(StorageFullError):
            cache.update_action_result(action_digest, result)

    boto3.resource("s3", **moto_server.opts).create_bucket(Bucket=moto_server.bucket("cachebucket"))
    cache = S3ActionCache(
        cas,
        allow_updates=True,
        cache_failed_actions=True,
        bucket=moto_server.bucket("cachebucket"),
        **moto_server.opts,
    )

    with patch("buildgrid.server.s3.s3utils.put_objects") as m:
        response = {"Error": {"Code": "QuotaExceededException"}}
        err = ClientError(response, "Error")

        def mock_put_objects(s3, objects, *args, **kwargs):
            for s3object in objects:
                s3object.error = err

        m.side_effect = mock_put_objects
        __test_action_cache_write()


@pytest.mark.parametrize("entryType", [ActionCacheEntryType.ACTION_RESULT_DIGEST, ActionCacheEntryType.ACTION_RESULT])
@pytest.mark.parametrize("migrateEntries", [False, True])
def test_s3_hits(cas, entryType, migrateEntries, moto_server: Moto):
    bucket_template = "cachebucket--{digest[0]}"
    nums = [str(i) for i in range(10)]
    lets = [chr(i) for i in range(ord("a"), ord("g"))]
    hexvals = nums + lets
    for i in range(len(hexvals)):
        bucket_name = "cachebucket--" + hexvals[i]
        boto3.resource("s3", **moto_server.opts).create_bucket(Bucket=moto_server.bucket(bucket_name))

    cache = S3ActionCache(
        cas,
        allow_updates=True,
        cache_failed_actions=True,
        entry_type=entryType,
        migrate_entries=migrateEntries,
        bucket=moto_server.bucket(bucket_template),
        **moto_server.opts,
    )

    action = remote_execution_pb2.Action(salt=b"action")
    action_digest = cas.put_message(action)

    # Populating the CAS with a non-empty `ActionResult`:
    action_result = remote_execution_pb2.ActionResult(exit_code=42)
    action_result_digest = cas.put_message(action_result)

    s3_cache_key = f"{action_digest.hash}_{action_digest.size_bytes}"
    s3object = s3utils.S3Object(cache._get_bucket_name(action_digest), s3_cache_key)

    # Depending on the configuration of the cache, the entries in S3
    # will contain the Digest or the `ActionResult` itself.
    if entryType == ActionCacheEntryType.ACTION_RESULT_DIGEST:
        entry_value = action_result_digest.SerializeToString()
    else:
        entry_value = action_result.SerializeToString()
    s3object.fileobj = io.BytesIO(entry_value)
    s3object.filesize = len(entry_value)
    s3utils.put_object(cache._s3cache, s3object)

    # When querying the cache we get a hit:
    cached_action_result = cache.get_action_result(action_digest)
    assert cached_action_result == action_result

    # And we also get hits for the other entry type:
    s3object = s3utils.S3Object(cache._get_bucket_name(action_digest), s3_cache_key)
    if entryType == ActionCacheEntryType.ACTION_RESULT:
        entry_value = action_result_digest.SerializeToString()
    else:
        entry_value = action_result.SerializeToString()
    s3object.fileobj = io.BytesIO(entry_value)
    s3object.filesize = len(entry_value)
    s3utils.put_object(cache._s3cache, s3object)

    assert cache.get_action_result(action_digest) == action_result


@pytest.mark.parametrize("entryType", [ActionCacheEntryType.ACTION_RESULT_DIGEST, ActionCacheEntryType.ACTION_RESULT])
@pytest.mark.parametrize("migrateEntries", [False, True])
def test_s3_writes(cas, entryType, migrateEntries, moto_server: Moto):
    bucket_template = "cachebucket--{digest[0]}"
    nums = [str(i) for i in range(10)]
    lets = [chr(i) for i in range(ord("a"), ord("g"))]
    hexvals = nums + lets
    for i in range(len(hexvals)):
        bucket_name = "cachebucket--" + hexvals[i]
        boto3.resource("s3", **moto_server.opts).create_bucket(Bucket=moto_server.bucket(bucket_name))

    cache = S3ActionCache(
        cas,
        allow_updates=True,
        cache_failed_actions=True,
        entry_type=entryType,
        migrate_entries=migrateEntries,
        bucket=moto_server.bucket(bucket_template),
        **moto_server.opts,
    )

    action = remote_execution_pb2.Action(salt=b"action")
    action_digest = cas.put_message(action)

    # Populating the CAS with a non-empty `ActionResult`:
    action_result = remote_execution_pb2.ActionResult(exit_code=42)
    action_result_digest = cas.put_message(action_result)

    cache.update_action_result(action_digest, action_result)
    assert cache.get_action_result(action_digest) == action_result

    # The `ActionResult` was added to the underlying storage:
    assert cas.get_message(action_result_digest, remote_execution_pb2.ActionResult) == action_result

    # Checking the entry created in S3:
    s3_cache_key = f"{action_digest.hash}_{action_digest.size_bytes}"
    s3object = s3utils.S3Object(cache._get_bucket_name(action_digest), s3_cache_key)
    s3object.fileobj = io.BytesIO()
    s3utils.get_object(cache._s3cache, s3object)
    s3object.fileobj.seek(0)
    s3_cache_entry = s3object.fileobj.read()

    # Depending on the cache configuration, the entry should contain
    # either the `ActionResult` or its digest.
    if entryType == ActionCacheEntryType.ACTION_RESULT_DIGEST:
        assert s3_cache_entry == action_result_digest.SerializeToString()
    else:
        assert s3_cache_entry == action_result.SerializeToString()


@pytest.mark.parametrize("entryType", [ActionCacheEntryType.ACTION_RESULT_DIGEST, ActionCacheEntryType.ACTION_RESULT])
def test_s3_migrate_entries_to_new_type(cas, entryType, moto_server: Moto):
    bucket_name = moto_server.bucket("cachebucket")
    boto3.resource("s3", **moto_server.opts).create_bucket(Bucket=bucket_name)

    cache = S3ActionCache(
        cas,
        allow_updates=True,
        cache_failed_actions=True,
        entry_type=entryType,
        migrate_entries=True,
        bucket=bucket_name,
        **moto_server.opts,
    )

    action = remote_execution_pb2.Action(salt=b"action")
    action_digest = cas.put_message(action)

    # Populating the CAS with a non-empty `ActionResult`:
    action_result = remote_execution_pb2.ActionResult(exit_code=42)
    action_result_digest = cas.put_message(action_result)

    # Storing an entry created in S3 with the old type:
    s3_cache_key = f"{action_digest.hash}_{action_digest.size_bytes}"
    s3object = s3utils.S3Object(bucket_name, s3_cache_key)

    if entryType == ActionCacheEntryType.ACTION_RESULT_DIGEST:
        entry_value = action_result.SerializeToString()
    else:
        entry_value = action_result_digest.SerializeToString()

    s3object.fileobj = io.BytesIO(entry_value)
    s3object.filesize = len(entry_value)
    s3utils.put_object(cache._s3cache, s3object)

    # Accessing the entry:
    assert cache.get_action_result(action_digest) is not None

    # Depending on the cache configuration, the entry should have been
    # migrated to the new selected format (`entry_type`)
    s3object = s3utils.S3Object(bucket_name, s3_cache_key)
    s3object.fileobj = io.BytesIO()
    s3utils.get_object(cache._s3cache, s3object)
    s3object.fileobj.seek(0)
    s3_cache_entry = s3object.fileobj.read()
    if entryType == ActionCacheEntryType.ACTION_RESULT_DIGEST:
        assert s3_cache_entry == action_result_digest.SerializeToString()
    else:
        assert s3_cache_entry == action_result.SerializeToString()


def test_s3_salt(cas: lru_memory_cache.LRUMemoryCache, moto_server: Moto):
    # GIVEN: three caches using the same s3 bucket
    # GIVEN: two of which have a matching cache key salt
    bucket_template = "cachebucket--{digest[0]}"
    nums = [str(i) for i in range(10)]
    lets = [chr(i) for i in range(ord("a"), ord("g"))]
    hexvals = nums + lets
    for i in range(len(hexvals)):
        bucket_name = "cachebucket--" + hexvals[i]
        boto3.resource("s3", **moto_server.opts).create_bucket(Bucket=moto_server.bucket(bucket_name))

    shared_a = S3ActionCache(
        storage=cas,
        allow_updates=True,
        cache_failed_actions=True,
        entry_type=ActionCacheEntryType.ACTION_RESULT,
        migrate_entries=False,
        cache_key_salt="shared",
        bucket=moto_server.bucket(bucket_template),
        **moto_server.opts,
    )
    shared_b = S3ActionCache(
        storage=cas,
        allow_updates=True,
        cache_failed_actions=True,
        entry_type=ActionCacheEntryType.ACTION_RESULT,
        migrate_entries=False,
        cache_key_salt="shared",
        bucket=moto_server.bucket(bucket_template),
        **moto_server.opts,
    )
    isolated = S3ActionCache(
        storage=cas,
        allow_updates=True,
        cache_failed_actions=True,
        entry_type=ActionCacheEntryType.ACTION_RESULT,
        migrate_entries=False,
        cache_key_salt="isolated",
        bucket=moto_server.bucket(bucket_template),
        **moto_server.opts,
    )

    # WHEN: An ActionResult is uploaded to the the isolated cache
    action = remote_execution_pb2.Action(salt=b"action")
    action_digest = cas.put_message(action)
    action_result = remote_execution_pb2.ActionResult(exit_code=42)
    isolated.update_action_result(action_digest, action_result)

    # THEN: The isolated cache has the result, but the others do not
    assert isolated.get_action_result(action_digest)
    with pytest.raises(NotFoundError):
        shared_a.get_action_result(action_digest)
        shared_b.get_action_result(action_digest)

    # WHEN: An ActionResult is uploaded to one of the shared caches
    shared_action = remote_execution_pb2.Action(salt=b"shared_action")
    shared_action_digest = cas.put_message(shared_action)
    shared_action_result = remote_execution_pb2.ActionResult(exit_code=42)
    shared_a.update_action_result(shared_action_digest, shared_action_result)

    # THEN: Both shared caches have the result, but the isolated cache does not
    assert shared_a.get_action_result(shared_action_digest)
    assert shared_b.get_action_result(shared_action_digest)
    with pytest.raises(NotFoundError):
        isolated.get_action_result(shared_action_digest)


@pytest.mark.parametrize("entryType", [ActionCacheEntryType.ACTION_RESULT_DIGEST, ActionCacheEntryType.ACTION_RESULT])
@pytest.mark.parametrize("migrateEntries", [False, True])
def test_redis_misses(cas, entryType, migrateEntries, fake_redis):
    cache = RedisActionCache(
        storage=cas,
        redis=fake_redis,
        allow_updates=True,
        cache_failed_actions=True,
        entry_type=entryType,
        migrate_entries=migrateEntries,
    )

    digest = remote_execution_pb2.Digest(hash="foo", size_bytes=123)
    with pytest.raises(NotFoundError):
        cache.get_action_result(digest)


@pytest.mark.parametrize("entryType", [ActionCacheEntryType.ACTION_RESULT_DIGEST, ActionCacheEntryType.ACTION_RESULT])
@pytest.mark.parametrize("migrateEntries", [False, True])
def test_redis_hits(cas, entryType, migrateEntries, fake_redis):
    redis = fake_redis
    cache = RedisActionCache(
        storage=cas,
        redis=redis,
        allow_updates=True,
        cache_failed_actions=True,
        entry_type=entryType,
        migrate_entries=migrateEntries,
    )

    action = remote_execution_pb2.Action(salt=b"action")
    action_digest = cas.put_message(action)

    # Populating the CAS with a non-empty `ActionResult`:
    action_result = remote_execution_pb2.ActionResult(exit_code=42)
    action_result_digest = cas.put_message(action_result)

    redis_cache_key = f"action-cache.{action_digest.hash}_{action_digest.size_bytes}"

    # Depending on the configuration of the cache, the entries in S3
    # will contain the Digest or the `ActionResult` itself.
    if entryType == ActionCacheEntryType.ACTION_RESULT_DIGEST:
        redis.execute_rw(lambda r: r.set(redis_cache_key, action_result_digest.SerializeToString()))
    else:
        redis.execute_rw(lambda r: r.set(redis_cache_key, action_result.SerializeToString()))

    # When querying the cache we get a hit:
    cached_action_result = cache.get_action_result(action_digest)
    assert cached_action_result == action_result

    # And we also get hits for the other entry type:
    if entryType == ActionCacheEntryType.ACTION_RESULT:
        redis.execute_rw(lambda r: r.set(redis_cache_key, action_result_digest.SerializeToString()))
    else:
        redis.execute_rw(lambda r: r.set(redis_cache_key, action_result.SerializeToString()))

    assert cache.get_action_result(action_digest) == action_result


@pytest.mark.parametrize("entryType", [ActionCacheEntryType.ACTION_RESULT_DIGEST, ActionCacheEntryType.ACTION_RESULT])
@pytest.mark.parametrize("migrateEntries", [False, True])
def test_redis_writes(cas, entryType, migrateEntries, fake_redis):
    redis = fake_redis
    cache = RedisActionCache(
        storage=cas,
        redis=redis,
        allow_updates=True,
        cache_failed_actions=True,
        entry_type=entryType,
        migrate_entries=migrateEntries,
    )

    action = remote_execution_pb2.Action(salt=b"action")
    action_digest = cas.put_message(action)

    # Populating the CAS with a non-empty `ActionResult`:
    action_result = remote_execution_pb2.ActionResult(exit_code=42)
    action_result_digest = cas.put_message(action_result)

    cache.update_action_result(action_digest, action_result)
    assert cache.get_action_result(action_digest) == action_result

    # The `ActionResult` was added to the underlying storage:
    assert cas.get_message(action_result_digest, remote_execution_pb2.ActionResult) == action_result

    # Checking the entry created in Redis:
    redis_cache_key = f"action-cache.{action_digest.hash}_{action_digest.size_bytes}"
    redis_cache_value = redis.execute_ro(lambda r: r.get(redis_cache_key))

    # Depending on the cache configuration, the entry should contain
    # either the `ActionResult` or its digest.
    if entryType == ActionCacheEntryType.ACTION_RESULT_DIGEST:
        assert redis_cache_value == action_result_digest.SerializeToString()
    else:
        assert redis_cache_value == action_result.SerializeToString()


@pytest.mark.parametrize("entryType", [ActionCacheEntryType.ACTION_RESULT_DIGEST, ActionCacheEntryType.ACTION_RESULT])
def test_redis_migrate_entries_to_new_type(cas, entryType, fake_redis):
    redis = fake_redis
    cache = RedisActionCache(
        storage=cas,
        redis=redis,
        allow_updates=True,
        cache_failed_actions=True,
        entry_type=entryType,
        migrate_entries=True,
    )

    action = remote_execution_pb2.Action(salt=b"action")
    action_digest = cas.put_message(action)

    # Populating the CAS with a non-empty `ActionResult`:
    action_result = remote_execution_pb2.ActionResult(exit_code=42)
    action_result_digest = cas.put_message(action_result)

    # Storing an entry created in Redis with the old type:
    redis_cache_key = f"action-cache.{action_digest.hash}_{action_digest.size_bytes}"
    if entryType == ActionCacheEntryType.ACTION_RESULT_DIGEST:
        entry_value = action_result.SerializeToString()
    else:
        entry_value = action_result_digest.SerializeToString()
    redis.execute_rw(lambda r: r.set(redis_cache_key, entry_value))

    # Accessing the entry:
    assert cache.get_action_result(action_digest) is not None

    # Depending on the cache configuration, the entry should have been
    # migrated to the new selected format (`entry_type`)
    s3_cache_entry = redis.execute_ro(lambda r: r.get(redis_cache_key))

    if entryType == ActionCacheEntryType.ACTION_RESULT_DIGEST:
        assert s3_cache_entry == action_result_digest.SerializeToString()
    else:
        assert s3_cache_entry == action_result.SerializeToString()


def test_redis_salt(cas: lru_memory_cache.LRUMemoryCache, fake_redis):
    # GIVEN: three caches using the same redis
    # GIVEN: two of which have a matching cache key salt
    shared_a = RedisActionCache(
        storage=cas,
        redis=fake_redis,
        allow_updates=True,
        cache_failed_actions=True,
        entry_type=ActionCacheEntryType.ACTION_RESULT,
        migrate_entries=False,
        cache_key_salt="shared",
    )
    shared_b = RedisActionCache(
        storage=cas,
        redis=fake_redis,
        allow_updates=True,
        cache_failed_actions=True,
        entry_type=ActionCacheEntryType.ACTION_RESULT,
        migrate_entries=False,
        cache_key_salt="shared",
    )
    isolated = RedisActionCache(
        storage=cas,
        redis=fake_redis,
        allow_updates=True,
        cache_failed_actions=True,
        entry_type=ActionCacheEntryType.ACTION_RESULT,
        migrate_entries=False,
        cache_key_salt="isolated",
    )

    # WHEN: An ActionResult is uploaded to the the isolated cache
    action = remote_execution_pb2.Action(salt=b"action")
    action_digest = cas.put_message(action)
    action_result = remote_execution_pb2.ActionResult(exit_code=42)
    isolated.update_action_result(action_digest, action_result)

    # THEN: The isolated cache has the result, but the others do not
    assert isolated.get_action_result(action_digest)
    with pytest.raises(NotFoundError):
        shared_a.get_action_result(action_digest)
        shared_b.get_action_result(action_digest)

    # WHEN: An ActionResult is uploaded to one of the shared caches
    shared_action = remote_execution_pb2.Action(salt=b"shared_action")
    shared_action_digest = cas.put_message(shared_action)
    shared_action_result = remote_execution_pb2.ActionResult(exit_code=42)
    shared_a.update_action_result(shared_action_digest, shared_action_result)

    # THEN: Both shared caches have the result, but the isolated cache does not
    assert shared_a.get_action_result(shared_action_digest)
    assert shared_b.get_action_result(shared_action_digest)
    with pytest.raises(NotFoundError):
        isolated.get_action_result(shared_action_digest)


def test_sharding(cas: lru_memory_cache.LRUMemoryCache):
    sharded_cache = ShardedActionCache(
        {
            "a": LruActionCache(cas, max_cached_refs=2, allow_updates=True),
            "b": LruActionCache(cas, max_cached_refs=2, allow_updates=True),
            "c": LruActionCache(cas, max_cached_refs=2, allow_updates=True),
            "d": LruActionCache(cas, max_cached_refs=2, allow_updates=True),
        }
    )
    action = remote_execution_pb2.Action(salt=b"action")
    action_digest = cas.put_message(action)

    # Populating the CAS with a non-empty `ActionResult`:
    action_result = remote_execution_pb2.ActionResult(exit_code=42)

    sharded_cache.update_action_result(action_digest, action_result)

    def cache_has_result(cache: LruActionCache):
        try:
            cache.get_action_result(action_digest)
            return True
        except NotFoundError:
            return False

    # Assert that exactly one shard contains the action result
    shard_has_result = iter(cache_has_result(shard) for shard in sharded_cache._shards.values())
    assert any(shard_has_result) and not any(shard_has_result)

    # Assert that the sharded cache can retrieve the result
    assert sharded_cache.get_action_result(action_digest) == action_result
