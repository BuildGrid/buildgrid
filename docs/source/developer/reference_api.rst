
.. _api_reference:

API reference
=============

BuildGrid's Application Programming Interface (API) reference documentation.

.. toctree::
   :maxdepth: 1

   ../api/modules.rst


.. _server-config-parser:

Configuration Parser API
------------------------

The tagged YAML nodes in the :ref:`reference configuration <server-config-reference>`
are handled by the YAML parser using the following set of objects:

.. automodule:: buildgrid.server.app.settings.parser
    :members:
    :undoc-members:
    :show-inheritance:
