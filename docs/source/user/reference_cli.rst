
.. _cli-reference:

CLI reference
=============

BuildGrid's Command Line Interface (CLI) reference documentation.

----

.. _invoking-bgd:

.. click:: buildgrid.server.app:cli
   :prog: bgd

----

.. _invoking-bgd-action-cache:

.. click:: buildgrid.server.app.commands.cmd_actioncache:cli
   :prog: bgd action-cache

----

.. _invoking-bgd-action-cache-get:

.. click:: buildgrid.server.app.commands.cmd_actioncache:get
   :prog: bgd action-cache get

----

.. _invoking-bgd-action-cache-update:

.. click:: buildgrid.server.app.commands.cmd_actioncache:update
   :prog: bgd action-cache update

----

.. _invoking-bgd-cas:

.. click:: buildgrid.server.app.commands.cmd_cas:cli
   :prog: bgd cas

----

.. _invoking-bgd-cas-download-dir:

.. click:: buildgrid.server.app.commands.cmd_cas:download_directory
   :prog: bgd cas download-dir

----

.. _invoking-bgd-cas-download-file:

.. click:: buildgrid.server.app.commands.cmd_cas:download_file
   :prog: bgd cas download-file

----

.. _invoking-bgd-cas-upload-dir:

.. click:: buildgrid.server.app.commands.cmd_cas:upload_directory
   :prog: bgd cas upload-dir

----

.. _invoking-bgd-cas-upload-file:

.. click:: buildgrid.server.app.commands.cmd_cas:upload_file
   :prog: bgd cas upload-file

----

.. _invoking-bgd-execute:

.. click:: buildgrid.server.app.commands.cmd_execute:cli
   :prog: bgd execute

----

.. _invoking-bgd-execute-command:

.. click:: buildgrid.server.app.commands.cmd_execute:run_command
   :prog: bgd execute command

----

.. _invoking-bgd-execute-request-dummy:

.. click:: buildgrid.server.app.commands.cmd_execute:request_dummy
   :prog: bgd execute request-dummy

----

.. _invoking-bgd-operation:

.. click:: buildgrid.server.app.commands.cmd_operation:cli
   :prog: bgd operation

----

.. _invoking-bgd-operation-list:

.. click:: buildgrid.server.app.commands.cmd_operation:lists
   :prog: bgd operation list

----

.. _invoking-bgd-operation-status:

.. click:: buildgrid.server.app.commands.cmd_operation:status
   :prog: bgd operation status

----

.. _invoking-bgd-operation-wait:

.. click:: buildgrid.server.app.commands.cmd_operation:wait
   :prog: bgd operation wait

----

.. _invoking-bgd-server:

.. click:: buildgrid.server.app.commands.cmd_server:cli
   :prog: bgd server

----

.. _invoking-bgd-server-start:

.. click:: buildgrid.server.app.commands.cmd_server:start
   :prog: bgd server start
