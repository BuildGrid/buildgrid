.. _cas-server:

CAS server
==========

It is possible to configure BuildGrid with just a Content Addressable Storage service.

.. _cas-configuration:

Configuration
-------------

Here is an example project configuration.

.. literalinclude:: ../data/cas-example-server.yml
   :language: yaml

.. hint::

   Use ``- name: ""`` if using with BuildStream, as instance names are not supported for that tool yet.

This defines a single ``main`` instance of the ``CAS`` and ``ByteStream`` services on port ``55051``. It is backed onto disk storage and will populate the folder ``$HOME/cas``. To start the server, simply type into your terminal:

.. code-block:: sh

   bgd server start data/config/artifacts.yml

The server should now be available to use.

.. note::

   According to the Remote Execution API, when receiving a ``ByteStream.Write()`` request for a digest that was already uploaded, the server should immediately return a response with ``commited_size`` equal to the size of the blob.

   However, half-closing a gRPC ByteStream can cause issues with some proxies. [#]_ In order to work around those limitations, the ``bytestream`` service provides an option to slightly alter its behavior:


   .. code-block:: yaml

      - !bytestream
            storage: *cas-storage
            disable-overwrite-early-return: true


   When enabled, the server will read all the client's ``WriteRequest`` messages from the stream and return successfully without performing an actual write to the storage.

   .. [#] https://github.com/haproxy/haproxy/issues/1219