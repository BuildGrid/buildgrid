.. _bgd-browser-frontend:

BuildGrid Browser
=================

BuildGrid Browser, or ``bgd browser`` is a Vue-based web client to the
HTTP/1.1 API exposed by ``bgd browser-backend``. This client and API
provide a way to explore Operations, Actions, and CAS content in the
targeted BuildGrid instance, all within a web browser.

Configuration
-------------

Backend
~~~~~~~

The ``bgd browser-backend`` command is used to start up the API service.
This command is included in the docker image published on GitLab for
BuildGrid, or can be installed in much the same way as the gRPC server
command.

This command takes a ``--remote`` option to specify the location of the
BuildGrid we're providing a front-end to. If the various services live
in different locations, there is also support for using individual
``--remote-$service`` options.

The ``bgd browser-backend serve`` subcommand actually starts the server,
and takes options to set the port and optionally serve a static directory
at ``/``, amongst other things.

An example startup command,

.. code-block:: shell

    bgd browser-backend --remote http://buildgrid:50051/ serve -p 8080 -v

This command starts the API listening on port 8080 pointing at a
BuildGrid instance running on port 50051 of whatever the ``buildgrid``
hostname resolves to. The API is running in verbose mode (``-v``), so
there will be a bit more logging than usual.

Frontend
~~~~~~~~

The web client lives in the `BuildGrid/bgd-browser`_ repository. This
repository publishes a docker image which serves the compiled JS client
from an nginx web server.

Alternatively, the code can be compiled manually using ``npm run build``,
with the result being served in some other way (perhaps using the backend
commands ``--static-path`` option).

The web client uses a config file which it expects to find at
``/config.json`` on the same server. An example of this config file,

.. code-block:: json

    {
      "logStreamUrl": {
        "host": "ws://localhost",
        "path": "/ws/logstream",
        "port": 8080
      },
      "backendUrl": {
        "host": "http://localhost",
        "path": "",
        "port": 8080
      },
      "allowOperationCancelling": false
    }

The ``host`` key in these dictionaries should point to the
``bgd browser-backend`` server, with ``port`` matching whatever port
the server is listening on.

The ``path`` values in this example are the defaults, but if the
backend server is running behind some custom proxy they can be used
to point the client to the right place.

.. _BuildGrid/bgd-browser: https://gitlab.com/BuildGrid/bgd-browser
