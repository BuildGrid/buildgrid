# Copyright (C) 2023 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import argparse
import os
import platform
import subprocess
import sys
from tempfile import TemporaryDirectory

import toml

ARG_JOIN = " \\\n  "


def assert_platform_requirements():
    assert (3, 11) <= sys.version_info < (3, 12), "This script should only be executed with python3.11"
    assert platform.system() == "Linux", "This script should only be executed on Linux"
    assert platform.machine() == "x86_64", "This script should only be executed on x86_64"


def main() -> None:
    parser = argparse.ArgumentParser("requirementsgen")
    parser.add_argument("--output-file", type=str, required=True)
    parser.add_argument("--input-file", type=str, required=True)
    parser.add_argument("--extras", type=str, nargs="*")
    args = parser.parse_args()

    assert_platform_requirements()

    py_project_path = os.path.abspath(args.input_file)
    assert os.path.exists(py_project_path), f"Could not find {py_project_path}"
    requirements_path = os.path.abspath(args.output_file)

    with open(py_project_path, "r") as f:
        pyproject_toml = toml.load(f)

    name = pyproject_toml["project"]["name"]
    deps_groups = {
        name: pyproject_toml["project"]["dependencies"],
        **{f"{name}[{extra}]": pyproject_toml["project"]["optional-dependencies"][extra] for extra in args.extras},
    }

    with TemporaryDirectory() as reqs_dir:
        for deps_group, deps in deps_groups.items():
            with open(os.path.join(reqs_dir, deps_group), "w") as f:
                for dep in deps:
                    f.write(f"{dep}\n")

        args = [
            "pip-compile",
            "--resolver=backtracking",
            "--no-emit-index-url",
            "--no-emit-find-links",
            "--no-emit-trusted-host",
            "--no-header",
            "--verbose",
            "--output-file",
            requirements_path,
            *deps_groups.keys(),
        ]

        print(f"Running command: {ARG_JOIN.join(args)}")
        if subprocess.run(args, cwd=reqs_dir).returncode != 0:
            raise Exception("Failed to generate requirements file")


if __name__ == "__main__":
    main()
