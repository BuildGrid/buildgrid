##
# BuildGrid's Docker build manifest.
#
#  ¡FOR LOCAL DEVELOPMENT ONLY!
#
# Builds an image from local sources.
#

FROM debian:bookworm

RUN apt-get update && \
    apt-get install -y \
        python3 python3-venv python3-pip \
        bubblewrap fuse3 \
        libffi-dev \
        postgresql postgresql-contrib libpq-dev \
        libssl-dev libcurl4-openssl-dev wget

RUN GRPC_HEALTH_PROBE_VERSION=v0.4.37 && \
    wget -qO/bin/grpc_health_probe https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-amd64 && \
    chmod +x /bin/grpc_health_probe

# Use /app as working directory:
WORKDIR /app

# Create a virtual environment and update core python modules
# NOTE: pip is upgraded separately to the others, so that further
# upgrades use the upgraded pip.
RUN python3 -m venv "/app/env" && \
    /app/env/bin/python -m pip install --upgrade pip && \
    /app/env/bin/python -m pip install --upgrade setuptools wheel

# Add all requirements.*.txt files
ADD requirements/requirements.txt /app/
# Install the requirements from all files in a single layer
RUN /app/env/bin/python -m pip install -r requirements.txt

# Add tools directory to the PATH:
ENV PATH=$PATH:/app/tools

# Copy BuildGrid source code (from now on we expect the least cached layers while developing)
ADD ./dockerfile-scripts/wait-for-postgres.sh /
COPY . /app

RUN /app/env/bin/python -m pip install -e "."

# Entrypoint for the image (the "[]" are necessary to run in "exec form")
ENTRYPOINT ["/app/env/bin/bgd"]

# Default command using default config (the "[]" are necessary to run in "exec form")
CMD ["server", "start", "data/config/default.yml", "-vvv"]
