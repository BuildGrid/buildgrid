##
#
#   ¡FOR LOCAL DEVELOPMENT ONLY!
#
# Spins up an example BuildGrid instance. The following BuildGrid services
# are available:
#   - Controller (Execution, Operations, Bots) at http://localhost:50051
#   - CAS (using Minio-based S3 for storage, with an SQL index)
#     at http://localhost:50052
#   - ActionCache at http://localhost:50053
#
# Minio's admin UI is exposed at http://localhost:9001.
#
# Basic usage:
#  - docker compose -f docker-compose-examples/s3-cas.yaml up --build
#  - docker compose -f docker-compose-examples/s3-cas.yaml down
#
name: buildgrid-s3-cas-example

services:
  database:
    image: postgres:latest
    environment:
      POSTGRES_USER: bgd
      POSTGRES_PASSWORD: insecure
      POSTGRES_DB: bgd
    volumes:
      - type: volume
        source: db
        target: /var/lib/postgresql/data
    networks:
      - host
      - backend
    ports:
      - "5432:5432"
    healthcheck:
      test: ["CMD", "pg_isready", "-U", "bgd"]
      interval: 1s
      timeout: 5s
      retries: 10

  minio:
    image: minio/minio:latest
    entrypoint: /bin/sh -c
    command: [
      "mkdir -p /data/buildgrid && \
       minio server /data --console-address :9001"
    ]
    volumes:
      - type: volume
        source: s3
        target: /data
    ports:
      - "9000:9000"
      - "9001:9001"
    networks:
      - host
      - backend
    healthcheck:
      test: ["CMD", "mc", "ready", "local"]
      interval: 5s
      timeout: 5s
      retries: 5

  cas:
    build:
      context: ..
    image: buildgrid:local
    command: ["server", "start", "-vvv", "/app/config/cas.yml"]
    volumes:
      - type: bind
        source: ../data/config/s3-cas/cas.yml
        target: /app/config/cas.yml
    ports:
      - "50052:50052"
    depends_on:
      database:
        condition: service_healthy
      minio:
        condition: service_healthy
    networks:
      - backend
      - host
    healthcheck:
      test: ["CMD", "grpc_health_probe", "-addr=:50052"]
      interval: 30s
      timeout: 10s
      retries: 3

  cache:
    build:
      context: ..
    image: buildgrid:local
    command: ["server", "start", "-vvv", "/app/config/action-cache.yml"]
    volumes:
      - type: bind
        source: ../data/config/s3-cas/action-cache.yml
        target: /app/config/action-cache.yml
    ports:
      - "50053:50053"
    networks:
      - backend
      - host
    healthcheck:
      test: ["CMD", "grpc_health_probe", "-addr=:50053"]
      interval: 30s
      timeout: 10s
      retries: 3

  execution:
    build:
      context: ..
    image: buildgrid:local
    command: ["server", "start", "-vvv", "/app/config/execution.yml"]
    volumes:
      - type: bind
        source: ../data/config/s3-cas/execution.yml
        target: /app/config/execution.yml
    ports:
      - "50051:50051"
    depends_on:
      cache:
        condition: service_healthy
      cas:
        condition: service_healthy
      database:
        condition: service_healthy
    networks:
      - backend
      - host
    healthcheck:
      test: ["CMD", "grpc_health_probe", "-addr=:50051"]
      interval: 30s
      timeout: 10s
      retries: 3

  bots:  # To be scaled horizontaly
    image: registry.gitlab.com/buildgrid/buildgrid.hub.docker.com/buildbox:nightly
    privileged: true
    restart: always
    environment:
      BUILDGRID_SERVER_URL: http://execution:50051
      CAS_SERVER_URL: http://cas:50052
      RUNNER_BINARY: buildbox-run-hosttools
    volumes:
      - type: volume
        source: worker-cache
        target: /var/lib/buildgrid/cache
    depends_on:
      execution:
        condition: service_healthy
      cas:
        condition: service_healthy
    networks:
      - backend

  janitor:
    build:
      context: ..
    image: buildgrid:local
    entrypoint: "/bin/bash"
    command:
      - "-c"
      - "sleep 10 && /app/env/bin/bgd janitor start -vv /app/config/janitor.yml"
    environment:
      S3_ACCESS_KEY: minioadmin
      S3_SECRET_KEY: minioadmin
    volumes:
      - type: bind
        source: ../data/config/s3-cas/janitor.yml
        target: /app/config/janitor.yml
    depends_on:
      minio:
        condition: service_healthy
      database:
        condition: service_healthy
    networks:
      - backend

networks:
  backend:
  host:

volumes:
  worker-cache:
    name: buildgrid-s3-cas-example-worker-cache
  db:
    name: buildgrid-s3-cas-example-database
  s3:
    name: buildgrid-s3-cas-example-s3-disk
