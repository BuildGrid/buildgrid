##
# BuildGrid's Docker Compose manifest.
#
#   ¡FOR LOCAL DEVELOPMENT ONLY!
#
# Spins up a local BuildGrid instance containing the following services,
#   - Execution
#   - Operations
#   - Bots
#   - CAS
#   - ActionCache
#
# The services are all exposed at http://localhost:50051
#
# This example demonstrates backing the ActionCache using Redis.
#
# Basic usage:
#  - docker-compose build
#  - docker-compose up --scale bots=10
#  - docker-compose down
#  - docker volume inspect buildgrid_data
#  - docker volume rm buildgrid_data
#  - docker image rm buildgrid:local
#
version: "3.4"

services:
  database:
    image: postgres:latest
    environment:
      POSTGRES_USER: bgd
      POSTGRES_PASSWORD: insecure
      POSTGRES_DB: bgd
    volumes:
      - type: volume
        source: db
        target: /var/lib/postgresql/data
    networks:
      - backend
    ports:
      - "5432:5432"
    healthcheck:
      test: ["CMD", "pg_isready", "-U", "bgd"]
      interval: 1s
      timeout: 5s
      retries: 10

  redis-master:
    image: redis:latest
    networks:
      - backend
    expose:
      - "6379"
    healthcheck:
      test: ["CMD", "redis-cli", "ping"]
      interval: 10s
      timeout: 5s
      retries: 3

  redis-replica:
    image: redis:latest
    command: redis-server --replicaof redis-master 6379
    networks:
      - backend
    expose:
      - "6379"
    depends_on:
      redis-master:
        condition: service_healthy
    healthcheck:
      test: ["CMD", "redis-cli", "ping"]
      interval: 10s
      timeout: 5s
      retries: 3

  redis-sentinel:
    build:
      context: ../data/config/redis-sentinel/dockerfiles/sentinel/
    restart: always
    networks:
      - backend
    expose:
      - "26379"
    depends_on:
      redis-master:
        condition: service_healthy
    healthcheck:
      test: ["CMD", "redis-cli", "-p", "26379", "ping"]
      interval: 10s
      timeout: 5s
      retries: 3

  buildgrid:
    build:
      context: ..
    image: buildgrid:local
    command: ["server", "start", "-vvv", "/app/config/buildgrid.yml"]
    volumes:
      - type: bind
        source: ../data/config/redis-sentinel/buildgrid.yml
        target: /app/config/buildgrid.yml
    ports:
      - "50051:50051"
    networks:
      - backend
      - host
    depends_on:
      redis-sentinel:
        condition: service_healthy
      database:
        condition: service_healthy
    healthcheck:
      test: ["CMD", "grpc_health_probe", "-addr=:50051"]
      interval: 30s
      timeout: 10s
      retries: 3

  bots:  # To be scaled horizontaly
    image: registry.gitlab.com/buildgrid/buildgrid.hub.docker.com/buildbox:nightly
    privileged: true
    command: sh -c "
      buildbox-casd --cas-remote=http://buildgrid:50051
      --bind=localhost:50011 /tmp/casd &
      buildbox-worker --buildbox-run=/usr/local/bin/buildbox-run-hosttools
      --cas-remote=http://localhost:50011 --bots-remote=http://buildgrid:50051
      --verbose"
    volumes:
      - type: volume
        source: cache
        target: /var/lib/buildgrid/cache
    depends_on:
      buildgrid:
        condition: service_healthy
    networks:
      - backend

networks:
  backend:
  host:

volumes:
  cache:
    name: redis-ac-worker-cache
  data:
    name: redis-ac-data
  db:
    name: redis-ac-scheduler-data-store
