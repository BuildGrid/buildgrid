# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

from buildgrid._protos.buildgrid.v2 import introspection_pb2 as buildgrid_dot_v2_dot_introspection__pb2


class IntrospectionStub(object):
    """BuildGrid's Introspection service is used to query the state of
    BuildGrid's internals, and obtain details on available functionality.
    """

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.ListWorkers = channel.unary_unary(
                '/buildgrid.v2.Introspection/ListWorkers',
                request_serializer=buildgrid_dot_v2_dot_introspection__pb2.ListWorkersRequest.SerializeToString,
                response_deserializer=buildgrid_dot_v2_dot_introspection__pb2.ListWorkersResponse.FromString,
                )
        self.GetOperationFilters = channel.unary_unary(
                '/buildgrid.v2.Introspection/GetOperationFilters',
                request_serializer=buildgrid_dot_v2_dot_introspection__pb2.GetOperationFiltersRequest.SerializeToString,
                response_deserializer=buildgrid_dot_v2_dot_introspection__pb2.OperationFilters.FromString,
                )


class IntrospectionServicer(object):
    """BuildGrid's Introspection service is used to query the state of
    BuildGrid's internals, and obtain details on available functionality.
    """

    def ListWorkers(self, request, context):
        """Query the list of workers known to a specific BuildGrid instance

        Errors:

        * `INVALID_ARGUMENT`: When the provided instance name doesn't exist
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def GetOperationFilters(self, request, context):
        """Get the list of valid filters for use in ListOperations requests
        for a specific BuildGrid instance

        Errors:

        * `INVALID_ARGUMENT`: When the provided instance name doesn't exist
        """
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_IntrospectionServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'ListWorkers': grpc.unary_unary_rpc_method_handler(
                    servicer.ListWorkers,
                    request_deserializer=buildgrid_dot_v2_dot_introspection__pb2.ListWorkersRequest.FromString,
                    response_serializer=buildgrid_dot_v2_dot_introspection__pb2.ListWorkersResponse.SerializeToString,
            ),
            'GetOperationFilters': grpc.unary_unary_rpc_method_handler(
                    servicer.GetOperationFilters,
                    request_deserializer=buildgrid_dot_v2_dot_introspection__pb2.GetOperationFiltersRequest.FromString,
                    response_serializer=buildgrid_dot_v2_dot_introspection__pb2.OperationFilters.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'buildgrid.v2.Introspection', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class Introspection(object):
    """BuildGrid's Introspection service is used to query the state of
    BuildGrid's internals, and obtain details on available functionality.
    """

    @staticmethod
    def ListWorkers(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/buildgrid.v2.Introspection/ListWorkers',
            buildgrid_dot_v2_dot_introspection__pb2.ListWorkersRequest.SerializeToString,
            buildgrid_dot_v2_dot_introspection__pb2.ListWorkersResponse.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def GetOperationFilters(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/buildgrid.v2.Introspection/GetOperationFilters',
            buildgrid_dot_v2_dot_introspection__pb2.GetOperationFiltersRequest.SerializeToString,
            buildgrid_dot_v2_dot_introspection__pb2.OperationFilters.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
