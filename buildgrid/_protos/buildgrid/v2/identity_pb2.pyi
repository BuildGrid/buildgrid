"""
@generated by mypy-protobuf.  Do not edit manually!
isort:skip_file
Copyright (C) 2024 Bloomberg LP

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

 <http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import builtins
import google.protobuf.descriptor
import google.protobuf.message
import sys

if sys.version_info >= (3, 8):
    import typing as typing_extensions
else:
    import typing_extensions

DESCRIPTOR: google.protobuf.descriptor.FileDescriptor

@typing_extensions.final
class ClientIdentity(google.protobuf.message.Message):
    DESCRIPTOR: google.protobuf.descriptor.Descriptor

    ACTOR_FIELD_NUMBER: builtins.int
    SUBJECT_FIELD_NUMBER: builtins.int
    WORKFLOW_FIELD_NUMBER: builtins.int
    actor: builtins.str
    subject: builtins.str
    workflow: builtins.str
    def __init__(
        self,
        *,
        actor: builtins.str = ...,
        subject: builtins.str = ...,
        workflow: builtins.str = ...,
    ) -> None: ...
    def ClearField(self, field_name: typing_extensions.Literal["actor", b"actor", "subject", b"subject", "workflow", b"workflow"]) -> None: ...

global___ClientIdentity = ClientIdentity
