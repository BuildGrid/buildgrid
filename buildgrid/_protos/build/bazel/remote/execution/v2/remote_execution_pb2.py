# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: build/bazel/remote/execution/v2/remote_execution.proto
# Protobuf Python Version: 4.25.1
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from buildgrid._protos.build.bazel.semver import semver_pb2 as build_dot_bazel_dot_semver_dot_semver__pb2
from buildgrid._protos.google.api import annotations_pb2 as google_dot_api_dot_annotations__pb2
from buildgrid._protos.google.longrunning import operations_pb2 as google_dot_longrunning_dot_operations__pb2
from google.protobuf import any_pb2 as google_dot_protobuf_dot_any__pb2
from google.protobuf import duration_pb2 as google_dot_protobuf_dot_duration__pb2
from google.protobuf import timestamp_pb2 as google_dot_protobuf_dot_timestamp__pb2
from google.protobuf import wrappers_pb2 as google_dot_protobuf_dot_wrappers__pb2
from buildgrid._protos.google.rpc import status_pb2 as google_dot_rpc_dot_status__pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n6build/bazel/remote/execution/v2/remote_execution.proto\x12\x1f\x62uild.bazel.remote.execution.v2\x1a\x1f\x62uild/bazel/semver/semver.proto\x1a\x1cgoogle/api/annotations.proto\x1a#google/longrunning/operations.proto\x1a\x19google/protobuf/any.proto\x1a\x1egoogle/protobuf/duration.proto\x1a\x1fgoogle/protobuf/timestamp.proto\x1a\x1egoogle/protobuf/wrappers.proto\x1a\x17google/rpc/status.proto\"\xa6\x02\n\x06\x41\x63tion\x12?\n\x0e\x63ommand_digest\x18\x01 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12\x42\n\x11input_root_digest\x18\x02 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12*\n\x07timeout\x18\x06 \x01(\x0b\x32\x19.google.protobuf.Duration\x12\x14\n\x0c\x64o_not_cache\x18\x07 \x01(\x08\x12\x0c\n\x04salt\x18\t \x01(\x0c\x12;\n\x08platform\x18\n \x01(\x0b\x32).build.bazel.remote.execution.v2.PlatformJ\x04\x08\x03\x10\x06J\x04\x08\x08\x10\t\"\xae\x04\n\x07\x43ommand\x12\x11\n\targuments\x18\x01 \x03(\t\x12[\n\x15\x65nvironment_variables\x18\x02 \x03(\x0b\x32<.build.bazel.remote.execution.v2.Command.EnvironmentVariable\x12\x18\n\x0coutput_files\x18\x03 \x03(\tB\x02\x18\x01\x12\x1e\n\x12output_directories\x18\x04 \x03(\tB\x02\x18\x01\x12\x14\n\x0coutput_paths\x18\x07 \x03(\t\x12?\n\x08platform\x18\x05 \x01(\x0b\x32).build.bazel.remote.execution.v2.PlatformB\x02\x18\x01\x12\x19\n\x11working_directory\x18\x06 \x01(\t\x12\x1e\n\x16output_node_properties\x18\x08 \x03(\t\x12_\n\x17output_directory_format\x18\t \x01(\x0e\x32>.build.bazel.remote.execution.v2.Command.OutputDirectoryFormat\x1a\x32\n\x13\x45nvironmentVariable\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\t\"R\n\x15OutputDirectoryFormat\x12\r\n\tTREE_ONLY\x10\x00\x12\x12\n\x0e\x44IRECTORY_ONLY\x10\x01\x12\x16\n\x12TREE_AND_DIRECTORY\x10\x02\"{\n\x08Platform\x12\x46\n\nproperties\x18\x01 \x03(\x0b\x32\x32.build.bazel.remote.execution.v2.Platform.Property\x1a\'\n\x08Property\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\t\"\x9a\x02\n\tDirectory\x12\x38\n\x05\x66iles\x18\x01 \x03(\x0b\x32).build.bazel.remote.execution.v2.FileNode\x12\x43\n\x0b\x64irectories\x18\x02 \x03(\x0b\x32..build.bazel.remote.execution.v2.DirectoryNode\x12>\n\x08symlinks\x18\x03 \x03(\x0b\x32,.build.bazel.remote.execution.v2.SymlinkNode\x12H\n\x0fnode_properties\x18\x05 \x01(\x0b\x32/.build.bazel.remote.execution.v2.NodePropertiesJ\x04\x08\x04\x10\x05\"+\n\x0cNodeProperty\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\r\n\x05value\x18\x02 \x01(\t\"\xaf\x01\n\x0eNodeProperties\x12\x41\n\nproperties\x18\x01 \x03(\x0b\x32-.build.bazel.remote.execution.v2.NodeProperty\x12)\n\x05mtime\x18\x02 \x01(\x0b\x32\x1a.google.protobuf.Timestamp\x12/\n\tunix_mode\x18\x03 \x01(\x0b\x32\x1c.google.protobuf.UInt32Value\"\xbe\x01\n\x08\x46ileNode\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x37\n\x06\x64igest\x18\x02 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12\x15\n\ris_executable\x18\x04 \x01(\x08\x12H\n\x0fnode_properties\x18\x06 \x01(\x0b\x32/.build.bazel.remote.execution.v2.NodePropertiesJ\x04\x08\x03\x10\x04J\x04\x08\x05\x10\x06\"V\n\rDirectoryNode\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x37\n\x06\x64igest\x18\x02 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\"{\n\x0bSymlinkNode\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x0e\n\x06target\x18\x02 \x01(\t\x12H\n\x0fnode_properties\x18\x04 \x01(\x0b\x32/.build.bazel.remote.execution.v2.NodePropertiesJ\x04\x08\x03\x10\x04\"*\n\x06\x44igest\x12\x0c\n\x04hash\x18\x01 \x01(\t\x12\x12\n\nsize_bytes\x18\x02 \x01(\x03\"\xdd\x05\n\x16\x45xecutedActionMetadata\x12\x0e\n\x06worker\x18\x01 \x01(\t\x12\x34\n\x10queued_timestamp\x18\x02 \x01(\x0b\x32\x1a.google.protobuf.Timestamp\x12:\n\x16worker_start_timestamp\x18\x03 \x01(\x0b\x32\x1a.google.protobuf.Timestamp\x12>\n\x1aworker_completed_timestamp\x18\x04 \x01(\x0b\x32\x1a.google.protobuf.Timestamp\x12?\n\x1binput_fetch_start_timestamp\x18\x05 \x01(\x0b\x32\x1a.google.protobuf.Timestamp\x12\x43\n\x1finput_fetch_completed_timestamp\x18\x06 \x01(\x0b\x32\x1a.google.protobuf.Timestamp\x12=\n\x19\x65xecution_start_timestamp\x18\x07 \x01(\x0b\x32\x1a.google.protobuf.Timestamp\x12\x41\n\x1d\x65xecution_completed_timestamp\x18\x08 \x01(\x0b\x32\x1a.google.protobuf.Timestamp\x12=\n\x1avirtual_execution_duration\x18\x0c \x01(\x0b\x32\x19.google.protobuf.Duration\x12\x41\n\x1doutput_upload_start_timestamp\x18\t \x01(\x0b\x32\x1a.google.protobuf.Timestamp\x12\x45\n!output_upload_completed_timestamp\x18\n \x01(\x0b\x32\x1a.google.protobuf.Timestamp\x12\x30\n\x12\x61uxiliary_metadata\x18\x0b \x03(\x0b\x32\x14.google.protobuf.Any\"\xa7\x05\n\x0c\x41\x63tionResult\x12\x41\n\x0coutput_files\x18\x02 \x03(\x0b\x32+.build.bazel.remote.execution.v2.OutputFile\x12P\n\x14output_file_symlinks\x18\n \x03(\x0b\x32..build.bazel.remote.execution.v2.OutputSymlinkB\x02\x18\x01\x12G\n\x0foutput_symlinks\x18\x0c \x03(\x0b\x32..build.bazel.remote.execution.v2.OutputSymlink\x12L\n\x12output_directories\x18\x03 \x03(\x0b\x32\x30.build.bazel.remote.execution.v2.OutputDirectory\x12U\n\x19output_directory_symlinks\x18\x0b \x03(\x0b\x32..build.bazel.remote.execution.v2.OutputSymlinkB\x02\x18\x01\x12\x11\n\texit_code\x18\x04 \x01(\x05\x12\x12\n\nstdout_raw\x18\x05 \x01(\x0c\x12>\n\rstdout_digest\x18\x06 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12\x12\n\nstderr_raw\x18\x07 \x01(\x0c\x12>\n\rstderr_digest\x18\x08 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12S\n\x12\x65xecution_metadata\x18\t \x01(\x0b\x32\x37.build.bazel.remote.execution.v2.ExecutedActionMetadataJ\x04\x08\x01\x10\x02\"\xd2\x01\n\nOutputFile\x12\x0c\n\x04path\x18\x01 \x01(\t\x12\x37\n\x06\x64igest\x18\x02 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12\x15\n\ris_executable\x18\x04 \x01(\x08\x12\x10\n\x08\x63ontents\x18\x05 \x01(\x0c\x12H\n\x0fnode_properties\x18\x07 \x01(\x0b\x32/.build.bazel.remote.execution.v2.NodePropertiesJ\x04\x08\x03\x10\x04J\x04\x08\x06\x10\x07\"~\n\x04Tree\x12\x38\n\x04root\x18\x01 \x01(\x0b\x32*.build.bazel.remote.execution.v2.Directory\x12<\n\x08\x63hildren\x18\x02 \x03(\x0b\x32*.build.bazel.remote.execution.v2.Directory\"\xcc\x01\n\x0fOutputDirectory\x12\x0c\n\x04path\x18\x01 \x01(\t\x12<\n\x0btree_digest\x18\x03 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12\x1f\n\x17is_topologically_sorted\x18\x04 \x01(\x08\x12\x46\n\x15root_directory_digest\x18\x05 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.DigestJ\x04\x08\x02\x10\x03\"}\n\rOutputSymlink\x12\x0c\n\x04path\x18\x01 \x01(\t\x12\x0e\n\x06target\x18\x02 \x01(\t\x12H\n\x0fnode_properties\x18\x04 \x01(\x0b\x32/.build.bazel.remote.execution.v2.NodePropertiesJ\x04\x08\x03\x10\x04\"#\n\x0f\x45xecutionPolicy\x12\x10\n\x08priority\x18\x01 \x01(\x05\"&\n\x12ResultsCachePolicy\x12\x10\n\x08priority\x18\x01 \x01(\x05\"\x83\x03\n\x0e\x45xecuteRequest\x12\x15\n\rinstance_name\x18\x01 \x01(\t\x12\x19\n\x11skip_cache_lookup\x18\x03 \x01(\x08\x12>\n\raction_digest\x18\x06 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12J\n\x10\x65xecution_policy\x18\x07 \x01(\x0b\x32\x30.build.bazel.remote.execution.v2.ExecutionPolicy\x12Q\n\x14results_cache_policy\x18\x08 \x01(\x0b\x32\x33.build.bazel.remote.execution.v2.ResultsCachePolicy\x12N\n\x0f\x64igest_function\x18\t \x01(\x0e\x32\x35.build.bazel.remote.execution.v2.DigestFunction.ValueJ\x04\x08\x02\x10\x03J\x04\x08\x04\x10\x05J\x04\x08\x05\x10\x06\"Z\n\x07LogFile\x12\x37\n\x06\x64igest\x18\x01 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12\x16\n\x0ehuman_readable\x18\x02 \x01(\x08\"\xd0\x02\n\x0f\x45xecuteResponse\x12=\n\x06result\x18\x01 \x01(\x0b\x32-.build.bazel.remote.execution.v2.ActionResult\x12\x15\n\rcached_result\x18\x02 \x01(\x08\x12\"\n\x06status\x18\x03 \x01(\x0b\x32\x12.google.rpc.Status\x12U\n\x0bserver_logs\x18\x04 \x03(\x0b\x32@.build.bazel.remote.execution.v2.ExecuteResponse.ServerLogsEntry\x12\x0f\n\x07message\x18\x05 \x01(\t\x1a[\n\x0fServerLogsEntry\x12\x0b\n\x03key\x18\x01 \x01(\t\x12\x37\n\x05value\x18\x02 \x01(\x0b\x32(.build.bazel.remote.execution.v2.LogFile:\x02\x38\x01\"a\n\x0e\x45xecutionStage\"O\n\x05Value\x12\x0b\n\x07UNKNOWN\x10\x00\x12\x0f\n\x0b\x43\x41\x43HE_CHECK\x10\x01\x12\n\n\x06QUEUED\x10\x02\x12\r\n\tEXECUTING\x10\x03\x12\r\n\tCOMPLETED\x10\x04\"\xb5\x02\n\x18\x45xecuteOperationMetadata\x12\x44\n\x05stage\x18\x01 \x01(\x0e\x32\x35.build.bazel.remote.execution.v2.ExecutionStage.Value\x12>\n\raction_digest\x18\x02 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12\x1a\n\x12stdout_stream_name\x18\x03 \x01(\t\x12\x1a\n\x12stderr_stream_name\x18\x04 \x01(\t\x12[\n\x1apartial_execution_metadata\x18\x05 \x01(\x0b\x32\x37.build.bazel.remote.execution.v2.ExecutedActionMetadata\"$\n\x14WaitExecutionRequest\x12\x0c\n\x04name\x18\x01 \x01(\t\"\x8a\x02\n\x16GetActionResultRequest\x12\x15\n\rinstance_name\x18\x01 \x01(\t\x12>\n\raction_digest\x18\x02 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12\x15\n\rinline_stdout\x18\x03 \x01(\x08\x12\x15\n\rinline_stderr\x18\x04 \x01(\x08\x12\x1b\n\x13inline_output_files\x18\x05 \x03(\t\x12N\n\x0f\x64igest_function\x18\x06 \x01(\x0e\x32\x35.build.bazel.remote.execution.v2.DigestFunction.Value\"\xdb\x02\n\x19UpdateActionResultRequest\x12\x15\n\rinstance_name\x18\x01 \x01(\t\x12>\n\raction_digest\x18\x02 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12\x44\n\raction_result\x18\x03 \x01(\x0b\x32-.build.bazel.remote.execution.v2.ActionResult\x12Q\n\x14results_cache_policy\x18\x04 \x01(\x0b\x32\x33.build.bazel.remote.execution.v2.ResultsCachePolicy\x12N\n\x0f\x64igest_function\x18\x05 \x01(\x0e\x32\x35.build.bazel.remote.execution.v2.DigestFunction.Value\"\xbf\x01\n\x17\x46indMissingBlobsRequest\x12\x15\n\rinstance_name\x18\x01 \x01(\t\x12=\n\x0c\x62lob_digests\x18\x02 \x03(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12N\n\x0f\x64igest_function\x18\x03 \x01(\x0e\x32\x35.build.bazel.remote.execution.v2.DigestFunction.Value\"a\n\x18\x46indMissingBlobsResponse\x12\x45\n\x14missing_blob_digests\x18\x02 \x03(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\"\xee\x02\n\x17\x42\x61tchUpdateBlobsRequest\x12\x15\n\rinstance_name\x18\x01 \x01(\t\x12R\n\x08requests\x18\x02 \x03(\x0b\x32@.build.bazel.remote.execution.v2.BatchUpdateBlobsRequest.Request\x12N\n\x0f\x64igest_function\x18\x05 \x01(\x0e\x32\x35.build.bazel.remote.execution.v2.DigestFunction.Value\x1a\x97\x01\n\x07Request\x12\x37\n\x06\x64igest\x18\x01 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12\x0c\n\x04\x64\x61ta\x18\x02 \x01(\x0c\x12\x45\n\ncompressor\x18\x03 \x01(\x0e\x32\x31.build.bazel.remote.execution.v2.Compressor.Value\"\xda\x01\n\x18\x42\x61tchUpdateBlobsResponse\x12U\n\tresponses\x18\x01 \x03(\x0b\x32\x42.build.bazel.remote.execution.v2.BatchUpdateBlobsResponse.Response\x1ag\n\x08Response\x12\x37\n\x06\x64igest\x18\x01 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12\"\n\x06status\x18\x02 \x01(\x0b\x32\x12.google.rpc.Status\"\x8b\x02\n\x15\x42\x61tchReadBlobsRequest\x12\x15\n\rinstance_name\x18\x01 \x01(\t\x12\x38\n\x07\x64igests\x18\x02 \x03(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12Q\n\x16\x61\x63\x63\x65ptable_compressors\x18\x03 \x03(\x0e\x32\x31.build.bazel.remote.execution.v2.Compressor.Value\x12N\n\x0f\x64igest_function\x18\x04 \x01(\x0e\x32\x35.build.bazel.remote.execution.v2.DigestFunction.Value\"\xac\x02\n\x16\x42\x61tchReadBlobsResponse\x12S\n\tresponses\x18\x01 \x03(\x0b\x32@.build.bazel.remote.execution.v2.BatchReadBlobsResponse.Response\x1a\xbc\x01\n\x08Response\x12\x37\n\x06\x64igest\x18\x01 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12\x0c\n\x04\x64\x61ta\x18\x02 \x01(\x0c\x12\x45\n\ncompressor\x18\x04 \x01(\x0e\x32\x31.build.bazel.remote.execution.v2.Compressor.Value\x12\"\n\x06status\x18\x03 \x01(\x0b\x32\x12.google.rpc.Status\"\xdc\x01\n\x0eGetTreeRequest\x12\x15\n\rinstance_name\x18\x01 \x01(\t\x12<\n\x0broot_digest\x18\x02 \x01(\x0b\x32\'.build.bazel.remote.execution.v2.Digest\x12\x11\n\tpage_size\x18\x03 \x01(\x05\x12\x12\n\npage_token\x18\x04 \x01(\t\x12N\n\x0f\x64igest_function\x18\x05 \x01(\x0e\x32\x35.build.bazel.remote.execution.v2.DigestFunction.Value\"k\n\x0fGetTreeResponse\x12?\n\x0b\x64irectories\x18\x01 \x03(\x0b\x32*.build.bazel.remote.execution.v2.Directory\x12\x17\n\x0fnext_page_token\x18\x02 \x01(\t\"/\n\x16GetCapabilitiesRequest\x12\x15\n\rinstance_name\x18\x01 \x01(\t\"\xe3\x02\n\x12ServerCapabilities\x12N\n\x12\x63\x61\x63he_capabilities\x18\x01 \x01(\x0b\x32\x32.build.bazel.remote.execution.v2.CacheCapabilities\x12V\n\x16\x65xecution_capabilities\x18\x02 \x01(\x0b\x32\x36.build.bazel.remote.execution.v2.ExecutionCapabilities\x12:\n\x16\x64\x65precated_api_version\x18\x03 \x01(\x0b\x32\x1a.build.bazel.semver.SemVer\x12\x33\n\x0flow_api_version\x18\x04 \x01(\x0b\x32\x1a.build.bazel.semver.SemVer\x12\x34\n\x10high_api_version\x18\x05 \x01(\x0b\x32\x1a.build.bazel.semver.SemVer\"\x8f\x01\n\x0e\x44igestFunction\"}\n\x05Value\x12\x0b\n\x07UNKNOWN\x10\x00\x12\n\n\x06SHA256\x10\x01\x12\x08\n\x04SHA1\x10\x02\x12\x07\n\x03MD5\x10\x03\x12\x07\n\x03VSO\x10\x04\x12\n\n\x06SHA384\x10\x05\x12\n\n\x06SHA512\x10\x06\x12\x0b\n\x07MURMUR3\x10\x07\x12\x0e\n\nSHA256TREE\x10\x08\x12\n\n\x06\x42LAKE3\x10\t\"7\n\x1d\x41\x63tionCacheUpdateCapabilities\x12\x16\n\x0eupdate_enabled\x18\x01 \x01(\x08\"\xac\x01\n\x14PriorityCapabilities\x12W\n\npriorities\x18\x01 \x03(\x0b\x32\x43.build.bazel.remote.execution.v2.PriorityCapabilities.PriorityRange\x1a;\n\rPriorityRange\x12\x14\n\x0cmin_priority\x18\x01 \x01(\x05\x12\x14\n\x0cmax_priority\x18\x02 \x01(\x05\"P\n\x1bSymlinkAbsolutePathStrategy\"1\n\x05Value\x12\x0b\n\x07UNKNOWN\x10\x00\x12\x0e\n\nDISALLOWED\x10\x01\x12\x0b\n\x07\x41LLOWED\x10\x02\"F\n\nCompressor\"8\n\x05Value\x12\x0c\n\x08IDENTITY\x10\x00\x12\x08\n\x04ZSTD\x10\x01\x12\x0b\n\x07\x44\x45\x46LATE\x10\x02\x12\n\n\x06\x42ROTLI\x10\x03\"\xeb\x04\n\x11\x43\x61\x63heCapabilities\x12O\n\x10\x64igest_functions\x18\x01 \x03(\x0e\x32\x35.build.bazel.remote.execution.v2.DigestFunction.Value\x12h\n action_cache_update_capabilities\x18\x02 \x01(\x0b\x32>.build.bazel.remote.execution.v2.ActionCacheUpdateCapabilities\x12Z\n\x1b\x63\x61\x63he_priority_capabilities\x18\x03 \x01(\x0b\x32\x35.build.bazel.remote.execution.v2.PriorityCapabilities\x12\"\n\x1amax_batch_total_size_bytes\x18\x04 \x01(\x03\x12j\n\x1esymlink_absolute_path_strategy\x18\x05 \x01(\x0e\x32\x42.build.bazel.remote.execution.v2.SymlinkAbsolutePathStrategy.Value\x12P\n\x15supported_compressors\x18\x06 \x03(\x0e\x32\x31.build.bazel.remote.execution.v2.Compressor.Value\x12]\n\"supported_batch_update_compressors\x18\x07 \x03(\x0e\x32\x31.build.bazel.remote.execution.v2.Compressor.Value\"\xd1\x02\n\x15\x45xecutionCapabilities\x12N\n\x0f\x64igest_function\x18\x01 \x01(\x0e\x32\x35.build.bazel.remote.execution.v2.DigestFunction.Value\x12\x14\n\x0c\x65xec_enabled\x18\x02 \x01(\x08\x12^\n\x1f\x65xecution_priority_capabilities\x18\x03 \x01(\x0b\x32\x35.build.bazel.remote.execution.v2.PriorityCapabilities\x12!\n\x19supported_node_properties\x18\x04 \x03(\t\x12O\n\x10\x64igest_functions\x18\x05 \x03(\x0e\x32\x35.build.bazel.remote.execution.v2.DigestFunction.Value\"6\n\x0bToolDetails\x12\x11\n\ttool_name\x18\x01 \x01(\t\x12\x14\n\x0ctool_version\x18\x02 \x01(\t\"\xed\x01\n\x0fRequestMetadata\x12\x42\n\x0ctool_details\x18\x01 \x01(\x0b\x32,.build.bazel.remote.execution.v2.ToolDetails\x12\x11\n\taction_id\x18\x02 \x01(\t\x12\x1a\n\x12tool_invocation_id\x18\x03 \x01(\t\x12!\n\x19\x63orrelated_invocations_id\x18\x04 \x01(\t\x12\x17\n\x0f\x61\x63tion_mnemonic\x18\x05 \x01(\t\x12\x11\n\ttarget_id\x18\x06 \x01(\t\x12\x18\n\x10\x63onfiguration_id\x18\x07 \x01(\t2\xb9\x02\n\tExecution\x12\x8e\x01\n\x07\x45xecute\x12/.build.bazel.remote.execution.v2.ExecuteRequest\x1a\x1d.google.longrunning.Operation\"1\x82\xd3\xe4\x93\x02+\"&/v2/{instance_name=**}/actions:execute:\x01*0\x01\x12\x9a\x01\n\rWaitExecution\x12\x35.build.bazel.remote.execution.v2.WaitExecutionRequest\x1a\x1d.google.longrunning.Operation\"1\x82\xd3\xe4\x93\x02+\"&/v2/{name=operations/**}:waitExecution:\x01*0\x01\x32\xd6\x03\n\x0b\x41\x63tionCache\x12\xd7\x01\n\x0fGetActionResult\x12\x37.build.bazel.remote.execution.v2.GetActionResultRequest\x1a-.build.bazel.remote.execution.v2.ActionResult\"\\\x82\xd3\xe4\x93\x02V\x12T/v2/{instance_name=**}/actionResults/{action_digest.hash}/{action_digest.size_bytes}\x12\xec\x01\n\x12UpdateActionResult\x12:.build.bazel.remote.execution.v2.UpdateActionResultRequest\x1a-.build.bazel.remote.execution.v2.ActionResult\"k\x82\xd3\xe4\x93\x02\x65\x1aT/v2/{instance_name=**}/actionResults/{action_digest.hash}/{action_digest.size_bytes}:\raction_result2\x9b\x06\n\x19\x43ontentAddressableStorage\x12\xbc\x01\n\x10\x46indMissingBlobs\x12\x38.build.bazel.remote.execution.v2.FindMissingBlobsRequest\x1a\x39.build.bazel.remote.execution.v2.FindMissingBlobsResponse\"3\x82\xd3\xe4\x93\x02-\"(/v2/{instance_name=**}/blobs:findMissing:\x01*\x12\xbc\x01\n\x10\x42\x61tchUpdateBlobs\x12\x38.build.bazel.remote.execution.v2.BatchUpdateBlobsRequest\x1a\x39.build.bazel.remote.execution.v2.BatchUpdateBlobsResponse\"3\x82\xd3\xe4\x93\x02-\"(/v2/{instance_name=**}/blobs:batchUpdate:\x01*\x12\xb4\x01\n\x0e\x42\x61tchReadBlobs\x12\x36.build.bazel.remote.execution.v2.BatchReadBlobsRequest\x1a\x37.build.bazel.remote.execution.v2.BatchReadBlobsResponse\"1\x82\xd3\xe4\x93\x02+\"&/v2/{instance_name=**}/blobs:batchRead:\x01*\x12\xc8\x01\n\x07GetTree\x12/.build.bazel.remote.execution.v2.GetTreeRequest\x1a\x30.build.bazel.remote.execution.v2.GetTreeResponse\"X\x82\xd3\xe4\x93\x02R\x12P/v2/{instance_name=**}/blobs/{root_digest.hash}/{root_digest.size_bytes}:getTree0\x01\x32\xbd\x01\n\x0c\x43\x61pabilities\x12\xac\x01\n\x0fGetCapabilities\x12\x37.build.bazel.remote.execution.v2.GetCapabilitiesRequest\x1a\x33.build.bazel.remote.execution.v2.ServerCapabilities\"+\x82\xd3\xe4\x93\x02%\x12#/v2/{instance_name=**}/capabilitiesB\xb4\x01\n\x1f\x62uild.bazel.remote.execution.v2B\x14RemoteExecutionProtoP\x01ZQgithub.com/bazelbuild/remote-apis/build/bazel/remote/execution/v2;remoteexecution\xa2\x02\x03REX\xaa\x02\x1f\x42uild.Bazel.Remote.Execution.V2b\x06proto3')

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2', _globals)
if _descriptor._USE_C_DESCRIPTORS == False:
  _globals['DESCRIPTOR']._options = None
  _globals['DESCRIPTOR']._serialized_options = b'\n\037build.bazel.remote.execution.v2B\024RemoteExecutionProtoP\001ZQgithub.com/bazelbuild/remote-apis/build/bazel/remote/execution/v2;remoteexecution\242\002\003REX\252\002\037Build.Bazel.Remote.Execution.V2'
  _globals['_COMMAND'].fields_by_name['output_files']._options = None
  _globals['_COMMAND'].fields_by_name['output_files']._serialized_options = b'\030\001'
  _globals['_COMMAND'].fields_by_name['output_directories']._options = None
  _globals['_COMMAND'].fields_by_name['output_directories']._serialized_options = b'\030\001'
  _globals['_COMMAND'].fields_by_name['platform']._options = None
  _globals['_COMMAND'].fields_by_name['platform']._serialized_options = b'\030\001'
  _globals['_ACTIONRESULT'].fields_by_name['output_file_symlinks']._options = None
  _globals['_ACTIONRESULT'].fields_by_name['output_file_symlinks']._serialized_options = b'\030\001'
  _globals['_ACTIONRESULT'].fields_by_name['output_directory_symlinks']._options = None
  _globals['_ACTIONRESULT'].fields_by_name['output_directory_symlinks']._serialized_options = b'\030\001'
  _globals['_EXECUTERESPONSE_SERVERLOGSENTRY']._options = None
  _globals['_EXECUTERESPONSE_SERVERLOGSENTRY']._serialized_options = b'8\001'
  _globals['_EXECUTION'].methods_by_name['Execute']._options = None
  _globals['_EXECUTION'].methods_by_name['Execute']._serialized_options = b'\202\323\344\223\002+\"&/v2/{instance_name=**}/actions:execute:\001*'
  _globals['_EXECUTION'].methods_by_name['WaitExecution']._options = None
  _globals['_EXECUTION'].methods_by_name['WaitExecution']._serialized_options = b'\202\323\344\223\002+\"&/v2/{name=operations/**}:waitExecution:\001*'
  _globals['_ACTIONCACHE'].methods_by_name['GetActionResult']._options = None
  _globals['_ACTIONCACHE'].methods_by_name['GetActionResult']._serialized_options = b'\202\323\344\223\002V\022T/v2/{instance_name=**}/actionResults/{action_digest.hash}/{action_digest.size_bytes}'
  _globals['_ACTIONCACHE'].methods_by_name['UpdateActionResult']._options = None
  _globals['_ACTIONCACHE'].methods_by_name['UpdateActionResult']._serialized_options = b'\202\323\344\223\002e\032T/v2/{instance_name=**}/actionResults/{action_digest.hash}/{action_digest.size_bytes}:\raction_result'
  _globals['_CONTENTADDRESSABLESTORAGE'].methods_by_name['FindMissingBlobs']._options = None
  _globals['_CONTENTADDRESSABLESTORAGE'].methods_by_name['FindMissingBlobs']._serialized_options = b'\202\323\344\223\002-\"(/v2/{instance_name=**}/blobs:findMissing:\001*'
  _globals['_CONTENTADDRESSABLESTORAGE'].methods_by_name['BatchUpdateBlobs']._options = None
  _globals['_CONTENTADDRESSABLESTORAGE'].methods_by_name['BatchUpdateBlobs']._serialized_options = b'\202\323\344\223\002-\"(/v2/{instance_name=**}/blobs:batchUpdate:\001*'
  _globals['_CONTENTADDRESSABLESTORAGE'].methods_by_name['BatchReadBlobs']._options = None
  _globals['_CONTENTADDRESSABLESTORAGE'].methods_by_name['BatchReadBlobs']._serialized_options = b'\202\323\344\223\002+\"&/v2/{instance_name=**}/blobs:batchRead:\001*'
  _globals['_CONTENTADDRESSABLESTORAGE'].methods_by_name['GetTree']._options = None
  _globals['_CONTENTADDRESSABLESTORAGE'].methods_by_name['GetTree']._serialized_options = b'\202\323\344\223\002R\022P/v2/{instance_name=**}/blobs/{root_digest.hash}/{root_digest.size_bytes}:getTree'
  _globals['_CAPABILITIES'].methods_by_name['GetCapabilities']._options = None
  _globals['_CAPABILITIES'].methods_by_name['GetCapabilities']._serialized_options = b'\202\323\344\223\002%\022#/v2/{instance_name=**}/capabilities'
  _globals['_ACTION']._serialized_start=341
  _globals['_ACTION']._serialized_end=635
  _globals['_COMMAND']._serialized_start=638
  _globals['_COMMAND']._serialized_end=1196
  _globals['_COMMAND_ENVIRONMENTVARIABLE']._serialized_start=1062
  _globals['_COMMAND_ENVIRONMENTVARIABLE']._serialized_end=1112
  _globals['_COMMAND_OUTPUTDIRECTORYFORMAT']._serialized_start=1114
  _globals['_COMMAND_OUTPUTDIRECTORYFORMAT']._serialized_end=1196
  _globals['_PLATFORM']._serialized_start=1198
  _globals['_PLATFORM']._serialized_end=1321
  _globals['_PLATFORM_PROPERTY']._serialized_start=1282
  _globals['_PLATFORM_PROPERTY']._serialized_end=1321
  _globals['_DIRECTORY']._serialized_start=1324
  _globals['_DIRECTORY']._serialized_end=1606
  _globals['_NODEPROPERTY']._serialized_start=1608
  _globals['_NODEPROPERTY']._serialized_end=1651
  _globals['_NODEPROPERTIES']._serialized_start=1654
  _globals['_NODEPROPERTIES']._serialized_end=1829
  _globals['_FILENODE']._serialized_start=1832
  _globals['_FILENODE']._serialized_end=2022
  _globals['_DIRECTORYNODE']._serialized_start=2024
  _globals['_DIRECTORYNODE']._serialized_end=2110
  _globals['_SYMLINKNODE']._serialized_start=2112
  _globals['_SYMLINKNODE']._serialized_end=2235
  _globals['_DIGEST']._serialized_start=2237
  _globals['_DIGEST']._serialized_end=2279
  _globals['_EXECUTEDACTIONMETADATA']._serialized_start=2282
  _globals['_EXECUTEDACTIONMETADATA']._serialized_end=3015
  _globals['_ACTIONRESULT']._serialized_start=3018
  _globals['_ACTIONRESULT']._serialized_end=3697
  _globals['_OUTPUTFILE']._serialized_start=3700
  _globals['_OUTPUTFILE']._serialized_end=3910
  _globals['_TREE']._serialized_start=3912
  _globals['_TREE']._serialized_end=4038
  _globals['_OUTPUTDIRECTORY']._serialized_start=4041
  _globals['_OUTPUTDIRECTORY']._serialized_end=4245
  _globals['_OUTPUTSYMLINK']._serialized_start=4247
  _globals['_OUTPUTSYMLINK']._serialized_end=4372
  _globals['_EXECUTIONPOLICY']._serialized_start=4374
  _globals['_EXECUTIONPOLICY']._serialized_end=4409
  _globals['_RESULTSCACHEPOLICY']._serialized_start=4411
  _globals['_RESULTSCACHEPOLICY']._serialized_end=4449
  _globals['_EXECUTEREQUEST']._serialized_start=4452
  _globals['_EXECUTEREQUEST']._serialized_end=4839
  _globals['_LOGFILE']._serialized_start=4841
  _globals['_LOGFILE']._serialized_end=4931
  _globals['_EXECUTERESPONSE']._serialized_start=4934
  _globals['_EXECUTERESPONSE']._serialized_end=5270
  _globals['_EXECUTERESPONSE_SERVERLOGSENTRY']._serialized_start=5179
  _globals['_EXECUTERESPONSE_SERVERLOGSENTRY']._serialized_end=5270
  _globals['_EXECUTIONSTAGE']._serialized_start=5272
  _globals['_EXECUTIONSTAGE']._serialized_end=5369
  _globals['_EXECUTIONSTAGE_VALUE']._serialized_start=5290
  _globals['_EXECUTIONSTAGE_VALUE']._serialized_end=5369
  _globals['_EXECUTEOPERATIONMETADATA']._serialized_start=5372
  _globals['_EXECUTEOPERATIONMETADATA']._serialized_end=5681
  _globals['_WAITEXECUTIONREQUEST']._serialized_start=5683
  _globals['_WAITEXECUTIONREQUEST']._serialized_end=5719
  _globals['_GETACTIONRESULTREQUEST']._serialized_start=5722
  _globals['_GETACTIONRESULTREQUEST']._serialized_end=5988
  _globals['_UPDATEACTIONRESULTREQUEST']._serialized_start=5991
  _globals['_UPDATEACTIONRESULTREQUEST']._serialized_end=6338
  _globals['_FINDMISSINGBLOBSREQUEST']._serialized_start=6341
  _globals['_FINDMISSINGBLOBSREQUEST']._serialized_end=6532
  _globals['_FINDMISSINGBLOBSRESPONSE']._serialized_start=6534
  _globals['_FINDMISSINGBLOBSRESPONSE']._serialized_end=6631
  _globals['_BATCHUPDATEBLOBSREQUEST']._serialized_start=6634
  _globals['_BATCHUPDATEBLOBSREQUEST']._serialized_end=7000
  _globals['_BATCHUPDATEBLOBSREQUEST_REQUEST']._serialized_start=6849
  _globals['_BATCHUPDATEBLOBSREQUEST_REQUEST']._serialized_end=7000
  _globals['_BATCHUPDATEBLOBSRESPONSE']._serialized_start=7003
  _globals['_BATCHUPDATEBLOBSRESPONSE']._serialized_end=7221
  _globals['_BATCHUPDATEBLOBSRESPONSE_RESPONSE']._serialized_start=7118
  _globals['_BATCHUPDATEBLOBSRESPONSE_RESPONSE']._serialized_end=7221
  _globals['_BATCHREADBLOBSREQUEST']._serialized_start=7224
  _globals['_BATCHREADBLOBSREQUEST']._serialized_end=7491
  _globals['_BATCHREADBLOBSRESPONSE']._serialized_start=7494
  _globals['_BATCHREADBLOBSRESPONSE']._serialized_end=7794
  _globals['_BATCHREADBLOBSRESPONSE_RESPONSE']._serialized_start=7606
  _globals['_BATCHREADBLOBSRESPONSE_RESPONSE']._serialized_end=7794
  _globals['_GETTREEREQUEST']._serialized_start=7797
  _globals['_GETTREEREQUEST']._serialized_end=8017
  _globals['_GETTREERESPONSE']._serialized_start=8019
  _globals['_GETTREERESPONSE']._serialized_end=8126
  _globals['_GETCAPABILITIESREQUEST']._serialized_start=8128
  _globals['_GETCAPABILITIESREQUEST']._serialized_end=8175
  _globals['_SERVERCAPABILITIES']._serialized_start=8178
  _globals['_SERVERCAPABILITIES']._serialized_end=8533
  _globals['_DIGESTFUNCTION']._serialized_start=8536
  _globals['_DIGESTFUNCTION']._serialized_end=8679
  _globals['_DIGESTFUNCTION_VALUE']._serialized_start=8554
  _globals['_DIGESTFUNCTION_VALUE']._serialized_end=8679
  _globals['_ACTIONCACHEUPDATECAPABILITIES']._serialized_start=8681
  _globals['_ACTIONCACHEUPDATECAPABILITIES']._serialized_end=8736
  _globals['_PRIORITYCAPABILITIES']._serialized_start=8739
  _globals['_PRIORITYCAPABILITIES']._serialized_end=8911
  _globals['_PRIORITYCAPABILITIES_PRIORITYRANGE']._serialized_start=8852
  _globals['_PRIORITYCAPABILITIES_PRIORITYRANGE']._serialized_end=8911
  _globals['_SYMLINKABSOLUTEPATHSTRATEGY']._serialized_start=8913
  _globals['_SYMLINKABSOLUTEPATHSTRATEGY']._serialized_end=8993
  _globals['_SYMLINKABSOLUTEPATHSTRATEGY_VALUE']._serialized_start=8944
  _globals['_SYMLINKABSOLUTEPATHSTRATEGY_VALUE']._serialized_end=8993
  _globals['_COMPRESSOR']._serialized_start=8995
  _globals['_COMPRESSOR']._serialized_end=9065
  _globals['_COMPRESSOR_VALUE']._serialized_start=9009
  _globals['_COMPRESSOR_VALUE']._serialized_end=9065
  _globals['_CACHECAPABILITIES']._serialized_start=9068
  _globals['_CACHECAPABILITIES']._serialized_end=9687
  _globals['_EXECUTIONCAPABILITIES']._serialized_start=9690
  _globals['_EXECUTIONCAPABILITIES']._serialized_end=10027
  _globals['_TOOLDETAILS']._serialized_start=10029
  _globals['_TOOLDETAILS']._serialized_end=10083
  _globals['_REQUESTMETADATA']._serialized_start=10086
  _globals['_REQUESTMETADATA']._serialized_end=10323
  _globals['_EXECUTION']._serialized_start=10326
  _globals['_EXECUTION']._serialized_end=10639
  _globals['_ACTIONCACHE']._serialized_start=10642
  _globals['_ACTIONCACHE']._serialized_end=11112
  _globals['_CONTENTADDRESSABLESTORAGE']._serialized_start=11115
  _globals['_CONTENTADDRESSABLESTORAGE']._serialized_end=11910
  _globals['_CAPABILITIES']._serialized_start=11913
  _globals['_CAPABILITIES']._serialized_end=12102
# @@protoc_insertion_point(module_scope)
