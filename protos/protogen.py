import pathlib
import subprocess
import sys

import yaml

SCRIPT_PATH = pathlib.Path(__file__).absolute()
SCRIPT_DIR = SCRIPT_PATH.parent
PROTOS_CONF = SCRIPT_DIR.joinpath(pathlib.Path("protos.yaml"))

ARG_JOIN = " \\\n  "

if __name__ == "__main__":
    with open(PROTOS_CONF, "r") as f:
        protos_conf = yaml.safe_load(f)

    proto_dir = SCRIPT_DIR.joinpath(pathlib.Path(protos_conf["proto_dir"]))
    python_dir = SCRIPT_DIR.joinpath(pathlib.Path(protos_conf["python_dir"]))
    python_package: str = protos_conf["python_package"]

    proto_paths = []
    proto_packages = set()
    for proto in protos_conf["protos"]:
        path = pathlib.Path(proto["path"])
        package: str = proto["package"]

        proto_paths.append(proto_path := proto_dir.joinpath(path))
        proto_packages.add(package)
        proto_path.parent.mkdir(parents=True, exist_ok=True)

        python_path = python_dir.joinpath(path).parent
        python_path.mkdir(parents=True, exist_ok=True)
        init_file = python_path.joinpath("__init__.py")

        if not init_file.exists():
            with open(init_file, "w") as f:
                f.write("# Generated __init__.py file\n")

    args = (
        f"{sys.executable} -m grpc_tools.protoc "
        f"-I{proto_dir} "
        f"--python_out={python_dir} "
        f"--grpc_python_out={python_dir} "
        f"--proto_path={proto_dir} "
        f"--mypy_out={python_dir} "
        f"--mypy_grpc_out={python_dir} "
    ).split() + list(map(str, proto_paths))

    print(f"Running command: {ARG_JOIN.join(args)}")
    if subprocess.run(args).returncode != 0:
        raise Exception("Failed to generate python code")

    print("Adjusting generated outputs...")

    for filepath in python_dir.rglob("*"):
        if not filepath.is_file():
            continue
        if "__pycache__" in filepath.as_posix():
            continue
        if not (
            filepath.name.endswith("_pb2.py")
            or filepath.name.endswith("_pb2_grpc.py")
            or filepath.name.endswith("_pb2.pyi")
            or filepath.name.endswith("_pb2_grpc.pyi")
        ):
            continue

        try:
            with open(filepath, "r") as f:
                contents = f.read()
        except Exception as e:
            raise Exception(f"Failed to read {filepath}") from e

        if filepath.name.endswith("_pb2.py"):
            contents = contents.replace(
                "_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, '",
                f"_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, '{python_package}.",
            )

        if filepath.name.endswith("_pb2.py") or filepath.name.endswith("_pb2_grpc.py"):
            for proto_package in proto_packages:
                contents = contents.replace(
                    f"from {proto_package} ",
                    f"from {python_package}.{proto_package} ",
                )
        if filepath.name.endswith("_pb2.pyi") or filepath.name.endswith("_pb2_grpc.pyi"):
            for proto in protos_conf["protos"]:
                pacakge = f"{proto['package']}.{proto['name']}"
                contents = contents.replace(
                    f"{pacakge}_pb2",
                    f"{python_package}.{pacakge}_pb2",
                )

        with open(filepath, "w") as f:
            f.write(contents)

        if filepath.name.endswith("_pb2_grpc.py"):
            aio_filepath = filepath.parent.joinpath(filepath.name.replace("_pb2_grpc.py", "_pb2_grpc_aio.py"))
            with open(aio_filepath, "w") as f:
                f.write(contents)

        if filepath.name.endswith("_pb2_grpc.pyi"):
            aio_filepath = filepath.parent.joinpath(filepath.name.replace("_pb2_grpc.pyi", "_pb2_grpc_aio.pyi"))
            contents = contents.replace(
                "import grpc",
                "import grpc.aio as grpc\nimport typing",
            )
            contents = contents.replace(
                "context: grpc.ServicerContext,",
                "context: grpc.ServicerContext[typing.Any, typing.Any],",
            )
            contents = contents.replace(
                "@abc.abstractmethod\n    def",
                "@abc.abstractmethod\n    async def",
            )
            with open(aio_filepath, "w") as f:
                f.write(contents)

    print("Complete!")
