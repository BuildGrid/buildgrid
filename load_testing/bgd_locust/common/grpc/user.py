# Copyright (C) 2021 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import grpc
from common.grpc.client import GrpcClient
from locust import User
from locust.exception import LocustError


class GrpcUser(User):
    abstract = True
    host = None
    stub_classes = None

    def __init__(self, environment) -> None:
        super().__init__(environment)
        for value, name in ((self.host, "host"), (self.stub_classes, "stub_classes")):
            if value is None:
                raise LocustError(f"You must specify the {name} attribute on your GrpcUser subclass.")

        for name, stub_class in self.stub_classes.items():
            setattr(self, f"{name}_client", GrpcClient(self.host, stub_class))
