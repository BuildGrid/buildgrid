# Copyright (C) 2021 Bloomberg LP
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  <http://www.apache.org/licenses/LICENSE-2.0>
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import math
import os
import random
import time
import uuid
from typing import List, Optional

from common.grpc.client import RequestType
from common.grpc.user import GrpcUser
from locust import task

from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2 import (
    BatchReadBlobsRequest,
    BatchUpdateBlobsRequest,
    Digest,
    FindMissingBlobsRequest,
)
from buildgrid._protos.build.bazel.remote.execution.v2.remote_execution_pb2_grpc import ContentAddressableStorageStub
from buildgrid._protos.google.bytestream.bytestream_pb2 import WriteRequest
from buildgrid._protos.google.bytestream.bytestream_pb2_grpc import ByteStreamStub
from buildgrid.server.settings import HASH, MAX_REQUEST_SIZE


class CasGrpcUser(GrpcUser):
    abstract = False
    host = "localhost:50052"
    stub_classes = {"bytestream": ByteStreamStub, "cas": ContentAddressableStorageStub}

    def __init__(self, environment):
        super().__init__(environment)

        self.blob_size = 120
        self.instance_name = ""

        self._blobs_uploaded = []

    def _gen_random_blob(self, size_bytes: int) -> bytes:
        blob = os.urandom(size_bytes)
        return blob

    def _make_digest(self, blob: bytes) -> Digest:
        return Digest(hash=HASH(blob).hexdigest(), size_bytes=len(blob))

    def _upload_blob(self, blob: bytes, digest: Digest) -> None:
        def _generate_write_requests():
            offset = 0
            finished = False
            remaining = len(blob)
            transaction_id = str(uuid.uuid4())
            while not finished:
                chunk_size = min(remaining, MAX_REQUEST_SIZE)
                remaining -= chunk_size

                request = WriteRequest()
                request.resource_name = "/".join(
                    [self.instance_name, "uploads", transaction_id, "blobs", digest.hash, str(digest.size_bytes)]
                )
                request.data = blob[offset : offset + chunk_size]
                request.write_offset = offset
                request.finish_write = remaining <= 0

                yield request

                offset += chunk_size
                finished = request.finish_write

        self.bytestream_client.Write(RequestType.STREAM_UNARY, _generate_write_requests())

        # Large blobs are not suitable for batch requests
        if digest.size_bytes < 10 * 1024:
            self._blobs_uploaded.append(digest)

    def _generate_digest(self) -> Digest:
        size = self.blob_size

        blob = self._gen_random_blob(size)
        digest = self._make_digest(blob)

        self._upload_blob(blob, digest)
        return digest

    def _pick_digests(self, k: int) -> list[Digest]:
        digests = random.sample(self._blobs_uploaded, min(k, len(self._blobs_uploaded)))
        while len(digests) < k:
            digests.append(self._generate_digest())
        return digests

    @task
    def bytestream_write(self) -> None:
        blob = self._gen_random_blob(1024 * 1024 * 2)
        digest = self._make_digest(blob)
        self._upload_blob(blob, digest)

    @task
    def batch_read_blobs(self) -> None:
        digest_count = random.randint(10, 100)
        digests = self._pick_digests(digest_count)
        request = BatchReadBlobsRequest(instance_name=self.instance_name, digests=digests)
        self.cas_client.BatchReadBlobs(RequestType.UNARY_UNARY, request)

    @task
    def batch_update_blobs(self) -> None:
        blob_count = random.randint(10, 100)
        requests = []
        for i in range(0, blob_count):
            blob = self._gen_random_blob(self.blob_size)
            digest = self._make_digest(blob)
            requests.append(BatchUpdateBlobsRequest.Request(digest=digest, data=blob))
        request = BatchUpdateBlobsRequest(instance_name=self.instance_name, requests=requests)
        self.cas_client.BatchUpdateBlobs(RequestType.UNARY_UNARY, request)
        self._blobs_uploaded.extend([request.digest for request in requests])

    @task
    def find_missing_blobs(self) -> None:
        digests = []
        digest_count = random.randint(10, 100)
        hit_rate = 0.25

        digests = self._pick_digests(math.floor(hit_rate * digest_count))
        for i in range(len(digests), digest_count):
            # Expected missing (random digest of not uploaded blob)
            blob = self._gen_random_blob(self.blob_size)
            digests.append(self._make_digest(blob))

        request = FindMissingBlobsRequest(instance_name=self.instance_name, blob_digests=digests)
        self.cas_client.FindMissingBlobs(RequestType.UNARY_UNARY, request)
